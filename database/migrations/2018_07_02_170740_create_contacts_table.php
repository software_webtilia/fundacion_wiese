<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('infocontacto_id');
            $table->string('nombres', 255);
            $table->string('apellidos', 255);
            $table->string('telefono', 15)->nullable();
            $table->string('email', 255);
            $table->text('mensaje')->nullable();
            $table->string('estado', 1)->nullable();
           /* $table->boolean('estado')->default(1);*/
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('contacts');
    }
}
