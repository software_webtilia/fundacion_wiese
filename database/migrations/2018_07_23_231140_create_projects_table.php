<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->increments('id');
            $table->string('titulo', 255);
            $table->string('imagen', 255);
            $table->string('programa', 255)->nullable();
            $table->string('link', 255)->nullable();
            $table->text('descripcion');
            $table->smallInteger('orden')->default(1);
            $table->smallInteger('estado')->default(1); //0:inactivo 1:nuevo 2:proceso 3:cerrado
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('projects');
    }
}
