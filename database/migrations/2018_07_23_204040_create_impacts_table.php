<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImpactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('impacts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('project_direct_id')->unsigned();
            $table->integer('cantidad');
            $table->string('label', 255);
            $table->string('icono', 255);
            $table->smallInteger('orden')->default(1);
            $table->boolean('estado')->default(1);
            $table->timestamps();

            $table->foreign('project_direct_id')->references('id')->on('project_directs');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('impacts');
    }
}
