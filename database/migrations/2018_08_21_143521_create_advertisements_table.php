<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdvertisementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advertisements', function (Blueprint $table) {
            $table->increments('id');
            $table->string('titulo',150);
            $table->string('subtitulo',150);
            $table->string('sumilla',250);
            $table->text('descripcion');
            $table->string('lugar',80);
            $table->integer('orden');
            $table->dateTime('fecha_inicio');
            $table->dateTime('fecha_fin');
            $table->char('estado',3)->default('PUB');
            $table->integer('project_id')->unsigned();
            $table->timestamps();
            $table->foreign('project_id')->references('id')->on('projects');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('advertisements');
    }
}
