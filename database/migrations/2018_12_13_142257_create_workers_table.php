<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('workers', function (Blueprint $table) {
          $table->increments('id');
          $table->string('nombres',120);
          $table->string('apellidos',120);
          $table->string('telefono',20);
          $table->string('correo',120);
          $table->integer('area_id')->unsigned();
          $table->string('direccion',120);
          $table->string('profesion',120);
          $table->string('cv',150);
          $table->char('estado')->default('A');
          $table->timestamps();
          $table->foreign('area_id')->references('id')->on('areas');

      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('workers');
    }
}
