<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateZonesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ubigeo',20);
            $table->string('distrito', 100);
            $table->string('provincia',100);
            $table->string('departamento',100);
            $table->smallInteger('poblacion');
            $table->smallInteger('area');
            $table->char('estado',1)->default('A');
            $table->string('email');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('zones');
    }
}
