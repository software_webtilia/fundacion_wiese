<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectDirectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_directs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('titulo', 255);
            $table->text('descripcion_general');
            $table->text('descripcion_1');
            $table->string('imagen1', 255);
            $table->text('descripcion_2');
            $table->string('imagen2', 255);
            $table->string('equipo', 255);
            $table->string('fondo', 255);
            $table->integer('impacto_numero')->nullable();
            $table->string('impacto_label', 255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('project_directs');
    }
}
