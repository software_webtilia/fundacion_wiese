<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBannersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banners', function (Blueprint $table) {
            $table->increments('id');
            $table->string('seccion',255)->default('home');
            $table->mediumText('titulo')->nullable();
            $table->mediumText('subttitulo')->nullable();
            $table->string('imagen',255);
            $table->string('link',255)->nullable();
            $table->smallInteger('orden')->default(1);
            $table->boolean('estado')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('banners');
    }
}
