<?php

use Illuminate\Database\Seeder;

class ProjectBeneficiariesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('project_beneficiaries')->delete();
        
        \DB::table('project_beneficiaries')->insert(array (
            0 => 
            array (
                'id' => 1,
                'project_id' => 9,
                'beneficiary_id' => 4,
                'created_at' => '2018-07-23 00:00:00',
                'updated_at' => '2018-07-23 00:00:00',
            ),
            1 => 
            array (
                'id' => 2,
                'project_id' => 9,
                'beneficiary_id' => 7,
                'created_at' => '2018-07-23 00:00:00',
                'updated_at' => '2018-07-23 00:00:00',
            ),
            2 => 
            array (
                'id' => 3,
                'project_id' => 9,
                'beneficiary_id' => 1,
                'created_at' => '2018-07-26 00:00:00',
                'updated_at' => '2018-07-26 00:00:00',
            ),
            3 => 
            array (
                'id' => 4,
                'project_id' => 11,
                'beneficiary_id' => 1,
                'created_at' => '2018-07-26 00:00:00',
                'updated_at' => '2018-07-26 00:00:00',
            ),
            4 => 
            array (
                'id' => 5,
                'project_id' => 3,
                'beneficiary_id' => 1,
                'created_at' => '2018-07-26 00:00:00',
                'updated_at' => '2018-07-26 00:00:00',
            ),
            5 => 
            array (
                'id' => 6,
                'project_id' => 12,
                'beneficiary_id' => 1,
                'created_at' => '2018-07-26 00:00:00',
                'updated_at' => '2018-07-26 00:00:00',
            ),
            6 => 
            array (
                'id' => 7,
                'project_id' => 13,
                'beneficiary_id' => 1,
                'created_at' => '2018-07-26 00:00:00',
                'updated_at' => '2018-07-26 00:00:00',
            ),
            7 => 
            array (
                'id' => 8,
                'project_id' => 7,
                'beneficiary_id' => 2,
                'created_at' => '2018-07-26 00:00:00',
                'updated_at' => '2018-07-26 00:00:00',
            ),
            8 => 
            array (
                'id' => 9,
                'project_id' => 9,
                'beneficiary_id' => 2,
                'created_at' => '2018-07-26 00:00:00',
                'updated_at' => '2018-07-26 00:00:00',
            ),
            9 => 
            array (
                'id' => 10,
                'project_id' => 3,
                'beneficiary_id' => 2,
                'created_at' => '2018-07-26 00:00:00',
                'updated_at' => '2018-07-26 00:00:00',
            ),
            10 => 
            array (
                'id' => 11,
                'project_id' => 13,
                'beneficiary_id' => 2,
                'created_at' => '2018-07-26 00:00:00',
                'updated_at' => '2018-07-26 00:00:00',
            ),
            11 => 
            array (
                'id' => 12,
                'project_id' => 9,
                'beneficiary_id' => 3,
                'created_at' => '2018-07-26 00:00:00',
                'updated_at' => '2018-07-26 00:00:00',
            ),
            12 => 
            array (
                'id' => 13,
                'project_id' => 3,
                'beneficiary_id' => 4,
                'created_at' => '2018-07-26 00:00:00',
                'updated_at' => '2018-07-26 00:00:00',
            ),
            13 => 
            array (
                'id' => 14,
                'project_id' => 8,
                'beneficiary_id' => 5,
                'created_at' => '2018-07-26 00:00:00',
                'updated_at' => '2018-07-26 00:00:00',
            ),
            14 => 
            array (
                'id' => 15,
                'project_id' => 10,
                'beneficiary_id' => 5,
                'created_at' => '2018-07-26 00:00:00',
                'updated_at' => '2018-07-26 00:00:00',
            ),
            15 => 
            array (
                'id' => 16,
                'project_id' => 5,
                'beneficiary_id' => 5,
                'created_at' => '2018-07-26 00:00:00',
                'updated_at' => '2018-07-26 00:00:00',
            ),
            16 => 
            array (
                'id' => 17,
                'project_id' => 6,
                'beneficiary_id' => 5,
                'created_at' => '2018-07-26 00:00:00',
                'updated_at' => '2018-07-26 00:00:00',
            ),
            17 => 
            array (
                'id' => 18,
                'project_id' => 2,
                'beneficiary_id' => 6,
                'created_at' => '2018-07-26 00:00:00',
                'updated_at' => '2018-07-26 00:00:00',
            ),
            18 => 
            array (
                'id' => 19,
                'project_id' => 4,
                'beneficiary_id' => 6,
                'created_at' => '2018-07-26 00:00:00',
                'updated_at' => '2018-07-26 00:00:00',
            ),
            19 => 
            array (
                'id' => 20,
                'project_id' => 3,
                'beneficiary_id' => 7,
                'created_at' => '2018-07-26 00:00:00',
                'updated_at' => '2018-07-26 00:00:00',
            ),
            20 => 
            array (
                'id' => 21,
                'project_id' => 4,
                'beneficiary_id' => 7,
                'created_at' => '2018-07-26 00:00:00',
                'updated_at' => '2018-07-26 00:00:00',
            ),
            21 => 
            array (
                'id' => 22,
                'project_id' => 3,
                'beneficiary_id' => 3,
                'created_at' => '2018-07-26 00:00:00',
                'updated_at' => '2018-07-26 00:00:00',
            ),
        ));
        
        
    }
}