<?php

use Illuminate\Database\Seeder;

class ProgramsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('programs')->delete();
        
        \DB::table('programs')->insert(array (
            0 => 
            array (
                'id' => 1,
                'nombre' => 'Proyecto Arqueológico El Brujo',
                'descripcion' => 'Proyecto Arqueológico El Brujo',
                'orden' => 1,
                'estado' => 'A',
                'created_at' => '2018-08-27 00:00:00',
                'updated_at' => '2018-08-27 00:00:00',
            ),
            1 => 
            array (
                'id' => 2,
                'nombre' => 'Calidad Educativa',
                'descripcion' => 'Calidad Educativa',
                'orden' => 2,
                'estado' => 'A',
                'created_at' => '2018-08-27 00:00:00',
                'updated_at' => '2018-08-27 00:00:00',
            ),
        ));
        
        
    }
}