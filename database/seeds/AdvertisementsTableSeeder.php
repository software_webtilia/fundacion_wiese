<?php

use Illuminate\Database\Seeder;

class AdvertisementsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('advertisements')->delete();
        
        \DB::table('advertisements')->insert(array (
            0 => 
            array (
                'id' => 1,
                'titulo' => 'Jefe de Laboratorio',
                'subtitulo' => 'Complejo Arqueológico El Brujo',
                'sumilla' => 'Estamos buscando un jefe de laboratorio para el Museo Cao.',
                'descripcion' => '<h2>PERFIL DE POSICIÓN:</h2>
<ul>

<li>Licenciado en Arqueología inscrito en el Registro Nacional de Arqueólogos del Perú.</li>
<li>Experiencia no menor de [04] cuatro años de ejercicio profesional.</li>
<li>Experiencia probada en manejo de colecciones de materiales arqueológicos contextualizados.</li>
<li>Nociones básicas y/o experiencia en monitoreo, conservación preventiva y museología de colecciones arqueológicas muebles en todas sus categorías.</li>
<li>Dominio de programas de bases de datos (Microsoft Access, SQL, y/u otros).</li>
<li>Tener un mínimo de cinco [05] publicaciones académicas. </li>
<li>Sustentar como mínimo [07] siete participaciones en Proyectos de Investigación Arqueológica (PIA).</li>
<li>Inglés intermedio.</li>

</ul>

<h2>FUNCIONES:</h2>
<ul>
<li>	Responsable del manejo y cuidado de la colección de bienes culturales muebles del Complejo Arqueológico El Brujo (CAEB) y Museo Cao, formada a través de los últimos 28 años de investigación arqueológica.</li>
<li>	Conceptualizar y liderar el desarrollo e implementación del Catálogo Virtual de Materiales del CAEB, una plataforma electrónica abierta donde se pondrá a disposición del público la información de la colección. </li>
<li>	Responsable del Laboratorio del Complejo Arqueológico El Brujo, el espacio donde se recibe, procesa, analiza y conserva el material arqueológico proveniente de las excavaciones.</li>
<li>	Monitoreo constante del estado de conservación, almacenaje e información de las colecciones en general, custodiadas en los depósitos o en exhibición en las instalaciones del Museo Cao.</li>
<li>	Coordinación con especialistas o grupos de interés sobre las colecciones del CAEB.</li>
<li>	Supervisión del personal de laboratorio, respecto a las distintas actividades relativas al manejo de bienes culturales muebles arqueológicos.</li>
<li>	Durante la ejecución de Proyectos de Investigación Arqueológica (PIA´s), coordina con los Arqueólogos de Campo y el Residente la recepción de los materiales para el procesamiento con fines de inventario y catalogación, elabora del inventario general de materiales arqueológicos, y selecciona/coordina la entrega muestras de material arqueológico destinadas a análisis especializados.</li>
</ul>',
                'lugar' => 'La Libertad',
                'orden' => 1,
                'fecha_inicio' => '2018-08-27 00:00:00',
                'fecha_fin' => '2018-10-17 00:00:00',
                'estado' => 'PUB',
                'project_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}