<?php

use Illuminate\Database\Seeder;

class ProjectDirectsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('project_directs')->delete();
        
        \DB::table('project_directs')->insert(array (
            0 => 
            array (
                'id' => 1,
                'titulo' => 'Complejo Arqueológico El Brujo',
                'descripcion_general' => 'El Brujo, un destino fascinante para todos sus visitantes y fuente de nuevos conocimientos para la humanidad.',
                'descripcion_1' => '<p>En el marco de sucesivos Convenios de Cooperación suscritos con el Ministerio de Cultura, la Fundación Wiese  trabaja por la investigación, conservación, puesta en valor y difusión del Complejo Arqueológico El Brujo.  <span class="complejo__descripcion--bold-lila">Actuamos allí, inspirados por el amor al Perú</span> y en la pasión de Don Guillermo “Pancho” Wiese, quien inició este proyecto en agosto de 1990. </p>',
                'imagen1' => 'img1.png',
                'descripcion_2' => '<ul>
<li class="complejo__descripcion-item"><span class="complejo__descripcion--bold-naranja">Impulsar nuevos proyectos de investigación científica</span> en colaboración con organizaciones, instituciones y aliados estratégicos.</li>
<li class="complejo__descripcion-item"><span class="complejo__descripcion--bold-naranja">Garantizar el acceso</span> de públicos diversos al conocimiento generado.</li>
<li class="complejo__descripcion-item"><span class="complejo__descripcion--bold-naranja">Integrar</span> a las comunidades aledañas al destino turístico.</li>
</ul>',
                'imagen2' => 'img2.png',
                'equipo' => 'img3.png',
                'fondo' => 'fondo.jpg',
                'impacto_numero' => 415,
                'impacto_label' => '<span class="beneficiados__numero-letra">
<span class="beneficiados__numero-letra beneficiados__numero-letra--ceros">000</span>
<br>PERSONAS<br>IMPACTADAS
</span',
                'created_at' => '2018-07-23 00:00:00',
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'titulo' => 'Calidad Educativa "Mejor clima escolar,<br> mejores aprendizajes"',
                'descripcion_general' => '"El buen clima en el aula permite que los estudiantes generen opciones creativas para resolver problemas, presentar posiciones de maneras asertivas, escuchar activamente a los demás… Y poner en práctica su pensamiento crítico frente a lo que pasa en su contexto cercano." <br>
<span style="float: right">(Enrique Chaux)</span>',
                'descripcion_1' => '<p>La Fundación Wiese trabaja con directores, subdirectores y docentes de escuelas públicas con el objetivo de mejorar significativamente el clima institucional y de aula para favorecer el desarrollo de mejores enseñanzas y aprendizajes.</p>',
                'imagen1' => 'educativo1.jpg',
                'descripcion_2' => '<ul>
<li class="complejo__descripcion-item"><span class="complejo__descripcion--bold-naranja">Mejorar el clima del aula</span>, desde el punto de vista de los estudiantes.</li>
<li class="complejo__descripcion-item"><span class="complejo__descripcion--bold-naranja">Mejorar las habilidades socioemocionales</span>, desde el punto de vista de los docentes.</li>
<li class="complejo__descripcion-item"><span class="complejo__descripcion--bold-naranja">Disminución de episodios de la violencia escolar.</span></li>
<li class="complejo__descripcion-item"><span class="complejo__descripcion--bold-naranja">Equidad de género en los aprendizajes y en las relaciones.</li>
</ul>',
                'imagen2' => 'educativo2.jpg',
                'equipo' => 'educativo3.jpg',
                'fondo' => 'fondo2.jpg',
                'impacto_numero' => 3261,
                'impacto_label' => '<span class="beneficiados__numero-letra">
<br>NIÑOS Y EDUCADORES <br> IMPACTADOS</span>',
                'created_at' => '2018-07-23 00:00:00',
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'titulo' => 'El Fondo Wiese',
                'descripcion_general' => '"Una plataforma que nos permite dirigir programas operados por terceros."',
                'descripcion_1' => '<p>El Fondo Wiese es la plataforma creada por la Fundación Wiese con el objetivo de ampliar su impacto social, a través de la dirección y patrocinio de programas operados por terceros.</p>
<p>El Fondo Wiese se divide en tres categorías:</p>
<ul>
<li >Fondo Wiese Educación (operado por IPFE)</li>
<li>Fondo Wiese Emprendedor (operado por NESsT)</li>
<li style="color:#c1c1c1;">Fondo Wiese Salud (próximamente)</li>
</ul>',
                'imagen1' => 'fondo_wiese1.jpg',
                'descripcion_2' => '<ul>
<li class="complejo__descripcion-item"><span class="complejo__descripcion--bold-naranja">El Fondo Wiese Educación</span><br> Financiar programas de crédito educativo para peruanos de comunidades vulnerables.</li>
<li class="complejo__descripcion-item"><span class="complejo__descripcion--bold-naranja">El Fondo Wiese Emprendedor</span><br>Impulsar emprendimientos con claro potencial de autosostenibilidad, cuya misión sea la de atender un problema social relevante del país.</li>
</ul>',
                'imagen2' => 'fondo_wiese2.jpg',
                'equipo' => 'fondo_wiese3.jpg',
                'fondo' => 'fondo3.jpg',
                'impacto_numero' => NULL,
                'impacto_label' => NULL,
                'created_at' => '2018-07-23 00:00:00',
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}