<?php

use Illuminate\Database\Seeder;

class BeneficiariesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('beneficiaries')->delete();
        
        \DB::table('beneficiaries')->insert(array (
            0 => 
            array (
                'id' => 1,
                'nombre' => 'Niños',
                'orden' => 1,
                'estado' => 1,
                'created_at' => '2018-07-23 00:00:00',
                'updated_at' => '2018-07-23 00:00:00',
            ),
            1 => 
            array (
                'id' => 2,
                'nombre' => 'Jóvenes',
                'orden' => 1,
                'estado' => 1,
                'created_at' => '2018-07-23 00:00:00',
                'updated_at' => '2018-07-23 00:00:00',
            ),
            2 => 
            array (
                'id' => 3,
                'nombre' => 'Ancianos',
                'orden' => 1,
                'estado' => 1,
                'created_at' => '2018-07-23 00:00:00',
                'updated_at' => '2018-07-23 00:00:00',
            ),
            3 => 
            array (
                'id' => 4,
                'nombre' => 'Familias',
                'orden' => 1,
                'estado' => 1,
                'created_at' => '2018-07-23 00:00:00',
                'updated_at' => '2018-07-23 00:00:00',
            ),
            4 => 
            array (
                'id' => 5,
                'nombre' => 'Academia',
                'orden' => 1,
                'estado' => 1,
                'created_at' => '2018-07-23 00:00:00',
                'updated_at' => '2018-07-23 00:00:00',
            ),
            5 => 
            array (
                'id' => 6,
                'nombre' => 'Negocio',
                'orden' => 1,
                'estado' => 1,
                'created_at' => '2018-07-23 00:00:00',
                'updated_at' => '2018-07-23 00:00:00',
            ),
            6 => 
            array (
                'id' => 7,
                'nombre' => 'Comunidades',
                'orden' => 1,
                'estado' => 1,
                'created_at' => '2018-07-23 00:00:00',
                'updated_at' => '2018-07-23 00:00:00',
            ),
        ));
        
        
    }
}