<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(ProjectsTableSeeder::class);
        $this->call(BeneficiariesTableSeeder::class);
        $this->call(LocationsTableSeeder::class);
        $this->call(ProjectBeneficiariesTableSeeder::class);
        $this->call(ProjectLocationsTableSeeder::class);
        $this->call(ProjectDirectsTableSeeder::class);
        $this->call(ZonesTableSeeder::class);
        $this->call(AdvertisementsTableSeeder::class);
        $this->call(ProgramsTableSeeder::class);
    }
}
