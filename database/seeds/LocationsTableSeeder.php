<?php

use Illuminate\Database\Seeder;

class LocationsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('locations')->delete();
        
        \DB::table('locations')->insert(array (
            0 => 
            array (
                'id' => 1,
                'nombre' => 'Chincha',
                'orden' => 1,
                'estado' => 1,
                'created_at' => '2018-07-23 00:00:00',
                'updated_at' => '2018-07-23 00:00:00',
            ),
            1 => 
            array (
                'id' => 2,
                'nombre' => 'Cusco',
                'orden' => 2,
                'estado' => 0,
                'created_at' => '2018-07-23 00:00:00',
                'updated_at' => '2018-07-23 00:00:00',
            ),
            2 => 
            array (
                'id' => 3,
                'nombre' => 'Huaraz',
                'orden' => 3,
                'estado' => 1,
                'created_at' => '2018-07-23 00:00:00',
                'updated_at' => '2018-07-23 00:00:00',
            ),
            3 => 
            array (
                'id' => 4,
                'nombre' => 'La Libertad',
                'orden' => 4,
                'estado' => 1,
                'created_at' => '2018-07-23 00:00:00',
                'updated_at' => '2018-07-23 00:00:00',
            ),
            4 => 
            array (
                'id' => 5,
                'nombre' => '	
Lima',
                'orden' => 5,
                'estado' => 1,
                'created_at' => '2018-07-23 00:00:00',
                'updated_at' => '2018-07-23 00:00:00',
            ),
            5 => 
            array (
                'id' => 6,
                'nombre' => 'Pachacamac',
                'orden' => 6,
                'estado' => 1,
                'created_at' => '2018-07-23 00:00:00',
                'updated_at' => '2018-07-23 00:00:00',
            ),
            6 => 
            array (
                'id' => 7,
                'nombre' => 'Puno',
                'orden' => 7,
                'estado' => 0,
                'created_at' => '2018-07-23 00:00:00',
                'updated_at' => '2018-07-23 00:00:00',
            ),
            7 => 
            array (
                'id' => 8,
                'nombre' => 'Tacna',
                'orden' => 8,
                'estado' => 0,
                'created_at' => '2018-07-23 00:00:00',
                'updated_at' => '2018-07-23 00:00:00',
            ),
        ));
        
        
    }
}