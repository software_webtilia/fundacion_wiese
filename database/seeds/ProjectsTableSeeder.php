<?php

use Illuminate\Database\Seeder;

class ProjectsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('projects')->delete();
        
        \DB::table('projects')->insert(array (
            0 => 
            array (
                'id' => 2,
            'titulo' => 'Proyecto Bienal de Venecia (2013 - 2033)',
                'imagen' => 'participa02.png',
                'programa' => NULL,
                'link' => 'http://www.fundacionwiese.org/bienal-de-venecia/',
                'descripcion' => 'Gracias a un acuerdo firmado en 2013 entre La Fundación Wiese y La Fundación Biennale di Venezia, se hizo posible la participación del Perú por 20 años en este prestigioso foro desarrollado en Italia. Desde el 2017, en el marco de una alianza con el Patronato Cultural del Perú, la Fundación Wiese le encarga la organización de las exposiciones que se realizan en Venecia, así como sus réplicas en el Perú. A través de la Bienal de Venecia, la Fundación Wiese busca revalorar el trabajo de los artistas y arquitectos peruanos, para que a través de ellos, se genere bienestar y desarrollo para el país.',
                'orden' => 6,
                'estado' => 2,
                'created_at' => '2018-07-23 00:00:00',
                'updated_at' => '2018-07-23 00:00:00',
            ),
            1 => 
            array (
                'id' => 3,
            'titulo' => 'Clínica de Puente Piedra (2008-2017)',
                'imagen' => 'participa04.png',
                'programa' => 'Fondo Salud',
                'link' => NULL,
            'descripcion' => 'Con el objeto promover la implementación de un establecimiento de salud de primer nivel de atención para la prevención y promoción de la salud, la atención integral y el desarrollo social de los habitantes de Lima Norte (Puente Piedra, Carabayllo, Ventanilla, Mi Perú, Ancón y Santa Rosa), la Fundación Wiese edificó una clínica, ubicada en la urbanización El Arenal del distrito de Puente Piedra. En 2017, la Fundación Wiese dispuso de un fondo para el desarrollo de un programa de atención médica  integral, el cual será operado por los actuales propietarios de la clínica y que brindará atenciones subvencionadas a los pacientes más vulnerables de la zona. ',
                'orden' => 7,
                'estado' => 3,
                'created_at' => '2018-07-23 00:00:00',
                'updated_at' => '2018-07-23 00:00:00',
            ),
            2 => 
            array (
                'id' => 4,
            'titulo' => 'Desarrollo Sostenible del Turismo en el Corredor de Conchucos (2015-2017)',
                'imagen' => 'participa05.png',
                'programa' => NULL,
                'link' => NULL,
                'descripcion' => 'El propósito de este proyecto fue generar ingresos y empleo, así como incentivar el desarrollo sostenido y responsable del turismo en los distritos de Chavín de Huántar, Acopalca y San Marcos, ubicados en el Corredor de Conchucos, Región Ancash. Este convenio se ejecutó en colaboración con la Compañía Minera Antamina y el Municipio Distrital de Chavín de Huántar.',
                'orden' => 9,
                'estado' => 3,
                'created_at' => '2018-07-23 00:00:00',
                'updated_at' => '2018-07-23 00:00:00',
            ),
            3 => 
            array (
                'id' => 5,
            'titulo' => 'Investigación Arqueológica y Conservación del Complejo Arqueológico de Chavín de Huántar (2013-2016)',
                'imagen' => 'participa06.png',
                'programa' => NULL,
                'link' => NULL,
                'descripcion' => 'Durante cuatro años, la Fundación Wiese participó en este proyecto, brindando su apoyo en la administración de los recursos donados por la Compañía Minera Antamina para el desarrollo de investigaciones arqueológicas y conservación en el Complejo Arqueológico de Chavín de Huántar, Región de Ancash.',
                'orden' => 10,
                'estado' => 3,
                'created_at' => '2018-07-23 00:00:00',
                'updated_at' => '2018-07-23 00:00:00',
            ),
            4 => 
            array (
                'id' => 6,
            'titulo' => 'Película “Chavín de Huántar, el teatro del más allá” (2014 - 2015)',
                'imagen' => 'participa07.png',
                'programa' => NULL,
                'link' => NULL,
                'descripcion' => 'El objetivo de este documental fue dar a conocer la cultura Chavín al mundo. Cómo se originó hace 3200 años, y la importancia que tuvo su influencia en las culturas posteriores. La Fundación Wiese y Wanda Films co-produjeron este largometraje que fue difundido en varias salas locales de cine y a través de la señal de National Geographic, a nivel regional.',
                'orden' => 12,
                'estado' => 3,
                'created_at' => '2018-07-23 00:00:00',
                'updated_at' => '2018-07-23 00:00:00',
            ),
            5 => 
            array (
                'id' => 7,
            'titulo' => 'Crédito de Estudios Senati  (2018 – 2021)',
                'imagen' => 'participa09.jpg',
                'programa' => 'Fondo Educativo',
                'link' => NULL,
                'descripcion' => 'Este proyecto es el primero en ser implementado en el marco del Fondo Wiese Educación con el propósito de financiar los estudios técnicos de aproximadamente 160 alumnos del SENATI – Servicio Nacional de Adiestramiento en Trabajo Industrial –, en cualquiera de sus 80 sedes en el territorio nacional, durante los próximos 3 años. El objetivo de este programa es permitir que alumnos talentosos, con riesgo de deserción económica, puedan culminar satisfactoriamente sus estudios, contribuyendo de esta forma a mejorar su calidad de vida y la de sus familias.',
                'orden' => 1,
                'estado' => 1,
                'created_at' => '2018-07-23 00:00:00',
                'updated_at' => '2018-07-23 00:00:00',
            ),
            6 => 
            array (
                'id' => 8,
            'titulo' => 'OXI - El Brujo (2018 – 2020)',
                'imagen' => 'participa10.jpg',
                'programa' => 'Programa El Brujo',
                'link' => NULL,
            'descripcion' => 'Se trata del primer proyecto de Obras por Impuestos (OXI) del Sector Cultura, en el Perú; es también el primero a ser co-ejecutado por una entidad nacional (Ministerio de Cultura) y una sub-nacional (Gobierno Nacional de La Libertad). Este proyecto será financiado por Inmuebles Panamericana S.A., quién ha encargado su ejecución al equipo técnico de la Fundación Wiese que conduce el Programa Arqueológico El Brujo.El objetivo principal del proyecto es incrementar la oferta turística del complejo, a través de la excavación arqueológica, investigación y conservación de 3 nuevas zonas dentro de su área intangible, así como de la difusión y promoción turística, basada en los hallazgos del proyecto.',
                'orden' => 2,
                'estado' => 1,
                'created_at' => '2018-07-23 00:00:00',
                'updated_at' => '2018-07-23 00:00:00',
            ),
            7 => 
            array (
                'id' => 9,
            'titulo' => 'Juntos por la Costa Norte  (2017)',
                'imagen' => 'participa03.png',
                'programa' => 'Campañas de auxilio ante desastres',
                'link' => NULL,
                'descripcion' => 'Ante los devastadores efectos de los huaycos e inundaciones ocurridos en el primer trimestre de 2017, como consecuencia del Fenómeno del Niño, la Fundación Wiese tomó acción inmediata, articulando y gestionando una red de ayuda humanitaria en alianza con diversas instituciones públicas y privadas, con el objetivo de aliviar el sufrimiento de los damnificados más vulnerables en las regiones de Piura, Lambayeque y La Libertad. Esta campaña recibió el nombre de “Juntos por la Costa Norte”, y a través de ella se logró atender oportuna y eficazmente a 15,620 familias.',
                'orden' => 3,
                'estado' => 3,
                'created_at' => '2018-07-23 00:00:00',
                'updated_at' => '2018-07-23 00:00:00',
            ),
            8 => 
            array (
                'id' => 10,
            'titulo' => 'El Rostro de la Señora de Cao (2016 – 2017)',
                'imagen' => 'participa11.jpg',
                'programa' => 'Programa El Brujo',
                'link' => NULL,
                'descripcion' => 'La tecnología de escaneo láser 3D y el concurso de un equipo multidisciplinario de investigadores de renombre mundial se unieron bajo el liderazgo de los profesionales del Programa Arqueológico El Brujo, para reconstruir la rostro de la enigmática momia, conocida como la Señora de Cao. Se cree que este personaje fue una poderosa líder femenina de la Civilización Moche que vivió hace más de 1700 años. El 4 de julio de 2017, se develaron al mundo los resultados de este proyecto de investigación, sin precedentes en el Perú. Por su relevancia, la noticia rápidamente dio la vuelta al mundo.',
                'orden' => 4,
                'estado' => 3,
                'created_at' => '2018-07-23 00:00:00',
                'updated_at' => '2018-07-23 00:00:00',
            ),
            9 => 
            array (
                'id' => 11,
            'titulo' => 'Mejor clima escolar, mejores aprendizajes (2016 – 2020)',
                'imagen' => 'participa12.jpg',
                'programa' => 'Programa Calidad Educativa
',
                'link' => NULL,
                'descripcion' => 'En el marco de su Programa de Calidad Educativa, en abril de 2016, la Fundación Wiese inició el proyecto “Mejor clima escolar, mejores aprendizajes”. Este proyecto trabaja principalmente con directores, subdirectores y docentes de 14 escuelas públicas de Lurín y Pachacamac, y tiene como objetivo principal mejorar significativamente el clima institucional y del aula para favorecer el desarrollo de mejores enseñanzas y aprendizajes.',
                'orden' => 5,
                'estado' => 2,
                'created_at' => '2018-07-23 00:00:00',
                'updated_at' => '2018-07-23 00:00:00',
            ),
            10 => 
            array (
                'id' => 12,
            'titulo' => 'Calidad Educativa – Primera Etapa (2009 – 2016)',
                'imagen' => 'participa13.jpg',
                'programa' => '	
Programa Calidad Educativa',
                'link' => NULL,
            'descripcion' => 'Proyecto ejecutado en el marco de dos convenios de colaboración suscritos con la Dirección Regional de Educación de Ica (DREI) y la Dirección Regional de Educación de Lima Metropolitana (DRELM), con el objeto de contribuir a mejorar las técnicas pedagógicas de matemáticas, comprensión lectora y personal social de docentes de escuelas públicas, en los distritos de Alto Larán, Chincha Baja, Pachacamác y Lurín.',
                'orden' => 8,
                'estado' => 2,
                'created_at' => '2018-07-23 00:00:00',
                'updated_at' => '2018-07-23 00:00:00',
            ),
            11 => 
            array (
                'id' => 13,
            'titulo' => 'Crea+ (2014 – 2016)',
                'imagen' => 'participa14.jpg',
                'programa' => NULL,
                'link' => NULL,
                'descripcion' => 'Crea+ es el primer Voluntariado profesional en educación del Perú que promueve la Responsabilidad Social Individual entre los jóvenes, quiénes donan su tiempo para buscar que más niños y adolescentes crean en sí mismos para lograr sus sueños;  a través de un modelo que educa en valores, desarrolla capacidades y potencia talentos en escuelas de zonas vulnerables del Perú. La Fundación Wiese patrocinó las etapas tempranas de este proyecto, haciendo posible su consolidación posterior.',
                'orden' => 11,
                'estado' => 2,
                'created_at' => '2018-07-23 00:00:00',
                'updated_at' => '2018-07-23 00:00:00',
            ),
        ));
        
        
    }
}