<?php

use Illuminate\Database\Seeder;

class ProjectLocationsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('project_locations')->delete();
        
        \DB::table('project_locations')->insert(array (
            0 => 
            array (
                'id' => 4,
                'project_id' => 3,
                'location_id' => 5,
                'created_at' => '2018-07-26 00:00:00',
                'updated_at' => '2018-07-26 00:00:00',
            ),
            1 => 
            array (
                'id' => 5,
                'project_id' => 7,
                'location_id' => 5,
                'created_at' => '2018-07-26 00:00:00',
                'updated_at' => '2018-07-26 00:00:00',
            ),
            2 => 
            array (
                'id' => 6,
                'project_id' => 13,
                'location_id' => 5,
                'created_at' => '2018-07-26 00:00:00',
                'updated_at' => '2018-07-26 00:00:00',
            ),
            3 => 
            array (
                'id' => 7,
                'project_id' => 2,
                'location_id' => 5,
                'created_at' => '2018-07-26 00:00:00',
                'updated_at' => '2018-07-26 00:00:00',
            ),
            4 => 
            array (
                'id' => 8,
                'project_id' => 9,
                'location_id' => 4,
                'created_at' => '2018-07-26 00:00:00',
                'updated_at' => '2018-07-26 00:00:00',
            ),
            5 => 
            array (
                'id' => 9,
                'project_id' => 8,
                'location_id' => 4,
                'created_at' => '2018-07-26 00:00:00',
                'updated_at' => '2018-07-26 00:00:00',
            ),
            6 => 
            array (
                'id' => 10,
                'project_id' => 10,
                'location_id' => 4,
                'created_at' => '2018-07-26 00:00:00',
                'updated_at' => '2018-07-26 00:00:00',
            ),
            7 => 
            array (
                'id' => 11,
                'project_id' => 11,
                'location_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            8 => 
            array (
                'id' => 12,
                'project_id' => 5,
                'location_id' => 3,
                'created_at' => '2018-07-26 00:00:00',
                'updated_at' => '2018-07-26 00:00:00',
            ),
            9 => 
            array (
                'id' => 13,
                'project_id' => 6,
                'location_id' => 3,
                'created_at' => '2018-07-26 00:00:00',
                'updated_at' => '2018-07-26 00:00:00',
            ),
            10 => 
            array (
                'id' => 14,
                'project_id' => 4,
                'location_id' => 3,
                'created_at' => '2018-07-26 00:00:00',
                'updated_at' => '2018-07-26 00:00:00',
            ),
            11 => 
            array (
                'id' => 15,
                'project_id' => 12,
                'location_id' => 1,
                'created_at' => '2018-07-26 00:00:00',
                'updated_at' => '2018-07-26 00:00:00',
            ),
        ));
        
        
    }
}