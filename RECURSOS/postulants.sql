-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 24-08-2018 a las 02:15:14
-- Versión del servidor: 10.1.21-MariaDB
-- Versión de PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `fundacion-wiese`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `postulants`
--

CREATE TABLE `postulants` (
  `id` int(11) NOT NULL,
  `nombres` varchar(120) NOT NULL,
  `apellidos` varchar(120) NOT NULL,
  `telefono` varchar(20) NOT NULL,
  `correo` varchar(100) NOT NULL,
  `departamento` varchar(120) NOT NULL,
  `provincia` varchar(120) NOT NULL,
  `profesion` varchar(120) NOT NULL,
  `cv` varchar(150) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `postulants`
--

INSERT INTO `postulants` (`id`, `nombres`, `apellidos`, `telefono`, `correo`, `departamento`, `provincia`, `profesion`, `cv`, `created_at`, `updated_at`) VALUES
(1, 'carlos', 'UBILLUS', '23333444', 'correo@correo.com', 'AREQUIPA', 'CASTILLA', 'profesion', '1535061466-PlantillaValidacionCodigoSocio.xls', '2018-08-24 02:57:46', '2018-08-24 02:57:46'),
(2, 'carlos', 'UBILLUS', '23333444', 'correo@correo.com', 'AREQUIPA', 'CAYLLOMA', 'profesion', '1535062112-PlantillaValidacionCodigoSocio.xls', '2018-08-24 03:08:32', '2018-08-24 03:08:32'),
(3, 'wewewe', 'UBILLUS', '23333444', 'correo@correo.com', 'AYACUCHO', 'LUCANAS', 'profesion', '1535065134-Reporte Historico (5).xls', '2018-08-24 03:58:54', '2018-08-24 03:58:54');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `postulants`
--
ALTER TABLE `postulants`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `postulants`
--
ALTER TABLE `postulants`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
