-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 12-12-2018 a las 17:29:48
-- Versión del servidor: 5.5.51-38.2
-- Versión de PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `wiese_fundacion_2018`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `advertisements`
--

CREATE TABLE `advertisements` (
  `id` int(10) UNSIGNED NOT NULL,
  `titulo` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subtitulo` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sumilla` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descripcion` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `lugar` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `orden` int(11) NOT NULL,
  `fecha_inicio` datetime NOT NULL,
  `fecha_fin` datetime NOT NULL,
  `estado` char(3) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'PUB',
  `project_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `advertisements`
--

INSERT INTO `advertisements` (`id`, `titulo`, `subtitulo`, `sumilla`, `descripcion`, `lugar`, `orden`, `fecha_inicio`, `fecha_fin`, `estado`, `project_id`, `created_at`, `updated_at`) VALUES
(1, 'Jefe de Laboratorio', 'Complejo Arqueológico El Brujo', '', '<h2>PERFIL DE POSICIÓN:</h2>\r\n<ul>\r\n\r\n<li>Licenciado en Arqueología inscrito en el Registro Nacional de Arqueólogos del Perú.</li>\r\n<li>Experiencia no menor de [04] cuatro años de ejercicio profesional.</li>\r\n<li>Experiencia probada en manejo de colecciones de materiales arqueológicos contextualizados.</li>\r\n<li>Nociones básicas y/o experiencia en monitoreo, conservación preventiva y museología de colecciones arqueológicas muebles en todas sus categorías.</li>\r\n<li>Dominio de programas de bases de datos (Microsoft Access, SQL, y/u otros).</li>\r\n<li>Contar con publicaciones académicas.</li>\r\n<li>Sustentar como mínimo [04] cuatro participaciones en Proyectos de Investigación Arqueológica (PIA).</li>\r\n<li>Inglés básico/intermedio.</li>\r\n\r\n</ul>\r\n\r\n<h2>FUNCIONES:</h2>\r\n<ul>\r\n<li>	Responsable del manejo y cuidado de la colección de bienes culturales muebles del Complejo Arqueológico El Brujo (CAEB) y Museo Cao, formada a través de los últimos 28 años de investigación arqueológica.</li>\r\n<li>	Conceptualizar y liderar el desarrollo e implementación del Catálogo Virtual de Materiales del CAEB, una plataforma electrónica abierta donde se pondrá a disposición del público la información de la colección. </li>\r\n<li>	Responsable del Laboratorio del Complejo Arqueológico El Brujo, el espacio donde se recibe, procesa, analiza y conserva el material arqueológico proveniente de las excavaciones.</li>\r\n<li>	Monitoreo constante del estado de conservación, almacenaje e información de las colecciones en general, custodiadas en los depósitos o en exhibición en las instalaciones del Museo Cao.</li>\r\n<li>	Coordinación con especialistas o grupos de interés sobre las colecciones del CAEB.</li>\r\n<li>	Supervisión del personal de laboratorio, respecto a las distintas actividades relativas al manejo de bienes culturales muebles arqueológicos.</li>\r\n<li>	Durante la ejecución de Proyectos de Investigación Arqueológica (PIA´s), coordina con los Arqueólogos de Campo y el Residente la recepción de los materiales para el procesamiento con fines de inventario y catalogación, elabora del inventario general de materiales arqueológicos, y selecciona/coordina la entrega muestras de material arqueológico destinadas a análisis especializados.</li>\r\n</ul>', 'La Libertad', 1, '2018-08-27 00:00:00', '2018-09-28 23:59:59', 'PVB', 8, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `banners`
--

CREATE TABLE `banners` (
  `id` int(10) UNSIGNED NOT NULL,
  `seccion` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'home',
  `titulo` mediumtext COLLATE utf8_unicode_ci,
  `subttitulo` mediumtext COLLATE utf8_unicode_ci,
  `imagen` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `orden` smallint(6) NOT NULL DEFAULT '1',
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `beneficiaries`
--

CREATE TABLE `beneficiaries` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `orden` smallint(6) NOT NULL DEFAULT '1',
  `estado` smallint(6) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `beneficiaries`
--

INSERT INTO `beneficiaries` (`id`, `nombre`, `orden`, `estado`, `created_at`, `updated_at`) VALUES
(1, 'Niños', 1, 1, '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(2, 'Jóvenes', 1, 1, '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(3, 'Ancianos', 1, 1, '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(4, 'Familias', 1, 1, '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(5, 'Academia', 1, 1, '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(6, 'Negocio', 1, 1, '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(7, 'Comunidades', 1, 1, '2018-07-23 05:00:00', '2018-07-23 05:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contacts`
--

CREATE TABLE `contacts` (
  `id` int(10) UNSIGNED NOT NULL,
  `infocontacto_id` int(10) DEFAULT '0',
  `nombres` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `apellidos` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefono` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mensaje` text COLLATE utf8_unicode_ci,
  `estado` varchar(1) COLLATE utf8_unicode_ci DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `contacts`
--

INSERT INTO `contacts` (`id`, `infocontacto_id`, `nombres`, `apellidos`, `telefono`, `email`, `mensaje`, `estado`, `created_at`, `updated_at`) VALUES
(1, 0, 'EVELYN ', 'ORTEGA CAMACHO', '958964117', 'evelyn_0709@hotmail.com', 'Deseo postular a una oportunidad laboral', '1', '2018-07-26 03:08:44', '2018-07-26 03:08:44'),
(2, 0, 'carlos', 'ubillus', '966369312', 'cubillus2003@yahoo.es', 'mensaje de prueba sssss', '1', '2018-07-31 21:01:38', '2018-07-31 21:01:38'),
(3, 0, 'Juan Alberto ', 'zuñiga Flores', '2620013', 'jalbertzuniga@hotmail.com', 'hola prueba ', '1', '2018-08-04 02:11:57', '2018-08-04 02:11:57'),
(4, 0, 'SARA ', 'LUYO', '2475085', 'contacto@argentaperu.com', 'Estimados Señores,\r\n\r\nSi deseas implementar el servicio en tu empresa ARGENTA PERU, Le brinda máquinas cafeteras gratuitas por el consumo de nuestro café en cápsulas con azúcar,  removedores,  delivery y mantenimiento o cambio de máquina si fuese necesario.\r\nSi están interesados, le proponemos una visita para una degustación sin ningún compromiso.\r\nSe adjunta modelos de máquinas de acuerdo a su necesidad. \r\nQuedamos atentos de su respuesta.\r\n\r\nAgradeciendo su gentil atención.\r\n\r\nSaludos Cordiales, \r\n\r\nSara Luyo A.\r\n\r\n \r\nArgenta Perú S.A.C.\r\nJr. Teniente Delucchi 205\r\nBarranco – Lima\r\nPerú\r\nTel. +511 247 5085\r\nwww.argentaperu.com\r\n', '1', '2018-08-09 18:57:58', '2018-08-09 18:57:58'),
(5, 0, 'Joel H.', 'Llerena Alfaro', '949338611', 'jllerena21@yahoo.es', 'Comunico que e recepcionado el proveído oficial de Carta FW 039-2018 de Adriana Doig Mnnucci, de invitación a celebración en honor a la señoa de Cao, el día 24 de agosto de 2018, en el Hotel Casa Andina, Trujillo, que les confirmo anticipadamente mi participación, en representación de la Gerencia Regional de Cooperación Técnica y Promoción de la Inversión Privada del Gobierno Regional La Libertad. Atentamente, Ing. Joel H. Llerena Alfaro.', '1', '2018-08-22 19:09:30', '2018-08-22 19:09:30'),
(6, 0, 'Lila Mercedes', 'Melendez Mazabel', '928506100', 'lila.melendez.15@gmail.com', 'Buenas tardes,\r\nSoy estudiante de SENATI curso el 4to ciclo y quiero saber como puedo postular para la fundación wiese y ser favorecida con su ayuda.\r\nGracias, saludos', '1', '2018-08-29 02:20:10', '2018-08-29 02:20:10'),
(7, 0, 'Herlita', 'Del Castillo', '980694224', 'herlitad@hotmail.com', 'Hola soy Herlita del castillo, hija del dr Jaime del castillo médico y gran aficionado por nuestra arqueología. Nos gustaría conversar para proponer un proyecto . Gracias', '1', '2018-09-01 15:51:12', '2018-09-01 15:51:12'),
(8, 0, 'heidy', 'Balza', '988653343', 'musidiom@gmail.com', 'Saludos me gustaria formar parte de su gran familia, actualmente tenemos un proyectp educativo increible de apoyo social y comunitario, contamos con el capital humano pero tenemos pocos recursos y nos gustaria poder presentarle este majestuoso proyecto . https://www.youtube.com/watch?v=aw8NB2m6-sI&t=12s&ab_channel=HeidyBalza', '1', '2018-09-03 21:26:13', '2018-09-03 21:26:13'),
(9, 0, 'heidy', 'Balza', '988653343', 'musidiom@gmail.com', 'Saludos me gustaria formar parte de su gran familia, actualmente tenemos un proyectp educativo increible de apoyo social y comunitario, contamos con el capital humano pero tenemos pocos recursos y nos gustaria poder presentarle este majestuoso proyecto\r\nhttps://www.youtube.com/watch?v=aw8NB2m6-sI&t=12s&ab_channel=HeidyBalza', '1', '2018-09-03 21:48:03', '2018-09-03 21:48:03'),
(10, 0, 'Alejandra', 'Visscher Piqueras', '924340744', 'alejandra.visscher@gmail.com', 'Hola, buenos días. Tengo un proyecto periodístico y editorial sobre abuso sexual infantil y quería saber si en la Fundación Wiese están interesados en que se los muestre para ver la posibilidad de financiamiento.\r\n\r\nSaludos y gracias,\r\n\r\nAlejandra.', '1', '2018-09-14 18:59:37', '2018-09-14 18:59:37'),
(11, 0, 'Yelitza Silvana', 'Meneses Meregildo', '977300047', 'yesime_mm@hotmail.com', 'buenas tardes,necesito fotos de los gobernantes y de los pobladores de esos tiempos y de la señora de cao es q tengo q terminar una maqueta y no ubico los dibujos', '1', '2018-10-05 22:06:24', '2018-10-05 22:06:24'),
(12, 0, 'Gabriela', 'Pfennig Mendo', '993692730', 'gabrielapfennig@gmail.com', 'Buenas tardes, soy Gabriela Pfennig, sociologa especializada en evaluación y seguimiento de proyectos sociales. Sé que no es común solicitar empleo por este medio, pero mis datos por si acaso. Muchas gracias', '1', '2018-10-13 00:53:04', '2018-10-13 00:53:04'),
(13, 0, 'José Elías', 'Bueno Pariente', '989338813', 'josbueno@gmail.com', 'Me comunico con ustedes, buscando ayuda para el pago de los estudios de mis hijos, mi nombre es José Elías Bueno Pariente y vivo en Lima, Perú, con mi esposa y mis dos hijos menores, el mayor está siguiendo estudios en otro país, por su cuenta. Actualmente estoy sin trabajo y los ingresos de mi esposa por la artesanía que elabora no son suficientes. Nosotros hemos tenido que emigrar de Venezuela por la situación que vive, vendiendo todo, aquí no hemos podido adquirir todo lo que teníamos allá, vivimos con una tía que sufre de demencia senil, ella tiene una pequeña pensión que sirve para la alimentación, he tenido que recurrir a créditos para financiar los estudios de mis hijos, ahora tengo unas deudas grandes que ya no puedo pagar,pero lo que más me preocupa son los estudios de mis hijos, también ellos trabajan en lo que pueden para ayudar, pero no es suficiente, les pido, por favor, ayuden a mis hijos con sus estudios, solo eso, a uno le falta solo un año para culminar y al otro dos años y medio, serán como 15,000 soles al primero y 40,000 soles al segundo, un total de 55,000 soles o 14,400 € aproximadamente.\r\nEspero puedan ser solidarios con nosotros y puedan ayudarnos. Puedo enviar toda información soliciten que corrobore lo que les digo, recibos de ingresos, estados de cuenta, pagos de Universidad, certificados de estudio, etc. También puedo trabajar para la fundación ad-honorem para devolver lo prestado.\r\nAgradezco su atención a este padre desesperado que solo pide para sus hijos.\r\nAtentamente\r\nJosé Bueno, correo josbueno@gmail.com', '1', '2018-10-20 20:53:55', '2018-10-20 20:53:55'),
(14, 0, 'RAFAEL EDGARDO', 'DUFFAUT ORDPÑEZ', '988599102', 'raffo_duor@hotmail.com', 'Buenos dìas Sres;\r\n\r\n\r\nLes escribo desde Lima - Perù y mi nombre es Rafael Duffaut Ordoñez, y es por la siguiente razòn ,\r\n\r\n\r\nMe dedico a la piuntura hace muchos años ,exactamente pintura al òleo sobre lienzo , abstracto , retratos\r\n\r\npaisajes y varias tècnicas y temàticas.  Uds. podràn observar bastante de mi trabajo en Instagram , me encontraràn \r\n\r\ncomo  \" rafaelduffaut \" , Oil Rafael Duffaut Paintings,.\r\n\r\n\r\nEl problema es que me encanta la pintura vivo para ello hago obras propias y replicas de famosos tambièn , pero no cuento \r\n\r\ncon el capital para seguir adelante , prácticamente ya no tengo recursos materiales  y pienso que puedo aportar mucho almundo artístico.\r\n\r\nUno de mis mayores anhelos es hacer un Instituto de Pintura ,dedicado a esos grandes artistas anònimos que tienen mucho talento pero que no pueden seguir este camino debido a los costos inalcanzables que para ellos les significa èsto.\r\n\r\n\r\nSi hubiera alguna manera de que me apoyen para seguir adelante ,por favor haganmelo saber.\r\n\r\n\r\nMi nombre:      Rafael Duffaut Ordoñez\r\n\r\nMi Direcciòn :  Los Robles 331, Chaclacayo, Lima - Peru ( Sur America)\r\n\r\nTelefono:      988599102\r\n\r\n\r\nMuchas gracias.\r\n\r\n\r\nAtentamente.-\r\n\r\n\r\nRafael.', '1', '2018-10-24 20:24:46', '2018-10-24 20:24:46'),
(29, 0, 'GILMER OSWALDO', 'RODRIGUEZ ZEGARRA', '953631498', 'gilmer_rodriguez38@hotmail.com', 'Soy de Ilo-Moquegua, y estamos muy interesados (03 adultos) en  conocer el complejo de el brujo y todo lo que significa, vi que el tour que ofrecen esta al alcance de mi familia por lo que deseo saber mas del servicio que brinda la fundación Wiese considerando que estaré en Trujillo el día domingo 04 de noviembre, gracias anticipadas.', '1', '2018-11-01 07:42:35', '2018-11-01 07:42:35'),
(57, 1, 'Rusbel Roy', 'Antaya fajardo', '910347183', 'rooice98@gmail.com', 'Buen día, soy estudiante de senati, estoy cursando en 5to, el cual  tengo percances para pagar la mensualidad de los últimos 4 meses, quiero saber si me pueden ayudar, gracias.', '1', '2018-11-10 18:46:16', '2018-11-10 18:46:16'),
(69, 3, 'RONY VAN', 'ZEGARRA CORDOVA', '991926851', 'rony793@hotmail.com', 'COOPERATIVA AGRARIA DE PRODUCTORES SOLIDARIOS AGROSOL\r\nDESEARÍAMOS CONOCER SI ES QUE SE REALIZARA ASESORÍA EN EL PERIODO DE POSTULACION\r\nGRACIAS...', '1', '2018-11-22 21:28:46', '2018-11-22 21:28:46'),
(70, 3, 'Martha', 'Vilchez Mendoza', '979954414', 'Marthavm02@hotmail.com', 'Deseamos desarrollar una idea de desprendimiento para jóvenes con habilidades especiales', '1', '2018-11-26 09:45:39', '2018-11-26 09:45:39'),
(71, 1, 'Tatiana Aylinne', 'Palomino Cumpa', '980255642', 'tatianaaylinnepalominocumpa@gmail.com', 'Quisiera informacion sobre los beneficios del programa.', '1', '2018-11-28 21:22:38', '2018-11-28 21:22:38'),
(72, 3, 'Juan Carlos', 'Morales Sierra', '993726604', 'ajuanca1@hotmail.com', 'Tenemos un proyecto de casa andina sostenible. En Urubamba Cusco.', '1', '2018-11-28 23:37:19', '2018-11-28 23:37:19'),
(73, 3, 'Wilson Jhon', 'Arocutipa Oscamayta', '952641048', 'xistoso182@gamil.com', 'Tengo una idea de negocio nueva quisiera apoyo', '1', '2018-12-05 16:34:46', '2018-12-05 16:34:46'),
(74, 3, 'Marco', 'Calderon', '941037773', 'marcoantonio.calderon@gmail.com', 'Hola! Quisiera saber cuál es el importe del financiamiento y si es que la fundación ingresa como accionista en la empresa beneficiada con el programa. Gcs!', '1', '2018-12-06 16:51:57', '2018-12-06 16:51:57'),
(75, 3, 'Mónica', 'suarez ubillus', '990283145', 'monicasuarez@hotmail.com', 'Buenas tardes, mi nombre es Mónica, soy Gerente General de la empresa Akela Tours, me interesa aplicar al Fondo del Emprendedor con el rubro de turismo pero mi empresa no ha facturado el año pasado mucho dinero, sin embargo pienso que todos los demás requisitos si cumple, hay algo que pueda hacer para poder aplicar? Gracias.', '1', '2018-12-08 02:04:52', '2018-12-08 02:04:52'),
(76, 3, 'Tulio', 'Santoyo Bustamante', '928563810', 'tulio.santiyo@hotmail.com', 'Con mi esposa hemos incursionado en el transporte de personal en empresas agroexportadoras de motupe, Lambayeque. Tenemos 2 buses tipo coaster que compramos usados y queremos renovarlos. Consulto si es factible presentar un proyecto en este rubro. El servicio de transporte es con familias de pobreza y pobreza extrema', '1', '2018-12-08 06:05:30', '2018-12-08 06:05:30'),
(77, 3, 'Ivan', 'Laguna', '991856889', 'ivanl@wellbi.center', 'Tengo un proyecto de desarrollo sostenible que multiplica 10 veces las utilidades anuales de caficultores y al mismo tiempo genera un fondo de pensión al mismo agricultor. Quisiera conversar con ustedes', '1', '2018-12-08 20:22:12', '2018-12-08 20:22:12'),
(78, 3, 'enrique javier', 'ARAGON BARRIOS', '942995454', 'e_aragonbarrios@yahoo.es', 'somos una empresa que se dedica a la exportación de hierbas aromáticas, granos andinos y hongos comestibles, nuestros proveedores son los agricultores de las comunidades de los departamentos de Ancash, Apurímac, Cusco, Ica entre otros, de de alguna manera trabajamos con ellos, el adquirir el producto, les genera ingresos, el organizarlos, darles asistencia tecnica y transferencia tecnologica mejoraria el producto, estariamos trabajando con responsabilidad social empresarial, mi pregunta es, si podemos participar, para poder mejorar la cadena productiva..', '1', '2018-12-08 20:30:02', '2018-12-08 20:30:02'),
(79, 3, 'Liliana', 'Quispe Maurtua', '981705088', 'liliquima@gmail.com', 'Agradecere puedan brindarme informacion y como puedo participar', '1', '2018-12-12 23:54:56', '2018-12-12 23:54:56');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `impacts`
--

CREATE TABLE `impacts` (
  `id` int(10) UNSIGNED NOT NULL,
  `project_direct_id` int(10) UNSIGNED NOT NULL,
  `cantidad` int(11) NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `icono` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `orden` smallint(6) NOT NULL DEFAULT '1',
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `impacts`
--

INSERT INTO `impacts` (`id`, `project_direct_id`, `cantidad`, `label`, `icono`, `orden`, `estado`, `created_at`, `updated_at`) VALUES
(1, 2, 14, 'Escuelas <br> atendidas', 'icon1.png', 1, 1, '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(2, 2, 247, 'Educadores', 'icon2.png', 2, 1, '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(3, 2, 3000, 'Estudiantes', 'icon3.png', 3, 1, '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(4, 1, 147, 'Puestos de <br> trabajo <br> generados', 'icon4.png', 1, 1, '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(5, 1, 415, 'Turistas <br> atendidos', 'icon5.png', 2, 1, '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(6, 1, 26, 'Negocios <br> impulsados', 'icon6.png', 3, 1, '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(7, 3, 160, 'Estudiantes <br>  Beneficiados', 'icon9.png', 1, 1, '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(8, 3, 7, 'Emprendimientos <br>  Capacitados', 'icon8.png', 2, 1, '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(9, 3, 4, 'Emprendimeintos <br> Financiados <br> y Asesorados ', 'icon7.png', 3, 1, '2018-07-23 05:00:00', '2018-07-23 05:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `infocontacto`
--

CREATE TABLE `infocontacto` (
  `id` int(10) NOT NULL,
  `title` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `nombre` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telefono` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email_destino` varchar(245) COLLATE utf8_unicode_ci NOT NULL,
  `estado` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `infocontacto`
--

INSERT INTO `infocontacto` (`id`, `title`, `nombre`, `telefono`, `email`, `email_destino`, `estado`, `created_at`, `updated_at`) VALUES
(1, 'Programa de Créditos Educativos, operado por IPFE', 'Carolina Boza', '965 407 559', 'creditos.fw@ipfe.org.pe', 'info@fundacionwiese.org', '1', NULL, NULL),
(2, 'Ayuda Humanitaria', 'Leticia López', '6114343 anexo 103', 'info@fundacionwiese.org', 'info@fundacionwiese.org', '1', NULL, NULL),
(3, 'Fondo Emprendedor, operado por NESST', 'Javier Gondo', '(51-1) 415-9487', 'fondoemprendedor@fundacionwiese.org', 'fondoemprendedor@fundacionwiese.org', '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `locations`
--

CREATE TABLE `locations` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `orden` smallint(6) NOT NULL DEFAULT '1',
  `estado` smallint(6) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `locations`
--

INSERT INTO `locations` (`id`, `nombre`, `orden`, `estado`, `created_at`, `updated_at`) VALUES
(1, 'Chincha', 1, 1, '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(2, 'Cusco', 2, 0, '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(3, 'Huaraz', 3, 1, '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(4, 'La Libertad', 4, 1, '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(5, '	\r\nLima', 5, 1, '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(6, 'Pachacamac', 6, 1, '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(7, 'Puno', 7, 0, '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(8, 'Tacna', 8, 0, '2018-07-23 05:00:00', '2018-07-23 05:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2018_07_02_170740_create_contacts_table', 1),
('2018_07_02_173047_create_newsletters_table', 1),
('2018_07_23_133635_create_beneficiaries_table', 1),
('2018_07_23_133734_create_locations_table', 1),
('2018_07_23_191123_create_banners_table', 1),
('2018_07_23_204035_create_project_directs_table', 1),
('2018_07_23_204040_create_impacts_table', 1),
('2018_07_23_231140_create_projects_table', 1),
('2018_07_24_001915_create_project_locations_table', 1),
('2018_07_24_001943_create_project_beneficiaries_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `newsletters`
--

CREATE TABLE `newsletters` (
  `id` int(10) UNSIGNED NOT NULL,
  `telefono` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `newsletters`
--

INSERT INTO `newsletters` (`id`, `telefono`, `email`, `created_at`, `updated_at`) VALUES
(1, '45657788', 'prueba@webtilia.com', '2018-07-04 03:50:43', '2018-07-04 03:50:43'),
(2, '926226208', 'marcosmcoronado@hotmail.com', '2018-07-06 00:58:22', '2018-07-06 00:58:22'),
(3, '940143936', 'trixsyasparrinvargas@gmail.com', '2018-07-06 17:31:40', '2018-07-06 17:31:40'),
(4, '981518752', 'nancy.casandra@gmail.com', '2018-07-07 18:34:10', '2018-07-07 18:34:10'),
(5, '963118144', 'anaflores3005@hotmail.com', '2018-07-09 00:54:31', '2018-07-09 00:54:31'),
(6, '948627486', 'edgarzam77@gmail.com', '2018-07-11 02:04:23', '2018-07-11 02:04:23'),
(7, '920726770', '967728@senati.pe', '2018-07-11 21:58:34', '2018-07-11 21:58:34'),
(8, '984218484', 'dhjavierfelixr@hotmail.com', '2018-07-14 03:52:12', '2018-07-14 03:52:12'),
(9, '950403080', 'surfari@speedy.com.pe', '2018-07-14 08:26:17', '2018-07-14 08:26:17'),
(10, '975549983', 'lisbeth.sohe28@gmail.com', '2018-07-16 18:04:36', '2018-07-16 18:04:36'),
(11, '962567242', 'zadithv@yahoo.com', '2018-07-17 01:19:10', '2018-07-17 01:19:10'),
(12, '5672824', 'esthersalguedo@hotmail.com', '2018-07-17 10:27:54', '2018-07-17 10:27:54'),
(13, '999009023', 'pepe.mendez@me.com', '2018-07-18 02:55:31', '2018-07-18 02:55:31'),
(14, '4573779', 'cbakula@embak.com.pe', '2018-07-18 05:51:03', '2018-07-18 05:51:03'),
(15, '997648516', 'gerardom10@gmail.com', '2018-07-18 08:22:56', '2018-07-18 08:22:56'),
(16, '997350625', 'vmarzals@gmail.com', '2018-07-18 19:30:44', '2018-07-18 19:30:44'),
(17, '968239450', 'floraguilar66@hotmail.com', '2018-07-19 15:58:38', '2018-07-19 15:58:38'),
(18, '934381101', 'jccmelec@gmail.com', '2018-07-25 06:46:26', '2018-07-25 06:46:26'),
(19, '942601852', 'bseminario.26@gmail.com', '2018-07-31 06:06:23', '2018-07-31 06:06:23'),
(20, '2620013', 'jalbertzuniga@hotmail.com', '2018-08-04 02:12:29', '2018-08-04 02:12:29'),
(21, '999892401', 'julia.tufinio@gmail.com', '2018-08-04 02:47:53', '2018-08-04 02:47:53'),
(22, '999892401', 'julia.tufinio@gmail.com', '2018-08-04 03:18:29', '2018-08-04 03:18:29'),
(23, '953844687', 'wendyruidias7@hotmail.com', '2018-08-04 06:36:59', '2018-08-04 06:36:59'),
(24, '981829573', 'jhc_007@hotmail.com', '2018-08-07 23:40:59', '2018-08-07 23:40:59'),
(25, '997639776', 'gonzales_1295@hotmail.com', '2018-08-08 02:30:05', '2018-08-08 02:30:05'),
(26, '998667846', 'rosamarielad@gmail.com', '2018-08-09 23:03:32', '2018-08-09 23:03:32'),
(27, '93180028', 'aacunai@uc.cl', '2018-08-17 04:08:32', '2018-08-17 04:08:32'),
(28, '993503140', 'cgatjens@gmail.com', '2018-08-21 21:05:11', '2018-08-21 21:05:11'),
(29, '943810914', 'ggogin18@gmail.com', '2018-08-25 06:05:53', '2018-08-25 06:05:53'),
(30, '928506100', 'lila.melendez.15@gmail.com', '2018-08-29 02:13:54', '2018-08-29 02:13:54'),
(31, '991494255', 'olgergerardo@gmail.com', '2018-09-09 01:52:45', '2018-09-09 01:52:45'),
(32, '991494255', 'olgergerardo@gmail.com', '2018-09-09 02:09:12', '2018-09-09 02:09:12'),
(33, '991494255', 'olgegipa@hotmail.com', '2018-09-09 02:11:04', '2018-09-09 02:11:04'),
(34, '935392471', 'jorgesihuat@hotmail.com', '2018-09-13 09:20:54', '2018-09-13 09:20:54'),
(35, '957586210', 'blossiers@gmail.com', '2018-09-14 03:49:28', '2018-09-14 03:49:28'),
(36, '957586210', 'blossiers@gmail.com', '2018-09-14 20:23:03', '2018-09-14 20:23:03'),
(37, '947635257', 'ataraxia_08@hotmail.com', '2018-09-15 13:01:43', '2018-09-15 13:01:43'),
(38, '993532916', 'florenciapretell_5@hotmail.com', '2018-09-18 04:53:33', '2018-09-18 04:53:33'),
(39, '993532916', 'florenciapretell_5@hotmail.com', '2018-09-18 04:58:17', '2018-09-18 04:58:17'),
(40, '984954935', 'arqlgadurand@gmail.com', '2018-09-20 20:49:55', '2018-09-20 20:49:55'),
(41, '964205137', 'capaccoya@gmail.com', '2018-09-22 10:41:42', '2018-09-22 10:41:42'),
(42, '964205137', 'capaccoya@gmail.com', '2018-09-22 11:03:31', '2018-09-22 11:03:31'),
(43, '938806701', 'camila1234va@gmail.com', '2018-09-27 19:24:54', '2018-09-27 19:24:54'),
(44, '323456543', '32262Fedorek@hotmail.com', '2018-09-29 18:55:01', '2018-09-29 18:55:01'),
(45, '956429732', 'andrea.aliagam@pucp.edu.pe', '2018-09-30 01:57:15', '2018-09-30 01:57:15'),
(46, '3202286', 'chidalgobenavente@gmail.com', '2018-10-02 16:56:58', '2018-10-02 16:56:58'),
(47, '936173017', 'Olgauno4@hotmail.com', '2018-10-06 18:21:12', '2018-10-06 18:21:12'),
(48, '979709816', 'bto2011@gmail.com', '2018-10-13 01:17:36', '2018-10-13 01:17:36'),
(49, '979709816', 'bto2011@gmail.com', '2018-10-13 01:19:37', '2018-10-13 01:19:37'),
(50, '979709816', 'bto2011@gmail.com', '2018-10-13 01:27:09', '2018-10-13 01:27:09'),
(51, '979709816', 'bto2011@gmail.com', '2018-10-15 16:49:35', '2018-10-15 16:49:35'),
(52, '979709816', 'bto2011@gmail.com', '2018-10-15 16:51:59', '2018-10-15 16:51:59'),
(53, '946117536', 'aobregont0203@gmai.com', '2018-10-17 21:08:06', '2018-10-17 21:08:06'),
(54, '962686313', 'edpoj@hotmail.com', '2018-10-18 04:23:45', '2018-10-18 04:23:45'),
(55, '983840343', 'orlandosolis12@gmail.com', '2018-11-06 14:27:38', '2018-11-06 14:27:38'),
(56, '98384343', 'orlandosolis12@gmail.com', '2018-11-06 14:51:35', '2018-11-06 14:51:35'),
(57, '940395985', 'luispascual.91@hotmail.com', '2018-11-07 00:19:26', '2018-11-07 00:19:26'),
(58, '954135965', 'celeste.coronado.26@gmail.com', '2018-11-07 00:27:09', '2018-11-07 00:27:09'),
(59, '927868739', 'more_98_09@hotmail.com', '2018-11-07 09:08:18', '2018-11-07 09:08:18'),
(60, '944592328', 'esthermarita04@gmail.com', '2018-11-07 22:15:02', '2018-11-07 22:15:02'),
(61, '944592328', 'esthermarita04@gmail.com', '2018-11-07 22:16:06', '2018-11-07 22:16:06'),
(62, '995932173', 'ginabardelli@yahoo.es', '2018-11-08 11:49:20', '2018-11-08 11:49:20'),
(63, '959251870', 'mrbq10@gmail.com', '2018-11-08 17:17:34', '2018-11-08 17:17:34'),
(64, '949774403', 'cristian_15_12_3@hotmail.com', '2018-11-09 21:14:45', '2018-11-09 21:14:45'),
(65, '949774403', 'cristian_15_12_3@hotmail.com', '2018-11-09 23:08:28', '2018-11-09 23:08:28'),
(66, '979322404', 'prueba@gmail.com', '2018-11-10 01:00:48', '2018-11-10 01:00:48'),
(67, '974979586', 'milagrosbayona2213@hotmail.com', '2018-11-10 05:38:52', '2018-11-10 05:38:52'),
(68, '974979586', 'milagrosbayona2213@hotmail.com', '2018-11-10 07:07:41', '2018-11-10 07:07:41'),
(69, '959251870', 'mrbq10@gmail.com', '2018-11-10 17:38:14', '2018-11-10 17:38:14'),
(70, '983558697', 'lorito_85_45@hotmail.com', '2018-11-10 20:55:53', '2018-11-10 20:55:53'),
(71, '941130894', 'kevihn_2013@hotmail.com', '2018-11-10 23:06:16', '2018-11-10 23:06:16'),
(72, '945488711', 'gutierrezguerraj@gmail.com', '2018-11-12 18:18:09', '2018-11-12 18:18:09'),
(73, '999945316', 'plchristianre@gmail.com', '2018-11-15 06:34:05', '2018-11-15 06:34:05'),
(74, '968249527', 'driverasanti@gmail.com', '2018-11-18 12:13:20', '2018-11-18 12:13:20'),
(75, '945642777', 'gianmarcomerino2@gmail.com', '2018-11-22 23:32:51', '2018-11-22 23:32:51'),
(76, '6162222', 'jacquelin.olaza@gmail.com', '2018-11-23 21:51:42', '2018-11-23 21:51:42'),
(77, '994301156', 'pruebas@pruebas.com', '2018-11-27 22:29:47', '2018-11-27 22:29:47'),
(78, '999405348', 'opazruiz@gmail.com', '2018-11-27 23:59:54', '2018-11-27 23:59:54'),
(79, '983775228', 'dgalecio@geotecperu.com', '2018-11-28 19:21:23', '2018-11-28 19:21:23'),
(80, '987181482', 'ecama_28@hotmail.com', '2018-11-29 07:36:59', '2018-11-29 07:36:59'),
(81, '993353731', 'gcomercial@begatell.com', '2018-11-29 09:14:11', '2018-11-29 09:14:11'),
(82, '988251581', 'claudioalvinofranco@gmail.com', '2018-11-29 15:11:20', '2018-11-29 15:11:20'),
(83, '961643874', 'jcarlos16zv@hotmail.com', '2018-11-29 16:34:31', '2018-11-29 16:34:31'),
(84, '989372427', 'sete.min@hotmail.com', '2018-11-29 17:41:51', '2018-11-29 17:41:51'),
(85, '986668990', 'lmendiola@esan.edu.pe', '2018-12-12 22:27:40', '2018-12-12 22:27:40');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `postulants`
--

CREATE TABLE `postulants` (
  `id` int(10) UNSIGNED NOT NULL,
  `advertisement_id` int(10) UNSIGNED NOT NULL,
  `nombres` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `apellidos` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefono` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `correo` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `departamento` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `provincia` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `profesion` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cv` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `estado` char(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'A',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `postulants`
--

INSERT INTO `postulants` (`id`, `advertisement_id`, `nombres`, `apellidos`, `telefono`, `correo`, `departamento`, `provincia`, `profesion`, `cv`, `estado`, `created_at`, `updated_at`) VALUES
(1, 1, 'carlos', 'prueba final', '9999999', 'cubillus2003@yahoo.es', 'HUANCAVELICA', 'CASTROVIRREYNA', 'profesion de pruebita', '1535419234-capturas ucal gracias.pdf', 'A', '2018-08-28 05:20:34', '2018-08-28 05:20:34'),
(2, 1, 'carlos', 'UBILLUS', '966369312', 'cubillus2003@yahoo.es', 'CALLAO', 'CALLAO', 'profesion', '1535419512-capturas ucal gracias.pdf', 'A', '2018-08-28 05:25:12', '2018-08-28 05:25:12'),
(3, 1, 'wewewe', 'paredes', '966369312', 'cubillus2003@yahoo.es', 'HUANCAVELICA', 'HUANCAVELICA', 'profesion', '1535419576-capturas ucal gracias.pdf', 'A', '2018-08-28 05:26:16', '2018-08-28 05:26:16'),
(4, 1, 'Pruebas', 'Pruebas', '9874566341', 'pruebas@pruebas.com', 'LIMA', 'BARRANCA', 'pruebas', '1535466831-Conoce a los Ponentes del IV Coloquio Internacional de Arqueología.pdf', 'A', '2018-08-28 18:33:51', '2018-08-28 18:33:51'),
(5, 1, 'carlos', 'ubillus', '56985456', 'correo@correo.com', 'APURIMAC', 'ANTABAMBA', 'profesion', '1535469476-MASTER WWW.FUNDACIONWIESE.ORG - ICL 070718.pdf', 'A', '2018-08-28 19:17:56', '2018-08-28 19:17:56'),
(6, 1, 'Augusto', 'Bazan', '994767617', 'ae.bazan.p@gmail.com', 'LIMA', 'LIMA', 'Arqueólogo', '1535475591-CV_Augusto Bazán ESP(ACTUALIZADO)_total.docx', 'A', '2018-08-28 20:59:51', '2018-08-28 20:59:51'),
(7, 1, 'carlos', 'ubillus', '966369312', 'cubillus2003@yahoo.es', 'CAJAMARCA', 'SAN IGNACIO', 'profesion', '1535484332-NP - TU SOLIDARIDAD ABRIGA.pdf', 'A', '2018-08-28 23:25:32', '2018-08-28 23:25:32'),
(8, 1, 'carlos', 'ubillus', '966369312', 'cubillus2003@yahoo.es', 'AYACUCHO', 'LUCANAS', 'prueba final', '1535484643-- CENTRO VEDA PROJECT.pdf', 'A', '2018-08-28 23:30:43', '2018-08-28 23:30:43'),
(9, 1, 'Rubén Héctor', 'Buitron Picharde', '953707315', 'rubenbuitronpicharde@gmail.com', 'LIMA', 'LIMA', 'Arqueólogo, Especialista en Conservación de Bienes Culturales Muebles.', '1535578114-CV-RUBEN 2018.pdf', 'A', '2018-08-30 01:28:34', '2018-08-30 01:28:34'),
(10, 1, 'yazmin', 'Gomez Casaverde', '980603672', 'yazmingc772@gmail.com', 'LIMA', 'LIMA', 'Arqueologa', '1535727361-Yazmin Gomez_cv sin documentar.pdf', 'A', '2018-08-31 18:56:01', '2018-08-31 18:56:01'),
(11, 1, 'INGRID', 'LASCOSQUE', '944421294', 'iclaudet@fundacionwiese.org', 'LA LIBERTAD', 'CHEPEN', 'administración empresas', '1536078705-NP - TU SOLIDARIDAD ABRIGA.docx', 'A', '2018-09-04 20:31:45', '2018-09-04 20:31:45'),
(12, 1, 'xxxxx', 'xxx', '444444444', 'leticia.lopez.elias@gmail.com', 'LIMA', 'CAJATAMBO', 'xxxx', '1536078714-Reserva de viaje 23 agosto para CINDY ZARATE.pdf', 'A', '2018-09-04 20:31:54', '2018-09-04 20:31:54'),
(13, 1, 'diana silvia', 'fernandez mascco', '956721157', 'gianellafm2006@gmail.com', 'LIMA', 'LIMA', 'arqueologa', '1536094743-cv actual 2018 diana  septiembre 2018.docx', 'A', '2018-09-05 00:59:03', '2018-09-05 00:59:03'),
(14, 1, 'carlos', 'ubillus', '344443333', 'cubillus2003@yahoo.es', 'APURIMAC', 'ANDAHUAYLAS', 'ninguna', '1536104459-estatuto.pdf', 'A', '2018-09-05 03:40:59', '2018-09-05 03:40:59'),
(15, 1, 'Rubén Héctor', 'Buitron Picharde', '953707315', 'rubenbuitronpicharde@gmail.com', 'LIMA', 'LIMA', 'Arqueólogo', '1536241856-CV-RUBEN 2018.pdf', 'A', '2018-09-06 17:50:56', '2018-09-06 17:50:56'),
(16, 1, 'Bryan', 'Nuñez', '991256661', 'bryannuap@gmail.com', 'LIMA', 'LIMA', 'Arqueólogo', '1536967817-hoja de vida bna.pdf', 'A', '2018-09-15 03:30:17', '2018-09-15 03:30:17'),
(17, 1, 'Cecilia Fiorela', 'Aguilar Lopez', '961826841', 'cecilia.aguilar.lopez@gmail.com', 'LIMA', 'LIMA', 'Arqueóloga', '1537050218-CV_Cecilia Aguilar Lopez.pdf', 'A', '2018-09-16 02:23:38', '2018-09-16 02:23:38'),
(18, 1, 'Javier', 'Vásquez Llanos', '991362779', 'haravicus@gmail.com', 'LIMA', 'LIMA', 'Arqueólogo', '1537308642-HojaDeVida_JVLL.pdf', 'A', '2018-09-19 02:10:42', '2018-09-19 02:10:42'),
(19, 1, 'Patricia Endora', 'Quiñonez Cuzcano', '999219800', 'pquinonezc@gmail.com', 'LIMA', 'LIMA', 'Arqueóloga', '1537378324-CV PQuiñonez.pdf', 'A', '2018-09-19 21:32:04', '2018-09-19 21:32:04'),
(20, 1, 'KAREN XIOMARA', 'DURAND CACERES', '984954935', 'arqlgadurand@gmail.com', 'CUSCO', 'CUSCO', 'ARQUEOLOGA', '1537462162-CV-KAREN DURAND-9-18.pdf', 'A', '2018-09-20 20:49:22', '2018-09-20 20:49:22'),
(21, 1, 'Eduardo', 'Alcántara Castro', '3202667', 'alcantaraced@gmail.com', 'LIMA', 'LIMA', 'Comunicador, gestor cultural', '1540693049-cv_Alcantara_Castro.pdf', 'A', '2018-10-28 06:17:29', '2018-10-28 06:17:29');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `programs`
--

CREATE TABLE `programs` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descripcion` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `orden` int(11) NOT NULL,
  `estado` char(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'A',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `programs`
--

INSERT INTO `programs` (`id`, `nombre`, `descripcion`, `orden`, `estado`, `created_at`, `updated_at`) VALUES
(1, 'Proyecto Arqueológico El Brujo', 'Proyecto Arqueológico El Brujo', 1, 'A', '2018-08-27 05:00:00', '2018-08-27 05:00:00'),
(2, 'Calidad Educativa', 'Calidad Educativa', 2, 'A', '2018-08-27 05:00:00', '2018-08-27 05:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `projects`
--

CREATE TABLE `projects` (
  `id` int(10) UNSIGNED NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagen` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `programa` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descripcion` text COLLATE utf8_unicode_ci NOT NULL,
  `orden` smallint(6) NOT NULL DEFAULT '1',
  `estado` smallint(6) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `projects`
--

INSERT INTO `projects` (`id`, `titulo`, `imagen`, `programa`, `link`, `descripcion`, `orden`, `estado`, `created_at`, `updated_at`) VALUES
(2, 'Proyecto Bienal de Venecia (2013 - 2033)', 'participa02.png', NULL, 'http://www.fundacionwiese.org/bienal-de-venecia/', 'Gracias a un acuerdo firmado en 2013 entre La Fundación Wiese y La Fundación Biennale di Venezia, se hizo posible la participación del Perú por 20 años en este prestigioso foro desarrollado en Italia. Desde el 2017, en el marco de una alianza con el Patronato Cultural del Perú, la Fundación Wiese le encarga la organización de las exposiciones que se realizan en Venecia, así como sus réplicas en el Perú. A través de la Bienal de Venecia, la Fundación Wiese busca revalorar el trabajo de los artistas y arquitectos peruanos, para que a través de ellos, se genere bienestar y desarrollo para el país.', 6, 2, '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(3, 'Clínica de Puente Piedra (2008-2017)', 'participa04.png', 'Fondo Salud', NULL, 'Con el objeto promover la implementación de un establecimiento de salud de primer nivel de atención para la prevención y promoción de la salud, la atención integral y el desarrollo social de los habitantes de Lima Norte (Puente Piedra, Carabayllo, Ventanilla, Mi Perú, Ancón y Santa Rosa), la Fundación Wiese edificó una clínica, ubicada en la urbanización El Arenal del distrito de Puente Piedra. En 2017, la Fundación Wiese dispuso de un fondo para el desarrollo de un programa de atención médica  integral, el cual será operado por los actuales propietarios de la clínica y que brindará atenciones subvencionadas a los pacientes más vulnerables de la zona. ', 7, 3, '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(4, 'Desarrollo Sostenible del Turismo en el Corredor de Conchucos (2015-2017)', 'participa05.png', NULL, NULL, 'El propósito de este proyecto fue generar ingresos y empleo, así como incentivar el desarrollo sostenido y responsable del turismo en los distritos de Chavín de Huántar, Acopalca y San Marcos, ubicados en el Corredor de Conchucos, Región Ancash. Este convenio se ejecutó en colaboración con la Compañía Minera Antamina y el Municipio Distrital de Chavín de Huántar.', 9, 3, '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(5, 'Investigación Arqueológica y Conservación del Complejo Arqueológico de Chavín de Huántar (2013-2016)', 'participa06.png', NULL, NULL, 'Durante cuatro años, la Fundación Wiese participó en este proyecto, brindando su apoyo en la administración de los recursos donados por la Compañía Minera Antamina para el desarrollo de investigaciones arqueológicas y conservación en el Complejo Arqueológico de Chavín de Huántar, Región de Ancash.', 10, 3, '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(6, 'Película “Chavín de Huántar, el teatro del más allá” (2014 - 2015)', 'participa07.png', NULL, NULL, 'El objetivo de este documental fue dar a conocer la cultura Chavín al mundo. Cómo se originó hace 3200 años, y la importancia que tuvo su influencia en las culturas posteriores. La Fundación Wiese y Wanda Films co-produjeron este largometraje que fue difundido en varias salas locales de cine y a través de la señal de National Geographic, a nivel regional.', 12, 3, '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(7, 'Crédito de Estudios Senati  (2018 – 2021)', 'participa09.jpg', 'Fondo Solidario', 'https://www.fundacionwiese.org/fondo-solidario/creditos-educativos', 'Este proyecto es el primero en ser implementado en el marco del Fondo Wiese Educación con el propósito de financiar los estudios técnicos de aproximadamente 160 alumnos del SENATI – Servicio Nacional de Adiestramiento en Trabajo Industrial –, en cualquiera de sus 80 sedes en el territorio nacional, durante los próximos 3 años. El objetivo de este programa es permitir que alumnos talentosos, con riesgo de deserción económica, puedan culminar satisfactoriamente sus estudios, contribuyendo de esta forma a mejorar su calidad de vida y la de sus familias.', 2, 1, '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(8, 'OXI - El Brujo (2018 – 2020)', 'participa10.jpg', 'Programa El Brujo', NULL, 'Se trata del primer proyecto de Obras por Impuestos (OXI) del Sector Cultura, en el Perú; es también el primero a ser co-ejecutado por una entidad nacional (Ministerio de Cultura) y una sub-nacional (Gobierno Nacional de La Libertad). Este proyecto será financiado por Inmuebles Panamericana S.A., quién ha encargado su ejecución al equipo técnico de la Fundación Wiese que conduce el Programa Arqueológico El Brujo.El objetivo principal del proyecto es incrementar la oferta turística del complejo, a través de la excavación arqueológica, investigación y conservación de 3 nuevas zonas dentro de su área intangible, así como de la difusión y promoción turística, basada en los hallazgos del proyecto.', 2, 1, '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(9, 'Juntos por la Costa Norte  (2017)', 'participa03.png', 'Campañas de auxilio ante desastres', NULL, 'Ante los devastadores efectos de los huaycos e inundaciones ocurridos en el primer trimestre de 2017, como consecuencia del Fenómeno del Niño, la Fundación Wiese tomó acción inmediata, articulando y gestionando una red de ayuda humanitaria en alianza con diversas instituciones públicas y privadas, con el objetivo de aliviar el sufrimiento de los damnificados más vulnerables en las regiones de Piura, Lambayeque y La Libertad. Esta campaña recibió el nombre de “Juntos por la Costa Norte”, y a través de ella se logró atender oportuna y eficazmente a 15,620 familias.', 3, 3, '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(10, 'El Rostro de la Señora de Cao (2016 – 2017)', 'participa11.jpg', 'Programa El Brujo', NULL, 'La tecnología de escaneo láser 3D y el concurso de un equipo multidisciplinario de investigadores de renombre mundial se unieron bajo el liderazgo de los profesionales del Programa Arqueológico El Brujo, para reconstruir la rostro de la enigmática momia, conocida como la Señora de Cao. Se cree que este personaje fue una poderosa líder femenina de la Civilización Moche que vivió hace más de 1700 años. El 4 de julio de 2017, se develaron al mundo los resultados de este proyecto de investigación, sin precedentes en el Perú. Por su relevancia, la noticia rápidamente dio la vuelta al mundo.', 4, 3, '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(11, 'Mejor clima escolar, mejores aprendizajes (2016 – 2020)', 'participa12.jpg', 'Programa Calidad Educativa\r\n', NULL, 'En el marco de su Programa de Calidad Educativa, en abril de 2016, la Fundación Wiese inició el proyecto “Mejor clima escolar, mejores aprendizajes”. Este proyecto trabaja principalmente con directores, subdirectores y docentes de 14 escuelas públicas de Lurín y Pachacamac, y tiene como objetivo principal mejorar significativamente el clima institucional y del aula para favorecer el desarrollo de mejores enseñanzas y aprendizajes.', 5, 2, '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(12, 'Calidad Educativa – Primera Etapa (2009 – 2016)', 'participa13.jpg', '	\r\nPrograma Calidad Educativa', NULL, 'Proyecto ejecutado en el marco de dos convenios de colaboración suscritos con la Dirección Regional de Educación de Ica (DREI) y la Dirección Regional de Educación de Lima Metropolitana (DRELM), con el objeto de contribuir a mejorar las técnicas pedagógicas de matemáticas, comprensión lectora y personal social de docentes de escuelas públicas, en los distritos de Alto Larán, Chincha Baja, Pachacamác y Lurín.', 8, 2, '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(13, 'Crea+ (2014 – 2016)', 'participa14.jpg', NULL, NULL, 'Crea+ es el primer Voluntariado profesional en educación del Perú que promueve la Responsabilidad Social Individual entre los jóvenes, quiénes donan su tiempo para buscar que más niños y adolescentes crean en sí mismos para lograr sus sueños;  a través de un modelo que educa en valores, desarrolla capacidades y potencia talentos en escuelas de zonas vulnerables del Perú. La Fundación Wiese patrocinó las etapas tempranas de este proyecto, haciendo posible su consolidación posterior.', 11, 2, '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(14, 'Fondo Emprendedor (2018)', 'participa15.jpeg', 'Fondo Solidario', 'https://www.fundacionwiese.org/fondo-solidario/fondo-emprendedor', 'El Fondo Solidario para el Emprendimiento Social (FONDO EMPRENDEDOR) de la Fundación Wiese es una plataforma de Inversión de Impacto que busca identificar e impulsar empresas debidamente constituidas en el Perú que (1) hayan sido creadas con la misión de atender un problema social relevante para las comunidades más vulnerables, a través de sus operaciones comerciales, (2) se encuentren en las etapas tempranas de su operación, es decir previas a su escalamiento comercial, (3) puedan acreditar alguna evidencia de éxito de su modelo de negocio, en términos de potencial de crecimiento económico, autosostenibilidad e impacto social.', 1, 1, '2018-12-04 05:00:00', '2018-12-04 05:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `project_beneficiaries`
--

CREATE TABLE `project_beneficiaries` (
  `id` int(10) UNSIGNED NOT NULL,
  `project_id` int(10) UNSIGNED NOT NULL,
  `beneficiary_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `project_beneficiaries`
--

INSERT INTO `project_beneficiaries` (`id`, `project_id`, `beneficiary_id`, `created_at`, `updated_at`) VALUES
(1, 9, 4, '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(2, 9, 7, '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(3, 9, 1, '2018-07-26 05:00:00', '2018-07-26 05:00:00'),
(4, 11, 1, '2018-07-26 05:00:00', '2018-07-26 05:00:00'),
(5, 3, 1, '2018-07-26 05:00:00', '2018-07-26 05:00:00'),
(6, 12, 1, '2018-07-26 05:00:00', '2018-07-26 05:00:00'),
(7, 13, 1, '2018-07-26 05:00:00', '2018-07-26 05:00:00'),
(8, 7, 2, '2018-07-26 05:00:00', '2018-07-26 05:00:00'),
(9, 9, 2, '2018-07-26 05:00:00', '2018-07-26 05:00:00'),
(10, 3, 2, '2018-07-26 05:00:00', '2018-07-26 05:00:00'),
(11, 13, 2, '2018-07-26 05:00:00', '2018-07-26 05:00:00'),
(12, 9, 3, '2018-07-26 05:00:00', '2018-07-26 05:00:00'),
(13, 3, 4, '2018-07-26 05:00:00', '2018-07-26 05:00:00'),
(14, 8, 5, '2018-07-26 05:00:00', '2018-07-26 05:00:00'),
(15, 10, 5, '2018-07-26 05:00:00', '2018-07-26 05:00:00'),
(16, 5, 5, '2018-07-26 05:00:00', '2018-07-26 05:00:00'),
(17, 6, 5, '2018-07-26 05:00:00', '2018-07-26 05:00:00'),
(18, 2, 6, '2018-07-26 05:00:00', '2018-07-26 05:00:00'),
(19, 4, 6, '2018-07-26 05:00:00', '2018-07-26 05:00:00'),
(20, 3, 7, '2018-07-26 05:00:00', '2018-07-26 05:00:00'),
(21, 4, 7, '2018-07-26 05:00:00', '2018-07-26 05:00:00'),
(22, 3, 3, '2018-07-26 05:00:00', '2018-07-26 05:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `project_directs`
--

CREATE TABLE `project_directs` (
  `id` int(10) UNSIGNED NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion_general` text COLLATE utf8_unicode_ci NOT NULL,
  `descripcion_1` text COLLATE utf8_unicode_ci NOT NULL,
  `imagen1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion_2` text COLLATE utf8_unicode_ci NOT NULL,
  `imagen2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `equipo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fondo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `impacto_numero` int(11) DEFAULT NULL,
  `impacto_label` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `project_directs`
--

INSERT INTO `project_directs` (`id`, `titulo`, `descripcion_general`, `descripcion_1`, `imagen1`, `descripcion_2`, `imagen2`, `equipo`, `fondo`, `impacto_numero`, `impacto_label`, `created_at`, `updated_at`) VALUES
(1, 'Complejo Arqueológico El Brujo', 'El Brujo, un destino fascinante para todos sus visitantes y fuente de nuevos conocimientos para la humanidad.', '<p>En el marco de sucesivos Convenios de Cooperación suscritos con el Ministerio de Cultura, la Fundación Wiese  trabaja por la investigación, conservación, puesta en valor y difusión del Complejo Arqueológico El Brujo.  <span class=\"complejo__descripcion--bold-lila\">Actuamos allí, inspirados por el amor al Perú</span> y en la pasión de Don Guillermo “Pancho” Wiese, quien inició este proyecto en agosto de 1990. </p>', 'img1.png', '<ul>\r\n  			  				<li class=\"complejo__descripcion-item\"><span class=\"complejo__descripcion--bold-naranja\">Impulsar nuevos proyectos de investigación científica</span> en colaboración con organizaciones, instituciones y aliados estratégicos.</li>\r\n  			  				<li class=\"complejo__descripcion-item\"><span class=\"complejo__descripcion--bold-naranja\">Garantizar el acceso</span> de públicos diversos al conocimiento generado.</li>\r\n  			  				<li class=\"complejo__descripcion-item\"><span class=\"complejo__descripcion--bold-naranja\">Integrar</span> a las comunidades aledañas al destino turístico.</li>\r\n  			  			</ul>', 'img2.png', 'img3.png', 'fondo.jpg', 415, '<span class=\"beneficiados__numero-letra\">\r\n          				<span class=\"beneficiados__numero-letra beneficiados__numero-letra--ceros\">000</span>\r\n          				<br>PERSONAS<br>IMPACTADAS\r\n          			</span', '2018-07-23 05:00:00', NULL),
(2, 'Calidad Educativa \"Mejor clima escolar,<br> mejores aprendizajes\"', '\"El buen clima en el aula permite que los estudiantes generen opciones creativas para resolver problemas, presentar posiciones de maneras asertivas, escuchar activamente a los demás… Y poner en práctica su pensamiento crítico frente a lo que pasa en su contexto cercano.\" <br>\n                <span style=\"float: right\">(Enrique Chaux)</span>', '<p>La Fundación Wiese trabaja con directores, subdirectores y docentes de escuelas públicas con el objetivo de mejorar significativamente el clima institucional y de aula para favorecer el desarrollo de mejores enseñanzas y aprendizajes.</p>', 'educativo1.jpg', '<ul>\n                <li class=\"complejo__descripcion-item\"><span class=\"complejo__descripcion--bold-naranja\">Mejorar el clima del aula</span>, desde el punto de vista de los estudiantes.</li>\n                <li class=\"complejo__descripcion-item\"><span class=\"complejo__descripcion--bold-naranja\">Mejorar las habilidades socioemocionales</span>, desde el punto de vista de los docentes.</li>\n                <li class=\"complejo__descripcion-item\"><span class=\"complejo__descripcion--bold-naranja\">Disminución de episodios de la violencia escolar.</span></li>\n                <li class=\"complejo__descripcion-item\"><span class=\"complejo__descripcion--bold-naranja\">Equidad de género en los aprendizajes y en las relaciones.</li>\n                </ul>', 'educativo2.jpg', 'educativo3.jpg', 'fondo2.jpg', 3261, '<span class=\"beneficiados__numero-letra\">\n                <br>NIÑOS Y EDUCADORES <br> IMPACTADOS</span>', '2018-07-23 05:00:00', NULL),
(3, 'El Fondo Wiese', '\"Una plataforma que nos permite dirigir programas operados por terceros.\"', '<p>El Fondo Wiese es la plataforma creada por la Fundación Wiese con el objetivo de ampliar su impacto social, a través de la dirección y patrocinio de programas operados por terceros.</p>\n                <p>El Fondo Wiese se divide en tres categorías:</p>\n                <ul>\n                  <li >Fondo Wiese Educación (operado por IPFE)</li>\n                  <li>Fondo Wiese Emprendedor (operado por NESsT)</li>\n                  <li style=\"color:#c1c1c1;\">Fondo Wiese Salud (próximamente)</li>\n                </ul>', 'fondo_wiese1.jpg', '<ul>\n  			  				<li class=\"complejo__descripcion-item\"><span class=\"complejo__descripcion--bold-naranja\">El Fondo Wiese Educación</span><br> Financiar programas de crédito educativo para peruanos de comunidades vulnerables.</li>\n  			  				<li class=\"complejo__descripcion-item\"><span class=\"complejo__descripcion--bold-naranja\">El Fondo Wiese Emprendedor</span><br>Impulsar emprendimientos con claro potencial de autosostenibilidad, cuya misión sea la de atender un problema social relevante del país.</li>\n  			  			</ul>', 'fondo_wiese2.jpg', 'fondo_wiese3.jpg', 'fondo3.jpg', NULL, NULL, '2018-07-23 05:00:00', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `project_locations`
--

CREATE TABLE `project_locations` (
  `id` int(10) UNSIGNED NOT NULL,
  `project_id` int(10) UNSIGNED NOT NULL,
  `location_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `project_locations`
--

INSERT INTO `project_locations` (`id`, `project_id`, `location_id`, `created_at`, `updated_at`) VALUES
(4, 3, 5, '2018-07-26 05:00:00', '2018-07-26 05:00:00'),
(5, 7, 5, '2018-07-26 05:00:00', '2018-07-26 05:00:00'),
(6, 13, 5, '2018-07-26 05:00:00', '2018-07-26 05:00:00'),
(7, 2, 5, '2018-07-26 05:00:00', '2018-07-26 05:00:00'),
(8, 9, 4, '2018-07-26 05:00:00', '2018-07-26 05:00:00'),
(9, 8, 4, '2018-07-26 05:00:00', '2018-07-26 05:00:00'),
(10, 10, 4, '2018-07-26 05:00:00', '2018-07-26 05:00:00'),
(11, 11, 6, NULL, NULL),
(12, 5, 3, '2018-07-26 05:00:00', '2018-07-26 05:00:00'),
(13, 6, 3, '2018-07-26 05:00:00', '2018-07-26 05:00:00'),
(14, 4, 3, '2018-07-26 05:00:00', '2018-07-26 05:00:00'),
(15, 12, 1, '2018-07-26 05:00:00', '2018-07-26 05:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `zones`
--

CREATE TABLE `zones` (
  `id` int(10) UNSIGNED NOT NULL,
  `ubigeo` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `distrito` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `provincia` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `departamento` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `poblacion` smallint(6) NOT NULL,
  `area` smallint(6) NOT NULL,
  `estado` char(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'A',
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `zones`
--

INSERT INTO `zones` (`id`, `ubigeo`, `distrito`, `provincia`, `departamento`, `poblacion`, `area`, `estado`, `email`, `created_at`, `updated_at`) VALUES
(1, '010101', 'CHACHAPOYAS', 'CHACHAPOYAS', 'AMAZONAS', 28731, 135, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(2, '010102', 'ASUNCION', 'CHACHAPOYAS', 'AMAZONAS', 288, 27, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(3, '010103', 'BALSAS', 'CHACHAPOYAS', 'AMAZONAS', 1625, 326, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(4, '010104', 'CHETO', 'CHACHAPOYAS', 'AMAZONAS', 591, 76, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(5, '010105', 'CHILIQUIN', 'CHACHAPOYAS', 'AMAZONAS', 711, 141, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(6, '010106', 'CHUQUIBAMBA', 'CHACHAPOYAS', 'AMAZONAS', 2069, 176, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(7, '010107', 'GRANADA', 'CHACHAPOYAS', 'AMAZONAS', 385, 170, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(8, '010108', 'HUANCAS', 'CHACHAPOYAS', 'AMAZONAS', 1305, 48, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(9, '010109', 'LA JALCA', 'CHACHAPOYAS', 'AMAZONAS', 5505, 127, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(10, '010110', 'LEIMEBAMBA', 'CHACHAPOYAS', 'AMAZONAS', 4190, 462, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(11, '010111', 'LEVANTO', 'CHACHAPOYAS', 'AMAZONAS', 873, 93, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(12, '010112', 'MAGDALENA', 'CHACHAPOYAS', 'AMAZONAS', 795, 134, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(13, '010113', 'MARISCAL CASTILLA', 'CHACHAPOYAS', 'AMAZONAS', 1006, 99, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(14, '010114', 'MOLINOPAMPA', 'CHACHAPOYAS', 'AMAZONAS', 2740, 344, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(15, '010115', 'MONTEVIDEO', 'CHACHAPOYAS', 'AMAZONAS', 589, 81, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(16, '010116', 'OLLEROS', 'CHACHAPOYAS', 'AMAZONAS', 362, 122, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(17, '010117', 'QUINJALCA', 'CHACHAPOYAS', 'AMAZONAS', 843, 89, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(18, '010118', 'SAN FRANCISCO DE DAGUAS', 'CHACHAPOYAS', 'AMAZONAS', 349, 47, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(19, '010119', 'SAN ISIDRO DE MAINO', 'CHACHAPOYAS', 'AMAZONAS', 706, 107, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(20, '010120', 'SOLOCO', 'CHACHAPOYAS', 'AMAZONAS', 1318, 77, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(21, '010121', 'SONCHE', 'CHACHAPOYAS', 'AMAZONAS', 220, 117, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(22, '010201', 'BAGUA', 'BAGUA', 'AMAZONAS', 25965, 75, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(23, '010202', 'ARAMANGO', 'BAGUA', 'AMAZONAS', 11032, 839, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(24, '010203', 'COPALLIN', 'BAGUA', 'AMAZONAS', 6328, 106, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(25, '010204', 'EL PARCO', 'BAGUA', 'AMAZONAS', 1476, 14, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(26, '010205', 'IMAZA', 'BAGUA', 'AMAZONAS', 24114, 4672, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(27, '010206', 'LA PECA', 'BAGUA', 'AMAZONAS', 8006, 183, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(28, '010301', 'JUMBILLA', 'BONGARA', 'AMAZONAS', 1748, 161, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(29, '010302', 'CHISQUILLA', 'BONGARA', 'AMAZONAS', 336, 177, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(30, '010303', 'CHURUJA', 'BONGARA', 'AMAZONAS', 269, 31, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(31, '010304', 'COROSHA', 'BONGARA', 'AMAZONAS', 1025, 47, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(32, '010305', 'CUISPES', 'BONGARA', 'AMAZONAS', 895, 104, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(33, '010306', 'FLORIDA', 'BONGARA', 'AMAZONAS', 8493, 196, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(34, '010307', 'JAZAN', 'BONGARA', 'AMAZONAS', 9260, 89, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(35, '010308', 'RECTA', 'BONGARA', 'AMAZONAS', 206, 22, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(36, '010309', 'SAN CARLOS', 'BONGARA', 'AMAZONAS', 317, 99, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(37, '010310', 'SHIPASBAMBA', 'BONGARA', 'AMAZONAS', 1786, 130, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(38, '010311', 'VALERA', 'BONGARA', 'AMAZONAS', 1281, 88, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(39, '010312', 'YAMBRASBAMBA', 'BONGARA', 'AMAZONAS', 8304, 1686, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(40, '010401', 'NIEVA', 'CONDORCANQUI', 'AMAZONAS', 28726, 4436, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(41, '010402', 'EL CENEPA', 'CONDORCANQUI', 'AMAZONAS', 9537, 5405, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(42, '010403', 'RIO SANTIAGO', 'CONDORCANQUI', 'AMAZONAS', 16686, 8107, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(43, '010501', 'LAMUD', 'LUYA', 'AMAZONAS', 2300, 72, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(44, '010502', 'CAMPORREDONDO', 'LUYA', 'AMAZONAS', 7048, 378, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(45, '010503', 'COCABAMBA', 'LUYA', 'AMAZONAS', 2498, 336, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(46, '010504', 'COLCAMAR', 'LUYA', 'AMAZONAS', 2284, 110, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(47, '010505', 'CONILA', 'LUYA', 'AMAZONAS', 2083, 228, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(48, '010506', 'INGUILPATA', 'LUYA', 'AMAZONAS', 603, 123, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(49, '010507', 'LONGUITA', 'LUYA', 'AMAZONAS', 1146, 64, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(50, '010508', 'LONYA CHICO', 'LUYA', 'AMAZONAS', 975, 57, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(51, '010509', 'LUYA', 'LUYA', 'AMAZONAS', 4404, 99, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(52, '010510', 'LUYA VIEJO', 'LUYA', 'AMAZONAS', 483, 61, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(53, '010511', 'MARIA', 'LUYA', 'AMAZONAS', 940, 83, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(54, '010512', 'OCALLI', 'LUYA', 'AMAZONAS', 4211, 171, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(55, '010513', 'OCUMAL', 'LUYA', 'AMAZONAS', 4164, 369, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(56, '010514', 'PISUQUIA', 'LUYA', 'AMAZONAS', 6081, 309, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(57, '010515', 'PROVIDENCIA', 'LUYA', 'AMAZONAS', 1533, 117, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(58, '010516', 'SAN CRISTOBAL', 'LUYA', 'AMAZONAS', 690, 37, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(59, '010517', 'SAN FRANCISCO DEL YESO', 'LUYA', 'AMAZONAS', 820, 110, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(60, '010518', 'SAN JERONIMO', 'LUYA', 'AMAZONAS', 890, 218, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(61, '010519', 'SAN JUAN DE LOPECANCHA', 'LUYA', 'AMAZONAS', 513, 83, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(62, '010520', 'SANTA CATALINA', 'LUYA', 'AMAZONAS', 1893, 126, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(63, '010521', 'SANTO TOMAS', 'LUYA', 'AMAZONAS', 3562, 93, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(64, '010522', 'TINGO', 'LUYA', 'AMAZONAS', 1355, 105, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(65, '010523', 'TRITA', 'LUYA', 'AMAZONAS', 1373, 12, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(66, '010601', 'SAN NICOLAS', 'RODRIGUEZ DE MENDOZA', 'AMAZONAS', 5224, 105, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(67, '010602', 'CHIRIMOTO', 'RODRIGUEZ DE MENDOZA', 'AMAZONAS', 2052, 161, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(68, '010603', 'COCHAMAL', 'RODRIGUEZ DE MENDOZA', 'AMAZONAS', 506, 213, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(69, '010604', 'HUAMBO', 'RODRIGUEZ DE MENDOZA', 'AMAZONAS', 2598, 88, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(70, '010605', 'LIMABAMBA', 'RODRIGUEZ DE MENDOZA', 'AMAZONAS', 3002, 645, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(71, '010606', 'LONGAR', 'RODRIGUEZ DE MENDOZA', 'AMAZONAS', 1624, 50, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(72, '010607', 'MARISCAL BENAVIDES', 'RODRIGUEZ DE MENDOZA', 'AMAZONAS', 1381, 186, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(73, '010608', 'MILPUC', 'RODRIGUEZ DE MENDOZA', 'AMAZONAS', 604, 45, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(74, '010609', 'OMIA', 'RODRIGUEZ DE MENDOZA', 'AMAZONAS', 9562, 223, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(75, '010610', 'SANTA ROSA', 'RODRIGUEZ DE MENDOZA', 'AMAZONAS', 464, 19, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(76, '010611', 'TOTORA', 'RODRIGUEZ DE MENDOZA', 'AMAZONAS', 450, 20, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(77, '010612', 'VISTA ALEGRE', 'RODRIGUEZ DE MENDOZA', 'AMAZONAS', 3725, 927, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(78, '010701', 'BAGUA GRANDE', 'UTCUBAMBA', 'AMAZONAS', 32767, 717, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(79, '010702', 'CAJARURO', 'UTCUBAMBA', 'AMAZONAS', 28403, 1797, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(80, '010703', 'CUMBA', 'UTCUBAMBA', 'AMAZONAS', 8815, 300, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(81, '010704', 'EL MILAGRO', 'UTCUBAMBA', 'AMAZONAS', 6369, 310, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(82, '010705', 'JAMALCA', 'UTCUBAMBA', 'AMAZONAS', 8219, 387, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(83, '010706', 'LONYA GRANDE', 'UTCUBAMBA', 'AMAZONAS', 10377, 216, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(84, '010707', 'YAMON', 'UTCUBAMBA', 'AMAZONAS', 2877, 143, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(85, '020101', 'HUARAZ', 'HUARAZ', 'ANCASH', 32767, 423, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(86, '020102', 'COCHABAMBA', 'HUARAZ', 'ANCASH', 1989, 138, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(87, '020103', 'COLCABAMBA', 'HUARAZ', 'ANCASH', 800, 52, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(88, '020104', 'HUANCHAY', 'HUARAZ', 'ANCASH', 2283, 208, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(89, '020105', 'INDEPENDENCIA', 'HUARAZ', 'ANCASH', 32767, 346, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(90, '020106', 'JANGAS', 'HUARAZ', 'ANCASH', 4988, 62, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(91, '020107', 'LA LIBERTAD', 'HUARAZ', 'ANCASH', 1162, 160, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(92, '020108', 'OLLEROS', 'HUARAZ', 'ANCASH', 2230, 230, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(93, '020109', 'PAMPAS', 'HUARAZ', 'ANCASH', 1190, 351, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(94, '020110', 'PARIACOTO', 'HUARAZ', 'ANCASH', 4718, 168, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(95, '020111', 'PIRA', 'HUARAZ', 'ANCASH', 3763, 248, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(96, '020112', 'TARICA', 'HUARAZ', 'ANCASH', 5837, 116, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(97, '020201', 'AIJA', 'AIJA', 'ANCASH', 1873, 163, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(98, '020202', 'CORIS', 'AIJA', 'ANCASH', 2239, 259, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(99, '020203', 'HUACLLAN', 'AIJA', 'ANCASH', 616, 40, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(100, '020204', 'LA MERCED', 'AIJA', 'ANCASH', 2220, 158, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(101, '020205', 'SUCCHA', 'AIJA', 'ANCASH', 841, 78, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(102, '020301', 'LLAMELLIN', 'ANTONIO RAYMONDI', 'ANCASH', 3591, 95, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(103, '020302', 'ACZO', 'ANTONIO RAYMONDI', 'ANCASH', 2176, 68, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(104, '020303', 'CHACCHO', 'ANTONIO RAYMONDI', 'ANCASH', 1718, 73, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(105, '020304', 'CHINGAS', 'ANTONIO RAYMONDI', 'ANCASH', 1936, 50, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(106, '020305', 'MIRGAS', 'ANTONIO RAYMONDI', 'ANCASH', 5338, 172, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(107, '020306', 'SAN JUAN DE RONTOY', 'ANTONIO RAYMONDI', 'ANCASH', 1642, 103, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(108, '020401', 'CHACAS', 'ASUNCION', 'ANCASH', 5573, 449, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(109, '020402', 'ACOCHACA', 'ASUNCION', 'ANCASH', 3222, 76, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(110, '020501', 'CHIQUIAN', 'BOLOGNESI', 'ANCASH', 3641, 187, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(111, '020502', 'ABELARDO PARDO LEZAMETA', 'BOLOGNESI', 'ANCASH', 1208, 11, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(112, '020503', 'ANTONIO RAYMONDI', 'BOLOGNESI', 'ANCASH', 1078, 121, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(113, '020504', 'AQUIA', 'BOLOGNESI', 'ANCASH', 2513, 435, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(114, '020505', 'CAJACAY', 'BOLOGNESI', 'ANCASH', 1602, 193, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(115, '020506', 'CANIS', 'BOLOGNESI', 'ANCASH', 1250, 20, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(116, '020507', 'COLQUIOC', 'BOLOGNESI', 'ANCASH', 4002, 274, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(117, '020508', 'HUALLANCA', 'BOLOGNESI', 'ANCASH', 8220, 817, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(118, '020509', 'HUASTA', 'BOLOGNESI', 'ANCASH', 2571, 395, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(119, '020510', 'HUAYLLACAYAN', 'BOLOGNESI', 'ANCASH', 1104, 126, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(120, '020511', 'LA PRIMAVERA', 'BOLOGNESI', 'ANCASH', 911, 64, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(121, '020512', 'MANGAS', 'BOLOGNESI', 'ANCASH', 567, 127, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(122, '020513', 'PACLLON', 'BOLOGNESI', 'ANCASH', 1722, 210, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(123, '020514', 'SAN MIGUEL DE CORPANQUI', 'BOLOGNESI', 'ANCASH', 1241, 44, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(124, '020515', 'TICLLOS', 'BOLOGNESI', 'ANCASH', 1243, 92, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(125, '020601', 'CARHUAZ', 'CARHUAZ', 'ANCASH', 15373, 198, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(126, '020602', 'ACOPAMPA', 'CARHUAZ', 'ANCASH', 2655, 15, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(127, '020603', 'AMASHCA', 'CARHUAZ', 'ANCASH', 1581, 13, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(128, '020604', 'ANTA', 'CARHUAZ', 'ANCASH', 2494, 43, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(129, '020605', 'ATAQUERO', 'CARHUAZ', 'ANCASH', 1377, 45, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(130, '020606', 'MARCARA', 'CARHUAZ', 'ANCASH', 9304, 176, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(131, '020607', 'PARIAHUANCA', 'CARHUAZ', 'ANCASH', 1609, 12, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(132, '020608', 'SAN MIGUEL DE ACO', 'CARHUAZ', 'ANCASH', 2750, 123, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(133, '020609', 'SHILLA', 'CARHUAZ', 'ANCASH', 3306, 128, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(134, '020610', 'TINCO', 'CARHUAZ', 'ANCASH', 3240, 16, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(135, '020611', 'YUNGAR', 'CARHUAZ', 'ANCASH', 3408, 45, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(136, '020701', 'SAN LUIS', 'CARLOS FERMIN FITZCA', 'ANCASH', 12566, 248, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(137, '020702', 'SAN NICOLAS', 'CARLOS FERMIN FITZCA', 'ANCASH', 3738, 204, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(138, '020703', 'YAUYA', 'CARLOS FERMIN FITZCA', 'ANCASH', 5527, 175, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(139, '020801', 'CASMA', 'CASMA', 'ANCASH', 32767, 1205, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(140, '020802', 'BUENA VISTA ALTA', 'CASMA', 'ANCASH', 4213, 481, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(141, '020803', 'COMANDANTE NOEL', 'CASMA', 'ANCASH', 2058, 218, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(142, '020804', 'YAUTAN', 'CASMA', 'ANCASH', 8383, 365, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(143, '020901', 'CORONGO', 'CORONGO', 'ANCASH', 1487, 151, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(144, '020902', 'ACO', 'CORONGO', 'ANCASH', 460, 58, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(145, '020903', 'BAMBAS', 'CORONGO', 'ANCASH', 537, 154, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(146, '020904', 'CUSCA', 'CORONGO', 'ANCASH', 2941, 426, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(147, '020905', 'LA PAMPA', 'CORONGO', 'ANCASH', 1030, 95, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(148, '020906', 'YANAC', 'CORONGO', 'ANCASH', 708, 47, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(149, '020907', 'YUPAN', 'CORONGO', 'ANCASH', 1002, 85, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(150, '021001', 'HUARI', 'HUARI', 'ANCASH', 10283, 400, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(151, '021002', 'ANRA', 'HUARI', 'ANCASH', 1618, 78, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(152, '021003', 'CAJAY', 'HUARI', 'ANCASH', 2638, 166, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(153, '021004', 'CHAVIN DE HUANTAR', 'HUARI', 'ANCASH', 9221, 418, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(154, '021005', 'HUACACHI', 'HUARI', 'ANCASH', 1876, 90, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(155, '021006', 'HUACCHIS', 'HUARI', 'ANCASH', 2075, 76, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(156, '021007', 'HUACHIS', 'HUARI', 'ANCASH', 3509, 152, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(157, '021008', 'HUANTAR', 'HUARI', 'ANCASH', 3044, 162, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(158, '021009', 'MASIN', 'HUARI', 'ANCASH', 1706, 75, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(159, '021010', 'PAUCAS', 'HUARI', 'ANCASH', 1863, 140, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(160, '021011', 'PONTO', 'HUARI', 'ANCASH', 3349, 119, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(161, '021012', 'RAHUAPAMPA', 'HUARI', 'ANCASH', 802, 10, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(162, '021013', 'RAPAYAN', 'HUARI', 'ANCASH', 1793, 145, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(163, '021014', 'SAN MARCOS', 'HUARI', 'ANCASH', 14781, 564, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(164, '021015', 'SAN PEDRO DE CHANA', 'HUARI', 'ANCASH', 2814, 144, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(165, '021016', 'UCO', 'HUARI', 'ANCASH', 1685, 52, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(166, '021101', 'HUARMEY', 'HUARMEY', 'ANCASH', 24316, 2909, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(167, '021102', 'COCHAPETI', 'HUARMEY', 'ANCASH', 771, 99, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(168, '021103', 'CULEBRAS', 'HUARMEY', 'ANCASH', 3661, 629, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(169, '021104', 'HUAYAN', 'HUARMEY', 'ANCASH', 1065, 112, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(170, '021105', 'MALVAS', 'HUARMEY', 'ANCASH', 931, 168, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(171, '021201', 'CARAZ', 'HUAYLAS', 'ANCASH', 26208, 245, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(172, '021202', 'HUALLANCA', 'HUAYLAS', 'ANCASH', 703, 173, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(173, '021203', 'HUATA', 'HUAYLAS', 'ANCASH', 1638, 70, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(174, '021204', 'HUAYLAS', 'HUAYLAS', 'ANCASH', 1466, 54, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(175, '021205', 'MATO', 'HUAYLAS', 'ANCASH', 2030, 106, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(176, '021206', 'PAMPAROMAS', 'HUAYLAS', 'ANCASH', 9153, 500, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(177, '021207', 'PUEBLO LIBRE', 'HUAYLAS', 'ANCASH', 7186, 128, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(178, '021208', 'SANTA CRUZ', 'HUAYLAS', 'ANCASH', 5164, 359, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(179, '021209', 'SANTO TORIBIO', 'HUAYLAS', 'ANCASH', 1100, 84, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(180, '021210', 'YURACMARCA', 'HUAYLAS', 'ANCASH', 1780, 566, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(181, '021301', 'PISCOBAMBA', 'MARISCAL LUZURIAGA', 'ANCASH', 3774, 46, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(182, '021302', 'CASCA', 'MARISCAL LUZURIAGA', 'ANCASH', 4507, 75, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(183, '021303', 'ELEAZAR GUZMAN BARRON', 'MARISCAL LUZURIAGA', 'ANCASH', 1376, 97, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(184, '021304', 'FIDEL OLIVAS ESCUDERO', 'MARISCAL LUZURIAGA', 'ANCASH', 2249, 213, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(185, '021305', 'LLAMA', 'MARISCAL LUZURIAGA', 'ANCASH', 1254, 49, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(186, '021306', 'LLUMPA', 'MARISCAL LUZURIAGA', 'ANCASH', 6321, 139, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(187, '021307', 'LUCMA', 'MARISCAL LUZURIAGA', 'ANCASH', 3246, 75, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(188, '021308', 'MUSGA', 'MARISCAL LUZURIAGA', 'ANCASH', 1027, 39, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(189, '021401', 'OCROS', 'OCROS', 'ANCASH', 1020, 225, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(190, '021402', 'ACAS', 'OCROS', 'ANCASH', 1024, 256, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(191, '021403', 'CAJAMARQUILLA', 'OCROS', 'ANCASH', 574, 79, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(192, '021404', 'CARHUAPAMPA', 'OCROS', 'ANCASH', 826, 106, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(193, '021405', 'COCHAS', 'OCROS', 'ANCASH', 1442, 415, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(194, '021406', 'CONGAS', 'OCROS', 'ANCASH', 1220, 131, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(195, '021407', 'LLIPA', 'OCROS', 'ANCASH', 1739, 31, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(196, '021408', 'SAN CRISTOBAL DE RAJAN', 'OCROS', 'ANCASH', 624, 70, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(197, '021409', 'SAN PEDRO', 'OCROS', 'ANCASH', 1951, 548, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(198, '021410', 'SANTIAGO DE CHILCAS', 'OCROS', 'ANCASH', 382, 88, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(199, '021501', 'CABANA', 'PALLASCA', 'ANCASH', 2724, 144, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(200, '021502', 'BOLOGNESI', 'PALLASCA', 'ANCASH', 1303, 84, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(201, '021503', 'CONCHUCOS', 'PALLASCA', 'ANCASH', 8359, 578, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(202, '021504', 'HUACASCHUQUE', 'PALLASCA', 'ANCASH', 583, 18, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(203, '021505', 'HUANDOVAL', 'PALLASCA', 'ANCASH', 1124, 109, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(204, '021506', 'LACABAMBA', 'PALLASCA', 'ANCASH', 576, 63, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(205, '021507', 'LLAPO', 'PALLASCA', 'ANCASH', 723, 31, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(206, '021508', 'PALLASCA', 'PALLASCA', 'ANCASH', 2447, 110, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(207, '021509', 'PAMPAS', 'PALLASCA', 'ANCASH', 8502, 437, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(208, '021510', 'SANTA ROSA', 'PALLASCA', 'ANCASH', 1057, 291, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(209, '021511', 'TAUCA', 'PALLASCA', 'ANCASH', 3172, 199, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(210, '021601', 'POMABAMBA', 'POMABAMBA', 'ANCASH', 16294, 345, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(211, '021602', 'HUAYLLAN', 'POMABAMBA', 'ANCASH', 3673, 90, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(212, '021603', 'PAROBAMBA', 'POMABAMBA', 'ANCASH', 6994, 340, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(213, '021604', 'QUINUABAMBA', 'POMABAMBA', 'ANCASH', 2414, 149, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(214, '021701', 'RECUAY', 'RECUAY', 'ANCASH', 4462, 149, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(215, '021702', 'CATAC', 'RECUAY', 'ANCASH', 4020, 1024, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(216, '021703', 'COTAPARACO', 'RECUAY', 'ANCASH', 644, 175, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(217, '021704', 'HUAYLLAPAMPA', 'RECUAY', 'ANCASH', 1305, 110, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(218, '021705', 'LLACLLIN', 'RECUAY', 'ANCASH', 1806, 97, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(219, '021706', 'MARCA', 'RECUAY', 'ANCASH', 980, 183, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(220, '021707', 'PAMPAS CHICO', 'RECUAY', 'ANCASH', 2036, 97, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(221, '021708', 'PARARIN', 'RECUAY', 'ANCASH', 1373, 259, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(222, '021709', 'TAPACOCHA', 'RECUAY', 'ANCASH', 464, 78, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(223, '021710', 'TICAPAMPA', 'RECUAY', 'ANCASH', 2258, 146, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(224, '021801', 'CHIMBOTE', 'SANTA', 'ANCASH', 32767, 1450, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(225, '021802', 'CACERES DEL PERU', 'SANTA', 'ANCASH', 4884, 487, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(226, '021803', 'COISHCO', 'SANTA', 'ANCASH', 15811, 10, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(227, '021804', 'MACATE', 'SANTA', 'ANCASH', 3425, 586, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(228, '021805', 'MORO', 'SANTA', 'ANCASH', 7528, 399, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(229, '021806', 'NEPENA', 'SANTA', 'ANCASH', 15589, 457, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(230, '021807', 'SAMANCO', 'SANTA', 'ANCASH', 4590, 146, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(231, '021808', 'SANTA', 'SANTA', 'ANCASH', 20532, 40, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(232, '021809', 'NUEVO CHIMBOTE', 'SANTA', 'ANCASH', 32767, 397, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(233, '021901', 'SIHUAS', 'SIHUAS', 'ANCASH', 5705, 62, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(234, '021902', 'ACOBAMBA', 'SIHUAS', 'ANCASH', 2183, 151, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(235, '021903', 'ALFONSO UGARTE', 'SIHUAS', 'ANCASH', 783, 90, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(236, '021904', 'CASHAPAMPA', 'SIHUAS', 'ANCASH', 2874, 73, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(237, '021905', 'CHINGALPO', 'SIHUAS', 'ANCASH', 1058, 172, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(238, '021906', 'HUAYLLABAMBA', 'SIHUAS', 'ANCASH', 4014, 258, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(239, '021907', 'QUICHES', 'SIHUAS', 'ANCASH', 2924, 148, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(240, '021908', 'RAGASH', 'SIHUAS', 'ANCASH', 2642, 209, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(241, '021909', 'SAN JUAN', 'SIHUAS', 'ANCASH', 6522, 212, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(242, '021910', 'SICSIBAMBA', 'SIHUAS', 'ANCASH', 1824, 82, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(243, '022001', 'YUNGAY', 'YUNGAY', 'ANCASH', 21911, 275, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(244, '022002', 'CASCAPARA', 'YUNGAY', 'ANCASH', 2287, 137, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(245, '022003', 'MANCOS', 'YUNGAY', 'ANCASH', 6977, 63, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(246, '022004', 'MATACOTO', 'YUNGAY', 'ANCASH', 1635, 47, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(247, '022005', 'QUILLO', 'YUNGAY', 'ANCASH', 13798, 370, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(248, '022006', 'RANRAHIRCA', 'YUNGAY', 'ANCASH', 2707, 22, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(249, '022007', 'SHUPLUY', 'YUNGAY', 'ANCASH', 2399, 166, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(250, '022008', 'YANAMA', 'YUNGAY', 'ANCASH', 6969, 284, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(251, '030101', 'ABANCAY', 'ABANCAY', 'APURIMAC', 32767, 288, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(252, '030102', 'CHACOCHE', 'ABANCAY', 'APURIMAC', 1213, 176, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(253, '030103', 'CIRCA', 'ABANCAY', 'APURIMAC', 2506, 635, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(254, '030104', 'CURAHUASI', 'ABANCAY', 'APURIMAC', 18328, 866, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(255, '030105', 'HUANIPACA', 'ABANCAY', 'APURIMAC', 4749, 427, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(256, '030106', 'LAMBRAMA', 'ABANCAY', 'APURIMAC', 5561, 524, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(257, '030107', 'PICHIRHUA', 'ABANCAY', 'APURIMAC', 4042, 373, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(258, '030108', 'SAN PEDRO DE CACHORA', 'ABANCAY', 'APURIMAC', 3838, 118, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(259, '030109', 'TAMBURCO', 'ABANCAY', 'APURIMAC', 9884, 54, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(260, '030201', 'ANDAHUAYLAS', 'ANDAHUAYLAS', 'APURIMAC', 32767, 376, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(261, '030202', 'ANDARAPA', 'ANDAHUAYLAS', 'APURIMAC', 6380, 212, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(262, '030203', 'CHIARA', 'ANDAHUAYLAS', 'APURIMAC', 1350, 148, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(263, '030204', 'HUANCARAMA', 'ANDAHUAYLAS', 'APURIMAC', 7441, 157, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(264, '030205', 'HUANCARAY', 'ANDAHUAYLAS', 'APURIMAC', 4632, 112, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(265, '030206', 'HUAYANA', 'ANDAHUAYLAS', 'APURIMAC', 1058, 95, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(266, '030207', 'KISHUARA', 'ANDAHUAYLAS', 'APURIMAC', 9282, 310, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(267, '030208', 'PACOBAMBA', 'ANDAHUAYLAS', 'APURIMAC', 4794, 258, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(268, '030209', 'PACUCHA', 'ANDAHUAYLAS', 'APURIMAC', 9994, 176, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(269, '030210', 'PAMPACHIRI', 'ANDAHUAYLAS', 'APURIMAC', 2780, 589, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(270, '030211', 'POMACOCHA', 'ANDAHUAYLAS', 'APURIMAC', 1042, 123, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(271, '030212', 'SAN ANTONIO DE CACHI', 'ANDAHUAYLAS', 'APURIMAC', 3237, 179, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(272, '030213', 'SAN JERONIMO', 'ANDAHUAYLAS', 'APURIMAC', 27665, 244, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(273, '030214', 'SAN MIGUEL DE CHACCRAMPA', 'ANDAHUAYLAS', 'APURIMAC', 2057, 85, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(274, '030215', 'SANTA MARIA DE CHICMO', 'ANDAHUAYLAS', 'APURIMAC', 9910, 157, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(275, '030216', 'TALAVERA', 'ANDAHUAYLAS', 'APURIMAC', 18313, 109, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(276, '030217', 'TUMAY HUARACA', 'ANDAHUAYLAS', 'APURIMAC', 2415, 454, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(277, '030218', 'TURPO', 'ANDAHUAYLAS', 'APURIMAC', 4197, 123, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(278, '030219', 'KAQUIABAMBA', 'ANDAHUAYLAS', 'APURIMAC', 2962, 113, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(279, '030301', 'ANTABAMBA', 'ANTABAMBA', 'APURIMAC', 3164, 603, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(280, '030302', 'EL ORO', 'ANTABAMBA', 'APURIMAC', 552, 67, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(281, '030303', 'HUAQUIRCA', 'ANTABAMBA', 'APURIMAC', 1571, 352, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(282, '030304', 'JUAN ESPINOZA MEDRANO', 'ANTABAMBA', 'APURIMAC', 2066, 624, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(283, '030305', 'OROPESA', 'ANTABAMBA', 'APURIMAC', 3110, 1171, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(284, '030306', 'PACHACONAS', 'ANTABAMBA', 'APURIMAC', 1286, 229, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(285, '030307', 'SABAINO', 'ANTABAMBA', 'APURIMAC', 1648, 176, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(286, '030401', 'CHALHUANCA', 'AYMARAES', 'APURIMAC', 5015, 329, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(287, '030402', 'CAPAYA', 'AYMARAES', 'APURIMAC', 970, 84, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(288, '030403', 'CARAYBAMBA', 'AYMARAES', 'APURIMAC', 1472, 237, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(289, '030404', 'CHAPIMARCA', 'AYMARAES', 'APURIMAC', 2160, 204, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(290, '030405', 'COLCABAMBA', 'AYMARAES', 'APURIMAC', 933, 91, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(291, '030406', 'COTARUSE', 'AYMARAES', 'APURIMAC', 5326, 1737, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(292, '030407', 'HUAYLLO', 'AYMARAES', 'APURIMAC', 728, 73, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(293, '030408', 'JUSTO APU SAHUARAURA', 'AYMARAES', 'APURIMAC', 1292, 101, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(294, '030409', 'LUCRE', 'AYMARAES', 'APURIMAC', 2159, 104, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(295, '030410', 'POCOHUANCA', 'AYMARAES', 'APURIMAC', 1177, 88, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(296, '030411', 'SAN JUAN DE CHACNA', 'AYMARAES', 'APURIMAC', 856, 96, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(297, '030412', 'SANAYCA', 'AYMARAES', 'APURIMAC', 1441, 364, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(298, '030413', 'SORAYA', 'AYMARAES', 'APURIMAC', 825, 45, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(299, '030414', 'TAPAIRIHUA', 'AYMARAES', 'APURIMAC', 2259, 160, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(300, '030415', 'TINTAY', 'AYMARAES', 'APURIMAC', 3227, 143, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(301, '030416', 'TORAYA', 'AYMARAES', 'APURIMAC', 1962, 168, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(302, '030417', 'YANACA', 'AYMARAES', 'APURIMAC', 1193, 104, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(303, '030501', 'TAMBOBAMBA', 'COTABAMBAS', 'APURIMAC', 11582, 715, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(304, '030502', 'COTABAMBAS', 'COTABAMBAS', 'APURIMAC', 4237, 326, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(305, '030503', 'COYLLURQUI', 'COTABAMBAS', 'APURIMAC', 8542, 419, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(306, '030504', 'HAQUIRA', 'COTABAMBAS', 'APURIMAC', 11802, 483, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(307, '030505', 'MARA', 'COTABAMBAS', 'APURIMAC', 6695, 223, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(308, '030506', 'CHALLHUAHUACHO', 'COTABAMBAS', 'APURIMAC', 9908, 455, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(309, '030601', 'CHINCHEROS', 'CHINCHEROS', 'APURIMAC', 6809, 136, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(310, '030602', 'ANCO_HUALLO', 'CHINCHEROS', 'APURIMAC', 12477, 118, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(311, '030603', 'COCHARCAS', 'CHINCHEROS', 'APURIMAC', 2675, 109, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(312, '030604', 'HUACCANA', 'CHINCHEROS', 'APURIMAC', 10327, 479, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(313, '030605', 'OCOBAMBA', 'CHINCHEROS', 'APURIMAC', 8316, 231, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(314, '030606', 'ONGOY', 'CHINCHEROS', 'APURIMAC', 9131, 230, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(315, '030607', 'URANMARCA', 'CHINCHEROS', 'APURIMAC', 3649, 147, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(316, '030608', 'RANRACANCHA', 'CHINCHEROS', 'APURIMAC', 5298, 74, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(317, '030701', 'CHUQUIBAMBILLA', 'GRAU', 'APURIMAC', 5402, 426, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(318, '030702', 'CURPAHUASI', 'GRAU', 'APURIMAC', 2369, 311, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(319, '030703', 'GAMARRA', 'GRAU', 'APURIMAC', 3985, 350, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(320, '030704', 'HUAYLLATI', 'GRAU', 'APURIMAC', 1685, 124, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(321, '030705', 'MAMARA', 'GRAU', 'APURIMAC', 981, 62, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(322, '030706', 'MICAELA BASTIDAS', 'GRAU', 'APURIMAC', 1680, 105, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(323, '030707', 'PATAYPAMPA', 'GRAU', 'APURIMAC', 1121, 147, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(324, '030708', 'PROGRESO', 'GRAU', 'APURIMAC', 3325, 241, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(325, '030709', 'SAN ANTONIO', 'GRAU', 'APURIMAC', 366, 25, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(326, '030710', 'SANTA ROSA', 'GRAU', 'APURIMAC', 716, 31, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(327, '030711', 'TURPAY', 'GRAU', 'APURIMAC', 769, 51, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(328, '030712', 'VILCABAMBA', 'GRAU', 'APURIMAC', 1401, 7, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(329, '030713', 'VIRUNDO', 'GRAU', 'APURIMAC', 1296, 114, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(330, '030714', 'CURASCO', 'GRAU', 'APURIMAC', 1624, 135, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(331, '040101', 'AREQUIPA', 'AREQUIPA', 'AREQUIPA', 32767, 10, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(332, '040102', 'ALTO SELVA ALEGRE', 'AREQUIPA', 'AREQUIPA', 32767, 67, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(333, '040103', 'CAYMA', 'AREQUIPA', 'AREQUIPA', 32767, 236, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(334, '040104', 'CERRO COLORADO', 'AREQUIPA', 'AREQUIPA', 32767, 173, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(335, '040105', 'CHARACATO', 'AREQUIPA', 'AREQUIPA', 9288, 85, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(336, '040106', 'CHIGUATA', 'AREQUIPA', 'AREQUIPA', 2940, 437, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(337, '040107', 'JACOBO HUNTER', 'AREQUIPA', 'AREQUIPA', 32767, 20, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(338, '040108', 'LA JOYA', 'AREQUIPA', 'AREQUIPA', 30233, 698, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(339, '040109', 'MARIANO MELGAR', 'AREQUIPA', 'AREQUIPA', 32767, 29, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(340, '040110', 'MIRAFLORES', 'AREQUIPA', 'AREQUIPA', 32767, 26, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(341, '040111', 'MOLLEBAYA', 'AREQUIPA', 'AREQUIPA', 1868, 31, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(342, '040112', 'PAUCARPATA', 'AREQUIPA', 'AREQUIPA', 32767, 28, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(343, '040113', 'POCSI', 'AREQUIPA', 'AREQUIPA', 547, 167, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(344, '040114', 'POLOBAYA', 'AREQUIPA', 'AREQUIPA', 1477, 399, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(345, '040115', 'QUEQUENA', 'AREQUIPA', 'AREQUIPA', 1376, 35, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(346, '040116', 'SABANDIA', 'AREQUIPA', 'AREQUIPA', 4136, 38, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(347, '040117', 'SACHACA', 'AREQUIPA', 'AREQUIPA', 19581, 11, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(348, '040118', 'SAN JUAN DE SIGUAS', 'AREQUIPA', 'AREQUIPA', 1535, 93, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(349, '040119', 'SAN JUAN DE TARUCANI', 'AREQUIPA', 'AREQUIPA', 2179, 2240, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(350, '040120', 'SANTA ISABEL DE SIGUAS', 'AREQUIPA', 'AREQUIPA', 1264, 186, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(351, '040121', 'SANTA RITA DE SIGUAS', 'AREQUIPA', 'AREQUIPA', 5592, 369, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(352, '040122', 'SOCABAYA', 'AREQUIPA', 'AREQUIPA', 32767, 25, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(353, '040123', 'TIABAYA', 'AREQUIPA', 'AREQUIPA', 14768, 39, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(354, '040124', 'UCHUMAYO', 'AREQUIPA', 'AREQUIPA', 12436, 271, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(355, '040125', 'VITOR', 'AREQUIPA', 'AREQUIPA', 2345, 1532, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(356, '040126', 'YANAHUARA', 'AREQUIPA', 'AREQUIPA', 25483, 6, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(357, '040127', 'YARABAMBA', 'AREQUIPA', 'AREQUIPA', 1125, 480, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(358, '040128', 'YURA', 'AREQUIPA', 'AREQUIPA', 25367, 1918, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(359, '040129', 'JOSE LUIS BUSTAMANTE Y RIVERO', 'AREQUIPA', 'AREQUIPA', 32767, 11, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(360, '040201', 'CAMANA', 'CAMANA', 'AREQUIPA', 14477, 11, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(361, '040202', 'JOSE MARIA QUIMPER', 'CAMANA', 'AREQUIPA', 4134, 16, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(362, '040203', 'MARIANO NICOLAS VALCARCEL', 'CAMANA', 'AREQUIPA', 6890, 557, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(363, '040204', 'MARISCAL CACERES', 'CAMANA', 'AREQUIPA', 6376, 593, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(364, '040205', 'NICOLAS DE PIEROLA', 'CAMANA', 'AREQUIPA', 6310, 387, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(365, '040206', 'OCONA', 'CAMANA', 'AREQUIPA', 4810, 1417, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(366, '040207', 'QUILCA', 'CAMANA', 'AREQUIPA', 661, 904, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(367, '040208', 'SAMUEL PASTOR', 'CAMANA', 'AREQUIPA', 15294, 114, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(368, '040301', 'CARAVELI', 'CARAVELI', 'AREQUIPA', 3722, 756, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(369, '040302', 'ACARI', 'CARAVELI', 'AREQUIPA', 3201, 771, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(370, '040303', 'ATICO', 'CARAVELI', 'AREQUIPA', 4147, 3099, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(371, '040304', 'ATIQUIPA', 'CARAVELI', 'AREQUIPA', 913, 423, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(372, '040305', 'BELLA UNION', 'CARAVELI', 'AREQUIPA', 6586, 1584, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(373, '040306', 'CAHUACHO', 'CARAVELI', 'AREQUIPA', 906, 1386, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(374, '040307', 'CHALA', 'CARAVELI', 'AREQUIPA', 6756, 340, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(375, '040308', 'CHAPARRA', 'CARAVELI', 'AREQUIPA', 5368, 1497, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(376, '040309', 'HUANUHUANU', 'CARAVELI', 'AREQUIPA', 3258, 703, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(377, '040310', 'JAQUI', 'CARAVELI', 'AREQUIPA', 1256, 422, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(378, '040311', 'LOMAS', 'CARAVELI', 'AREQUIPA', 1328, 445, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(379, '040312', 'QUICACHA', 'CARAVELI', 'AREQUIPA', 1881, 1037, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(380, '040313', 'YAUCA', 'CARAVELI', 'AREQUIPA', 1582, 546, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(381, '040401', 'APLAO', 'CASTILLA', 'AREQUIPA', 8844, 616, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(382, '040402', 'ANDAGUA', 'CASTILLA', 'AREQUIPA', 1152, 473, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(383, '040403', 'AYO', 'CASTILLA', 'AREQUIPA', 396, 284, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(384, '040404', 'CHACHAS', 'CASTILLA', 'AREQUIPA', 1720, 1197, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(385, '040405', 'CHILCAYMARCA', 'CASTILLA', 'AREQUIPA', 1243, 182, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(386, '040406', 'CHOCO', 'CASTILLA', 'AREQUIPA', 1009, 905, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(387, '040407', 'HUANCARQUI', 'CASTILLA', 'AREQUIPA', 1317, 795, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(388, '040408', 'MACHAGUAY', 'CASTILLA', 'AREQUIPA', 723, 245, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(389, '040409', 'ORCOPAMPA', 'CASTILLA', 'AREQUIPA', 9661, 729, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(390, '040410', 'PAMPACOLCA', 'CASTILLA', 'AREQUIPA', 2713, 266, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(391, '040411', 'TIPAN', 'CASTILLA', 'AREQUIPA', 522, 55, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(392, '040412', 'UNON', 'CASTILLA', 'AREQUIPA', 442, 339, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(393, '040413', 'URACA', 'CASTILLA', 'AREQUIPA', 7216, 696, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(394, '040414', 'VIRACO', 'CASTILLA', 'AREQUIPA', 1712, 137, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(395, '040501', 'CHIVAY', 'CAYLLOMA', 'AREQUIPA', 7688, 243, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(396, '040502', 'ACHOMA', 'CAYLLOMA', 'AREQUIPA', 908, 365, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(397, '040503', 'CABANACONDE', 'CAYLLOMA', 'AREQUIPA', 2406, 460, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(398, '040504', 'CALLALLI', 'CAYLLOMA', 'AREQUIPA', 2003, 1542, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(399, '040505', 'CAYLLOMA', 'CAYLLOMA', 'AREQUIPA', 3173, 1517, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(400, '040506', 'COPORAQUE', 'CAYLLOMA', 'AREQUIPA', 1520, 114, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(401, '040507', 'HUAMBO', 'CAYLLOMA', 'AREQUIPA', 614, 717, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(402, '040508', 'HUANCA', 'CAYLLOMA', 'AREQUIPA', 1450, 377, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(403, '040509', 'ICHUPAMPA', 'CAYLLOMA', 'AREQUIPA', 663, 74, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(404, '040510', 'LARI', 'CAYLLOMA', 'AREQUIPA', 1526, 371, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(405, '040511', 'LLUTA', 'CAYLLOMA', 'AREQUIPA', 1275, 1218, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(406, '040512', 'MACA', 'CAYLLOMA', 'AREQUIPA', 723, 237, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(407, '040513', 'MADRIGAL', 'CAYLLOMA', 'AREQUIPA', 498, 160, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(408, '040514', 'SAN ANTONIO DE CHUCA', 'CAYLLOMA', 'AREQUIPA', 1547, 1538, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(409, '040515', 'SIBAYO', 'CAYLLOMA', 'AREQUIPA', 675, 284, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(410, '040516', 'TAPAY', 'CAYLLOMA', 'AREQUIPA', 545, 415, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(411, '040517', 'TISCO', 'CAYLLOMA', 'AREQUIPA', 1450, 1441, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(412, '040518', 'TUTI', 'CAYLLOMA', 'AREQUIPA', 758, 240, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(413, '040519', 'YANQUE', 'CAYLLOMA', 'AREQUIPA', 2137, 1111, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(414, '040520', 'MAJES', 'CAYLLOMA', 'AREQUIPA', 32767, 1638, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(415, '040601', 'CHUQUIBAMBA', 'CONDESUYOS', 'AREQUIPA', 3346, 1188, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(416, '040602', 'ANDARAY', 'CONDESUYOS', 'AREQUIPA', 670, 811, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(417, '040603', 'CAYARANI', 'CONDESUYOS', 'AREQUIPA', 3159, 1396, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(418, '040604', 'CHICHAS', 'CONDESUYOS', 'AREQUIPA', 672, 390, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(419, '040605', 'IRAY', 'CONDESUYOS', 'AREQUIPA', 646, 238, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(420, '040606', 'RIO GRANDE', 'CONDESUYOS', 'AREQUIPA', 2751, 531, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(421, '040607', 'SALAMANCA', 'CONDESUYOS', 'AREQUIPA', 879, 1244, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(422, '040608', 'YANAQUIHUA', 'CONDESUYOS', 'AREQUIPA', 5820, 1055, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(423, '040701', 'MOLLENDO', 'ISLAY', 'AREQUIPA', 22389, 930, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(424, '040702', 'COCACHACRA', 'ISLAY', 'AREQUIPA', 8984, 1548, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(425, '040703', 'DEAN VALDIVIA', 'ISLAY', 'AREQUIPA', 6619, 134, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(426, '040704', 'ISLAY', 'ISLAY', 'AREQUIPA', 7124, 365, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(427, '040705', 'MEJIA', 'ISLAY', 'AREQUIPA', 1037, 102, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00');
INSERT INTO `zones` (`id`, `ubigeo`, `distrito`, `provincia`, `departamento`, `poblacion`, `area`, `estado`, `email`, `created_at`, `updated_at`) VALUES
(428, '040706', 'PUNTA DE BOMBON', 'ISLAY', 'AREQUIPA', 6477, 757, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(429, '040801', 'COTAHUASI', 'LA UNION', 'AREQUIPA', 2937, 166, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(430, '040802', 'ALCA', 'LA UNION', 'AREQUIPA', 2019, 191, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(431, '040803', 'CHARCANA', 'LA UNION', 'AREQUIPA', 556, 163, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(432, '040804', 'HUAYNACOTAS', 'LA UNION', 'AREQUIPA', 2251, 935, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(433, '040805', 'PAMPAMARCA', 'LA UNION', 'AREQUIPA', 1265, 787, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(434, '040806', 'PUYCA', 'LA UNION', 'AREQUIPA', 2807, 1505, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(435, '040807', 'QUECHUALLA', 'LA UNION', 'AREQUIPA', 236, 135, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(436, '040808', 'SAYLA', 'LA UNION', 'AREQUIPA', 574, 71, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(437, '040809', 'TAURIA', 'LA UNION', 'AREQUIPA', 323, 318, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(438, '040810', 'TOMEPAMPA', 'LA UNION', 'AREQUIPA', 826, 96, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(439, '040811', 'TORO', 'LA UNION', 'AREQUIPA', 808, 526, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(440, '050101', 'AYACUCHO', 'HUAMANGA', 'AYACUCHO', 32767, 82, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(441, '050102', 'ACOCRO', 'HUAMANGA', 'AYACUCHO', 10199, 387, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(442, '050103', 'ACOS VINCHOS', 'HUAMANGA', 'AYACUCHO', 5948, 155, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(443, '050104', 'CARMEN ALTO', 'HUAMANGA', 'AYACUCHO', 21350, 21, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(444, '050105', 'CHIARA', 'HUAMANGA', 'AYACUCHO', 7163, 497, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(445, '050106', 'OCROS', 'HUAMANGA', 'AYACUCHO', 5508, 203, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(446, '050107', 'PACAYCASA', 'HUAMANGA', 'AYACUCHO', 3192, 58, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(447, '050108', 'QUINUA', 'HUAMANGA', 'AYACUCHO', 6200, 121, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(448, '050109', 'SAN JOSE DE TICLLAS', 'HUAMANGA', 'AYACUCHO', 3688, 65, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(449, '050110', 'SAN JUAN BAUTISTA', 'HUAMANGA', 'AYACUCHO', 32767, 18, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(450, '050111', 'SANTIAGO DE PISCHA', 'HUAMANGA', 'AYACUCHO', 1799, 123, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(451, '050112', 'SOCOS', 'HUAMANGA', 'AYACUCHO', 7108, 82, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(452, '050113', 'TAMBILLO', 'HUAMANGA', 'AYACUCHO', 5715, 179, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(453, '050114', 'VINCHOS', 'HUAMANGA', 'AYACUCHO', 16710, 951, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(454, '050115', 'JESUS NAZARENO', 'HUAMANGA', 'AYACUCHO', 18054, 17, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(455, '050201', 'CANGALLO', 'CANGALLO', 'AYACUCHO', 6747, 185, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(456, '050202', 'CHUSCHI', 'CANGALLO', 'AYACUCHO', 7965, 412, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(457, '050203', 'LOS MOROCHUCOS', 'CANGALLO', 'AYACUCHO', 8205, 249, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(458, '050204', 'MARIA PARADO DE BELLIDO', 'CANGALLO', 'AYACUCHO', 2574, 131, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(459, '050205', 'PARAS', 'CANGALLO', 'AYACUCHO', 4575, 780, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(460, '050206', 'TOTOS', 'CANGALLO', 'AYACUCHO', 3720, 115, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(461, '050301', 'SANCOS', 'HUANCA SANCOS', 'AYACUCHO', 3584, 1357, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(462, '050302', 'CARAPO', 'HUANCA SANCOS', 'AYACUCHO', 2504, 196, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(463, '050303', 'SACSAMARCA', 'HUANCA SANCOS', 'AYACUCHO', 1613, 634, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(464, '050304', 'SANTIAGO DE LUCANAMARCA', 'HUANCA SANCOS', 'AYACUCHO', 2638, 645, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(465, '050401', 'HUANTA', 'HUANTA', 'AYACUCHO', 32767, 355, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(466, '050402', 'AYAHUANCO', 'HUANTA', 'AYACUCHO', 14485, 1076, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(467, '050403', 'HUAMANGUILLA', 'HUANTA', 'AYACUCHO', 4981, 77, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(468, '050404', 'IGUAIN', 'HUANTA', 'AYACUCHO', 3163, 71, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(469, '050405', 'LURICOCHA', 'HUANTA', 'AYACUCHO', 4991, 140, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(470, '050406', 'SANTILLANA', 'HUANTA', 'AYACUCHO', 7168, 518, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(471, '050407', 'SIVIA', 'HUANTA', 'AYACUCHO', 12345, 793, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(472, '050408', 'LLOCHEGUA', 'HUANTA', 'AYACUCHO', 14047, 783, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(473, '050501', 'SAN MIGUEL', 'LA MAR', 'AYACUCHO', 17240, 809, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(474, '050502', 'ANCO', 'LA MAR', 'AYACUCHO', 16875, 1062, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(475, '050503', 'AYNA', 'LA MAR', 'AYACUCHO', 10560, 315, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(476, '050504', 'CHILCAS', 'LA MAR', 'AYACUCHO', 3060, 154, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(477, '050505', 'CHUNGUI', 'LA MAR', 'AYACUCHO', 7287, 1047, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(478, '050506', 'LUIS CARRANZA', 'LA MAR', 'AYACUCHO', 1838, 212, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(479, '050507', 'SANTA ROSA', 'LA MAR', 'AYACUCHO', 11286, 399, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(480, '050508', 'TAMBO', 'LA MAR', 'AYACUCHO', 20567, 315, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(481, '050601', 'PUQUIO', 'LUCANAS', 'AYACUCHO', 13813, 861, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(482, '050602', 'AUCARA', 'LUCANAS', 'AYACUCHO', 5433, 880, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(483, '050603', 'CABANA', 'LUCANAS', 'AYACUCHO', 4489, 405, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(484, '050604', 'CARMEN SALCEDO', 'LUCANAS', 'AYACUCHO', 3985, 463, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(485, '050605', 'CHAVINA', 'LUCANAS', 'AYACUCHO', 2018, 372, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(486, '050606', 'CHIPAO', 'LUCANAS', 'AYACUCHO', 3741, 1156, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(487, '050607', 'HUAC-HUAS', 'LUCANAS', 'AYACUCHO', 2781, 312, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(488, '050608', 'LARAMATE', 'LUCANAS', 'AYACUCHO', 1443, 781, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(489, '050609', 'LEONCIO PRADO', 'LUCANAS', 'AYACUCHO', 1374, 1112, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(490, '050610', 'LLAUTA', 'LUCANAS', 'AYACUCHO', 1145, 492, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(491, '050611', 'LUCANAS', 'LUCANAS', 'AYACUCHO', 4089, 1209, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(492, '050612', 'OCANA', 'LUCANAS', 'AYACUCHO', 2914, 856, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(493, '050613', 'OTOCA', 'LUCANAS', 'AYACUCHO', 3020, 718, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(494, '050614', 'SAISA', 'LUCANAS', 'AYACUCHO', 906, 577, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(495, '050615', 'SAN CRISTOBAL', 'LUCANAS', 'AYACUCHO', 2105, 407, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(496, '050616', 'SAN JUAN', 'LUCANAS', 'AYACUCHO', 1559, 47, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(497, '050617', 'SAN PEDRO', 'LUCANAS', 'AYACUCHO', 3000, 729, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(498, '050618', 'SAN PEDRO DE PALCO', 'LUCANAS', 'AYACUCHO', 1371, 521, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(499, '050619', 'SANCOS', 'LUCANAS', 'AYACUCHO', 7236, 1505, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(500, '050620', 'SANTA ANA DE HUAYCAHUACHO', 'LUCANAS', 'AYACUCHO', 667, 45, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(501, '050621', 'SANTA LUCIA', 'LUCANAS', 'AYACUCHO', 914, 1031, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(502, '050701', 'CORACORA', 'PARINACOCHAS', 'AYACUCHO', 15378, 1389, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(503, '050702', 'CHUMPI', 'PARINACOCHAS', 'AYACUCHO', 2656, 372, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(504, '050703', 'CORONEL CASTANEDA', 'PARINACOCHAS', 'AYACUCHO', 1872, 1089, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(505, '050704', 'PACAPAUSA', 'PARINACOCHAS', 'AYACUCHO', 2874, 147, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(506, '050705', 'PULLO', 'PARINACOCHAS', 'AYACUCHO', 4893, 1562, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(507, '050706', 'PUYUSCA', 'PARINACOCHAS', 'AYACUCHO', 2076, 711, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(508, '050707', 'SAN FRANCISCO DE RAVACAYCO', 'PARINACOCHAS', 'AYACUCHO', 753, 103, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(509, '050708', 'UPAHUACHO', 'PARINACOCHAS', 'AYACUCHO', 2740, 577, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(510, '050801', 'PAUSA', 'PAUCAR DEL SARA SARA', 'AYACUCHO', 2792, 251, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(511, '050802', 'COLTA', 'PAUCAR DEL SARA SARA', 'AYACUCHO', 1140, 248, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(512, '050803', 'CORCULLA', 'PAUCAR DEL SARA SARA', 'AYACUCHO', 455, 101, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(513, '050804', 'LAMPA', 'PAUCAR DEL SARA SARA', 'AYACUCHO', 2528, 281, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(514, '050805', 'MARCABAMBA', 'PAUCAR DEL SARA SARA', 'AYACUCHO', 773, 120, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(515, '050806', 'OYOLO', 'PAUCAR DEL SARA SARA', 'AYACUCHO', 1195, 792, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(516, '050807', 'PARARCA', 'PAUCAR DEL SARA SARA', 'AYACUCHO', 660, 56, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(517, '050808', 'SAN JAVIER DE ALPABAMBA', 'PAUCAR DEL SARA SARA', 'AYACUCHO', 538, 117, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(518, '050809', 'SAN JOSE DE USHUA', 'PAUCAR DEL SARA SARA', 'AYACUCHO', 177, 29, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(519, '050810', 'SARA SARA', 'PAUCAR DEL SARA SARA', 'AYACUCHO', 731, 86, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(520, '050901', 'QUEROBAMBA', 'SUCRE', 'AYACUCHO', 2733, 282, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(521, '050902', 'BELEN', 'SUCRE', 'AYACUCHO', 756, 37, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(522, '050903', 'CHALCOS', 'SUCRE', 'AYACUCHO', 637, 55, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(523, '050904', 'CHILCAYOC', 'SUCRE', 'AYACUCHO', 575, 30, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(524, '050905', 'HUACANA', 'SUCRE', 'AYACUCHO', 674, 144, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(525, '050906', 'MORCOLLA', 'SUCRE', 'AYACUCHO', 1074, 289, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(526, '050907', 'PAICO', 'SUCRE', 'AYACUCHO', 842, 95, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(527, '050908', 'SAN PEDRO DE LARCAY', 'SUCRE', 'AYACUCHO', 1021, 316, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(528, '050909', 'SAN SALVADOR DE QUIJE', 'SUCRE', 'AYACUCHO', 1642, 126, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(529, '050910', 'SANTIAGO DE PAUCARAY', 'SUCRE', 'AYACUCHO', 747, 62, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(530, '050911', 'SORAS', 'SUCRE', 'AYACUCHO', 1292, 352, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(531, '051001', 'HUANCAPI', 'VICTOR FAJARDO', 'AYACUCHO', 1934, 240, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(532, '051002', 'ALCAMENCA', 'VICTOR FAJARDO', 'AYACUCHO', 2414, 116, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(533, '051003', 'APONGO', 'VICTOR FAJARDO', 'AYACUCHO', 1403, 176, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(534, '051004', 'ASQUIPATA', 'VICTOR FAJARDO', 'AYACUCHO', 447, 71, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(535, '051005', 'CANARIA', 'VICTOR FAJARDO', 'AYACUCHO', 3997, 264, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(536, '051006', 'CAYARA', 'VICTOR FAJARDO', 'AYACUCHO', 1165, 63, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(537, '051007', 'COLCA', 'VICTOR FAJARDO', 'AYACUCHO', 1014, 64, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(538, '051008', 'HUAMANQUIQUIA', 'VICTOR FAJARDO', 'AYACUCHO', 1254, 72, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(539, '051009', 'HUANCARAYLLA', 'VICTOR FAJARDO', 'AYACUCHO', 1077, 163, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(540, '051010', 'HUAYA', 'VICTOR FAJARDO', 'AYACUCHO', 3241, 157, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(541, '051011', 'SARHUA', 'VICTOR FAJARDO', 'AYACUCHO', 2763, 378, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(542, '051012', 'VILCANCHOS', 'VICTOR FAJARDO', 'AYACUCHO', 2674, 500, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(543, '051101', 'VILCAS HUAMAN', 'VILCAS HUAMAN', 'AYACUCHO', 8369, 220, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(544, '051102', 'ACCOMARCA', 'VILCAS HUAMAN', 'AYACUCHO', 982, 87, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(545, '051103', 'CARHUANCA', 'VILCAS HUAMAN', 'AYACUCHO', 1012, 54, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(546, '051104', 'CONCEPCION', 'VILCAS HUAMAN', 'AYACUCHO', 3100, 229, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(547, '051105', 'HUAMBALPA', 'VILCAS HUAMAN', 'AYACUCHO', 2168, 162, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(548, '051106', 'INDEPENDENCIA', 'VILCAS HUAMAN', 'AYACUCHO', 1593, 88, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(549, '051107', 'SAURAMA', 'VILCAS HUAMAN', 'AYACUCHO', 1292, 89, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(550, '051108', 'VISCHONGO', 'VILCAS HUAMAN', 'AYACUCHO', 4697, 278, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(551, '060101', 'CAJAMARCA', 'CAJAMARCA', 'CAJAMARCA', 32767, 379, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(552, '060102', 'ASUNCION', 'CAJAMARCA', 'CAJAMARCA', 13365, 214, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(553, '060103', 'CHETILLA', 'CAJAMARCA', 'CAJAMARCA', 4294, 74, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(554, '060104', 'COSPAN', 'CAJAMARCA', 'CAJAMARCA', 7887, 552, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(555, '060105', 'ENCANADA', 'CAJAMARCA', 'CAJAMARCA', 24190, 631, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(556, '060106', 'JESUS', 'CAJAMARCA', 'CAJAMARCA', 14703, 296, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(557, '060107', 'LLACANORA', 'CAJAMARCA', 'CAJAMARCA', 5363, 50, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(558, '060108', 'LOS BANOS DEL INCA', 'CAJAMARCA', 'CAJAMARCA', 32767, 283, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(559, '060109', 'MAGDALENA', 'CAJAMARCA', 'CAJAMARCA', 9650, 209, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(560, '060110', 'MATARA', 'CAJAMARCA', 'CAJAMARCA', 3567, 63, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(561, '060111', 'NAMORA', 'CAJAMARCA', 'CAJAMARCA', 10637, 157, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(562, '060112', 'SAN JUAN', 'CAJAMARCA', 'CAJAMARCA', 5195, 72, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(563, '060201', 'CAJABAMBA', 'CAJABAMBA', 'CAJAMARCA', 30603, 190, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(564, '060202', 'CACHACHI', 'CAJABAMBA', 'CAJAMARCA', 26794, 815, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(565, '060203', 'CONDEBAMBA', 'CAJABAMBA', 'CAJAMARCA', 13954, 199, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(566, '060204', 'SITACOCHA', 'CAJABAMBA', 'CAJAMARCA', 8910, 584, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(567, '060301', 'CELENDIN', 'CELENDIN', 'CAJAMARCA', 28030, 407, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(568, '060302', 'CHUMUCH', 'CELENDIN', 'CAJAMARCA', 3196, 216, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(569, '060303', 'CORTEGANA', 'CELENDIN', 'CAJAMARCA', 8819, 230, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(570, '060304', 'HUASMIN', 'CELENDIN', 'CAJAMARCA', 13611, 445, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(571, '060305', 'JORGE CHAVEZ', 'CELENDIN', 'CAJAMARCA', 597, 54, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(572, '060306', 'JOSE GALVEZ', 'CELENDIN', 'CAJAMARCA', 2545, 48, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(573, '060307', 'MIGUEL IGLESIAS', 'CELENDIN', 'CAJAMARCA', 5556, 249, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(574, '060308', 'OXAMARCA', 'CELENDIN', 'CAJAMARCA', 6937, 285, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(575, '060309', 'SOROCHUCO', 'CELENDIN', 'CAJAMARCA', 9892, 166, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(576, '060310', 'SUCRE', 'CELENDIN', 'CAJAMARCA', 6073, 260, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(577, '060311', 'UTCO', 'CELENDIN', 'CAJAMARCA', 1408, 100, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(578, '060312', 'LA LIBERTAD DE PALLAN', 'CELENDIN', 'CAJAMARCA', 8988, 187, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(579, '060401', 'CHOTA', 'CHOTA', 'CAJAMARCA', 32767, 268, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(580, '060402', 'ANGUIA', 'CHOTA', 'CAJAMARCA', 4298, 150, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(581, '060403', 'CHADIN', 'CHOTA', 'CAJAMARCA', 4111, 64, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(582, '060404', 'CHIGUIRIP', 'CHOTA', 'CAJAMARCA', 4672, 49, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(583, '060405', 'CHIMBAN', 'CHOTA', 'CAJAMARCA', 3663, 145, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(584, '060406', 'CHOROPAMPA', 'CHOTA', 'CAJAMARCA', 2663, 206, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(585, '060407', 'COCHABAMBA', 'CHOTA', 'CAJAMARCA', 6441, 119, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(586, '060408', 'CONCHAN', 'CHOTA', 'CAJAMARCA', 7015, 156, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(587, '060409', 'HUAMBOS', 'CHOTA', 'CAJAMARCA', 9508, 246, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(588, '060410', 'LAJAS', 'CHOTA', 'CAJAMARCA', 12552, 123, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(589, '060411', 'LLAMA', 'CHOTA', 'CAJAMARCA', 8061, 492, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(590, '060412', 'MIRACOSTA', 'CHOTA', 'CAJAMARCA', 3910, 420, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(591, '060413', 'PACCHA', 'CHOTA', 'CAJAMARCA', 5327, 106, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(592, '060414', 'PION', 'CHOTA', 'CAJAMARCA', 1575, 139, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(593, '060415', 'QUEROCOTO', 'CHOTA', 'CAJAMARCA', 8968, 300, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(594, '060416', 'SAN JUAN DE LICUPIS', 'CHOTA', 'CAJAMARCA', 986, 201, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(595, '060417', 'TACABAMBA', 'CHOTA', 'CAJAMARCA', 20049, 191, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(596, '060418', 'TOCMOCHE', 'CHOTA', 'CAJAMARCA', 995, 216, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(597, '060419', 'CHALAMARCA', 'CHOTA', 'CAJAMARCA', 11222, 181, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(598, '060501', 'CONTUMAZA', 'CONTUMAZA', 'CAJAMARCA', 8499, 347, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(599, '060502', 'CHILETE', 'CONTUMAZA', 'CAJAMARCA', 2787, 131, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(600, '060503', 'CUPISNIQUE', 'CONTUMAZA', 'CAJAMARCA', 1471, 288, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(601, '060504', 'GUZMANGO', 'CONTUMAZA', 'CAJAMARCA', 3130, 50, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(602, '060505', 'SAN BENITO', 'CONTUMAZA', 'CAJAMARCA', 3823, 486, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(603, '060506', 'SANTA CRUZ DE TOLED', 'CONTUMAZA', 'CAJAMARCA', 1056, 66, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(604, '060507', 'TANTARICA', 'CONTUMAZA', 'CAJAMARCA', 3247, 147, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(605, '060508', 'YONAN', 'CONTUMAZA', 'CAJAMARCA', 7899, 546, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(606, '060601', 'CUTERVO', 'CUTERVO', 'CAJAMARCA', 32767, 426, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(607, '060602', 'CALLAYUC', 'CUTERVO', 'CAJAMARCA', 10321, 311, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(608, '060603', 'CHOROS', 'CUTERVO', 'CAJAMARCA', 3599, 260, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(609, '060604', 'CUJILLO', 'CUTERVO', 'CAJAMARCA', 3033, 103, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(610, '060605', 'LA RAMADA', 'CUTERVO', 'CAJAMARCA', 4855, 36, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(611, '060606', 'PIMPINGOS', 'CUTERVO', 'CAJAMARCA', 5767, 169, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(612, '060607', 'QUEROCOTILLO', 'CUTERVO', 'CAJAMARCA', 16988, 687, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(613, '060608', 'SAN ANDRES DE CUTERVO', 'CUTERVO', 'CAJAMARCA', 5259, 124, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(614, '060609', 'SAN JUAN DE CUTERVO', 'CUTERVO', 'CAJAMARCA', 2005, 65, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(615, '060610', 'SAN LUIS DE LUCMA', 'CUTERVO', 'CAJAMARCA', 4041, 102, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(616, '060611', 'SANTA CRUZ', 'CUTERVO', 'CAJAMARCA', 2936, 130, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(617, '060612', 'SANTO DOMINGO DE LA CAPILLA', 'CUTERVO', 'CAJAMARCA', 5643, 105, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(618, '060613', 'SANTO TOMAS', 'CUTERVO', 'CAJAMARCA', 7988, 229, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(619, '060614', 'SOCOTA', 'CUTERVO', 'CAJAMARCA', 10747, 154, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(620, '060615', 'TORIBIO CASANOVA', 'CUTERVO', 'CAJAMARCA', 1294, 127, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(621, '060701', 'BAMBAMARCA', 'HUALGAYOC', 'CAJAMARCA', 32767, 449, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(622, '060702', 'CHUGUR', 'HUALGAYOC', 'CAJAMARCA', 3603, 106, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(623, '060703', 'HUALGAYOC', 'HUALGAYOC', 'CAJAMARCA', 16994, 229, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(624, '060801', 'JAEN', 'JAEN', 'CAJAMARCA', 32767, 554, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(625, '060802', 'BELLAVISTA', 'JAEN', 'CAJAMARCA', 15361, 881, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(626, '060803', 'CHONTALI', 'JAEN', 'CAJAMARCA', 10235, 427, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(627, '060804', 'COLASAY', 'JAEN', 'CAJAMARCA', 10577, 603, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(628, '060805', 'HUABAL', 'JAEN', 'CAJAMARCA', 7056, 83, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(629, '060806', 'LAS PIRIAS', 'JAEN', 'CAJAMARCA', 4054, 59, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(630, '060807', 'POMAHUACA', 'JAEN', 'CAJAMARCA', 10078, 803, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(631, '060808', 'PUCARA', 'JAEN', 'CAJAMARCA', 7657, 228, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(632, '060809', 'SALLIQUE', 'JAEN', 'CAJAMARCA', 8656, 367, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(633, '060810', 'SAN FELIPE', 'JAEN', 'CAJAMARCA', 6218, 256, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(634, '060811', 'SAN JOSE DEL ALTO', 'JAEN', 'CAJAMARCA', 7192, 485, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(635, '060812', 'SANTA ROSA', 'JAEN', 'CAJAMARCA', 11466, 279, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(636, '060901', 'SAN IGNACIO', 'SAN IGNACIO', 'CAJAMARCA', 32767, 325, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(637, '060902', 'CHIRINOS', 'SAN IGNACIO', 'CAJAMARCA', 14299, 352, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(638, '060903', 'HUARANGO', 'SAN IGNACIO', 'CAJAMARCA', 20614, 906, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(639, '060904', 'LA COIPA', 'SAN IGNACIO', 'CAJAMARCA', 20882, 417, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(640, '060905', 'NAMBALLE', 'SAN IGNACIO', 'CAJAMARCA', 11600, 693, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(641, '060906', 'SAN JOSE DE LOURDES', 'SAN IGNACIO', 'CAJAMARCA', 21847, 1359, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(642, '060907', 'TABACONAS', 'SAN IGNACIO', 'CAJAMARCA', 21686, 813, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(643, '061001', 'PEDRO GALVEZ', 'SAN MARCOS', 'CAJAMARCA', 21345, 243, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(644, '061002', 'CHANCAY', 'SAN MARCOS', 'CAJAMARCA', 3337, 67, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(645, '061003', 'EDUARDO VILLANUEVA', 'SAN MARCOS', 'CAJAMARCA', 2292, 62, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(646, '061004', 'GREGORIO PITA', 'SAN MARCOS', 'CAJAMARCA', 6711, 216, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(647, '061005', 'ICHOCAN', 'SAN MARCOS', 'CAJAMARCA', 1698, 69, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(648, '061006', 'JOSE MANUEL QUIROZ', 'SAN MARCOS', 'CAJAMARCA', 3988, 109, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(649, '061007', 'JOSE SABOGAL', 'SAN MARCOS', 'CAJAMARCA', 15115, 583, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(650, '061101', 'SAN MIGUEL', 'SAN MIGUEL', 'CAJAMARCA', 15885, 354, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(651, '061102', 'BOLIVAR', 'SAN MIGUEL', 'CAJAMARCA', 1488, 65, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(652, '061103', 'CALQUIS', 'SAN MIGUEL', 'CAJAMARCA', 4429, 336, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(653, '061104', 'CATILLUC', 'SAN MIGUEL', 'CAJAMARCA', 3486, 203, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(654, '061105', 'EL PRADO', 'SAN MIGUEL', 'CAJAMARCA', 1402, 70, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(655, '061106', 'LA FLORIDA', 'SAN MIGUEL', 'CAJAMARCA', 2205, 58, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(656, '061107', 'LLAPA', 'SAN MIGUEL', 'CAJAMARCA', 6035, 141, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(657, '061108', 'NANCHOC', 'SAN MIGUEL', 'CAJAMARCA', 1538, 373, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(658, '061109', 'NIEPOS', 'SAN MIGUEL', 'CAJAMARCA', 4058, 154, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(659, '061110', 'SAN GREGORIO', 'SAN MIGUEL', 'CAJAMARCA', 2293, 310, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(660, '061111', 'SAN SILVESTRE DE COCHAN', 'SAN MIGUEL', 'CAJAMARCA', 4475, 135, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(661, '061112', 'TONGOD', 'SAN MIGUEL', 'CAJAMARCA', 4857, 156, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(662, '061113', 'UNION AGUA BLANCA', 'SAN MIGUEL', 'CAJAMARCA', 3594, 171, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(663, '061201', 'SAN PABLO', 'SAN PABLO', 'CAJAMARCA', 13591, 200, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(664, '061202', 'SAN BERNARDINO', 'SAN PABLO', 'CAJAMARCA', 4827, 166, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(665, '061203', 'SAN LUIS', 'SAN PABLO', 'CAJAMARCA', 1276, 43, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(666, '061204', 'TUMBADEN', 'SAN PABLO', 'CAJAMARCA', 3604, 257, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(667, '061301', 'SANTA CRUZ', 'SANTA CRUZ', 'CAJAMARCA', 12250, 104, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(668, '061302', 'ANDABAMBA', 'SANTA CRUZ', 'CAJAMARCA', 1527, 8, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(669, '061303', 'CATACHE', 'SANTA CRUZ', 'CAJAMARCA', 10010, 567, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(670, '061304', 'CHANCAYBANOS', 'SANTA CRUZ', 'CAJAMARCA', 3905, 126, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(671, '061305', 'LA ESPERANZA', 'SANTA CRUZ', 'CAJAMARCA', 2601, 59, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(672, '061306', 'NINABAMBA', 'SANTA CRUZ', 'CAJAMARCA', 2791, 57, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(673, '061307', 'PULAN', 'SANTA CRUZ', 'CAJAMARCA', 4492, 159, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(674, '061308', 'SAUCEPAMPA', 'SANTA CRUZ', 'CAJAMARCA', 1871, 31, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(675, '061309', 'SEXI', 'SANTA CRUZ', 'CAJAMARCA', 566, 192, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(676, '061310', 'UTICYACU', 'SANTA CRUZ', 'CAJAMARCA', 1614, 42, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(677, '061311', 'YAUYUCAN', 'SANTA CRUZ', 'CAJAMARCA', 3595, 38, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(678, '070101', 'CALLAO', 'CALLAO', 'CALLAO', 32767, 50, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(679, '070102', 'BELLAVISTA', 'CALLAO', 'CALLAO', 32767, 5, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(680, '070103', 'CARMEN DE LA LEGUA Y REYNOSO', 'CALLAO', 'CALLAO', 32767, 2, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(681, '070104', 'LA PERLA', 'CALLAO', 'CALLAO', 32767, 3, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(682, '070105', 'LA PUNTA', 'CALLAO', 'CALLAO', 3392, 1, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(683, '070106', 'VENTANILLA', 'CALLAO', 'CALLAO', 32767, 77, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(684, '070107', 'MI PERU', 'CALLAO', 'CALLAO', 32767, 3, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(685, '080101', 'CUSCO', 'CUSCO', 'CUSCO', 32767, 101, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(686, '080102', 'CCORCA', 'CUSCO', 'CUSCO', 2235, 162, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(687, '080103', 'POROY', 'CUSCO', 'CUSCO', 7817, 13, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(688, '080104', 'SAN JERONIMO', 'CUSCO', 'CUSCO', 32767, 89, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(689, '080105', 'SAN SEBASTIAN', 'CUSCO', 'CUSCO', 32767, 77, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(690, '080106', 'SANTIAGO', 'CUSCO', 'CUSCO', 32767, 59, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(691, '080107', 'SAYLLA', 'CUSCO', 'CUSCO', 5389, 24, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(692, '080108', 'WANCHAQ', 'CUSCO', 'CUSCO', 32767, 5, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(693, '080201', 'ACOMAYO', 'ACOMAYO', 'CUSCO', 5552, 143, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(694, '080202', 'ACOPIA', 'ACOMAYO', 'CUSCO', 2379, 70, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(695, '080203', 'ACOS', 'ACOMAYO', 'CUSCO', 2338, 136, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(696, '080204', 'MOSOC LLACTA', 'ACOMAYO', 'CUSCO', 2287, 44, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(697, '080205', 'POMACANCHI', 'ACOMAYO', 'CUSCO', 9020, 268, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(698, '080206', 'RONDOCAN', 'ACOMAYO', 'CUSCO', 2379, 185, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(699, '080207', 'SANGARARA', 'ACOMAYO', 'CUSCO', 3738, 86, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(700, '080301', 'ANTA', 'ANTA', 'CUSCO', 16703, 187, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(701, '080302', 'ANCAHUASI', 'ANTA', 'CUSCO', 6947, 124, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(702, '080303', 'CACHIMAYO', 'ANTA', 'CUSCO', 2285, 43, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(703, '080304', 'CHINCHAYPUJIO', 'ANTA', 'CUSCO', 4303, 395, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(704, '080305', 'HUAROCONDO', 'ANTA', 'CUSCO', 5762, 220, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(705, '080306', 'LIMATAMBO', 'ANTA', 'CUSCO', 9801, 513, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(706, '080307', 'MOLLEPATA', 'ANTA', 'CUSCO', 2600, 356, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(707, '080308', 'PUCYURA', 'ANTA', 'CUSCO', 4258, 34, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(708, '080309', 'ZURITE', 'ANTA', 'CUSCO', 3643, 60, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(709, '080401', 'CALCA', 'CALCA', 'CUSCO', 23316, 336, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(710, '080402', 'COYA', 'CALCA', 'CUSCO', 4026, 71, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(711, '080403', 'LAMAY', 'CALCA', 'CUSCO', 5768, 96, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(712, '080404', 'LARES', 'CALCA', 'CUSCO', 7210, 743, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(713, '080405', 'PISAC', 'CALCA', 'CUSCO', 10188, 147, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(714, '080406', 'SAN SALVADOR', 'CALCA', 'CUSCO', 5622, 128, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(715, '080407', 'TARAY', 'CALCA', 'CUSCO', 4728, 54, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(716, '080408', 'YANATILE', 'CALCA', 'CUSCO', 13337, 1992, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(717, '080501', 'YANAOCA', 'CANAS', 'CUSCO', 9976, 288, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(718, '080502', 'CHECCA', 'CANAS', 'CUSCO', 6302, 505, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(719, '080503', 'KUNTURKANKI', 'CANAS', 'CUSCO', 5738, 393, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(720, '080504', 'LANGUI', 'CANAS', 'CUSCO', 2467, 170, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(721, '080505', 'LAYO', 'CANAS', 'CUSCO', 6333, 481, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(722, '080506', 'PAMPAMARCA', 'CANAS', 'CUSCO', 2003, 31, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(723, '080507', 'QUEHUE', 'CANAS', 'CUSCO', 3606, 148, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(724, '080508', 'TUPAC AMARU', 'CANAS', 'CUSCO', 2868, 117, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(725, '080601', 'SICUANI', 'CANCHIS', 'CUSCO', 32767, 646, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(726, '080602', 'CHECACUPE', 'CANCHIS', 'CUSCO', 5000, 938, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(727, '080603', 'COMBAPATA', 'CANCHIS', 'CUSCO', 5394, 173, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(728, '080604', 'MARANGANI', 'CANCHIS', 'CUSCO', 11247, 439, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(729, '080605', 'PITUMARCA', 'CANCHIS', 'CUSCO', 7506, 1092, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(730, '080606', 'SAN PABLO', 'CANCHIS', 'CUSCO', 4680, 524, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(731, '080607', 'SAN PEDRO', 'CANCHIS', 'CUSCO', 2804, 56, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(732, '080608', 'TINTA', 'CANCHIS', 'CUSCO', 5626, 82, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(733, '080701', 'SANTO TOMAS', 'CHUMBIVILCAS', 'CUSCO', 26564, 1899, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(734, '080702', 'CAPACMARCA', 'CHUMBIVILCAS', 'CUSCO', 4596, 268, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(735, '080703', 'CHAMACA', 'CHUMBIVILCAS', 'CUSCO', 8864, 672, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(736, '080704', 'COLQUEMARCA', 'CHUMBIVILCAS', 'CUSCO', 8579, 451, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(737, '080705', 'LIVITACA', 'CHUMBIVILCAS', 'CUSCO', 13357, 749, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(738, '080706', 'LLUSCO', 'CHUMBIVILCAS', 'CUSCO', 7064, 312, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(739, '080707', 'QUINOTA', 'CHUMBIVILCAS', 'CUSCO', 4895, 224, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(740, '080708', 'VELILLE', 'CHUMBIVILCAS', 'CUSCO', 8492, 761, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(741, '080801', 'ESPINAR', 'ESPINAR', 'CUSCO', 32767, 736, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(742, '080802', 'CONDOROMA', 'ESPINAR', 'CUSCO', 1400, 516, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(743, '080803', 'COPORAQUE', 'ESPINAR', 'CUSCO', 17846, 1544, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(744, '080804', 'OCORURO', 'ESPINAR', 'CUSCO', 1606, 359, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(745, '080805', 'PALLPATA', 'ESPINAR', 'CUSCO', 5542, 818, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(746, '080806', 'PICHIGUA', 'ESPINAR', 'CUSCO', 3603, 274, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(747, '080807', 'SUYCKUTAMBO', 'ESPINAR', 'CUSCO', 2768, 630, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(748, '080808', 'ALTO PICHIGUA', 'ESPINAR', 'CUSCO', 3139, 359, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(749, '080901', 'SANTA ANA', 'LA CONVENCION', 'CUSCO', 32767, 375, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(750, '080902', 'ECHARATE', 'LA CONVENCION', 'CUSCO', 32767, 22178, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(751, '080903', 'HUAYOPATA', 'LA CONVENCION', 'CUSCO', 4698, 542, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(752, '080904', 'MARANURA', 'LA CONVENCION', 'CUSCO', 6058, 177, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(753, '080905', 'OCOBAMBA', 'LA CONVENCION', 'CUSCO', 6767, 850, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(754, '080906', 'QUELLOUNO', 'LA CONVENCION', 'CUSCO', 18089, 1731, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(755, '080907', 'KIMBIRI', 'LA CONVENCION', 'CUSCO', 16865, 987, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(756, '080908', 'SANTA TERESA', 'LA CONVENCION', 'CUSCO', 6476, 1335, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(757, '080909', 'VILCABAMBA', 'LA CONVENCION', 'CUSCO', 21159, 2958, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(758, '080910', 'PICHARI', 'LA CONVENCION', 'CUSCO', 20316, 832, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(759, '081001', 'PARURO', 'PARURO', 'CUSCO', 3338, 152, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(760, '081002', 'ACCHA', 'PARURO', 'CUSCO', 3787, 239, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(761, '081003', 'CCAPI', 'PARURO', 'CUSCO', 3682, 326, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(762, '081004', 'COLCHA', 'PARURO', 'CUSCO', 1138, 141, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(763, '081005', 'HUANOQUITE', 'PARURO', 'CUSCO', 5648, 362, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(764, '081006', 'OMACHA', 'PARURO', 'CUSCO', 7203, 427, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(765, '081007', 'PACCARITAMBO', 'PARURO', 'CUSCO', 2012, 119, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(766, '081008', 'PILLPINTO', 'PARURO', 'CUSCO', 1220, 78, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(767, '081009', 'YAURISQUE', 'PARURO', 'CUSCO', 2473, 124, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(768, '081101', 'PAUCARTAMBO', 'PAUCARTAMBO', 'CUSCO', 13190, 792, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(769, '081102', 'CAICAY', 'PAUCARTAMBO', 'CUSCO', 2701, 107, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(770, '081103', 'CHALLABAMBA', 'PAUCARTAMBO', 'CUSCO', 11264, 725, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(771, '081104', 'COLQUEPATA', 'PAUCARTAMBO', 'CUSCO', 10662, 463, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(772, '081105', 'HUANCARANI', 'PAUCARTAMBO', 'CUSCO', 7634, 143, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(773, '081106', 'KOSNIPATA', 'PAUCARTAMBO', 'CUSCO', 5609, 3670, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(774, '081201', 'URCOS', 'QUISPICANCHI', 'CUSCO', 9254, 141, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(775, '081202', 'ANDAHUAYLILLAS', 'QUISPICANCHI', 'CUSCO', 5465, 85, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(776, '081203', 'CAMANTI', 'QUISPICANCHI', 'CUSCO', 2094, 3183, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(777, '081204', 'CCARHUAYO', 'QUISPICANCHI', 'CUSCO', 3129, 297, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(778, '081205', 'CCATCA', 'QUISPICANCHI', 'CUSCO', 17944, 296, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(779, '081206', 'CUSIPATA', 'QUISPICANCHI', 'CUSCO', 4770, 244, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(780, '081207', 'HUARO', 'QUISPICANCHI', 'CUSCO', 4491, 107, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(781, '081208', 'LUCRE', 'QUISPICANCHI', 'CUSCO', 4000, 119, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(782, '081209', 'MARCAPATA', 'QUISPICANCHI', 'CUSCO', 4514, 1579, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(783, '081210', 'OCONGATE', 'QUISPICANCHI', 'CUSCO', 15614, 948, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(784, '081211', 'OROPESA', 'QUISPICANCHI', 'CUSCO', 7280, 78, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(785, '081212', 'QUIQUIJANA', 'QUISPICANCHI', 'CUSCO', 10962, 364, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(786, '081301', 'URUBAMBA', 'URUBAMBA', 'CUSCO', 20879, 162, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(787, '081302', 'CHINCHERO', 'URUBAMBA', 'CUSCO', 9763, 102, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(788, '081303', 'HUAYLLABAMBA', 'URUBAMBA', 'CUSCO', 5228, 74, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(789, '081304', 'MACHUPICCHU', 'URUBAMBA', 'CUSCO', 8332, 374, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(790, '081305', 'MARAS', 'URUBAMBA', 'CUSCO', 5794, 142, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(791, '081306', 'OLLANTAYTAMBO', 'URUBAMBA', 'CUSCO', 11225, 583, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(792, '081307', 'YUCAY', 'URUBAMBA', 'CUSCO', 3299, 24, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(793, '090101', 'HUANCAVELICA', 'HUANCAVELICA', 'HUANCAVELICA', 32767, 508, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(794, '090102', 'ACOBAMBILLA', 'HUANCAVELICA', 'HUANCAVELICA', 4593, 747, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(795, '090103', 'ACORIA', 'HUANCAVELICA', 'HUANCAVELICA', 32767, 533, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(796, '090104', 'CONAYCA', 'HUANCAVELICA', 'HUANCAVELICA', 1219, 42, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(797, '090105', 'CUENCA', 'HUANCAVELICA', 'HUANCAVELICA', 1974, 55, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(798, '090106', 'HUACHOCOLPA', 'HUANCAVELICA', 'HUANCAVELICA', 2883, 337, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(799, '090107', 'HUAYLLAHUARA', 'HUANCAVELICA', 'HUANCAVELICA', 752, 37, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(800, '090108', 'IZCUCHACA', 'HUANCAVELICA', 'HUANCAVELICA', 879, 12, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(801, '090109', 'LARIA', 'HUANCAVELICA', 'HUANCAVELICA', 1440, 64, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(802, '090110', 'MANTA', 'HUANCAVELICA', 'HUANCAVELICA', 1848, 159, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(803, '090111', 'MARISCAL CACERES', 'HUANCAVELICA', 'HUANCAVELICA', 1017, 10, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(804, '090112', 'MOYA', 'HUANCAVELICA', 'HUANCAVELICA', 2479, 95, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(805, '090113', 'NUEVO OCCORO', 'HUANCAVELICA', 'HUANCAVELICA', 2676, 218, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(806, '090114', 'PALCA', 'HUANCAVELICA', 'HUANCAVELICA', 3225, 81, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(807, '090115', 'PILCHACA', 'HUANCAVELICA', 'HUANCAVELICA', 510, 40, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(808, '090116', 'VILCA', 'HUANCAVELICA', 'HUANCAVELICA', 3051, 309, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(809, '090117', 'YAULI', 'HUANCAVELICA', 'HUANCAVELICA', 32767, 320, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(810, '090118', 'ASCENSION', 'HUANCAVELICA', 'HUANCAVELICA', 12252, 428, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(811, '090119', 'HUANDO', 'HUANCAVELICA', 'HUANCAVELICA', 7638, 196, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(812, '090201', 'ACOBAMBA', 'ACOBAMBA', 'HUANCAVELICA', 10058, 124, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(813, '090202', 'ANDABAMBA', 'ACOBAMBA', 'HUANCAVELICA', 5569, 82, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(814, '090203', 'ANTA', 'ACOBAMBA', 'HUANCAVELICA', 9407, 92, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(815, '090204', 'CAJA', 'ACOBAMBA', 'HUANCAVELICA', 2818, 86, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(816, '090205', 'MARCAS', 'ACOBAMBA', 'HUANCAVELICA', 2381, 171, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(817, '090206', 'PAUCARA', 'ACOBAMBA', 'HUANCAVELICA', 32767, 220, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(818, '090207', 'POMACOCHA', 'ACOBAMBA', 'HUANCAVELICA', 3936, 55, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(819, '090208', 'ROSARIO', 'ACOBAMBA', 'HUANCAVELICA', 7752, 97, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(820, '090301', 'LIRCAY', 'ANGARAES', 'HUANCAVELICA', 24699, 830, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(821, '090302', 'ANCHONGA', 'ANGARAES', 'HUANCAVELICA', 7966, 75, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(822, '090303', 'CALLANMARCA', 'ANGARAES', 'HUANCAVELICA', 764, 25, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(823, '090304', 'CCOCHACCASA', 'ANGARAES', 'HUANCAVELICA', 2737, 107, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(824, '090305', 'CHINCHO', 'ANGARAES', 'HUANCAVELICA', 3438, 173, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(825, '090306', 'CONGALLA', 'ANGARAES', 'HUANCAVELICA', 4109, 210, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(826, '090307', 'HUANCA-HUANCA', 'ANGARAES', 'HUANCAVELICA', 1746, 108, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(827, '090308', 'HUAYLLAY GRANDE', 'ANGARAES', 'HUANCAVELICA', 2168, 32, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(828, '090309', 'JULCAMARCA', 'ANGARAES', 'HUANCAVELICA', 1753, 51, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(829, '090310', 'SAN ANTONIO DE ANTAPARCO', 'ANGARAES', 'HUANCAVELICA', 7514, 34, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(830, '090311', 'SANTO TOMAS DE PATA', 'ANGARAES', 'HUANCAVELICA', 2610, 139, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(831, '090312', 'SECCLLA', 'ANGARAES', 'HUANCAVELICA', 3751, 162, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(832, '090401', 'CASTROVIRREYNA', 'CASTROVIRREYNA', 'HUANCAVELICA', 3248, 921, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(833, '090402', 'ARMA', 'CASTROVIRREYNA', 'HUANCAVELICA', 1436, 301, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(834, '090403', 'AURAHUA', 'CASTROVIRREYNA', 'HUANCAVELICA', 2234, 345, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(835, '090404', 'CAPILLAS', 'CASTROVIRREYNA', 'HUANCAVELICA', 1440, 400, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(836, '090405', 'CHUPAMARCA', 'CASTROVIRREYNA', 'HUANCAVELICA', 1207, 371, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(837, '090406', 'COCAS', 'CASTROVIRREYNA', 'HUANCAVELICA', 912, 81, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(838, '090407', 'HUACHOS', 'CASTROVIRREYNA', 'HUANCAVELICA', 1676, 175, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(839, '090408', 'HUAMATAMBO', 'CASTROVIRREYNA', 'HUANCAVELICA', 388, 59, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(840, '090409', 'MOLLEPAMPA', 'CASTROVIRREYNA', 'HUANCAVELICA', 1659, 170, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(841, '090410', 'SAN JUAN', 'CASTROVIRREYNA', 'HUANCAVELICA', 473, 197, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(842, '090411', 'SANTA ANA', 'CASTROVIRREYNA', 'HUANCAVELICA', 2157, 646, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(843, '090412', 'TANTARA', 'CASTROVIRREYNA', 'HUANCAVELICA', 722, 111, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(844, '090413', 'TICRAPO', 'CASTROVIRREYNA', 'HUANCAVELICA', 1617, 182, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(845, '090501', 'CHURCAMPA', 'CHURCAMPA', 'HUANCAVELICA', 5479, 133, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(846, '090502', 'ANCO', 'CHURCAMPA', 'HUANCAVELICA', 11420, 270, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00');
INSERT INTO `zones` (`id`, `ubigeo`, `distrito`, `provincia`, `departamento`, `poblacion`, `area`, `estado`, `email`, `created_at`, `updated_at`) VALUES
(847, '090503', 'CHINCHIHUASI', 'CHURCAMPA', 'HUANCAVELICA', 4786, 166, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(848, '090504', 'EL CARMEN', 'CHURCAMPA', 'HUANCAVELICA', 2998, 76, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(849, '090505', 'LA MERCED', 'CHURCAMPA', 'HUANCAVELICA', 1834, 72, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(850, '090506', 'LOCROJA', 'CHURCAMPA', 'HUANCAVELICA', 4048, 81, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(851, '090507', 'PAUCARBAMBA', 'CHURCAMPA', 'HUANCAVELICA', 7232, 103, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(852, '090508', 'SAN MIGUEL DE MAYOCC', 'CHURCAMPA', 'HUANCAVELICA', 1261, 26, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(853, '090509', 'SAN PEDRO DE CORIS', 'CHURCAMPA', 'HUANCAVELICA', 2823, 137, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(854, '090510', 'PACHAMARCA', 'CHURCAMPA', 'HUANCAVELICA', 2701, 157, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(855, '090601', 'HUAYTARA', 'HUAYTARA', 'HUANCAVELICA', 2100, 393, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(856, '090602', 'AYAVI', 'HUAYTARA', 'HUANCAVELICA', 617, 180, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(857, '090603', 'CORDOVA', 'HUAYTARA', 'HUANCAVELICA', 2826, 103, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(858, '090604', 'HUAYACUNDO ARMA', 'HUAYTARA', 'HUANCAVELICA', 467, 38, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(859, '090605', 'LARAMARCA', 'HUAYTARA', 'HUANCAVELICA', 850, 208, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(860, '090606', 'OCOYO', 'HUAYTARA', 'HUANCAVELICA', 2436, 154, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(861, '090607', 'PILPICHACA', 'HUAYTARA', 'HUANCAVELICA', 3688, 2176, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(862, '090608', 'QUERCO', 'HUAYTARA', 'HUANCAVELICA', 1006, 682, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(863, '090609', 'QUITO-ARMA', 'HUAYTARA', 'HUANCAVELICA', 775, 238, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(864, '090610', 'SAN ANTONIO DE CUSICANCHA', 'HUAYTARA', 'HUANCAVELICA', 1658, 240, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(865, '090611', 'SAN FRANCISCO DE SANGAYAICO', 'HUAYTARA', 'HUANCAVELICA', 580, 78, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(866, '090612', 'SAN ISIDRO', 'HUAYTARA', 'HUANCAVELICA', 1171, 169, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(867, '090613', 'SANTIAGO DE CHOCORVOS', 'HUAYTARA', 'HUANCAVELICA', 2887, 1146, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(868, '090614', 'SANTIAGO DE QUIRAHUARA', 'HUAYTARA', 'HUANCAVELICA', 655, 177, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(869, '090615', 'SANTO DOMINGO DE CAPILLAS', 'HUAYTARA', 'HUANCAVELICA', 983, 257, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(870, '090616', 'TAMBO', 'HUAYTARA', 'HUANCAVELICA', 322, 227, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(871, '090701', 'PAMPAS', 'TAYACAJA', 'HUANCAVELICA', 11166, 109, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(872, '090702', 'ACOSTAMBO', 'TAYACAJA', 'HUANCAVELICA', 4131, 162, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(873, '090703', 'ACRAQUIA', 'TAYACAJA', 'HUANCAVELICA', 4984, 109, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(874, '090704', 'AHUAYCHA', 'TAYACAJA', 'HUANCAVELICA', 5497, 105, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(875, '090705', 'COLCABAMBA', 'TAYACAJA', 'HUANCAVELICA', 18802, 579, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(876, '090706', 'DANIEL HERNANDEZ', 'TAYACAJA', 'HUANCAVELICA', 10243, 105, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(877, '090707', 'HUACHOCOLPA', 'TAYACAJA', 'HUANCAVELICA', 6286, 278, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(878, '090709', 'HUARIBAMBA', 'TAYACAJA', 'HUANCAVELICA', 7850, 364, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(879, '090710', 'NAHUIMPUQUIO', 'TAYACAJA', 'HUANCAVELICA', 1904, 67, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(880, '090711', 'PAZOS', 'TAYACAJA', 'HUANCAVELICA', 7230, 155, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(881, '090713', 'QUISHUAR', 'TAYACAJA', 'HUANCAVELICA', 899, 33, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(882, '090714', 'SALCABAMBA', 'TAYACAJA', 'HUANCAVELICA', 4619, 189, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(883, '090715', 'SALCAHUASI', 'TAYACAJA', 'HUANCAVELICA', 3345, 117, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(884, '090716', 'SAN MARCOS DE ROCCHAC', 'TAYACAJA', 'HUANCAVELICA', 2880, 288, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(885, '090717', 'SURCUBAMBA', 'TAYACAJA', 'HUANCAVELICA', 4897, 271, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(886, '090718', 'TINTAY PUNCU', 'TAYACAJA', 'HUANCAVELICA', 12975, 448, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(887, '100101', 'HUANUCO', 'HUANUCO', 'HUANUCO', 32767, 98, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(888, '100102', 'AMARILIS', 'HUANUCO', 'HUANUCO', 32767, 137, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(889, '100103', 'CHINCHAO', 'HUANUCO', 'HUANUCO', 25961, 1839, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(890, '100104', 'CHURUBAMBA', 'HUANUCO', 'HUANUCO', 29599, 558, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(891, '100105', 'MARGOS', 'HUANUCO', 'HUANUCO', 15029, 286, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(892, '100106', 'QUISQUI', 'HUANUCO', 'HUANUCO', 7978, 160, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(893, '100107', 'SAN FRANCISCO DE CAYRAN', 'HUANUCO', 'HUANUCO', 5141, 99, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(894, '100108', 'SAN PEDRO DE CHAULAN', 'HUANUCO', 'HUANUCO', 7745, 279, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(895, '100109', 'SANTA MARIA DEL VALLE', 'HUANUCO', 'HUANUCO', 18248, 490, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(896, '100110', 'YARUMAYO', 'HUANUCO', 'HUANUCO', 2881, 63, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(897, '100111', 'PILLCO MARCA', 'HUANUCO', 'HUANUCO', 32767, 69, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(898, '100201', 'AMBO', 'AMBO', 'HUANUCO', 17138, 288, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(899, '100202', 'CAYNA', 'AMBO', 'HUANUCO', 3528, 156, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(900, '100203', 'COLPAS', 'AMBO', 'HUANUCO', 2513, 185, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(901, '100204', 'CONCHAMARCA', 'AMBO', 'HUANUCO', 6822, 108, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(902, '100205', 'HUACAR', 'AMBO', 'HUANUCO', 7643, 238, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(903, '100206', 'SAN FRANCISCO', 'AMBO', 'HUANUCO', 3327, 118, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(904, '100207', 'SAN RAFAEL', 'AMBO', 'HUANUCO', 12186, 441, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(905, '100208', 'TOMAY KICHWA', 'AMBO', 'HUANUCO', 4082, 43, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(906, '100301', 'LA UNION', 'DOS DE MAYO', 'HUANUCO', 6332, 169, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(907, '100307', 'CHUQUIS', 'DOS DE MAYO', 'HUANUCO', 5894, 153, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(908, '100311', 'MARIAS', 'DOS DE MAYO', 'HUANUCO', 9538, 616, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(909, '100313', 'PACHAS', 'DOS DE MAYO', 'HUANUCO', 13039, 268, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(910, '100316', 'QUIVILLA', 'DOS DE MAYO', 'HUANUCO', 3035, 34, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(911, '100317', 'RIPAN', 'DOS DE MAYO', 'HUANUCO', 7050, 76, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(912, '100321', 'SHUNQUI', 'DOS DE MAYO', 'HUANUCO', 2520, 33, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(913, '100322', 'SILLAPATA', 'DOS DE MAYO', 'HUANUCO', 2549, 71, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(914, '100323', 'YANAS', 'DOS DE MAYO', 'HUANUCO', 3367, 37, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(915, '100401', 'HUACAYBAMBA', 'HUACAYBAMBA', 'HUANUCO', 7245, 562, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(916, '100402', 'CANCHABAMBA', 'HUACAYBAMBA', 'HUANUCO', 3301, 186, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(917, '100403', 'COCHABAMBA', 'HUACAYBAMBA', 'HUANUCO', 3478, 722, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(918, '100404', 'PINRA', 'HUACAYBAMBA', 'HUANUCO', 8819, 295, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(919, '100501', 'LLATA', 'HUAMALIES', 'HUANUCO', 15059, 412, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(920, '100502', 'ARANCAY', 'HUAMALIES', 'HUANUCO', 1513, 118, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(921, '100503', 'CHAVIN DE PARIARCA', 'HUAMALIES', 'HUANUCO', 3846, 92, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(922, '100504', 'JACAS GRANDE', 'HUAMALIES', 'HUANUCO', 5916, 223, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(923, '100505', 'JIRCAN', 'HUAMALIES', 'HUANUCO', 3569, 251, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(924, '100506', 'MIRAFLORES', 'HUAMALIES', 'HUANUCO', 3565, 97, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(925, '100507', 'MONZON', 'HUAMALIES', 'HUANUCO', 28605, 1397, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(926, '100508', 'PUNCHAO', 'HUAMALIES', 'HUANUCO', 2536, 40, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(927, '100509', 'PUNOS', 'HUAMALIES', 'HUANUCO', 4412, 97, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(928, '100510', 'SINGA', 'HUAMALIES', 'HUANUCO', 3482, 162, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(929, '100511', 'TANTAMAYO', 'HUAMALIES', 'HUANUCO', 3002, 279, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(930, '100601', 'RUPA-RUPA', 'LEONCIO PRADO', 'HUANUCO', 32767, 369, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(931, '100602', 'DANIEL ALOMIAS ROBLES', 'LEONCIO PRADO', 'HUANUCO', 7775, 780, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(932, '100603', 'HERMILIO VALDIZAN', 'LEONCIO PRADO', 'HUANUCO', 4101, 129, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(933, '100604', 'JOSE CRESPO Y CASTILLO', 'LEONCIO PRADO', 'HUANUCO', 32767, 2791, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(934, '100605', 'LUYANDO', 'LEONCIO PRADO', 'HUANUCO', 9851, 113, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(935, '100606', 'MARIANO DAMASO BERAUN', 'LEONCIO PRADO', 'HUANUCO', 9586, 742, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(936, '100701', 'HUACRACHUCO', 'MARANON', 'HUANUCO', 15793, 703, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(937, '100702', 'CHOLON', 'MARANON', 'HUANUCO', 13643, 4087, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(938, '100703', 'SAN BUENAVENTURA', 'MARANON', 'HUANUCO', 2682, 89, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(939, '100801', 'PANAO', 'PACHITEA', 'HUANUCO', 24223, 1614, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(940, '100802', 'CHAGLLA', 'PACHITEA', 'HUANUCO', 11768, 638, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(941, '100803', 'MOLINO', 'PACHITEA', 'HUANUCO', 14840, 238, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(942, '100804', 'UMARI', 'PACHITEA', 'HUANUCO', 21398, 157, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(943, '100901', 'PUERTO INCA', 'PUERTO INCA', 'HUANUCO', 7784, 2227, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(944, '100902', 'CODO DEL POZUZO', 'PUERTO INCA', 'HUANUCO', 6603, 3222, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(945, '100903', 'HONORIA', 'PUERTO INCA', 'HUANUCO', 6303, 813, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(946, '100904', 'TOURNAVISTA', 'PUERTO INCA', 'HUANUCO', 4585, 1621, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(947, '100905', 'YUYAPICHIS', 'PUERTO INCA', 'HUANUCO', 6154, 1932, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(948, '101001', 'JESUS', 'LAURICOCHA', 'HUANUCO', 5786, 456, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(949, '101002', 'BANOS', 'LAURICOCHA', 'HUANUCO', 7035, 193, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(950, '101003', 'JIVIA', 'LAURICOCHA', 'HUANUCO', 2795, 62, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(951, '101004', 'QUEROPALCA', 'LAURICOCHA', 'HUANUCO', 2944, 133, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(952, '101005', 'RONDOS', 'LAURICOCHA', 'HUANUCO', 7648, 172, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(953, '101006', 'SAN FRANCISCO DE ASIS', 'LAURICOCHA', 'HUANUCO', 2173, 85, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(954, '101007', 'SAN MIGUEL DE CAURI', 'LAURICOCHA', 'HUANUCO', 10286, 822, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(955, '101101', 'CHAVINILLO', 'YAROWILCA', 'HUANUCO', 5992, 208, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(956, '101102', 'CAHUAC', 'YAROWILCA', 'HUANUCO', 4635, 30, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(957, '101103', 'CHACABAMBA', 'YAROWILCA', 'HUANUCO', 3679, 17, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(958, '101104', 'APARICIO POMARES', 'YAROWILCA', 'HUANUCO', 5594, 187, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(959, '101105', 'JACAS CHICO', 'YAROWILCA', 'HUANUCO', 2045, 69, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(960, '101106', 'OBAS', 'YAROWILCA', 'HUANUCO', 5528, 125, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(961, '101107', 'PAMPAMARCA', 'YAROWILCA', 'HUANUCO', 2071, 74, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(962, '101108', 'CHORAS', 'YAROWILCA', 'HUANUCO', 3691, 62, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(963, '110101', 'ICA', 'ICA', 'ICA', 32767, 886, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(964, '110102', 'LA TINGUINA', 'ICA', 'ICA', 32767, 81, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(965, '110103', 'LOS AQUIJES', 'ICA', 'ICA', 19259, 92, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(966, '110104', 'OCUCAJE', 'ICA', 'ICA', 3745, 1402, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(967, '110105', 'PACHACUTEC', 'ICA', 'ICA', 6729, 41, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(968, '110106', 'PARCONA', 'ICA', 'ICA', 32767, 18, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(969, '110107', 'PUEBLO NUEVO', 'ICA', 'ICA', 4784, 29, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(970, '110108', 'SALAS', 'ICA', 'ICA', 23504, 657, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(971, '110109', 'SAN JOSE DE LOS MOLINOS', 'ICA', 'ICA', 6235, 362, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(972, '110110', 'SAN JUAN BAUTISTA', 'ICA', 'ICA', 14663, 31, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(973, '110111', 'SANTIAGO', 'ICA', 'ICA', 29117, 2772, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(974, '110112', 'SUBTANJALLA', 'ICA', 'ICA', 27706, 186, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(975, '110113', 'TATE', 'ICA', 'ICA', 4574, 7, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(976, '110114', 'YAUCA DEL ROSARIO', 'ICA', 'ICA', 986, 1263, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(977, '110201', 'CHINCHA ALTA', 'CHINCHA', 'ICA', 32767, 226, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(978, '110202', 'ALTO LARAN', 'CHINCHA', 'ICA', 7387, 364, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(979, '110203', 'CHAVIN', 'CHINCHA', 'ICA', 1417, 426, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(980, '110204', 'CHINCHA BAJA', 'CHINCHA', 'ICA', 12323, 73, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(981, '110205', 'EL CARMEN', 'CHINCHA', 'ICA', 13296, 752, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(982, '110206', 'GROCIO PRADO', 'CHINCHA', 'ICA', 24049, 177, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(983, '110207', 'PUEBLO NUEVO', 'CHINCHA', 'ICA', 32767, 211, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(984, '110208', 'SAN JUAN DE YANAC', 'CHINCHA', 'ICA', 316, 439, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(985, '110209', 'SAN PEDRO DE HUACARPANA', 'CHINCHA', 'ICA', 1660, 206, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(986, '110210', 'SUNAMPE', 'CHINCHA', 'ICA', 27496, 16, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(987, '110211', 'TAMBO DE MORA', 'CHINCHA', 'ICA', 4990, 11, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(988, '110301', 'NAZCA', 'NAZCA', 'ICA', 26719, 1144, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(989, '110302', 'CHANGUILLO', 'NAZCA', 'ICA', 1537, 957, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(990, '110303', 'EL INGENIO', 'NAZCA', 'ICA', 2702, 612, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(991, '110304', 'MARCONA', 'NAZCA', 'ICA', 12403, 1945, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(992, '110305', 'VISTA ALEGRE', 'NAZCA', 'ICA', 15419, 534, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(993, '110401', 'PALPA', 'PALPA', 'ICA', 7195, 132, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(994, '110402', 'LLIPATA', 'PALPA', 'ICA', 1497, 182, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(995, '110403', 'RIO GRANDE', 'PALPA', 'ICA', 2268, 315, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(996, '110404', 'SANTA CRUZ', 'PALPA', 'ICA', 988, 254, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(997, '110405', 'TIBILLO', 'PALPA', 'ICA', 331, 337, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(998, '110501', 'PISCO', 'PISCO', 'ICA', 32767, 23, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(999, '110502', 'HUANCANO', 'PISCO', 'ICA', 1594, 900, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1000, '110503', 'HUMAY', 'PISCO', 'ICA', 5869, 912, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1001, '110504', 'INDEPENDENCIA', 'PISCO', 'ICA', 14390, 269, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1002, '110505', 'PARACAS', 'PISCO', 'ICA', 7009, 1427, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1003, '110506', 'SAN ANDRES', 'PISCO', 'ICA', 13539, 232, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1004, '110507', 'SAN CLEMENTE', 'PISCO', 'ICA', 21796, 134, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1005, '110508', 'TUPAC AMARU INCA', 'PISCO', 'ICA', 17651, 56, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1006, '120101', 'HUANCAYO', 'HUANCAYO', 'JUNIN', 32767, 224, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1007, '120104', 'CARHUACALLANGA', 'HUANCAYO', 'JUNIN', 1337, 13, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1008, '120105', 'CHACAPAMPA', 'HUANCAYO', 'JUNIN', 888, 126, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1009, '120106', 'CHICCHE', 'HUANCAYO', 'JUNIN', 968, 42, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1010, '120107', 'CHILCA', 'HUANCAYO', 'JUNIN', 32767, 29, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1011, '120108', 'CHONGOS ALTO', 'HUANCAYO', 'JUNIN', 1389, 702, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1012, '120111', 'CHUPURO', 'HUANCAYO', 'JUNIN', 1778, 40, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1013, '120112', 'COLCA', 'HUANCAYO', 'JUNIN', 2053, 111, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1014, '120113', 'CULLHUAS', 'HUANCAYO', 'JUNIN', 2247, 111, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1015, '120114', 'EL TAMBO', 'HUANCAYO', 'JUNIN', 32767, 167, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1016, '120116', 'HUACRAPUQUIO', 'HUANCAYO', 'JUNIN', 1284, 24, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1017, '120117', 'HUALHUAS', 'HUANCAYO', 'JUNIN', 4488, 14, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1018, '120119', 'HUANCAN', 'HUANCAYO', 'JUNIN', 20835, 10, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1019, '120120', 'HUASICANCHA', 'HUANCAYO', 'JUNIN', 859, 52, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1020, '120121', 'HUAYUCACHI', 'HUANCAYO', 'JUNIN', 8558, 14, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1021, '120122', 'INGENIO', 'HUANCAYO', 'JUNIN', 2503, 53, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1022, '120124', 'PARIAHUANCA', 'HUANCAYO', 'JUNIN', 5941, 581, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1023, '120125', 'PILCOMAYO', 'HUANCAYO', 'JUNIN', 16443, 10, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1024, '120126', 'PUCARA', 'HUANCAYO', 'JUNIN', 5063, 110, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1025, '120127', 'QUICHUAY', 'HUANCAYO', 'JUNIN', 1757, 31, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1026, '120128', 'QUILCAS', 'HUANCAYO', 'JUNIN', 4186, 161, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1027, '120129', 'SAN AGUSTIN', 'HUANCAYO', 'JUNIN', 11607, 26, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1028, '120130', 'SAN JERONIMO DE TUNAN', 'HUANCAYO', 'JUNIN', 10203, 22, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1029, '120132', 'SANO', 'HUANCAYO', 'JUNIN', 4026, 13, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1030, '120133', 'SAPALLANGA', 'HUANCAYO', 'JUNIN', 12769, 124, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1031, '120134', 'SICAYA', 'HUANCAYO', 'JUNIN', 7988, 41, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1032, '120135', 'SANTO DOMINGO DE ACOBAMBA', 'HUANCAYO', 'JUNIN', 7737, 878, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1033, '120136', 'VIQUES', 'HUANCAYO', 'JUNIN', 2222, 6, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1034, '120201', 'CONCEPCION', 'CONCEPCION', 'JUNIN', 14756, 18, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1035, '120202', 'ACO', 'CONCEPCION', 'JUNIN', 1642, 38, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1036, '120203', 'ANDAMARCA', 'CONCEPCION', 'JUNIN', 4638, 505, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1037, '120204', 'CHAMBARA', 'CONCEPCION', 'JUNIN', 2868, 100, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1038, '120205', 'COCHAS', 'CONCEPCION', 'JUNIN', 1829, 109, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1039, '120206', 'COMAS', 'CONCEPCION', 'JUNIN', 6258, 951, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1040, '120207', 'HEROINAS TOLEDO', 'CONCEPCION', 'JUNIN', 1222, 26, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1041, '120208', 'MANZANARES', 'CONCEPCION', 'JUNIN', 1401, 17, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1042, '120209', 'MARISCAL CASTILLA', 'CONCEPCION', 'JUNIN', 1633, 56, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1043, '120210', 'MATAHUASI', 'CONCEPCION', 'JUNIN', 5114, 23, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1044, '120211', 'MITO', 'CONCEPCION', 'JUNIN', 1372, 25, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1045, '120212', 'NUEVE DE JULIO', 'CONCEPCION', 'JUNIN', 1522, 7, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1046, '120213', 'ORCOTUNA', 'CONCEPCION', 'JUNIN', 4135, 45, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1047, '120214', 'SAN JOSE DE QUERO', 'CONCEPCION', 'JUNIN', 6080, 312, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1048, '120215', 'SANTA ROSA DE OCOPA', 'CONCEPCION', 'JUNIN', 2025, 16, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1049, '120301', 'CHANCHAMAYO', 'CHANCHAMAYO', 'JUNIN', 24675, 761, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1050, '120302', 'PERENE', 'CHANCHAMAYO', 'JUNIN', 32767, 1520, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1051, '120303', 'PICHANAQUI', 'CHANCHAMAYO', 'JUNIN', 32767, 1236, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1052, '120304', 'SAN LUIS DE SHUARO', 'CHANCHAMAYO', 'JUNIN', 7233, 161, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1053, '120305', 'SAN RAMON', 'CHANCHAMAYO', 'JUNIN', 27011, 623, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1054, '120306', 'VITOC', 'CHANCHAMAYO', 'JUNIN', 1866, 372, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1055, '120401', 'JAUJA', 'JAUJA', 'JUNIN', 14717, 9, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1056, '120402', 'ACOLLA', 'JAUJA', 'JUNIN', 7343, 121, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1057, '120403', 'APATA', 'JAUJA', 'JUNIN', 4198, 419, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1058, '120404', 'ATAURA', 'JAUJA', 'JUNIN', 1161, 6, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1059, '120405', 'CANCHAYLLO', 'JAUJA', 'JUNIN', 1658, 945, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1060, '120406', 'CURICACA', 'JAUJA', 'JUNIN', 1645, 66, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1061, '120407', 'EL MANTARO', 'JAUJA', 'JUNIN', 2541, 21, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1062, '120408', 'HUAMALI', 'JAUJA', 'JUNIN', 1815, 20, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1063, '120409', 'HUARIPAMPA', 'JAUJA', 'JUNIN', 867, 20, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1064, '120410', 'HUERTAS', 'JAUJA', 'JUNIN', 1664, 12, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1065, '120411', 'JANJAILLO', 'JAUJA', 'JUNIN', 717, 32, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1066, '120412', 'JULCAN', 'JAUJA', 'JUNIN', 697, 19, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1067, '120413', 'LEONOR ORDONEZ', 'JAUJA', 'JUNIN', 1492, 21, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1068, '120414', 'LLOCLLAPAMPA', 'JAUJA', 'JUNIN', 1058, 110, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1069, '120415', 'MARCO', 'JAUJA', 'JUNIN', 1659, 29, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1070, '120416', 'MASMA', 'JAUJA', 'JUNIN', 2069, 15, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1071, '120417', 'MASMA CHICCHE', 'JAUJA', 'JUNIN', 777, 34, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1072, '120418', 'MOLINOS', 'JAUJA', 'JUNIN', 1558, 315, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1073, '120419', 'MONOBAMBA', 'JAUJA', 'JUNIN', 1101, 961, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1074, '120420', 'MUQUI', 'JAUJA', 'JUNIN', 966, 11, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1075, '120421', 'MUQUIYAUYO', 'JAUJA', 'JUNIN', 2216, 21, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1076, '120422', 'PACA', 'JAUJA', 'JUNIN', 1027, 33, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1077, '120423', 'PACCHA', 'JAUJA', 'JUNIN', 1841, 89, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1078, '120424', 'PANCAN', 'JAUJA', 'JUNIN', 1285, 11, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1079, '120425', 'PARCO', 'JAUJA', 'JUNIN', 1208, 34, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1080, '120426', 'POMACANCHA', 'JAUJA', 'JUNIN', 1980, 284, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1081, '120427', 'RICRAN', 'JAUJA', 'JUNIN', 1626, 320, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1082, '120428', 'SAN LORENZO', 'JAUJA', 'JUNIN', 2447, 22, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1083, '120429', 'SAN PEDRO DE CHUNAN', 'JAUJA', 'JUNIN', 854, 9, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1084, '120430', 'SAUSA', 'JAUJA', 'JUNIN', 3009, 5, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1085, '120431', 'SINCOS', 'JAUJA', 'JUNIN', 4795, 233, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1086, '120432', 'TUNAN MARCA', 'JAUJA', 'JUNIN', 1196, 33, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1087, '120433', 'YAULI', 'JAUJA', 'JUNIN', 1353, 93, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1088, '120434', 'YAUYOS', 'JAUJA', 'JUNIN', 9256, 14, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1089, '120501', 'JUNIN', 'JUNIN', 'JUNIN', 9893, 869, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1090, '120502', 'CARHUAMAYO', 'JUNIN', 'JUNIN', 7784, 205, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1091, '120503', 'ONDORES', 'JUNIN', 'JUNIN', 1965, 558, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1092, '120504', 'ULCUMAYO', 'JUNIN', 'JUNIN', 5840, 994, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1093, '120601', 'SATIPO', 'SATIPO', 'JUNIN', 32767, 816, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1094, '120602', 'COVIRIALI', 'SATIPO', 'JUNIN', 6101, 97, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1095, '120603', 'LLAYLLA', 'SATIPO', 'JUNIN', 6168, 309, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1096, '120604', 'MAZAMARI', 'SATIPO', 'JUNIN', 32767, 2035, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1097, '120605', 'PAMPA HERMOSA', 'SATIPO', 'JUNIN', 10414, 947, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1098, '120606', 'PANGOA', 'SATIPO', 'JUNIN', 32767, 4244, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1099, '120607', 'RIO NEGRO', 'SATIPO', 'JUNIN', 28301, 488, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1100, '120608', 'RIO TAMBO', 'SATIPO', 'JUNIN', 32767, 10284, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1101, '120701', 'TARMA', 'TARMA', 'JUNIN', 32767, 454, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1102, '120702', 'ACOBAMBA', 'TARMA', 'JUNIN', 13419, 108, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1103, '120703', 'HUARICOLCA', 'TARMA', 'JUNIN', 3212, 163, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1104, '120704', 'HUASAHUASI', 'TARMA', 'JUNIN', 15239, 634, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1105, '120705', 'LA UNION', 'TARMA', 'JUNIN', 3225, 143, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1106, '120706', 'PALCA', 'TARMA', 'JUNIN', 5674, 328, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1107, '120707', 'PALCAMAYO', 'TARMA', 'JUNIN', 9305, 169, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1108, '120708', 'SAN PEDRO DE CAJAS', 'TARMA', 'JUNIN', 5633, 512, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1109, '120709', 'TAPO', 'TARMA', 'JUNIN', 5988, 153, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1110, '120801', 'LA OROYA', 'YAULI', 'JUNIN', 13637, 380, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1111, '120802', 'CHACAPALPA', 'YAULI', 'JUNIN', 737, 186, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1112, '120803', 'HUAY-HUAY', 'YAULI', 'JUNIN', 1494, 192, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1113, '120804', 'MARCAPOMACOCHA', 'YAULI', 'JUNIN', 1287, 885, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1114, '120805', 'MOROCOCHA', 'YAULI', 'JUNIN', 4432, 265, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1115, '120806', 'PACCHA', 'YAULI', 'JUNIN', 1669, 324, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1116, '120807', 'SANTA BARBARA DE CARHUACAYAN', 'YAULI', 'JUNIN', 2292, 680, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1117, '120808', 'SANTA ROSA DE SACCO', 'YAULI', 'JUNIN', 10421, 100, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1118, '120809', 'SUITUCANCHA', 'YAULI', 'JUNIN', 990, 214, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1119, '120810', 'YAULI', 'YAULI', 'JUNIN', 5211, 412, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1120, '120901', 'CHUPACA', 'CHUPACA', 'JUNIN', 21952, 23, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1121, '120902', 'AHUAC', 'CHUPACA', 'JUNIN', 5968, 72, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1122, '120903', 'CHONGOS BAJO', 'CHUPACA', 'JUNIN', 4031, 104, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1123, '120904', 'HUACHAC', 'CHUPACA', 'JUNIN', 3946, 20, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1124, '120905', 'HUAMANCACA CHICO', 'CHUPACA', 'JUNIN', 5912, 13, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1125, '120906', 'SAN JUAN DE YSCOS', 'CHUPACA', 'JUNIN', 2135, 26, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1126, '120907', 'SAN JUAN DE JARPA', 'CHUPACA', 'JUNIN', 3569, 128, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1127, '120908', 'TRES DE DICIEMBRE', 'CHUPACA', 'JUNIN', 2092, 16, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1128, '120909', 'YANACANCHA', 'CHUPACA', 'JUNIN', 3475, 762, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1129, '130101', 'TRUJILLO', 'TRUJILLO', 'LA LIBERTAD', 32767, 39, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1130, '130102', 'EL PORVENIR', 'TRUJILLO', 'LA LIBERTAD', 32767, 38, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1131, '130103', 'FLORENCIA DE MORA', 'TRUJILLO', 'LA LIBERTAD', 32767, 3, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1132, '130104', 'HUANCHACO', 'TRUJILLO', 'LA LIBERTAD', 32767, 318, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1133, '130105', 'LA ESPERANZA', 'TRUJILLO', 'LA LIBERTAD', 32767, 20, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1134, '130106', 'LAREDO', 'TRUJILLO', 'LA LIBERTAD', 32767, 491, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1135, '130107', 'MOCHE', 'TRUJILLO', 'LA LIBERTAD', 32767, 28, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1136, '130108', 'POROTO', 'TRUJILLO', 'LA LIBERTAD', 3195, 126, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1137, '130109', 'SALAVERRY', 'TRUJILLO', 'LA LIBERTAD', 18129, 290, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1138, '130110', 'SIMBAL', 'TRUJILLO', 'LA LIBERTAD', 4317, 389, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1139, '130111', 'VICTOR LARCO HERRERA', 'TRUJILLO', 'LA LIBERTAD', 32767, 12, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1140, '130201', 'ASCOPE', 'ASCOPE', 'LA LIBERTAD', 6677, 289, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1141, '130202', 'CHICAMA', 'ASCOPE', 'LA LIBERTAD', 15492, 897, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1142, '130203', 'CHOCOPE', 'ASCOPE', 'LA LIBERTAD', 9413, 101, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1143, '130204', 'MAGDALENA DE CAO', 'ASCOPE', 'LA LIBERTAD', 3232, 156, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1144, '130205', 'PAIJAN', 'ASCOPE', 'LA LIBERTAD', 25584, 82, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1145, '130206', 'RAZURI', 'ASCOPE', 'LA LIBERTAD', 9079, 313, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1146, '130207', 'SANTIAGO DE CAO', 'ASCOPE', 'LA LIBERTAD', 19660, 130, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1147, '130208', 'CASA GRANDE', 'ASCOPE', 'LA LIBERTAD', 31174, 674, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1148, '130301', 'BOLIVAR', 'BOLIVAR', 'LA LIBERTAD', 4838, 734, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1149, '130302', 'BAMBAMARCA', 'BOLIVAR', 'LA LIBERTAD', 3868, 165, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1150, '130303', 'CONDORMARCA', 'BOLIVAR', 'LA LIBERTAD', 2063, 324, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1151, '130304', 'LONGOTEA', 'BOLIVAR', 'LA LIBERTAD', 2232, 189, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1152, '130305', 'UCHUMARCA', 'BOLIVAR', 'LA LIBERTAD', 2759, 234, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1153, '130306', 'UCUNCHA', 'BOLIVAR', 'LA LIBERTAD', 815, 99, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1154, '130401', 'CHEPEN', 'CHEPEN', 'LA LIBERTAD', 32767, 289, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1155, '130402', 'PACANGA', 'CHEPEN', 'LA LIBERTAD', 23643, 586, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1156, '130403', 'PUEBLO NUEVO', 'CHEPEN', 'LA LIBERTAD', 14805, 271, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1157, '130501', 'JULCAN', 'JULCAN', 'LA LIBERTAD', 11662, 184, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1158, '130502', 'CALAMARCA', 'JULCAN', 'LA LIBERTAD', 5657, 208, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1159, '130503', 'CARABAMBA', 'JULCAN', 'LA LIBERTAD', 6518, 280, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1160, '130504', 'HUASO', 'JULCAN', 'LA LIBERTAD', 7253, 439, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1161, '130601', 'OTUZCO', 'OTUZCO', 'LA LIBERTAD', 27257, 444, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1162, '130602', 'AGALLPAMPA', 'OTUZCO', 'LA LIBERTAD', 9859, 257, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1163, '130604', 'CHARAT', 'OTUZCO', 'LA LIBERTAD', 2847, 65, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1164, '130605', 'HUARANCHAL', 'OTUZCO', 'LA LIBERTAD', 5077, 126, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1165, '130606', 'LA CUESTA', 'OTUZCO', 'LA LIBERTAD', 687, 41, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1166, '130608', 'MACHE', 'OTUZCO', 'LA LIBERTAD', 3112, 38, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1167, '130610', 'PARANDAY', 'OTUZCO', 'LA LIBERTAD', 730, 22, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1168, '130611', 'SALPO', 'OTUZCO', 'LA LIBERTAD', 6142, 196, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1169, '130613', 'SINSICAP', 'OTUZCO', 'LA LIBERTAD', 8619, 453, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1170, '130614', 'USQUIL', 'OTUZCO', 'LA LIBERTAD', 27383, 445, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1171, '130701', 'SAN PEDRO DE LLOC', 'PACASMAYO', 'LA LIBERTAD', 16519, 679, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1172, '130702', 'GUADALUPE', 'PACASMAYO', 'LA LIBERTAD', 32767, 166, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1173, '130703', 'JEQUETEPEQUE', 'PACASMAYO', 'LA LIBERTAD', 3808, 49, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1174, '130704', 'PACASMAYO', 'PACASMAYO', 'LA LIBERTAD', 27434, 35, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1175, '130705', 'SAN JOSE', 'PACASMAYO', 'LA LIBERTAD', 12259, 179, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1176, '130801', 'TAYABAMBA', 'PATAZ', 'LA LIBERTAD', 14586, 338, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1177, '130802', 'BULDIBUYO', 'PATAZ', 'LA LIBERTAD', 3763, 229, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1178, '130803', 'CHILLIA', 'PATAZ', 'LA LIBERTAD', 13402, 306, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1179, '130804', 'HUANCASPATA', 'PATAZ', 'LA LIBERTAD', 6390, 242, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1180, '130805', 'HUAYLILLAS', 'PATAZ', 'LA LIBERTAD', 3518, 92, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1181, '130806', 'HUAYO', 'PATAZ', 'LA LIBERTAD', 4373, 126, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1182, '130807', 'ONGON', 'PATAZ', 'LA LIBERTAD', 1761, 1308, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1183, '130808', 'PARCOY', 'PATAZ', 'LA LIBERTAD', 21784, 323, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1184, '130809', 'PATAZ', 'PATAZ', 'LA LIBERTAD', 8804, 394, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1185, '130810', 'PIAS', 'PATAZ', 'LA LIBERTAD', 1316, 270, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1186, '130811', 'SANTIAGO DE CHALLAS', 'PATAZ', 'LA LIBERTAD', 2533, 129, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1187, '130812', 'TAURIJA', 'PATAZ', 'LA LIBERTAD', 3004, 130, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1188, '130813', 'URPAY', 'PATAZ', 'LA LIBERTAD', 2804, 102, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1189, '130901', 'HUAMACHUCO', 'SANCHEZ CARRION', 'LA LIBERTAD', 32767, 415, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1190, '130902', 'CHUGAY', 'SANCHEZ CARRION', 'LA LIBERTAD', 18753, 417, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1191, '130903', 'COCHORCO', 'SANCHEZ CARRION', 'LA LIBERTAD', 9340, 262, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1192, '130904', 'CURGOS', 'SANCHEZ CARRION', 'LA LIBERTAD', 8526, 98, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1193, '130905', 'MARCABAL', 'SANCHEZ CARRION', 'LA LIBERTAD', 16698, 234, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1194, '130906', 'SANAGORAN', 'SANCHEZ CARRION', 'LA LIBERTAD', 14859, 338, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1195, '130907', 'SARIN', 'SANCHEZ CARRION', 'LA LIBERTAD', 9945, 334, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1196, '130908', 'SARTIMBAMBA', 'SANCHEZ CARRION', 'LA LIBERTAD', 13691, 397, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1197, '131001', 'SANTIAGO DE CHUCO', 'SANTIAGO DE CHUCO', 'LA LIBERTAD', 20372, 1078, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1198, '131002', 'ANGASMARCA', 'SANTIAGO DE CHUCO', 'LA LIBERTAD', 7266, 152, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1199, '131003', 'CACHICADAN', 'SANTIAGO DE CHUCO', 'LA LIBERTAD', 7964, 269, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1200, '131004', 'MOLLEBAMBA', 'SANTIAGO DE CHUCO', 'LA LIBERTAD', 2312, 69, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1201, '131005', 'MOLLEPATA', 'SANTIAGO DE CHUCO', 'LA LIBERTAD', 2666, 69, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1202, '131006', 'QUIRUVILCA', 'SANTIAGO DE CHUCO', 'LA LIBERTAD', 14295, 552, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1203, '131007', 'SANTA CRUZ DE CHUCA', 'SANTIAGO DE CHUCO', 'LA LIBERTAD', 3187, 165, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1204, '131008', 'SITABAMBA', 'SANTIAGO DE CHUCO', 'LA LIBERTAD', 3412, 319, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1205, '131101', 'CASCAS', 'GRAN CHIMU', 'LA LIBERTAD', 14187, 475, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1206, '131102', 'LUCMA', 'GRAN CHIMU', 'LA LIBERTAD', 6896, 295, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1207, '131103', 'COMPIN', 'GRAN CHIMU', 'LA LIBERTAD', 2118, 301, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1208, '131104', 'SAYAPULLO', 'GRAN CHIMU', 'LA LIBERTAD', 7908, 236, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1209, '131201', 'VIRU', 'VIRU', 'LA LIBERTAD', 32767, 1084, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1210, '131202', 'CHAO', 'VIRU', 'LA LIBERTAD', 32767, 1672, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1211, '131203', 'GUADALUPITO', 'VIRU', 'LA LIBERTAD', 9588, 426, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1212, '140101', 'CHICLAYO', 'CHICLAYO', 'LAMBAYEQUE', 32767, 50, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1213, '140102', 'CHONGOYAPE', 'CHICLAYO', 'LAMBAYEQUE', 17940, 723, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1214, '140103', 'ETEN', 'CHICLAYO', 'LAMBAYEQUE', 10571, 84, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1215, '140104', 'ETEN PUERTO', 'CHICLAYO', 'LAMBAYEQUE', 2167, 13, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1216, '140105', 'JOSE LEONARDO ORTIZ', 'CHICLAYO', 'LAMBAYEQUE', 32767, 29, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1217, '140106', 'LA VICTORIA', 'CHICLAYO', 'LAMBAYEQUE', 32767, 41, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1218, '140107', 'LAGUNAS', 'CHICLAYO', 'LAMBAYEQUE', 10234, 429, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1219, '140108', 'MONSEFU', 'CHICLAYO', 'LAMBAYEQUE', 31847, 45, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1220, '140109', 'NUEVA ARICA', 'CHICLAYO', 'LAMBAYEQUE', 2338, 212, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1221, '140110', 'OYOTUN', 'CHICLAYO', 'LAMBAYEQUE', 9854, 486, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1222, '140111', 'PICSI', 'CHICLAYO', 'LAMBAYEQUE', 9782, 53, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1223, '140112', 'PIMENTEL', 'CHICLAYO', 'LAMBAYEQUE', 32767, 65, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1224, '140113', 'REQUE', 'CHICLAYO', 'LAMBAYEQUE', 14942, 46, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1225, '140114', 'SANTA ROSA', 'CHICLAYO', 'LAMBAYEQUE', 12687, 13, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1226, '140115', 'SANA', 'CHICLAYO', 'LAMBAYEQUE', 12288, 311, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1227, '140116', 'CAYALTI', 'CHICLAYO', 'LAMBAYEQUE', 15967, 163, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1228, '140117', 'PATAPO', 'CHICLAYO', 'LAMBAYEQUE', 22452, 181, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1229, '140118', 'POMALCA', 'CHICLAYO', 'LAMBAYEQUE', 25323, 78, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1230, '140119', 'PUCALA', 'CHICLAYO', 'LAMBAYEQUE', 8979, 177, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1231, '140120', 'TUMAN', 'CHICLAYO', 'LAMBAYEQUE', 30194, 124, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1232, '140201', 'FERRENAFE', 'FERRENAFE', 'LAMBAYEQUE', 32767, 61, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1233, '140202', 'CANARIS', 'FERRENAFE', 'LAMBAYEQUE', 14516, 287, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1234, '140203', 'INCAHUASI', 'FERRENAFE', 'LAMBAYEQUE', 15518, 438, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1235, '140204', 'MANUEL ANTONIO MESONES MURO', 'FERRENAFE', 'LAMBAYEQUE', 4230, 211, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1236, '140205', 'PITIPO', 'FERRENAFE', 'LAMBAYEQUE', 23572, 550, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1237, '140206', 'PUEBLO NUEVO', 'FERRENAFE', 'LAMBAYEQUE', 13404, 31, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1238, '140301', 'LAMBAYEQUE', 'LAMBAYEQUE', 'LAMBAYEQUE', 32767, 328, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1239, '140302', 'CHOCHOPE', 'LAMBAYEQUE', 'LAMBAYEQUE', 1139, 78, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1240, '140303', 'ILLIMO', 'LAMBAYEQUE', 'LAMBAYEQUE', 9328, 25, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1241, '140304', 'JAYANCA', 'LAMBAYEQUE', 'LAMBAYEQUE', 17523, 685, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1242, '140305', 'MOCHUMI', 'LAMBAYEQUE', 'LAMBAYEQUE', 19158, 103, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1243, '140306', 'MORROPE', 'LAMBAYEQUE', 'LAMBAYEQUE', 32767, 1046, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1244, '140307', 'MOTUPE', 'LAMBAYEQUE', 'LAMBAYEQUE', 26409, 556, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1245, '140308', 'OLMOS', 'LAMBAYEQUE', 'LAMBAYEQUE', 32767, 5326, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1246, '140309', 'PACORA', 'LAMBAYEQUE', 'LAMBAYEQUE', 7190, 89, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1247, '140310', 'SALAS', 'LAMBAYEQUE', 'LAMBAYEQUE', 12999, 998, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1248, '140311', 'SAN JOSE', 'LAMBAYEQUE', 'LAMBAYEQUE', 16172, 45, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1249, '140312', 'TUCUME', 'LAMBAYEQUE', 'LAMBAYEQUE', 22805, 63, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1250, '150101', 'LIMA', 'LIMA', 'LIMA', 32767, 21, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1251, '150102', 'ANCON', 'LIMA', 'LIMA', 43, 317, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1252, '150103', 'ATE', 'LIMA', 'LIMA', 630, 87, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1253, '150104', 'BARRANCO', 'LIMA', 'LIMA', 29, 3, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1254, '150105', 'BRENA', 'LIMA', 'LIMA', 75, 3, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1255, '150106', 'CARABAYLLO', 'LIMA', 'LIMA', 301, 416, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1256, '150107', 'CHACLACAYO', 'LIMA', 'LIMA', 43, 46, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1257, '150108', 'CHORRILLOS', 'LIMA', 'LIMA', 325, 37, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1258, '150109', 'CIENEGUILLA', 'LIMA', 'LIMA', 47, 228, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1259, '150110', 'COMAS', 'LIMA', 'LIMA', 524, 49, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1260, '150111', 'EL AGUSTINO', 'LIMA', 'LIMA', 191, 15, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1261, '150112', 'INDEPENDENCIA', 'LIMA', 'LIMA', 216, 16, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1262, '150113', 'JESUS MARIA', 'LIMA', 'LIMA', 71, 4, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1263, '150114', 'LA MOLINA', 'LIMA', 'LIMA', 171, 52, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1264, '150115', 'LA VICTORIA', 'LIMA', 'LIMA', 171, 9, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1265, '150116', 'LINCE', 'LIMA', 'LIMA', 50, 3, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1266, '150117', 'LOS OLIVOS', 'LIMA', 'LIMA', 371, 18, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1267, '150118', 'LURIGANCHO', 'LIMA', 'LIMA', 218, 293, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1268, '150119', 'LURIN', 'LIMA', 'LIMA', 85, 157, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1269, '150120', 'MAGDALENA DEL MAR', 'LIMA', 'LIMA', 54, 3, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00');
INSERT INTO `zones` (`id`, `ubigeo`, `distrito`, `provincia`, `departamento`, `poblacion`, `area`, `estado`, `email`, `created_at`, `updated_at`) VALUES
(1270, '150121', 'PUEBLO LIBRE', 'LIMA', 'LIMA', 76, 5, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1271, '150122', 'MIRAFLORES', 'LIMA', 'LIMA', 81, 9, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1272, '150123', 'PACHACAMAC', 'LIMA', 'LIMA', 129, 197, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1273, '150124', 'PUCUSANA', 'LIMA', 'LIMA', 17, 90, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1274, '150125', 'PUENTE PIEDRA', 'LIMA', 'LIMA', 353, 57, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1275, '150126', 'PUNTA HERMOSA', 'LIMA', 'LIMA', 7, 3, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1276, '150127', 'PUNTA NEGRA', 'LIMA', 'LIMA', 7, 3, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1277, '150128', 'RIMAC', 'LIMA', 'LIMA', 164, 12, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1278, '150129', 'SAN BARTOLO', 'LIMA', 'LIMA', 7, 280, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1279, '150130', 'SAN BORJA', 'LIMA', 'LIMA', 111, 10, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1280, '150131', 'SAN ISIDRO', 'LIMA', 'LIMA', 54, 10, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1281, '150132', 'SAN JUAN DE LURIGANCHO', 'LIMA', 'LIMA', 1, 144, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1282, '150133', 'SAN JUAN DE MIRAFLORES', 'LIMA', 'LIMA', 404, 20, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1283, '150134', 'SAN LUIS', 'LIMA', 'LIMA', 57, 3, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1284, '150135', 'SAN MARTIN DE PORRES', 'LIMA', 'LIMA', 700, 36, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1285, '150136', 'SAN MIGUEL', 'LIMA', 'LIMA', 135, 10, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1286, '150137', 'SANTA ANITA', 'LIMA', 'LIMA', 228, 10, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1287, '150138', 'SANTA MARIA DEL MAR', 'LIMA', 'LIMA', 1, 11, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1288, '150139', 'SANTA ROSA', 'LIMA', 'LIMA', 18, 23, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1289, '150140', 'SANTIAGO DE SURCO', 'LIMA', 'LIMA', 344, 33, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1290, '150141', 'SURQUILLO', 'LIMA', 'LIMA', 91, 5, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1291, '150142', 'VILLA EL SALVADOR', 'LIMA', 'LIMA', 463, 35, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1292, '150143', 'VILLA MARIA DEL TRIUNFO', 'LIMA', 'LIMA', 448, 66, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1293, '150201', 'BARRANCA', 'BARRANCA', 'LIMA', 32767, 160, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1294, '150202', 'PARAMONGA', 'BARRANCA', 'LIMA', 22387, 414, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1295, '150203', 'PATIVILCA', 'BARRANCA', 'LIMA', 19272, 268, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1296, '150204', 'SUPE', 'BARRANCA', 'LIMA', 22543, 15, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1297, '150205', 'SUPE PUERTO', 'BARRANCA', 'LIMA', 11609, 519, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1298, '150301', 'CAJATAMBO', 'CAJATAMBO', 'LIMA', 2281, 582, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1299, '150302', 'COPA', 'CAJATAMBO', 'LIMA', 841, 204, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1300, '150303', 'GORGOR', 'CAJATAMBO', 'LIMA', 2683, 308, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1301, '150304', 'HUANCAPON', 'CAJATAMBO', 'LIMA', 1030, 151, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1302, '150305', 'MANAS', 'CAJATAMBO', 'LIMA', 993, 282, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1303, '150401', 'CANTA', 'CANTA', 'LIMA', 2794, 136, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1304, '150402', 'ARAHUAY', 'CANTA', 'LIMA', 750, 136, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1305, '150403', 'HUAMANTANGA', 'CANTA', 'LIMA', 1300, 494, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1306, '150404', 'HUAROS', 'CANTA', 'LIMA', 776, 327, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1307, '150405', 'LACHAQUI', 'CANTA', 'LIMA', 878, 129, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1308, '150406', 'SAN BUENAVENTURA', 'CANTA', 'LIMA', 526, 109, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1309, '150407', 'SANTA ROSA DE QUIVES', 'CANTA', 'LIMA', 8098, 311, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1310, '150501', 'SAN VICENTE DE CANETE', 'CANETE', 'LIMA', 32767, 519, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1311, '150502', 'ASIA', 'CANETE', 'LIMA', 9321, 282, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1312, '150503', 'CALANGO', 'CANETE', 'LIMA', 2377, 526, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1313, '150504', 'CERRO AZUL', 'CANETE', 'LIMA', 8053, 119, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1314, '150505', 'CHILCA', 'CANETE', 'LIMA', 15801, 449, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1315, '150506', 'COAYLLO', 'CANETE', 'LIMA', 1077, 600, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1316, '150507', 'IMPERIAL', 'CANETE', 'LIMA', 32767, 52, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1317, '150508', 'LUNAHUANA', 'CANETE', 'LIMA', 4812, 494, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1318, '150509', 'MALA', 'CANETE', 'LIMA', 32767, 131, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1319, '150510', 'NUEVO IMPERIAL', 'CANETE', 'LIMA', 23130, 320, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1320, '150511', 'PACARAN', 'CANETE', 'LIMA', 1791, 255, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1321, '150512', 'QUILMANA', 'CANETE', 'LIMA', 15200, 447, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1322, '150513', 'SAN ANTONIO', 'CANETE', 'LIMA', 4169, 38, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1323, '150514', 'SAN LUIS', 'CANETE', 'LIMA', 12971, 37, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1324, '150515', 'SANTA CRUZ DE FLORES', 'CANETE', 'LIMA', 2793, 96, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1325, '150516', 'ZUNIGA', 'CANETE', 'LIMA', 1818, 193, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1326, '150601', 'HUARAL', 'HUARAL', 'LIMA', 32767, 645, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1327, '150602', 'ATAVILLOS ALTO', 'HUARAL', 'LIMA', 712, 347, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1328, '150603', 'ATAVILLOS BAJO', 'HUARAL', 'LIMA', 1173, 175, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1329, '150604', 'AUCALLAMA', 'HUARAL', 'LIMA', 19502, 708, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1330, '150605', 'CHANCAY', 'HUARAL', 'LIMA', 32767, 155, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1331, '150606', 'IHUARI', 'HUARAL', 'LIMA', 2381, 473, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1332, '150607', 'LAMPIAN', 'HUARAL', 'LIMA', 416, 147, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1333, '150608', 'PACARAOS', 'HUARAL', 'LIMA', 490, 305, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1334, '150609', 'SAN MIGUEL DE ACOS', 'HUARAL', 'LIMA', 768, 42, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1335, '150610', 'SANTA CRUZ DE ANDAMARCA', 'HUARAL', 'LIMA', 1407, 216, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1336, '150611', 'SUMBILCA', 'HUARAL', 'LIMA', 986, 238, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1337, '150612', 'VEINTISIETE DE NOVIEMBRE', 'HUARAL', 'LIMA', 440, 207, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1338, '150701', 'MATUCANA', 'HUAROCHIRI', 'LIMA', 3680, 181, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1339, '150702', 'ANTIOQUIA', 'HUAROCHIRI', 'LIMA', 1238, 415, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1340, '150703', 'CALLAHUANCA', 'HUAROCHIRI', 'LIMA', 4080, 51, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1341, '150704', 'CARAMPOMA', 'HUAROCHIRI', 'LIMA', 1788, 231, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1342, '150705', 'CHICLA', 'HUAROCHIRI', 'LIMA', 7632, 235, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1343, '150706', 'CUENCA', 'HUAROCHIRI', 'LIMA', 395, 69, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1344, '150707', 'HUACHUPAMPA', 'HUAROCHIRI', 'LIMA', 2814, 79, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1345, '150708', 'HUANZA', 'HUAROCHIRI', 'LIMA', 2674, 233, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1346, '150709', 'HUAROCHIRI', 'HUAROCHIRI', 'LIMA', 1291, 240, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1347, '150710', 'LAHUAYTAMBO', 'HUAROCHIRI', 'LIMA', 674, 82, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1348, '150711', 'LANGA', 'HUAROCHIRI', 'LIMA', 851, 76, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1349, '150712', 'LARAOS', 'HUAROCHIRI', 'LIMA', 2298, 120, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1350, '150713', 'MARIATANA', 'HUAROCHIRI', 'LIMA', 1309, 171, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1351, '150714', 'RICARDO PALMA', 'HUAROCHIRI', 'LIMA', 6103, 34, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1352, '150715', 'SAN ANDRES DE TUPICOCHA', 'HUAROCHIRI', 'LIMA', 1268, 97, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1353, '150716', 'SAN ANTONIO', 'HUAROCHIRI', 'LIMA', 5469, 525, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1354, '150717', 'SAN BARTOLOME', 'HUAROCHIRI', 'LIMA', 2271, 42, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1355, '150718', 'SAN DAMIAN', 'HUAROCHIRI', 'LIMA', 1183, 333, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1356, '150719', 'SAN JUAN DE IRIS', 'HUAROCHIRI', 'LIMA', 1772, 127, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1357, '150720', 'SAN JUAN DE TANTARANCHE', 'HUAROCHIRI', 'LIMA', 471, 137, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1358, '150721', 'SAN LORENZO DE QUINTI', 'HUAROCHIRI', 'LIMA', 1532, 452, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1359, '150722', 'SAN MATEO', 'HUAROCHIRI', 'LIMA', 5017, 419, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1360, '150723', 'SAN MATEO DE OTAO', 'HUAROCHIRI', 'LIMA', 1603, 136, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1361, '150724', 'SAN PEDRO DE CASTA', 'HUAROCHIRI', 'LIMA', 1303, 82, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1362, '150725', 'SAN PEDRO DE HUANCAYRE', 'HUAROCHIRI', 'LIMA', 246, 40, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1363, '150726', 'SANGALLAYA', 'HUAROCHIRI', 'LIMA', 576, 91, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1364, '150727', 'SANTA CRUZ DE COCACHACRA', 'HUAROCHIRI', 'LIMA', 2477, 33, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1365, '150728', 'SANTA EULALIA', 'HUAROCHIRI', 'LIMA', 11787, 109, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1366, '150729', 'SANTIAGO DE ANCHUCAYA', 'HUAROCHIRI', 'LIMA', 522, 95, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1367, '150730', 'SANTIAGO DE TUNA', 'HUAROCHIRI', 'LIMA', 729, 63, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1368, '150731', 'SANTO DOMINGO DE LOS OLLEROS', 'HUAROCHIRI', 'LIMA', 4705, 577, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1369, '150732', 'SURCO', 'HUAROCHIRI', 'LIMA', 1938, 107, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1370, '150801', 'HUACHO', 'HUAURA', 'LIMA', 32767, 711, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1371, '150802', 'AMBAR', 'HUAURA', 'LIMA', 2737, 941, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1372, '150803', 'CALETA DE CARQUIN', 'HUAURA', 'LIMA', 6801, 4, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1373, '150804', 'CHECRAS', 'HUAURA', 'LIMA', 1781, 167, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1374, '150805', 'HUALMAY', 'HUAURA', 'LIMA', 28589, 7, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1375, '150806', 'HUAURA', 'HUAURA', 'LIMA', 32767, 481, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1376, '150807', 'LEONCIO PRADO', 'HUAURA', 'LIMA', 1980, 294, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1377, '150808', 'PACCHO', 'HUAURA', 'LIMA', 2189, 237, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1378, '150809', 'SANTA LEONOR', 'HUAURA', 'LIMA', 1455, 375, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1379, '150810', 'SANTA MARIA', 'HUAURA', 'LIMA', 32767, 135, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1380, '150811', 'SAYAN', 'HUAURA', 'LIMA', 24095, 1314, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1381, '150812', 'VEGUETA', 'HUAURA', 'LIMA', 22031, 257, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1382, '150901', 'OYON', 'OYON', 'LIMA', 14479, 873, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1383, '150902', 'ANDAJES', 'OYON', 'LIMA', 1045, 158, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1384, '150903', 'CAUJUL', 'OYON', 'LIMA', 1036, 97, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1385, '150904', 'COCHAMARCA', 'OYON', 'LIMA', 1607, 258, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1386, '150905', 'NAVAN', 'OYON', 'LIMA', 1192, 231, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1387, '150906', 'PACHANGARA', 'OYON', 'LIMA', 3423, 254, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1388, '151001', 'YAUYOS', 'YAUYOS', 'LIMA', 2791, 332, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1389, '151002', 'ALIS', 'YAUYOS', 'LIMA', 1203, 141, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1390, '151003', 'AYAUCA', 'YAUYOS', 'LIMA', 2203, 441, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1391, '151004', 'AYAVIRI', 'YAUYOS', 'LIMA', 690, 244, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1392, '151005', 'AZANGARO', 'YAUYOS', 'LIMA', 532, 78, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1393, '151006', 'CACRA', 'YAUYOS', 'LIMA', 384, 212, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1394, '151007', 'CARANIA', 'YAUYOS', 'LIMA', 367, 122, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1395, '151008', 'CATAHUASI', 'YAUYOS', 'LIMA', 951, 126, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1396, '151009', 'CHOCOS', 'YAUYOS', 'LIMA', 1194, 211, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1397, '151010', 'COCHAS', 'YAUYOS', 'LIMA', 419, 38, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1398, '151011', 'COLONIA', 'YAUYOS', 'LIMA', 1315, 319, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1399, '151012', 'HONGOS', 'YAUYOS', 'LIMA', 396, 122, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1400, '151013', 'HUAMPARA', 'YAUYOS', 'LIMA', 188, 55, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1401, '151014', 'HUANCAYA', 'YAUYOS', 'LIMA', 1330, 279, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1402, '151015', 'HUANGASCAR', 'YAUYOS', 'LIMA', 570, 50, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1403, '151016', 'HUANTAN', 'YAUYOS', 'LIMA', 941, 512, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1404, '151017', 'HUANEC', 'YAUYOS', 'LIMA', 484, 42, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1405, '151018', 'LARAOS', 'YAUYOS', 'LIMA', 762, 411, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1406, '151019', 'LINCHA', 'YAUYOS', 'LIMA', 917, 217, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1407, '151020', 'MADEAN', 'YAUYOS', 'LIMA', 808, 208, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1408, '151021', 'MIRAFLORES', 'YAUYOS', 'LIMA', 448, 202, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1409, '151022', 'OMAS', 'YAUYOS', 'LIMA', 578, 294, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1410, '151023', 'PUTINZA', 'YAUYOS', 'LIMA', 481, 61, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1411, '151024', 'QUINCHES', 'YAUYOS', 'LIMA', 978, 116, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1412, '151025', 'QUINOCAY', 'YAUYOS', 'LIMA', 540, 158, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1413, '151026', 'SAN JOAQUIN', 'YAUYOS', 'LIMA', 428, 76, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1414, '151027', 'SAN PEDRO DE PILAS', 'YAUYOS', 'LIMA', 374, 99, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1415, '151028', 'TANTA', 'YAUYOS', 'LIMA', 505, 343, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1416, '151029', 'TAURIPAMPA', 'YAUYOS', 'LIMA', 428, 531, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1417, '151030', 'TOMAS', 'YAUYOS', 'LIMA', 1127, 294, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1418, '151031', 'TUPE', 'YAUYOS', 'LIMA', 649, 318, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1419, '151032', 'VINAC', 'YAUYOS', 'LIMA', 1848, 157, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1420, '151033', 'VITIS', 'YAUYOS', 'LIMA', 630, 104, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1421, '160101', 'IQUITOS', 'MAYNAS', 'LORETO', 32767, 361, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1422, '160102', 'ALTO NANAY', 'MAYNAS', 'LORETO', 2784, 14526, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1423, '160103', 'FERNANDO LORES', 'MAYNAS', 'LORETO', 20225, 4546, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1424, '160104', 'INDIANA', 'MAYNAS', 'LORETO', 11301, 3348, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1425, '160105', 'LAS AMAZONAS', 'MAYNAS', 'LORETO', 9885, 6690, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1426, '160106', 'MAZAN', 'MAYNAS', 'LORETO', 13779, 10082, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1427, '160107', 'NAPO', 'MAYNAS', 'LORETO', 16286, 24643, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1428, '160108', 'PUNCHANA', 'MAYNAS', 'LORETO', 32767, 1598, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1429, '160109', 'PUTUMAYO', 'MAYNAS', 'LORETO', 6187, 32767, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1430, '160110', 'TORRES CAUSANA', 'MAYNAS', 'LORETO', 5152, 7550, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1431, '160112', 'BELEN', 'MAYNAS', 'LORETO', 32767, 647, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1432, '160113', 'SAN JUAN BAUTISTA', 'MAYNAS', 'LORETO', 32767, 3166, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1433, '160114', 'TENIENTE MANUEL CLAVERO', 'MAYNAS', 'LORETO', 5657, 9580, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1434, '160201', 'YURIMAGUAS', 'ALTO AMAZONAS', 'LORETO', 32767, 2718, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1435, '160202', 'BALSAPUERTO', 'ALTO AMAZONAS', 'LORETO', 17436, 2939, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1436, '160205', 'JEBEROS', 'ALTO AMAZONAS', 'LORETO', 5271, 5282, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1437, '160206', 'LAGUNAS', 'ALTO AMAZONAS', 'LORETO', 14308, 6241, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1438, '160210', 'SANTA CRUZ', 'ALTO AMAZONAS', 'LORETO', 4449, 1112, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1439, '160211', 'TENIENTE CESAR LOPEZ ROJAS', 'ALTO AMAZONAS', 'LORETO', 6587, 1521, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1440, '160301', 'NAUTA', 'LORETO', 'LORETO', 30086, 6782, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1441, '160302', 'PARINARI', 'LORETO', 'LORETO', 7264, 13167, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1442, '160303', 'TIGRE', 'LORETO', 'LORETO', 8421, 20174, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1443, '160304', 'TROMPETEROS', 'LORETO', 'LORETO', 10745, 12472, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1444, '160305', 'URARINAS', 'LORETO', 'LORETO', 14716, 16040, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1445, '160401', 'RAMON CASTILLA', 'MARISCAL RAMON CASTILLA', 'LORETO', 24141, 7104, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1446, '160402', 'PEBAS', 'MARISCAL RAMON CASTILLA', 'LORETO', 17061, 11595, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1447, '160403', 'YAVARI', 'MARISCAL RAMON CASTILLA', 'LORETO', 15638, 14061, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1448, '160404', 'SAN PABLO', 'MARISCAL RAMON CASTILLA', 'LORETO', 16069, 5109, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1449, '160501', 'REQUENA', 'REQUENA', 'LORETO', 30156, 3088, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1450, '160502', 'ALTO TAPICHE', 'REQUENA', 'LORETO', 2106, 8862, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1451, '160503', 'CAPELO', 'REQUENA', 'LORETO', 4454, 856, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1452, '160504', 'EMILIO SAN MARTIN', 'REQUENA', 'LORETO', 7488, 4648, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1453, '160505', 'MAQUIA', 'REQUENA', 'LORETO', 8371, 4872, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1454, '160506', 'PUINAHUA', 'REQUENA', 'LORETO', 6017, 6046, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1455, '160507', 'SAQUENA', 'REQUENA', 'LORETO', 4927, 2114, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1456, '160508', 'SOPLIN', 'REQUENA', 'LORETO', 690, 4788, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1457, '160509', 'TAPICHE', 'REQUENA', 'LORETO', 1211, 2047, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1458, '160510', 'JENARO HERRERA', 'REQUENA', 'LORETO', 5632, 1542, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1459, '160511', 'YAQUERANA', 'REQUENA', 'LORETO', 2989, 11205, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1460, '160601', 'CONTAMANA', 'UCAYALI', 'LORETO', 27273, 10852, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1461, '160602', 'INAHUAYA', 'UCAYALI', 'LORETO', 2659, 657, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1462, '160603', 'PADRE MARQUEZ', 'UCAYALI', 'LORETO', 7597, 2517, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1463, '160604', 'PAMPA HERMOSA', 'UCAYALI', 'LORETO', 10630, 7468, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1464, '160605', 'SARAYACU', 'UCAYALI', 'LORETO', 16569, 6408, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1465, '160606', 'VARGAS GUERRA', 'UCAYALI', 'LORETO', 8932, 1877, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1466, '160701', 'BARRANCA', 'DATEM DEL MARANON', 'LORETO', 13608, 6803, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1467, '160702', 'CAHUAPANAS', 'DATEM DEL MARANON', 'LORETO', 8331, 4957, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1468, '160703', 'MANSERICHE', 'DATEM DEL MARANON', 'LORETO', 10370, 4315, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1469, '160704', 'MORONA', 'DATEM DEL MARANON', 'LORETO', 13024, 9653, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1470, '160705', 'PASTAZA', 'DATEM DEL MARANON', 'LORETO', 6363, 8444, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1471, '160706', 'ANDOAS', 'DATEM DEL MARANON', 'LORETO', 12364, 12519, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1472, '170101', 'TAMBOPATA', 'TAMBOPATA', 'MADRE DE DIOS', 32767, 20677, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1473, '170102', 'INAMBARI', 'TAMBOPATA', 'MADRE DE DIOS', 10110, 4878, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1474, '170103', 'LAS PIEDRAS', 'TAMBOPATA', 'MADRE DE DIOS', 5826, 7492, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1475, '170104', 'LABERINTO', 'TAMBOPATA', 'MADRE DE DIOS', 5091, 2815, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1476, '170201', 'MANU', 'MANU', 'MADRE DE DIOS', 3118, 8609, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1477, '170202', 'FITZCARRALD', 'MANU', 'MADRE DE DIOS', 1536, 10526, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1478, '170203', 'MADRE DE DIOS', 'MANU', 'MADRE DE DIOS', 12810, 7754, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1479, '170204', 'HUEPETUHE', 'MANU', 'MADRE DE DIOS', 6633, 1499, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1480, '170301', 'INAPARI', 'TAHUAMANU', 'MADRE DE DIOS', 1555, 14343, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1481, '170302', 'IBERIA', 'TAHUAMANU', 'MADRE DE DIOS', 8836, 2556, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1482, '170303', 'TAHUAMANU', 'TAHUAMANU', 'MADRE DE DIOS', 3423, 3430, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1483, '180101', 'MOQUEGUA', 'MARISCAL NIETO', 'MOQUEGUA', 32767, 3948, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1484, '180102', 'CARUMAS', 'MARISCAL NIETO', 'MOQUEGUA', 5602, 2263, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1485, '180103', 'CUCHUMBAYA', 'MARISCAL NIETO', 'MOQUEGUA', 2177, 69, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1486, '180104', 'SAMEGUA', 'MARISCAL NIETO', 'MOQUEGUA', 6496, 64, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1487, '180105', 'SAN CRISTOBAL', 'MARISCAL NIETO', 'MOQUEGUA', 4058, 542, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1488, '180106', 'TORATA', 'MARISCAL NIETO', 'MOQUEGUA', 5874, 1772, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1489, '180201', 'OMATE', 'GENERAL SANCHEZ CERRO', 'MOQUEGUA', 4477, 255, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1490, '180202', 'CHOJATA', 'GENERAL SANCHEZ CERRO', 'MOQUEGUA', 2573, 858, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1491, '180203', 'COALAQUE', 'GENERAL SANCHEZ CERRO', 'MOQUEGUA', 1125, 247, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1492, '180204', 'ICHUNA', 'GENERAL SANCHEZ CERRO', 'MOQUEGUA', 4826, 999, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1493, '180205', 'LA CAPILLA', 'GENERAL SANCHEZ CERRO', 'MOQUEGUA', 2213, 775, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1494, '180206', 'LLOQUE', 'GENERAL SANCHEZ CERRO', 'MOQUEGUA', 1975, 255, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1495, '180207', 'MATALAQUE', 'GENERAL SANCHEZ CERRO', 'MOQUEGUA', 1187, 562, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1496, '180208', 'PUQUINA', 'GENERAL SANCHEZ CERRO', 'MOQUEGUA', 2521, 593, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1497, '180209', 'QUINISTAQUILLAS', 'GENERAL SANCHEZ CERRO', 'MOQUEGUA', 1410, 194, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1498, '180210', 'UBINAS', 'GENERAL SANCHEZ CERRO', 'MOQUEGUA', 3649, 877, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1499, '180211', 'YUNGA', 'GENERAL SANCHEZ CERRO', 'MOQUEGUA', 2377, 112, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1500, '180301', 'ILO', 'ILO', 'MOQUEGUA', 32767, 294, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1501, '180302', 'EL ALGARROBAL', 'ILO', 'MOQUEGUA', 320, 737, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1502, '180303', 'PACOCHA', 'ILO', 'MOQUEGUA', 3498, 334, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1503, '190101', 'CHAUPIMARCA', 'PASCO', 'PASCO', 26085, 15, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1504, '190102', 'HUACHON', 'PASCO', 'PASCO', 4722, 466, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1505, '190103', 'HUARIACA', 'PASCO', 'PASCO', 8257, 123, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1506, '190104', 'HUAYLLAY', 'PASCO', 'PASCO', 11412, 1018, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1507, '190105', 'NINACACA', 'PASCO', 'PASCO', 3418, 541, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1508, '190106', 'PALLANCHACRA', 'PASCO', 'PASCO', 4866, 72, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1509, '190107', 'PAUCARTAMBO', 'PASCO', 'PASCO', 24303, 721, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1510, '190108', 'SAN FRANCISCO DE ASIS DE YARUSYACAN', 'PASCO', 'PASCO', 9901, 122, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1511, '190109', 'SIMON BOLIVAR', 'PASCO', 'PASCO', 11913, 689, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1512, '190110', 'TICLACAYAN', 'PASCO', 'PASCO', 13285, 558, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1513, '190111', 'TINYAHUARCO', 'PASCO', 'PASCO', 6286, 96, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1514, '190112', 'VICCO', 'PASCO', 'PASCO', 2292, 173, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1515, '190113', 'YANACANCHA', 'PASCO', 'PASCO', 30570, 156, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1516, '190201', 'YANAHUANCA', 'DANIEL ALCIDES CARRION', 'PASCO', 12922, 745, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1517, '190202', 'CHACAYAN', 'DANIEL ALCIDES CARRION', 'PASCO', 4295, 163, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1518, '190203', 'GOYLLARISQUIZGA', 'DANIEL ALCIDES CARRION', 'PASCO', 3896, 16, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1519, '190204', 'PAUCAR', 'DANIEL ALCIDES CARRION', 'PASCO', 1797, 108, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1520, '190205', 'SAN PEDRO DE PILLAO', 'DANIEL ALCIDES CARRION', 'PASCO', 1823, 74, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1521, '190206', 'SANTA ANA DE TUSI', 'DANIEL ALCIDES CARRION', 'PASCO', 22945, 285, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1522, '190207', 'TAPUC', 'DANIEL ALCIDES CARRION', 'PASCO', 4360, 49, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1523, '190208', 'VILCABAMBA', 'DANIEL ALCIDES CARRION', 'PASCO', 1609, 83, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1524, '190301', 'OXAPAMPA', 'OXAPAMPA', 'PASCO', 14257, 414, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1525, '190302', 'CHONTABAMBA', 'OXAPAMPA', 'PASCO', 3504, 442, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1526, '190303', 'HUANCABAMBA', 'OXAPAMPA', 'PASCO', 6536, 1245, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1527, '190304', 'PALCAZU', 'OXAPAMPA', 'PASCO', 10710, 2868, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1528, '190305', 'POZUZO', 'OXAPAMPA', 'PASCO', 9342, 1249, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1529, '190306', 'PUERTO BERMUDEZ', 'OXAPAMPA', 'PASCO', 28669, 10692, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1530, '190307', 'VILLA RICA', 'OXAPAMPA', 'PASCO', 20183, 786, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1531, '200101', 'PIURA', 'PIURA', 'PIURA', 32767, 318, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1532, '200104', 'CASTILLA', 'PIURA', 'PIURA', 32767, 660, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1533, '200105', 'CATACAOS', 'PIURA', 'PIURA', 32767, 2535, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1534, '200107', 'CURA MORI', 'PIURA', 'PIURA', 18639, 196, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1535, '200108', 'EL TALLAN', 'PIURA', 'PIURA', 4962, 106, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1536, '200109', 'LA ARENA', 'PIURA', 'PIURA', 32767, 168, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1537, '200110', 'LA UNION', 'PIURA', 'PIURA', 32767, 209, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1538, '200111', 'LAS LOMAS', 'PIURA', 'PIURA', 26768, 510, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1539, '200114', 'TAMBO GRANDE', 'PIURA', 'PIURA', 32767, 1447, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1540, '200201', 'AYABACA', 'AYABACA', 'PIURA', 32767, 1544, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1541, '200202', 'FRIAS', 'AYABACA', 'PIURA', 24203, 559, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1542, '200203', 'JILILI', 'AYABACA', 'PIURA', 2775, 99, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1543, '200204', 'LAGUNAS', 'AYABACA', 'PIURA', 7251, 196, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1544, '200205', 'MONTERO', 'AYABACA', 'PIURA', 6683, 130, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1545, '200206', 'PACAIPAMPA', 'AYABACA', 'PIURA', 24796, 970, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1546, '200207', 'PAIMAS', 'AYABACA', 'PIURA', 10332, 325, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1547, '200208', 'SAPILLICA', 'AYABACA', 'PIURA', 12194, 271, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1548, '200209', 'SICCHEZ', 'AYABACA', 'PIURA', 1897, 34, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1549, '200210', 'SUYO', 'AYABACA', 'PIURA', 12287, 1069, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1550, '200301', 'HUANCABAMBA', 'HUANCABAMBA', 'PIURA', 30404, 445, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1551, '200302', 'CANCHAQUE', 'HUANCABAMBA', 'PIURA', 8235, 313, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1552, '200303', 'EL CARMEN DE LA FRONTERA', 'HUANCABAMBA', 'PIURA', 13864, 653, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1553, '200304', 'HUARMACA', 'HUANCABAMBA', 'PIURA', 32767, 1938, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1554, '200305', 'LALAQUIZ', 'HUANCABAMBA', 'PIURA', 4626, 146, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1555, '200306', 'SAN MIGUEL DE EL FAIQUE', 'HUANCABAMBA', 'PIURA', 8994, 208, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1556, '200307', 'SONDOR', 'HUANCABAMBA', 'PIURA', 8564, 344, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1557, '200308', 'SONDORILLO', 'HUANCABAMBA', 'PIURA', 10758, 230, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1558, '200401', 'CHULUCANAS', 'MORROPON', 'PIURA', 32767, 862, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1559, '200402', 'BUENOS AIRES', 'MORROPON', 'PIURA', 7985, 246, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1560, '200403', 'CHALACO', 'MORROPON', 'PIURA', 8992, 149, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1561, '200404', 'LA MATANZA', 'MORROPON', 'PIURA', 12761, 1036, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1562, '200405', 'MORROPON', 'MORROPON', 'PIURA', 14099, 171, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1563, '200406', 'SALITRAL', 'MORROPON', 'PIURA', 8409, 610, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1564, '200407', 'SAN JUAN DE BIGOTE', 'MORROPON', 'PIURA', 6566, 250, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1565, '200408', 'SANTA CATALINA DE MOSSA', 'MORROPON', 'PIURA', 4095, 80, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1566, '200409', 'SANTO DOMINGO', 'MORROPON', 'PIURA', 7207, 190, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1567, '200410', 'YAMANGO', 'MORROPON', 'PIURA', 9567, 217, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1568, '200501', 'PAITA', 'PAITA', 'PIURA', 32767, 731, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1569, '200502', 'AMOTAPE', 'PAITA', 'PIURA', 2310, 61, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1570, '200503', 'ARENAL', 'PAITA', 'PIURA', 1006, 8, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1571, '200504', 'COLAN', 'PAITA', 'PIURA', 12429, 123, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1572, '200505', 'LA HUACA', 'PAITA', 'PIURA', 11696, 596, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1573, '200506', 'TAMARINDO', 'PAITA', 'PIURA', 4555, 66, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1574, '200507', 'VICHAYAL', 'PAITA', 'PIURA', 4761, 158, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1575, '200601', 'SULLANA', 'SULLANA', 'PIURA', 32767, 487, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1576, '200602', 'BELLAVISTA', 'SULLANA', 'PIURA', 32767, 2, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1577, '200603', 'IGNACIO ESCUDERO', 'SULLANA', 'PIURA', 19987, 183, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1578, '200604', 'LANCONES', 'SULLANA', 'PIURA', 13245, 2164, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1579, '200605', 'MARCAVELICA', 'SULLANA', 'PIURA', 28876, 1647, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1580, '200606', 'MIGUEL CHECA', 'SULLANA', 'PIURA', 8639, 456, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1581, '200607', 'QUERECOTILLO', 'SULLANA', 'PIURA', 25290, 282, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1582, '200608', 'SALITRAL', 'SULLANA', 'PIURA', 6663, 31, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1583, '200701', 'PARINAS', 'TALARA', 'PIURA', 32767, 1122, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1584, '200702', 'EL ALTO', 'TALARA', 'PIURA', 7056, 479, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1585, '200703', 'LA BREA', 'TALARA', 'PIURA', 11817, 830, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1586, '200704', 'LOBITOS', 'TALARA', 'PIURA', 1646, 237, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1587, '200705', 'LOS ORGANOS', 'TALARA', 'PIURA', 9411, 163, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1588, '200706', 'MANCORA', 'TALARA', 'PIURA', 12888, 97, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1589, '200801', 'SECHURA', 'SECHURA', 'PIURA', 32767, 5731, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1590, '200802', 'BELLAVISTA DE LA UNION', 'SECHURA', 'PIURA', 4303, 15, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1591, '200803', 'BERNAL', 'SECHURA', 'PIURA', 7276, 70, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1592, '200804', 'CRISTO NOS VALGA', 'SECHURA', 'PIURA', 3878, 262, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1593, '200805', 'VICE', 'SECHURA', 'PIURA', 14108, 334, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1594, '200806', 'RINCONADA LLICUAR', 'SECHURA', 'PIURA', 3113, 19, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1595, '210101', 'PUNO', 'PUNO', 'PUNO', 32767, 460, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1596, '210102', 'ACORA', 'PUNO', 'PUNO', 28189, 1937, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1597, '210103', 'AMANTANI', 'PUNO', 'PUNO', 4447, 163, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1598, '210104', 'ATUNCOLLA', 'PUNO', 'PUNO', 5653, 159, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1599, '210105', 'CAPACHICA', 'PUNO', 'PUNO', 11336, 99, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1600, '210106', 'CHUCUITO', 'PUNO', 'PUNO', 7012, 89, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1601, '210107', 'COATA', 'PUNO', 'PUNO', 8034, 104, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1602, '210108', 'HUATA', 'PUNO', 'PUNO', 10353, 129, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1603, '210109', 'MANAZO', 'PUNO', 'PUNO', 5369, 402, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1604, '210110', 'PAUCARCOLLA', 'PUNO', 'PUNO', 5135, 175, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1605, '210111', 'PICHACANI', 'PUNO', 'PUNO', 5324, 1639, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1606, '210112', 'PLATERIA', 'PUNO', 'PUNO', 7743, 249, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1607, '210113', 'SAN ANTONIO', 'PUNO', 'PUNO', 3799, 337, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1608, '210114', 'TIQUILLACA', 'PUNO', 'PUNO', 1790, 489, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1609, '210115', 'VILQUE', 'PUNO', 'PUNO', 3129, 193, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1610, '210201', 'AZANGARO', 'AZANGARO', 'PUNO', 28195, 721, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1611, '210202', 'ACHAYA', 'AZANGARO', 'PUNO', 4479, 126, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1612, '210203', 'ARAPA', 'AZANGARO', 'PUNO', 7483, 337, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1613, '210204', 'ASILLO', 'AZANGARO', 'PUNO', 17407, 404, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1614, '210205', 'CAMINACA', 'AZANGARO', 'PUNO', 3564, 147, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1615, '210206', 'CHUPA', 'AZANGARO', 'PUNO', 13045, 151, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1616, '210207', 'JOSE DOMINGO CHOQUEHUANCA', 'AZANGARO', 'PUNO', 5458, 67, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1617, '210208', 'MUNANI', 'AZANGARO', 'PUNO', 8180, 785, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1618, '210209', 'POTONI', 'AZANGARO', 'PUNO', 6456, 623, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1619, '210210', 'SAMAN', 'AZANGARO', 'PUNO', 14249, 337, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1620, '210211', 'SAN ANTON', 'AZANGARO', 'PUNO', 9978, 513, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1621, '210212', 'SAN JOSE', 'AZANGARO', 'PUNO', 5751, 397, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1622, '210213', 'SAN JUAN DE SALINAS', 'AZANGARO', 'PUNO', 4325, 104, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1623, '210214', 'SANTIAGO DE PUPUJA', 'AZANGARO', 'PUNO', 5172, 319, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1624, '210215', 'TIRAPATA', 'AZANGARO', 'PUNO', 3077, 200, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1625, '210301', 'MACUSANI', 'CARABAYA', 'PUNO', 12869, 1017, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1626, '210302', 'AJOYANI', 'CARABAYA', 'PUNO', 2079, 427, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1627, '210303', 'AYAPATA', 'CARABAYA', 'PUNO', 11975, 776, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1628, '210304', 'COASA', 'CARABAYA', 'PUNO', 15879, 3071, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1629, '210305', 'CORANI', 'CARABAYA', 'PUNO', 3916, 890, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1630, '210306', 'CRUCERO', 'CARABAYA', 'PUNO', 9208, 854, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1631, '210307', 'ITUATA', 'CARABAYA', 'PUNO', 6341, 1251, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1632, '210308', 'OLLACHEA', 'CARABAYA', 'PUNO', 5566, 709, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1633, '210309', 'SAN GABAN', 'CARABAYA', 'PUNO', 4109, 2042, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1634, '210310', 'USICAYOS', 'CARABAYA', 'PUNO', 23448, 654, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1635, '210401', 'JULI', 'CHUCUITO', 'PUNO', 21462, 772, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1636, '210402', 'DESAGUADERO', 'CHUCUITO', 'PUNO', 31524, 177, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1637, '210403', 'HUACULLANI', 'CHUCUITO', 'PUNO', 23188, 627, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1638, '210404', 'KELLUYO', 'CHUCUITO', 'PUNO', 25415, 487, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1639, '210405', 'PISACOMA', 'CHUCUITO', 'PUNO', 13608, 957, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1640, '210406', 'POMATA', 'CHUCUITO', 'PUNO', 16094, 401, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1641, '210407', 'ZEPITA', 'CHUCUITO', 'PUNO', 18948, 525, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1642, '210501', 'ILAVE', 'EL COLLAO', 'PUNO', 32767, 917, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1643, '210502', 'CAPAZO', 'EL COLLAO', 'PUNO', 2203, 1043, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1644, '210503', 'PILCUYO', 'EL COLLAO', 'PUNO', 12850, 153, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1645, '210504', 'SANTA ROSA', 'EL COLLAO', 'PUNO', 7735, 2698, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1646, '210505', 'CONDURIRI', 'EL COLLAO', 'PUNO', 4387, 841, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1647, '210601', 'HUANCANE', 'HUANCANE', 'PUNO', 18253, 387, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1648, '210602', 'COJATA', 'HUANCANE', 'PUNO', 4239, 880, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1649, '210603', 'HUATASANI', 'HUANCANE', 'PUNO', 5371, 107, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1650, '210604', 'INCHUPALLA', 'HUANCANE', 'PUNO', 3275, 297, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1651, '210605', 'PUSI', 'HUANCANE', 'PUNO', 6278, 147, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1652, '210606', 'ROSASPATA', 'HUANCANE', 'PUNO', 5106, 305, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1653, '210607', 'TARACO', 'HUANCANE', 'PUNO', 14014, 197, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1654, '210608', 'VILQUE CHICO', 'HUANCANE', 'PUNO', 8290, 507, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1655, '210701', 'LAMPA', 'LAMPA', 'PUNO', 10420, 660, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1656, '210702', 'CABANILLA', 'LAMPA', 'PUNO', 5325, 385, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1657, '210703', 'CALAPUJA', 'LAMPA', 'PUNO', 1473, 141, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1658, '210704', 'NICASIO', 'LAMPA', 'PUNO', 2666, 134, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1659, '210705', 'OCUVIRI', 'LAMPA', 'PUNO', 3059, 877, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1660, '210706', 'PALCA', 'LAMPA', 'PUNO', 2855, 497, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1661, '210707', 'PARATIA', 'LAMPA', 'PUNO', 8778, 746, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1662, '210708', 'PUCARA', 'LAMPA', 'PUNO', 5342, 526, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1663, '210709', 'SANTA LUCIA', 'LAMPA', 'PUNO', 7485, 1590, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1664, '210710', 'VILAVILA', 'LAMPA', 'PUNO', 4125, 162, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1665, '210801', 'AYAVIRI', 'MELGAR', 'PUNO', 22397, 1017, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1666, '210802', 'ANTAUTA', 'MELGAR', 'PUNO', 4516, 655, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1667, '210803', 'CUPI', 'MELGAR', 'PUNO', 3274, 217, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1668, '210804', 'LLALLI', 'MELGAR', 'PUNO', 4719, 229, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1669, '210805', 'MACARI', 'MELGAR', 'PUNO', 8532, 692, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1670, '210806', 'NUNOA', 'MELGAR', 'PUNO', 11017, 2199, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1671, '210807', 'ORURILLO', 'MELGAR', 'PUNO', 10805, 397, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1672, '210808', 'SANTA ROSA', 'MELGAR', 'PUNO', 7342, 804, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1673, '210809', 'UMACHIRI', 'MELGAR', 'PUNO', 4384, 331, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1674, '210901', 'MOHO', 'MOHO', 'PUNO', 15656, 506, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1675, '210902', 'CONIMA', 'MOHO', 'PUNO', 2909, 68, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1676, '210903', 'HUAYRAPATA', 'MOHO', 'PUNO', 4258, 401, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1677, '210904', 'TILALI', 'MOHO', 'PUNO', 2649, 52, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1678, '211001', 'PUTINA', 'SAN ANTONIO DE PUTIN', 'PUNO', 26628, 1034, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1679, '211002', 'ANANEA', 'SAN ANTONIO DE PUTIN', 'PUNO', 32285, 979, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1680, '211003', 'PEDRO VILCA APAZA', 'SAN ANTONIO DE PUTIN', 'PUNO', 2934, 143, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1681, '211004', 'QUILCAPUNCU', 'SAN ANTONIO DE PUTIN', 'PUNO', 5743, 525, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1682, '211005', 'SINA', 'SAN ANTONIO DE PUTIN', 'PUNO', 1660, 465, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1683, '211101', 'JULIACA', 'SAN ROMAN', 'PUNO', 32767, 526, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1684, '211102', 'CABANA', 'SAN ROMAN', 'PUNO', 4224, 193, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1685, '211103', 'CABANILLAS', 'SAN ROMAN', 'PUNO', 5374, 1275, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1686, '211104', 'CARACOTO', 'SAN ROMAN', 'PUNO', 5655, 287, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1687, '211201', 'SANDIA', 'SANDIA', 'PUNO', 12191, 696, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1688, '211202', 'CUYOCUYO', 'SANDIA', 'PUNO', 4707, 503, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1689, '211203', 'LIMBANI', 'SANDIA', 'PUNO', 4274, 2422, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1690, '211204', 'PATAMBUCO', 'SANDIA', 'PUNO', 3960, 474, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1691, '211205', 'PHARA', 'SANDIA', 'PUNO', 4844, 428, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1692, '211206', 'QUIACA', 'SANDIA', 'PUNO', 2374, 413, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1693, '211207', 'SAN JUAN DEL ORO', 'SANDIA', 'PUNO', 13111, 196, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1694, '211208', 'YANAHUAYA', 'SANDIA', 'PUNO', 2269, 648, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1695, '211209', 'ALTO INAMBARI', 'SANDIA', 'PUNO', 9241, 1360, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1696, '211210', 'SAN PEDRO DE PUTINA PUNCO', 'SANDIA', 'PUNO', 13577, 5415, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1697, '211301', 'YUNGUYO', 'YUNGUYO', 'PUNO', 27074, 175, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00');
INSERT INTO `zones` (`id`, `ubigeo`, `distrito`, `provincia`, `departamento`, `poblacion`, `area`, `estado`, `email`, `created_at`, `updated_at`) VALUES
(1698, '211302', 'ANAPIA', 'YUNGUYO', 'PUNO', 3334, 121, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1699, '211303', 'COPANI', 'YUNGUYO', 'PUNO', 5021, 60, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1700, '211304', 'CUTURAPI', 'YUNGUYO', 'PUNO', 1214, 24, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1701, '211305', 'OLLARAYA', 'YUNGUYO', 'PUNO', 5336, 27, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1702, '211306', 'TINICACHI', 'YUNGUYO', 'PUNO', 1593, 4, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1703, '211307', 'UNICACHI', 'YUNGUYO', 'PUNO', 3824, 6, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1704, '220101', 'MOYOBAMBA', 'MOYOBAMBA', 'SAN MARTIN', 32767, 2805, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1705, '220102', 'CALZADA', 'MOYOBAMBA', 'SAN MARTIN', 4302, 116, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1706, '220103', 'HABANA', 'MOYOBAMBA', 'SAN MARTIN', 1993, 70, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1707, '220104', 'JEPELACIO', 'MOYOBAMBA', 'SAN MARTIN', 21164, 245, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1708, '220105', 'SORITOR', 'MOYOBAMBA', 'SAN MARTIN', 32767, 483, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1709, '220106', 'YANTALO', 'MOYOBAMBA', 'SAN MARTIN', 3375, 72, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1710, '220201', 'BELLAVISTA', 'BELLAVISTA', 'SAN MARTIN', 13395, 299, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1711, '220202', 'ALTO BIAVO', 'BELLAVISTA', 'SAN MARTIN', 7015, 5780, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1712, '220203', 'BAJO BIAVO', 'BELLAVISTA', 'SAN MARTIN', 19335, 1004, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1713, '220204', 'HUALLAGA', 'BELLAVISTA', 'SAN MARTIN', 3003, 114, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1714, '220205', 'SAN PABLO', 'BELLAVISTA', 'SAN MARTIN', 8916, 351, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1715, '220206', 'SAN RAFAEL', 'BELLAVISTA', 'SAN MARTIN', 7290, 99, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1716, '220301', 'SAN JOSE DE SISA', 'EL DORADO', 'SAN MARTIN', 11796, 275, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1717, '220302', 'AGUA BLANCA', 'EL DORADO', 'SAN MARTIN', 2359, 177, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1718, '220303', 'SAN MARTIN', 'EL DORADO', 'SAN MARTIN', 13022, 548, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1719, '220304', 'SANTA ROSA', 'EL DORADO', 'SAN MARTIN', 10052, 244, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1720, '220305', 'SHATOJA', 'EL DORADO', 'SAN MARTIN', 3120, 75, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1721, '220401', 'SAPOSOA', 'HUALLAGA', 'SAN MARTIN', 11341, 513, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1722, '220402', 'ALTO SAPOSOA', 'HUALLAGA', 'SAN MARTIN', 3148, 1346, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1723, '220403', 'EL ESLABON', 'HUALLAGA', 'SAN MARTIN', 3753, 126, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1724, '220404', 'PISCOYACU', 'HUALLAGA', 'SAN MARTIN', 3830, 184, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1725, '220405', 'SACANCHE', 'HUALLAGA', 'SAN MARTIN', 2584, 150, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1726, '220406', 'TINGO DE SAPOSOA', 'HUALLAGA', 'SAN MARTIN', 672, 41, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1727, '220501', 'LAMAS', 'LAMAS', 'SAN MARTIN', 12434, 96, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1728, '220502', 'ALONSO DE ALVARADO', 'LAMAS', 'SAN MARTIN', 18862, 247, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1729, '220503', 'BARRANQUITA', 'LAMAS', 'SAN MARTIN', 5085, 1075, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1730, '220504', 'CAYNARACHI', 'LAMAS', 'SAN MARTIN', 7899, 1287, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1731, '220505', 'CUNUMBUQUI', 'LAMAS', 'SAN MARTIN', 4681, 193, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1732, '220506', 'PINTO RECODO', 'LAMAS', 'SAN MARTIN', 10663, 819, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1733, '220507', 'RUMISAPA', 'LAMAS', 'SAN MARTIN', 2481, 41, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1734, '220508', 'SAN ROQUE DE CUMBAZA', 'LAMAS', 'SAN MARTIN', 1450, 645, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1735, '220509', 'SHANAO', 'LAMAS', 'SAN MARTIN', 3460, 25, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1736, '220510', 'TABALOSOS', 'LAMAS', 'SAN MARTIN', 13130, 397, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1737, '220511', 'ZAPATERO', 'LAMAS', 'SAN MARTIN', 4776, 178, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1738, '220601', 'JUANJUI', 'MARISCAL CACERES', 'SAN MARTIN', 26364, 353, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1739, '220602', 'CAMPANILLA', 'MARISCAL CACERES', 'SAN MARTIN', 7642, 2160, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1740, '220603', 'HUICUNGO', 'MARISCAL CACERES', 'SAN MARTIN', 6481, 9799, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1741, '220604', 'PACHIZA', 'MARISCAL CACERES', 'SAN MARTIN', 4180, 1767, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1742, '220605', 'PAJARILLO', 'MARISCAL CACERES', 'SAN MARTIN', 5941, 357, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1743, '220701', 'PICOTA', 'PICOTA', 'SAN MARTIN', 8094, 185, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1744, '220702', 'BUENOS AIRES', 'PICOTA', 'SAN MARTIN', 3202, 291, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1745, '220703', 'CASPISAPA', 'PICOTA', 'SAN MARTIN', 2052, 89, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1746, '220704', 'PILLUANA', 'PICOTA', 'SAN MARTIN', 713, 69, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1747, '220705', 'PUCACACA', 'PICOTA', 'SAN MARTIN', 2456, 222, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1748, '220706', 'SAN CRISTOBAL', 'PICOTA', 'SAN MARTIN', 1375, 24, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1749, '220707', 'SAN HILARION', 'PICOTA', 'SAN MARTIN', 5458, 96, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1750, '220708', 'SHAMBOYACU', 'PICOTA', 'SAN MARTIN', 11449, 311, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1751, '220709', 'TINGO DE PONASA', 'PICOTA', 'SAN MARTIN', 4659, 416, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1752, '220710', 'TRES UNIDOS', 'PICOTA', 'SAN MARTIN', 5075, 352, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1753, '220801', 'RIOJA', 'RIOJA', 'SAN MARTIN', 23472, 205, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1754, '220802', 'AWAJUN', 'RIOJA', 'SAN MARTIN', 11630, 475, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1755, '220803', 'ELIAS SOPLIN VARGAS', 'RIOJA', 'SAN MARTIN', 13156, 159, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1756, '220804', 'NUEVA CAJAMARCA', 'RIOJA', 'SAN MARTIN', 32767, 332, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1757, '220805', 'PARDO MIGUEL', 'RIOJA', 'SAN MARTIN', 22345, 1151, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1758, '220806', 'POSIC', 'RIOJA', 'SAN MARTIN', 1633, 61, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1759, '220807', 'SAN FERNANDO', 'RIOJA', 'SAN MARTIN', 3389, 75, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1760, '220808', 'YORONGOS', 'RIOJA', 'SAN MARTIN', 3587, 81, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1761, '220809', 'YURACYACU', 'RIOJA', 'SAN MARTIN', 3914, 60, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1762, '220901', 'TARAPOTO', 'SAN MARTIN', 'SAN MARTIN', 32767, 45, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1763, '220902', 'ALBERTO LEVEAU', 'SAN MARTIN', 'SAN MARTIN', 673, 51, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1764, '220903', 'CACATACHI', 'SAN MARTIN', 'SAN MARTIN', 3327, 45, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1765, '220904', 'CHAZUTA', 'SAN MARTIN', 'SAN MARTIN', 8111, 1415, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1766, '220905', 'CHIPURANA', 'SAN MARTIN', 'SAN MARTIN', 1794, 280, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1767, '220906', 'EL PORVENIR', 'SAN MARTIN', 'SAN MARTIN', 2692, 452, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1768, '220907', 'HUIMBAYOC', 'SAN MARTIN', 'SAN MARTIN', 3444, 1228, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1769, '220908', 'JUAN GUERRA', 'SAN MARTIN', 'SAN MARTIN', 3117, 208, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1770, '220909', 'LA BANDA DE SHILCAYO', 'SAN MARTIN', 'SAN MARTIN', 32767, 273, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1771, '220910', 'MORALES', 'SAN MARTIN', 'SAN MARTIN', 29302, 53, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1772, '220911', 'PAPAPLAYA', 'SAN MARTIN', 'SAN MARTIN', 2062, 888, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1773, '220912', 'SAN ANTONIO', 'SAN MARTIN', 'SAN MARTIN', 1340, 125, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1774, '220913', 'SAUCE', 'SAN MARTIN', 'SAN MARTIN', 15840, 98, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1775, '220914', 'SHAPAJA', 'SAN MARTIN', 'SAN MARTIN', 1489, 235, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1776, '221001', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', 25271, 1117, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1777, '221002', 'NUEVO PROGRESO', 'TOCACHE', 'SAN MARTIN', 11971, 819, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1778, '221003', 'POLVORA', 'TOCACHE', 'SAN MARTIN', 13684, 2311, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1779, '221004', 'SHUNTE', 'TOCACHE', 'SAN MARTIN', 1006, 1083, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1780, '221005', 'UCHIZA', 'TOCACHE', 'SAN MARTIN', 20318, 773, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1781, '230101', 'TACNA', 'TACNA', 'TACNA', 32767, 2429, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1782, '230102', 'ALTO DE LA ALIANZA', 'TACNA', 'TACNA', 32767, 375, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1783, '230103', 'CALANA', 'TACNA', 'TACNA', 3189, 117, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1784, '230104', 'CIUDAD NUEVA', 'TACNA', 'TACNA', 32767, 175, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1785, '230105', 'INCLAN', 'TACNA', 'TACNA', 7684, 1440, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1786, '230106', 'PACHIA', 'TACNA', 'TACNA', 1964, 614, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1787, '230107', 'PALCA', 'TACNA', 'TACNA', 1669, 1433, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1788, '230108', 'POCOLLAY', 'TACNA', 'TACNA', 21278, 293, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1789, '230109', 'SAMA', 'TACNA', 'TACNA', 2604, 1135, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1790, '230110', 'CORONEL GREGORIO ALBARRACIN LANCHIPA', 'TACNA', 'TACNA', 32767, 168, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1791, '230201', 'CANDARAVE', 'CANDARAVE', 'TACNA', 3001, 1378, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1792, '230202', 'CAIRANI', 'CANDARAVE', 'TACNA', 1301, 176, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1793, '230203', 'CAMILACA', 'CANDARAVE', 'TACNA', 1514, 491, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1794, '230204', 'CURIBAYA', 'CANDARAVE', 'TACNA', 180, 113, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1795, '230205', 'HUANUARA', 'CANDARAVE', 'TACNA', 898, 90, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1796, '230206', 'QUILAHUANI', 'CANDARAVE', 'TACNA', 1201, 55, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1797, '230301', 'LOCUMBA', 'JORGE BASADRE', 'TACNA', 2601, 843, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1798, '230302', 'ILABAYA', 'JORGE BASADRE', 'TACNA', 3008, 1056, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1799, '230303', 'ITE', 'JORGE BASADRE', 'TACNA', 3425, 826, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1800, '230401', 'TARATA', 'TARATA', 'TACNA', 3252, 883, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1801, '230402', 'HEROES ALBARRACIN', 'TARATA', 'TACNA', 655, 378, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1802, '230403', 'ESTIQUE', 'TARATA', 'TACNA', 710, 300, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1803, '230404', 'ESTIQUE-PAMPA', 'TARATA', 'TACNA', 666, 146, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1804, '230405', 'SITAJARA', 'TARATA', 'TACNA', 697, 234, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1805, '230406', 'SUSAPAYA', 'TARATA', 'TACNA', 768, 360, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1806, '230407', 'TARUCACHI', 'TARATA', 'TACNA', 410, 106, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1807, '230408', 'TICACO', 'TARATA', 'TACNA', 587, 355, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1808, '240101', 'TUMBES', 'TUMBES', 'TUMBES', 32767, 161, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1809, '240102', 'CORRALES', 'TUMBES', 'TUMBES', 23868, 127, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1810, '240103', 'LA CRUZ', 'TUMBES', 'TUMBES', 9173, 66, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1811, '240104', 'PAMPAS DE HOSPITAL', 'TUMBES', 'TUMBES', 7050, 724, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1812, '240105', 'SAN JACINTO', 'TUMBES', 'TUMBES', 8541, 585, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1813, '240106', 'SAN JUAN DE LA VIRGEN', 'TUMBES', 'TUMBES', 4089, 116, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1814, '240201', 'ZORRITOS', 'CONTRALMIRANTE VILLA', 'TUMBES', 12313, 648, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1815, '240202', 'CASITAS', 'CONTRALMIRANTE VILLA', 'TUMBES', 2109, 855, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1816, '240203', 'CANOAS DE PUNTA SAL', 'CONTRALMIRANTE VILLA', 'TUMBES', 5474, 624, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1817, '240301', 'ZARUMILLA', 'ZARUMILLA', 'TUMBES', 22257, 99, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1818, '240302', 'AGUAS VERDES', 'ZARUMILLA', 'TUMBES', 23480, 47, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1819, '240303', 'MATAPALO', 'ZARUMILLA', 'TUMBES', 2395, 389, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1820, '240304', 'PAPAYAL', 'ZARUMILLA', 'TUMBES', 5253, 206, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1821, '250101', 'CALLERIA', 'CORONEL PORTILLO', 'UCAYALI', 32767, 11926, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1822, '250102', 'CAMPOVERDE', 'CORONEL PORTILLO', 'UCAYALI', 15743, 1725, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1823, '250103', 'IPARIA', 'CORONEL PORTILLO', 'UCAYALI', 11826, 6735, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1824, '250104', 'MASISEA', 'CORONEL PORTILLO', 'UCAYALI', 12758, 15157, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1825, '250105', 'YARINACOCHA', 'CORONEL PORTILLO', 'UCAYALI', 32767, 243, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1826, '250106', 'NUEVA REQUENA', 'CORONEL PORTILLO', 'UCAYALI', 5538, 2249, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1827, '250107', 'MANANTAY', 'CORONEL PORTILLO', 'UCAYALI', 32767, 1177, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1828, '250201', 'RAYMONDI', 'ATALAYA', 'UCAYALI', 32767, 14876, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1829, '250202', 'SEPAHUA', 'ATALAYA', 'UCAYALI', 8793, 7715, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1830, '250203', 'TAHUANIA', 'ATALAYA', 'UCAYALI', 8020, 7236, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1831, '250204', 'YURUA', 'ATALAYA', 'UCAYALI', 2587, 8758, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1832, '250301', 'PADRE ABAD', 'PADRE ABAD', 'UCAYALI', 25971, 4691, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1833, '250302', 'IRAZOLA', 'PADRE ABAD', 'UCAYALI', 24833, 2736, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1834, '250303', 'CURIMANA', 'PADRE ABAD', 'UCAYALI', 8543, 1952, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00'),
(1835, '250401', 'PURUS', 'PURUS', 'UCAYALI', 4481, 18147, 'A', '', '2018-07-23 05:00:00', '2018-07-23 05:00:00');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `advertisements`
--
ALTER TABLE `advertisements`
  ADD PRIMARY KEY (`id`),
  ADD KEY `advertisements_project_id_foreign` (`project_id`);

--
-- Indices de la tabla `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `beneficiaries`
--
ALTER TABLE `beneficiaries`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `impacts`
--
ALTER TABLE `impacts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `impacts_project_direct_id_foreign` (`project_direct_id`);

--
-- Indices de la tabla `infocontacto`
--
ALTER TABLE `infocontacto`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `locations`
--
ALTER TABLE `locations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `newsletters`
--
ALTER TABLE `newsletters`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indices de la tabla `postulants`
--
ALTER TABLE `postulants`
  ADD PRIMARY KEY (`id`),
  ADD KEY `postulants_advertisement_id_foreign` (`advertisement_id`);

--
-- Indices de la tabla `programs`
--
ALTER TABLE `programs`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `project_beneficiaries`
--
ALTER TABLE `project_beneficiaries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `project_beneficiaries_project_id_foreign` (`project_id`),
  ADD KEY `project_beneficiaries_beneficiary_id_foreign` (`beneficiary_id`);

--
-- Indices de la tabla `project_directs`
--
ALTER TABLE `project_directs`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `project_locations`
--
ALTER TABLE `project_locations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `project_locations_project_id_foreign` (`project_id`),
  ADD KEY `project_locations_location_id_foreign` (`location_id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indices de la tabla `zones`
--
ALTER TABLE `zones`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `advertisements`
--
ALTER TABLE `advertisements`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `banners`
--
ALTER TABLE `banners`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `beneficiaries`
--
ALTER TABLE `beneficiaries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=80;

--
-- AUTO_INCREMENT de la tabla `impacts`
--
ALTER TABLE `impacts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `infocontacto`
--
ALTER TABLE `infocontacto`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `locations`
--
ALTER TABLE `locations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `newsletters`
--
ALTER TABLE `newsletters`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=86;

--
-- AUTO_INCREMENT de la tabla `postulants`
--
ALTER TABLE `postulants`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT de la tabla `programs`
--
ALTER TABLE `programs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `projects`
--
ALTER TABLE `projects`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `project_beneficiaries`
--
ALTER TABLE `project_beneficiaries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT de la tabla `project_directs`
--
ALTER TABLE `project_directs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `project_locations`
--
ALTER TABLE `project_locations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `zones`
--
ALTER TABLE `zones`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1836;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `advertisements`
--
ALTER TABLE `advertisements`
  ADD CONSTRAINT `advertisements_project_id_foreign` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`);

--
-- Filtros para la tabla `impacts`
--
ALTER TABLE `impacts`
  ADD CONSTRAINT `impacts_project_direct_id_foreign` FOREIGN KEY (`project_direct_id`) REFERENCES `project_directs` (`id`);

--
-- Filtros para la tabla `postulants`
--
ALTER TABLE `postulants`
  ADD CONSTRAINT `postulants_advertisement_id_foreign` FOREIGN KEY (`advertisement_id`) REFERENCES `advertisements` (`id`);

--
-- Filtros para la tabla `project_beneficiaries`
--
ALTER TABLE `project_beneficiaries`
  ADD CONSTRAINT `project_beneficiaries_beneficiary_id_foreign` FOREIGN KEY (`beneficiary_id`) REFERENCES `beneficiaries` (`id`),
  ADD CONSTRAINT `project_beneficiaries_project_id_foreign` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`);

--
-- Filtros para la tabla `project_locations`
--
ALTER TABLE `project_locations`
  ADD CONSTRAINT `project_locations_location_id_foreign` FOREIGN KEY (`location_id`) REFERENCES `locations` (`id`),
  ADD CONSTRAINT `project_locations_project_id_foreign` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
