let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

 mix.setResourceRoot('../');

 mix.js('resources/assets/js/app.js', 'public/js')
    .scripts([
      'resources/assets/js/vendor/jquery-3.3.1.min.js',
      'resources/assets/js/vendor/sweetalert2.min.js'],'public/js/vendor.js')
    .sass('resources/assets/sass/app.scss', 'public/css')
    .scripts([
       'resources/assets/js/postula.js',
     ],'public/js/app2.js')
    .browserSync({
      proxy: "http://localhost/fundacion-wiese2/public/"
 });
