<?php
/**
 * Configuración básica de WordPress.
 *
 * Este archivo contiene las siguientes configuraciones: ajustes de MySQL, prefijo de tablas,
 * claves secretas, idioma de WordPress y ABSPATH. Para obtener más información,
 * visita la página del Codex{@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} . Los ajustes de MySQL te los proporcionará tu proveedor de alojamiento web.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** Ajustes de MySQL. Solicita estos datos a tu proveedor de alojamiento web. ** //
/** El nombre de tu base de datos de WordPress */
define('DB_NAME', 'wp_fundacion_wiese');


/** Tu nombre de usuario de MySQL */
define('DB_USER', 'root');


/** Tu contraseña de MySQL */
define('DB_PASSWORD', 'root');


/** Host de MySQL (es muy probable que no necesites cambiarlo) */
define('DB_HOST', 'localhost:8889');


/** Codificación de caracteres para la base de datos. */
define('DB_CHARSET', 'utf8mb4');


/** Cotejamiento de la base de datos. No lo modifiques si tienes dudas. */
define('DB_COLLATE', '');

/**#@+
 * Claves únicas de autentificación.
 *
 * Define cada clave secreta con una frase aleatoria distinta.
 * Puedes generarlas usando el {@link https://api.wordpress.org/secret-key/1.1/salt/ servicio de claves secretas de WordPress}
 * Puedes cambiar las claves en cualquier momento para invalidar todas las cookies existentes. Esto forzará a todos los usuarios a volver a hacer login.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', 'xtSvI1fA9k:ZzM0H9ZzUIy,C#6I*O^r@A.uajU`$9J+WZ}iKQmYI=K3D]$yltM0P');

define('SECURE_AUTH_KEY', '/-R{<:axQ/Fs3Rcud=IT=Xu1l6/:eVm+ohcYl)7q|I^=+A}>sh^sdC-lfr=3rDup');

define('LOGGED_IN_KEY', 'Fu)[Vz!FGY}uJCrjIvL =a?<SI9NB#z:8%DBmmAqWR&}!MzFHK7+00I( |4t&q#t');

define('NONCE_KEY', 'J31.HqDWVV!2Rpp$9rPY85-JB:Zmm2V>K4iCx|IQ.s$d[zE5_@hFX{`G5YXtrsFG');

define('AUTH_SALT', '+}e7mp]2M-e@`$Xw=_F[s)saRWX>B>D@P x99uL11mTmvA!<%Q(C]XUMjczzg+}`');

define('SECURE_AUTH_SALT', 'bRc:>0kS]f!@IF^e+!j(/|gbOO6X.?uAB:$eE1s>[5z#_+s;Q)!.*R}&@BqD1>)M');

define('LOGGED_IN_SALT', 'FEug2@bXVK>M/Z6X819q7nt^F>lgKQK&gKcsrSuAv)6A^Z1?a$p%:*Lt]&N2ueR!');

define('NONCE_SALT', 'fHY,7`pY(HNQodCPg~zPK5yg~t49o0/.XM17|R8V0PN/x[SY(1-||hmUGDrejoI.');


/**#@-*/

/**
 * Prefijo de la base de datos de WordPress.
 *
 * Cambia el prefijo si deseas instalar multiples blogs en una sola base de datos.
 * Emplea solo números, letras y guión bajo.
 */
$table_prefix  = 'wp_';



/**
 * Para desarrolladores: modo debug de WordPress.
 *
 * Cambia esto a true para activar la muestra de avisos durante el desarrollo.
 * Se recomienda encarecidamente a los desarrolladores de temas y plugins que usen WP_DEBUG
 * en sus entornos de desarrollo.
 */
define('WP_DEBUG', false);

/* ¡Eso es todo, deja de editar! Feliz blogging */

/** WordPress absolute path to the Wordpress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
