jQuery(document).ready(function ($) {
  $('.footer_bottom__botton').click(function(){
    $("html, body").animate({ scrollTop: 0 }, 600);
    return false;
  });
  $("#leftside-navigation .sub-menu > a").click(function(e) {
    $("#leftside-navigation ul ul").slideUp(), $(this).next().is(":visible") || $(this).next().slideDown(),
    e.stopPropagation()
  })
  $("#formNewsletter").validate({
      rules: {
          telefono: {
            required: true,
            number: true,
            minlength: 7
          },
          email: {
            required: true,
            email: true
          },
      },
      success: function(label, element) {
          $(element).removeClass('is-invalid');
      },
      errorPlacement: function(error, element) {
          $(element).addClass('is-invalid');
      },
      invalidHandler: function(form, validator) {
          validator.focusInvalid();
      },
      submitHandler: function (form) {
          mensaje('Enviando...');

          var url = $(form).attr('action');
          var data = $(form).serialize();
          console.log(url);

          $.ajax({
              method: 'POST',
              url: url,
              data: data,
              success: function (response) {
                  mensaje('¡Gracias por suscribirte al boletín de la Fundación Wiese!');
                  setTimeout($.unblockUI, 4000);
                  $('#formNewsletter')[0].reset();
              }
          });

       }
  });

  function mensaje (msj) {
      $.blockUI({
          message: msj,
          css: {
              border: 'none',
              padding: '12px',
              backgroundColor: '#000',
              '-webkit-border-radius': '10px',
              '-moz-border-radius': '10px',
              opacity: .9,
              color: '#fff'
          }
      });
  }

  function closeNav() {
      document.getElementById("mySidenav").style.width = "0";
      $(".nano-content" ).delay(0).fadeOut(0);
      $('.overlay3').hide();
  }
});
