<?php
global $porto_settings;
?>
<div class="panel-overlay"></div>
<div id="nav-panel" class="<?php echo (isset($porto_settings['mobile-panel-pos']) && $porto_settings['mobile-panel-pos']) ? $porto_settings['mobile-panel-pos'] : '' ?>">
    <?php
    // show welcome message
    if ($porto_settings['welcome-msg'])
        echo '<span class="welcome-msg">' . $porto_settings['welcome-msg'] . '</span>';

    // show currency and view switcher
    $switcher = '';
    $switcher .= porto_mobile_currency_switcher();
    $switcher .= porto_mobile_view_switcher(); ?>


    <?php
    if ($switcher)
        echo '<div class="switcher-wrap">'.$switcher.'</div>';

    // show top navigation and mobile menu
    $menu = porto_mobile_menu();

    if ($menu)
        //echo '<div class="menu-wrap">'.$menu.'</div>';

    $header_type = porto_get_header_type();

    if (($header_type == 1 || $header_type == 4 || $header_type == 9 || $header_type == 13 || $header_type == 14) && $porto_settings['menu-block']) {
        echo '<div class="menu-custom-block">' . force_balance_tags($porto_settings['menu-block']) . '</div>';
    }

    $menu = porto_mobile_top_navigation();

    if ($menu)
        //echo '<div class="menu-wrap">'.$menu.'</div>';

    // show social links
    //echo porto_header_socials();
    ?>

    <aside class="sidebar">
      <div id="leftside-navigation" class="nano">
        <!-- <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a> -->
        <a href="#" id="nav-panel-close" class="closebtn <?php echo (isset($porto_settings['mobile-panel-pos']) && $porto_settings['mobile-panel-pos']) ? $porto_settings['mobile-panel-pos'] : '' ?>">&times;</a>
        <ul class="nano-content">
          <li>
            <a href="../"><i class="fa fa-home"></i><span>Inicio</span></a>
          </li>
          <li class="sub-menu">
            <a href="javascript:void(0);"><i class="fas fa-hands-helping"></i><span>¿Cómo ayudamos?</span></a>
            <ul>
              <li><a href="../proyectos-iniciativas">Proyectos e Iniciativas</a></li>
              <li><a href="../historias" >Historias</a></li>
            </ul>
          </li>
          <li>
            <a href="../quienes-somos"><i class="fas fa-info-circle"></i><span>¿Quiénes somos?</span></a>
          </li>
          <li>
            <a href="../novedades"><i class="fas fa-newspaper"></i><span>Novedades</span></a>
          </li>
          <li>
            <a href="../contacto"><i class="fas fa-inbox"></i><span>Contáctanos</span></a>
          </li>
          <li>
            <a href="javascript:void(0);"><i class="fas fa-phone"></i><span>+511 611 4343 anx. 122 </span></a>
          </li>
          <li>
            <a href="mailto:info@fundacionwiese.org" class="mail"><i class="fa fa-envelope"></i><span>info@fundacionwiese.org</span></a>
          </li>

        </ul>
      </div>
    </aside>
</div>
