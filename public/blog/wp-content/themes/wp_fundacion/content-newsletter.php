<div class="container-fluid newsletter">
  <h2 class="newsletter__title">Newsletter</h2>
  <p class="newsletter__subtitle">Suscríbete para mantenerte informado</p>
</div>

<div class="container newsletter newsletter--noBorder">
  <div class="newsletter__contenedor">
    <form id="formNewsletter" name="formNewsletter" class="row justify-content-center align-items-center" action="../newsletter">
      <div class="form-group col-12 col-sm-6 col-md-5">
        <input type="text" class="form-control" id="telefono" name="telefono" placeholder="Teléfono">
      </div>
      <div class="form-group col-12 col-sm-6 col-md-5">
        <input type="email" class="form-control" id="email" name="email" placeholder="Email">
      </div>
      <div class="col-12 col-md-2 newsletter__submit-container">
        <button type="submit" class="form-group  btn newsletter__submit">Suscribirme</button>
      </div>

    </form>
  </div>
</div>
