$(".btn_contacoclase").click( function(){
	event.preventDefault(); jQuery.noConflict();
$.ajax({
		    async: false,
		    type: 'GET',
		    url: 'infocontacto/'+2,
		    data: {},
		    dataType: 'json',
		    success: function(response) {
						$("#nameprincipal").text("Ayuda Humanitaria");
					$("#namecontact").text(response.infocontact.nombre);
					$("#nametelef").text(response.infocontact.telefono);
					$("#namecorreo").text(response.infocontact.email);
					$('#modalcontacto').modal('show');

		    },
		    error: function(jqXHR, status, err) {
		        alert("Local error callback.");
		        console.log(err);
		    }
});
});

$('#save_contacto').on('submit', function(e) {
	e.preventDefault();
	var formData = new FormData(this);
	formData.append('_token', $('input[name=_token]').val());
	formData.append('infocontacto_id', 2);
	$.ajax({
	            type:'POST',
	            url: 'savecontacto',
	            data:formData,
	            cache:false,
	            contentType: false,
	            processData: false,
	            success:function(data){
								if(data.status){
									$('#save_contacto')[0].reset();
									$('#modalcontacto').modal('hide');
		              $("#fondoNombres").val("");
									$("#fondoApellidos").val("");
									$("#fondoEmail").val("");
									$("#fondoTelefono").val("");
									$("#mensaje").val("");
									swal(
								 'Excelente!!!',
								 data.mensaje,
								 'success'
								 )
							 }else{
								 swal({
									  type: 'error',
									  title: 'Error...!!',
									  text: data.mensaje,

									})
							 }


	            },
	            error: function(jqXHR, text, error){
	                console.log(error);
	            }
	        });


});
