if(window.location.hash === '#proceso'){
    saludo();
}

function saludo(){

	$("html, body").animate({
			scrollTop: $('.proyectos').offset().top - 40
	}, 600);
}

$(".postular_emprendedor").click( function(){
  event.preventDefault();
  $("html, body").animate({
			scrollTop: $('.criterios').offset().top - 40
	}, 600);

});
$(".btn_contacoclase").click( function(){
	event.preventDefault(); jQuery.noConflict();
$.ajax({
		    async: false,
		    type: 'GET',
		    url: 'infocontacto/'+3,
		    data: {},
		    dataType: 'json',
		    success: function(response) {
          $("#nameprincipal").text("Fondo Emprendedor, operado por NESST");
					$("#namecontact").text(response.infocontact.nombre);
					$("#nametelef").text(response.infocontact.telefono);
					$("#namecorreo").text(response.infocontact.email);
					$('#modalcontacto').modal('show');

		    },
		    error: function(jqXHR, status, err) {
		        alert("Local error callback.");
		        console.log(err);
		    }
});
});

$('#save_contacto').on('submit', function(e) {
	e.preventDefault();
	var formData = new FormData(this);
	formData.append('_token', $('input[name=_token]').val());
	formData.append('infocontacto_id', 3);
	$.ajax({
	            type:'POST',
	            url: 'savecontacto',
	            data:formData,
	            cache:false,
	            contentType: false,
	            processData: false,
	            success:function(data){
								$('#save_contacto')[0].reset();
								$('#modalcontacto').modal('hide');
	              $("#fondoNombres").val("");
								$("#fondoApellidos").val("");
								$("#fondoEmail").val("");
								$("#fondoTelefono").val("");
								$("#mensaje").val("");
								swal(
							 'Excelente!!!',
							 data.mensaje,
							 'success'
							 )


	            },
	            error: function(jqXHR, text, error){
	                console.log(error);
	            }
	        });


});


$("#btn_contact2").click( function(){

  		event.preventDefault(); jQuery.noConflict();
      $.ajax({
      		    async: false,
      		    type: 'GET',
      		    url: 'infocontacto/'+3,
      		    data: {},
      		    dataType: 'json',
      		    success: function(response) {
      					$("#namecontact2").text(response.infocontact.nombre);
      					$("#nametelef2").text(response.infocontact.telefono);
      					$("#namecorreo2").text(response.infocontact.email);
      					$('#modalcontacto2').modal('show');

      		    },
      		    error: function(jqXHR, status, err) {
      		        alert("Local error callback.");
      		        console.log(err);
      		    }
      });
});
$('#save_contacto2').on('submit', function(e) {
	e.preventDefault();
	var formData = new FormData(this);
	formData.append('_token', $('input[name=_token]').val());
	formData.append('infocontacto_id', 3);
	$.ajax({
	            type:'POST',
	            url: 'savecontacto',
	            data:formData,
	            cache:false,
	            contentType: false,
	            processData: false,
	            success:function(data){
								$('#save_contacto2')[0].reset();
								$('#modalcontacto2').modal('hide');
	              $("#fondoNombres").val("");
								$("#fondoApellidos").val("");
								$("#fondoEmail").val("");
								$("#fondoTelefono").val("");
								$("#mensaje").val("");
								swal(
							 'Excelente!!!',
							 data.mensaje,
							 'success'
							 )


	            },
	            error: function(jqXHR, text, error){
	                console.log(error);
	            }
	        });


});
