<?php

/* URL ALAN*/


/*Route::get('/', function () {
    return view('welcome');
});*/

Route::get('/', 'HomeController@index')->name('wiese.index');
Route::get('/quienes-somos', 'QuienesSomosController@index');
Route::get('/fundacion-wiese-dirige', 'ComoAyudamosController@dirige');
Route::get('/fundacion-wiese-auspicia', 'ComoAyudamosController@auspicia');
Route::get('/complejo-arqueologico-el-brujo', 'ProyectosDirigeController@brujo');
Route::get('/fondo-wiese', 'ProyectosDirigeController@fondo');
Route::get('/proyectos-iniciativas', 'ComoAyudamosController@participa');
Route::get('/proyecto-educativo', 'ProyectosDirigeController@educacion');
Route::get('/proyectos-cerrados', 'ProyectosDirigeController@cerrados');
Route::get('/contacto', 'ContactoController@index');
Route::post('/contacto', 'ContactoController@enviar');
Route::get('/se-voluntario', 'ContactoController@voluntario');
Route::post('/newsletter', 'ContactoController@enviarNewsletter');
Route::get('/historias', 'HistoriasController@index');
Route::get('/novedades', 'NovedadesController@index');
Route::get('/Politica-Privacidad-de-Datos', 'HomeController@politicaprivacidad')->name('politica.privacidad');
Route::get('/novedades', function () {
    return redirect('/blog');
});

Route::get('/bolsa-de-trabajo/detalle', 'TrabajaController@detalle');
Route::post('/provincia', ['as' => 'provincia' , 'uses' => 'TrabajaController@provincia']);
Route::get('/bolsa-de-trabajo/{aviso_id}/{aviso_nombre}', ['as' => 'bolsa-detail' , 'uses' => 'TrabajaController@trabajaDetail']);
Route::resource('/bolsa-de-trabajo', 'TrabajaController');

Route::post('/postulacion', 'TrabajaController@postulacionStore');


Route::get('/novedades/nueva-identidad-visual', 'NovedadesController@detalle1');
Route::get('/novedades/fondo-credito-educativo', 'NovedadesController@detalle2');
Route::get('/novedades/te-acompano-en-tu-desarrollo', 'NovedadesController@detalle3');
Route::get('/novedades/obras-por-impuestos-el-brujo', 'NovedadesController@detalle4');
Route::get('/novedades/tu-solidaridad-abriga', 'NovedadesController@detalle5');
Route::get('/novedades/premio-ipae', 'NovedadesController@detalle6');
Route::get('/novedades/entrega-de-donativos', 'NovedadesController@detalle7');


Route::group(['prefix' => 'fondo-solidario'], function () {
    Route::get('/', 'FondoEmprendedorController@index')->name('ala');
    Route::get('/creditos-educativos', 'FondoEmprendedorController@educacion')->name('solidario.education');
    Route::get('/ayuda-humanitaria', 'FondoEmprendedorController@humanitaria')->name('solidario.humanitaria');
    Route::get('/ayuda-humanitaria-detalle', 'FondoEmprendedorController@humanitariaDetalle')->name('solidario.humanitaria.detalle');
    Route::get('/contacto', 'FondoEmprendedorController@contacto')->name('solidario.contacto');
    Route::get('/fondo-emprendedor', 'FondoEmprendedorController@fondo')->name('solidario.conocefondo');
    Route::get('/fondo-emprendedor/formulario', 'FondoEmprendedorController@postula')->name('solidario.postula');
    Route::get('/infocontacto/{id?}', 'ContactoController@infocontact')->name('infocontacto');
    Route::post('/savecontacto', 'ContactoController@savecontact')->name('savecontact');
});

Route::post('exportPostulacion', 'TrabajaController@export');
Route::post('postulacionGeneral', 'TrabajaController@exportGeneral');


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
