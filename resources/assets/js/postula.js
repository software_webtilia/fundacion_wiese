$("#formPostula").validate({
    rules: {
        inputNombres: {
          required: true
        },
        inputApellidos: {
          required: true
        },
        inputTelefono: {
          required: true,
          number: true,
          minlength: 7
        },
        inputCorreo: {
          required: true,
          email: true
        },
        area_id: {
          required: true
        },

        inputProfesion: {
          required: true
        },
        inputDireccion: {
          required: true
        },
        inputCv: {
          required: true,
          extension: "pdf|doc|docx",
          filesize_max: 1000000
        }
    }
    ,errorPlacement: function(error, element) {
    error.insertBefore( element );
    },
    messages: {
      inputNombres: "Ingresa tus nombres",
      inputApellidos: "Ingresa tus apellidos",
      inputTelefono: {
        required: "Ingresa tu teléfono",
        number: "El teléfono debe ser numérico",
        minlength: "Ingresa mínimo 7 números",
      },
      inputCorreo: {
        required: "Ingresa tu correo",
        email: "Ingresa tu correo"
      },
      area: {
        required: "Selecciona tu área de interés"
      },
      inputDireccion: {
        required: "Ingrese su dirección"
      },
      inputProfesion: {
        required: "Ingresa tu profesión"
      },
      inputCv: {
        required: "Selecciona tu cv",
        extension: "Los archivos permitidos son pdf y word",
        filesize_max: "El peso máximo permitido es 1mb"
      }

    },
    success: function(label, element) {
        $(element).removeClass('is-invalid');
    },
    errorPlacement: function(error, element) {
        $(element).addClass('is-invalid');
    },
    invalidHandler: function(form, validator) {
        validator.focusInvalid();
    },
    submitHandler: function (form) {

        // mensaje('Enviando...');
        var token = document.head.querySelector('meta[name="csrf-token"]');
        var formData = new FormData(form);
        var url = $(form).attr('action');
        var click=false;
        var nav = $('.modal-body');

        $.ajax({
            method: 'POST',
            url: url,
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            headers: {
                'X-CSRF-TOKEN': token.content
            },
            success: function (response) {
                /*mensaje('Gracias. Pronto estaremos en contacto.');*/
                setTimeout($.unblockUI, 500);
                $('#formPostula')[0].reset();
                  click=true;
                  $(".modal-header").fadeOut();
                  $(".modal-header2").fadeIn();
                  $(".form-container").fadeOut();
                  $(".modal-body__mensaje").fadeOut();
                  $(".modal-body").css("padding-top", "50px");
                  $(this).html("Aceptar");
                  animateTime = 500;
                  if(nav.height() === 0){
                  autoHeightAnimate(nav, animateTime);
                  } else {
                    nav.stop().animate({ height: '0' }, animateTime);
                  }
                  $(".trabaja-detalle__enviar").css("display","none");
                  $(".trabaja-detalle__aceptar").fadeIn();
            }
        });
    }
});

//validate file size.

// jQuery.validator.addMethod("filesize_max", function(value, element, param) {
//
//     var isOptional = this.optional(element),
//         file;
//
//     if(isOptional) {
//         return isOptional;
//     }
//
//     if ($(element).attr("type") === "file") {
//         if (element.files && element.files.length) {
//             file = element.files[0];
//             return ( file.size && file.size <= param );
//         }
//     }
//     return false;
// }, "File size is too large.");

// function mensaje(msg){
//     $.blockUI({
//       message: msg,
//       css: {
//         border: 'none',
//         padding: '20px',
//         'font-size': '17px',
//         backgroundColor: '#000',
//         '-webkit-border-radius': '6px',
//         '-moz-border-radius': '10px',
//         opacity: .6,
//         color: '#fff'
//       }
//     });
//   }
//
// function autoHeightAnimate(element, time){
//     var curHeight = element.height(), // Get Default Height
//         autoHeight = element.css('height', 'auto').height(); // Get Auto Height
//         element.height(curHeight); // Reset to Default Height
//         element.stop().animate({ height: autoHeight }, time); // Animate to Auto Height
// }
// //
// // $(".modal-content__cerrar").click(function(e) {
// //
// //     if(click){
// //         window.location.replace(URL_SITE + '/bolsa-de-trabajo');
// //     }
// // });
// //
// // $(".trabaja-detalle__aceptar").click(function(e) {
// //     window.location.replace(URL_SITE + '/bolsa-de-trabajo');
// // });
