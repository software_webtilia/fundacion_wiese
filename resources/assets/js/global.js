jQuery(document).ready(function(){

    function h_mm(){
        var vvv = $(window).width();
        var hhh = $('.cbp-hsmenubg').height();
        if (vvv > 824 && $('ul.cbp-hsmenu').hasClass("open")) {
            $('.cbp-hsmenubg').hide();
            $('.menu_niveles').css('background','#ffffff')
        }
    }

    $('.nav-item').click(function() {
      var color = $(this).data("color");
      $(".mision__imagen-w").attr("src",URL_SITE + "/images/proyectos-dirige/w-" + color +".jpg" );
    });

    function hover_m(){

        var ventana = $(window).width();
        if (ventana > 1024) {

            var h_megmenu= $('ul.menu_niveles').outerHeight(true);
            $("li.menu_max.mm").hover(function(){
                $('.cbp-hsmenubg').height(h_megmenu);
                $(this).attr("data-open", "open");
                $(this).addClass("cbp-hsitem-open animated fadeIn");
            }, function(){
                $('.cbp-hsmenubg').height("auto");
                $(this).attr("data-open", "");
                $(this).removeClass("cbp-hsitem-open animated fadeIn");
            });
            $("li.menu_max").hover(function(){

                $(this).attr("data-open", "open");
                $(this).addClass("cbp-hsitem-open animated fadeIn");
            }, function(){

                $(this).attr("data-open", "");
                $(this).removeClass("cbp-hsitem-open animated fadeIn");
            });
            if($(".playmenu li").hasClass('cbp-hsitem-open')){
                $(".playmenu li").removeClass('cbp-hsitem-open');
                $('.cbp-hsmenubg').height(0);
            }
            else{

            }

        }else{
            $('#playmen').addClass('playmenu');

        };
    };
    hover_m();
    $(window).resize(hover_m);
    $(window).resize(h_mm);

    var b = $(".boton_menu");
    var w = $(".menu_colap");
    var z = $("#cerrar_menucolapse");
    b.click(function() {

      if(w.hasClass('open')) {
        w.removeClass('open animated fadeIn');
      } else {
        w.addClass('open animated fadeIn');
      }

    });
    z.click(function() {

      if(w.hasClass('open')) {
        w.removeClass('open animated fadeIn');
      } else {
        w.addClass('open animated fadeIn');
      }

    });

    $(".menu_niveles__menu2").hover(function(){
        $(".img_menu").attr("src",window.location.href +"images/menu2.png");
    }, function(){
        $(".img_menu").attr("src",window.location.href +"/images/menu.png");
    });

    $('.footer_bottom__botton').click(function(){
      $("html, body").animate({ scrollTop: 0 }, 600);
      return false;
    });

    $(".como_ayudamos").click(function(e) {
      e.preventDefault();
      $("html, body").animate({
        scrollTop: $('.proyectos').offset().top-40
      }, 600);
    });


    $( ".trabaja-card" )
    .mouseover(function() {
        $( this ).css("background-color", "#5e2584");
        $( this ).find( "h2" ).css("color","white");
        $( this ).find( "h3" ).css("color","white");
        $( this ).find( "i" ).css("color","white");
        $( this ).find( "span" ).css("color","white");
        $( this ).find( "p" ).css("color","white");
        $( this ).find( ".trabaja__link" ).css("color", "white");
        $( this ).find( ".trabaja__link" ).css("border", "1px solid white");

    })
    .mouseout(function() {
        $( this ).css({"background": "white"});
        $( this ).find( "h2" ).css("color","#4c5154");
        $( this ).find( "h3" ).css("color","#690670");
        $( this ).find( "i" ).css("color","#690670");
        $( this ).find( "span" ).css("color","#4c5154");
        $( this ).find( "p" ).css("color","#4c5154");

        $( this ).find( ".trabaja__link" ).css("color", "#690670");
        $( this ).find( ".trabaja__link" ).css("border", "1px solid #690670");
    });





});
