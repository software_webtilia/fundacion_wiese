/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
require('./bootstrap');
require('./global');
require('./quienes_somos');
require('./contacto');
require('./trabaja');
require('./fondoContacto');
const WOW = require('wowjs');

//sample to load npm library:
//var Isotype = require('isotope-layout');

//sample to load vendor library (no npm)
var fancybox = require('./vendor/jquery.fancybox.min');

$('.fancybox').fancybox();

$("#leftside-navigation .sub-menu > a").click(function(e) {
  $("#leftside-navigation ul ul").slideUp(), $(this).next().is(":visible") || $(this).next().slideDown(),
  e.stopPropagation()
})

function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
    $(".nano-content" ).delay(300).fadeIn( 400 );
    $('.overlay3').show();
}

/* Set the width of the side navigation to 0 and the left margin of the page content to 0, and the background color of body to white */
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    $(".nano-content" ).delay(0).fadeOut(0);
    $('.overlay3').hide();
}

window.wow = new WOW.WOW({ live: false });

window.wow.init();

window.openNav = openNav;
window.closeNav = closeNav;
