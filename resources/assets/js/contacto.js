
$("#formContacto").validate({
    rules: {
        nombres: {
          required: true
        },
        apellidos: {
          required: true
        },
        telefono: {
          required: true,
          number: true,
          minlength: 7
        },
        email: {
          required: true,
          email: true
        },
        mensaje: {
          required: true
        }
    },
    success: function(label, element) {

        $(element).removeClass('is-invalid');
    },
    errorPlacement: function(error, element) {
        $(element).addClass('is-invalid');
    },
    invalidHandler: function(form, validator) {
        validator.focusInvalid();
    },
    submitHandler: function (form) {
      
        mensaje('Enviando...');
        var token = document.head.querySelector('meta[name="csrf-token"]');
        var data = $(form).serialize();
        var url = $(form).attr('action');

        $.ajax({
            method: 'POST',
            url: url,
            data: data,
            headers: {
                'X-CSRF-TOKEN': token.content
            },
            success: function (response) {
                mensaje('Gracias. Pronto estaremos en contacto.');
                setTimeout($.unblockUI, 4000);
                $('#formContacto')[0].reset();
            }
        });
    }
});

$("#formNewsletter").validate({
    rules: {
        telefono: {
          required: true,
          number: true,
          minlength: 7
        },
        email: {
          required: true,
          email: true
        },
    },
    success: function(label, element) {
        $(element).removeClass('is-invalid');
    },
    errorPlacement: function(error, element) {
        $(element).addClass('is-invalid');
    },
    invalidHandler: function(form, validator) {
        validator.focusInvalid();
    },
    submitHandler: function (form) {
        mensaje('Enviando...');
        var token = document.head.querySelector('meta[name="csrf-token"]');
        var data = $(form).serialize();
        var url = $(form).attr('action');

        $.ajax({
            method: 'POST',
            url: url,
            data: data,
            headers: {
                'X-CSRF-TOKEN': token.content
            },
            success: function (response) {
                mensaje('¡Gracias por suscribirte al boletín de la Fundación Wiese!');
                setTimeout($.unblockUI, 4000);
                $('#formNewsletter')[0].reset();
            }
        });
    }
});

function mensaje (msj) {
    $.blockUI({
        message: msj,
        css: {
            border: 'none',
            padding: '12px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .9,
            color: '#fff'
        }
    });
}
