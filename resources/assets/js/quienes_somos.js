jQuery(document).ready(function(){
  if($('.carousel_aliados').length){
      var owl = $('.carousel_aliados');
      owl.owlCarousel({
          items:4,
          loop:true,
          margin:10,
          autoplay:false,
          autoplayTimeout:1000,
          autoplayHoverPause:true,
          responsive:{
              0:{
                  items:1,
                  nav:true
              },
              600:{
                  items:3,
                  nav:true
              },
              1000:{
                  items:4,
                  nav:true
              }
          }
      });
    }

    $(".nav-item.complejo__item").click(function(e) {
        $(this).parent().children().each(function( index, element ) {
            var colorprev = $(element).attr('class').split("--");
            $(element).children().removeClass('complejo__link_active complejo__link_active--'+colorprev[1]);
        });

        var colorthis = $(this).attr('class').split("--");
        $(this).children().addClass('complejo__link_active complejo__link_active--'+colorthis[1]);
    });



});
