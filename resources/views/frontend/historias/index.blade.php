@extends('frontend.layouts.layout')
@section('title', 'Fundación Wiese')

@section('content')
<div class="banner participa__banner participa__banner--position-left d-flex align-items-center">
	<img src="{{ url('images/historias/banner_historias.jpg') }}" class="img-fluid" alt="">
</div>

<div class="container-fluid">
	<div class="row">
		<div class="col-md-2 pl-0">
				<img class="mision__imagen-w img-fluid" src="{{url('images/historias/w-gris.png')}}">
		</div>
		<div class="col-md-8">
			<div class="participa__titulo-box text-center">
				<h1 class="participa__title participa__title--color-gris">Conoce nuestras historias</h1>
				<p class="participa__subtitle">Siente el impacto de la labor que realiza la Fundación es a través del testimonio de los beneficiarios de sus proyectos e iniciativas.</p>
			</div>
		</div>
		<div class="col-md-2">
		</div>
	</div>
</div>



<div class="container historias">

	<div class="row mt-5">
		<div class="col-12 col-lg-5 mx-auto historias__imagen">
			<a href="https://www.youtube.com/watch?v=6RiCcTP1Wn0&rel=0&autoplay=1&showinfo=0&iv_load_policy=3&playlist=6RiCcTP1Wn0" data-lity>
			<img class="historias__imagen-img img-fluid" src="{{url('images/historias/historia1.jpg')}}"></a>

				<!--img class="historias__imagen-play img-fluid" src="{{url('images/historias/play.png')}}"-->
			<div class="historias__background historias__background--color-rojo d-flex flex-row">
	            <div class="historias__iso">
	              <img src="{{url('images/W-white.png')}}"  alt="" class="img-fluid proyectos__iso-imagen">
	            </div>
	            <div class="testimonio ml-auto">
	              	<p class="testimonio__item">Nombre: Luis Iparraguirre</p>
	              	<p class="testimonio__item">Proyecto: Complejo Arqueológico El Brujo</p>
	              	<p class="testimonio__item">Localización: Magdalena de Cao</p>
	              	<p class="testimonio__item">Año: 2018</p>
	            </div>
	        </div>
	    </div>

		<div class="col-12 col-lg-6 historias__descripcion d-flex flex-column justify-content-center align-items-center">

			<div class="historias__box">
				<div class="historias__comilla">
					<img class="historias__comilla-img img-fluid" src="{{url('images/historias/comilla.png')}}">
				</div>
			</div>
			<p class="historias__texto">“Gracias a la Fundación Wiese voy por segunda vez a la Feria de Ruraq Maki, quienes me apoyaron para participar en esta feria muy importante que se hace en Lima. Estoy muy agradecido con ellos.”</p>
		</div>
	</div>

	<div class="row mt-5">
		<div class="col-12 col-lg-5 mx-auto historias__imagen">
			<a href="https://www.youtube.com/watch?v=_DqjElAOnmM&rel=0&autoplay=1&showinfo=0&iv_load_policy=3&playlist=_DqjElAOnmM" data-lity>
			<img class="historias__imagen-img img-fluid" src="{{url('images/historias/historia2.jpg')}}"></a>
			<div class="historias__background historias__background--color-naranja d-flex flex-row">
	            <div class="historias__iso">
	              <img src="{{url('images/W-white.png')}}"  alt="" class="img-fluid proyectos__iso-imagen">
	            </div>
	            <div class="testimonio ml-auto">
	              	<p class="testimonio__item">Nombre: Daphne Castillo</p>
	              	<p class="testimonio__item">Proyecto: Calidad Educativa</p>
	              	<p class="testimonio__item">Localización: Pachacamac</p>
	              	<p class="testimonio__item">Año: 2018</p>
	            </div>
	        </div>
	    </div>

		<div class="col-12 col-lg-6 historias__descripcion d-flex flex-column justify-content-center align-items-center">

			<div class="historias__box">
				<div class="historias__comilla">
					<img class="historias__comilla-img img-fluid" src="{{url('images/historias/comilla.png')}}">
				</div>
			</div>
			<p class="historias__texto">“Me parece muy importante el proyecto porque que la conversación del acompañamiento no busca decirte cuáles son tus errores sino lo que me llama la atención es que la conversación se conduce a que tú veas por ti misma cuáles son tus errores y busques salir de esas dificultades que tienes en la sesión. Por otro lado, los talleres de habilidades blandas me han ayudado mucho no solo como docente sino como persona tanto en casa como en mi trabajo, me ha ayudado a mejorar como persona, y lo aplico con mis alumnos, como con mis hijos.”</p>
		</div>
	</div>

	<div class="row mt-5">
		<div class="col-12 col-lg-5 mx-auto historias__imagen">
			<a href="https://www.youtube.com/watch?v=AmABnvmE8_c&rel=0&autoplay=1&showinfo=0&iv_load_policy=3&playlist=AmABnvmE8_c" data-lity>
			<img class="historias__imagen-img img-fluid" src="{{url('images/historias/historia3.jpg')}}"></a>

				<!--img class="historias__imagen-play img-fluid" src="{{url('images/historias/play.png')}}"-->
			<div class="historias__background historias__background--color-morado d-flex flex-row">
	            <div class="historias__iso">
	              <img src="{{url('images/W-white.png')}}"  alt="" class="img-fluid proyectos__iso-imagen">
	            </div>
	            <div class="testimonio ml-auto">
	              	<p class="testimonio__item">Nombre: Teresa García</p>
	              	<p class="testimonio__item">Proyecto: Complejo Arqueológico El Brujo</p>
	              	<p class="testimonio__item">Localización: Magdalena de Cao</p>
	              	<p class="testimonio__item">Año: 2018</p>
	            </div>
	        </div>
	    </div>

		<div class="col-12 col-lg-6 historias__descripcion d-flex flex-column justify-content-center align-items-center">

			<div class="historias__box">
				<div class="historias__comilla">
					<img class="historias__comilla-img img-fluid" src="{{url('images/historias/comilla.png')}}">
				</div>
			</div>
			<p class="historias__texto">“Estoy muy agradecida con la Fundación Wiese por el apoyo que nos da a la población y especialmente a mí, que me dan capacitaciones para atender mejor a los turistas que vienen. Gracias a ellos los artesanos de Magdalena de Cao nos estamos haciendo conocidos a nivel nacional y quizás también a nivel internacional.”</p>
		</div>
	</div>

	<div class="row mt-5">
		<div class="col-12 col-lg-5 mx-auto historias__imagen">
			<a href="https://www.youtube.com/watch?v=jPj6SJNjJvk&rel=0&autoplay=1&showinfo=0&iv_load_policy=3&playlist=jPj6SJNjJvk" data-lity>
			<img class="historias__imagen-img img-fluid" src="{{url('images/historias/historia4.jpg')}}"></a>

			<div class="historias__background historias__background--color-rojo d-flex flex-row">
	            <div class="historias__iso">
	              <img src="{{url('images/W-white.png')}}"  alt="" class="img-fluid proyectos__iso-imagen">
	            </div>
	            <div class="testimonio ml-auto">
	              	<p class="testimonio__item">Nombre: Fernando Gamarra</p>
	              	<p class="testimonio__item">Proyecto: Calidad Educativa</p>
	              	<p class="testimonio__item">Localización: Pachacamac</p>
	              	<p class="testimonio__item">Año: 2017</p>
	            </div>
	        </div>
	    </div>

		<div class="col-12 col-lg-6 historias__descripcion d-flex flex-column justify-content-center align-items-center">

			<div class="historias__box">
				<div class="historias__comilla">
					<img class="historias__comilla-img img-fluid" src="{{url('images/historias/comilla.png')}}">
				</div>
			</div>
			<p class="historias__texto">“Gracias a la Fundación Wiese voy por segunda vez a la Feria de Ruraq Maki, quienes me apoyaron para participar en esta feria muy importante que se hace en Lima. Estoy muy agradecido con ellos.”</p>
		</div>
	</div>

</div>

@include('frontend.partials.newsletter')
@endsection

@section('scripts')
@endsection
