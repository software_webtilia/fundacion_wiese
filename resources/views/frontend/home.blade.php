@extends('frontend.layouts.layout')
@section('title', 'Fundación Wiese')
@section('content')
<div id="carouselExampleIndicators" class="carousel slide slider-container" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
     <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
  </ol>
  <div class="carousel-inner" role="listbox">
    <div class="carousel-item active">
      <a href="{{url('https://www.fundacionwiese.org/blog/fundacion-wiese-ganadora-de-creatividad-empresarial-2018/')}}">
        <img class="carousel-item__imagen d-block" src="{{url('images/banner-creatividad-empresarial.jpg')}}" alt="Fundacion Wiese">
      </a>
    </div>
    <div class="carousel-item">
      <a href="{{url('blog/fundacion-wiese-el-fondo-emprendedor-impulsar-empresas-sociales-en-peru/')}}">
        <img class="carousel-item__imagen d-block" src="{{url('images/banner-fondo-emprendedor.jpg')}}" alt="Fundacion Wiese">
      </a>
    </div>
    <div class="carousel-item">
      <a href="blog/una-nueva-etapa-una-nueva-identidad-visual/">
        <img class="carousel-item__imagen d-block" src="{{url('images/slider12-2.jpg')}}" alt="Fundacion Wiese">
      </a>
    </div>
    <div class="carousel-item">
      <a href="{{url('blog/la-fundacion-wiese-recibe-premio-ipae-a-la-empresa-en-cultura/')}}">
        <img class="carousel-item__imagen d-block" src="{{url('images/slider61.jpg')}}" alt="Fundacion Wiese">
      </a>
    </div>
    <div class="carousel-item">
      <a href="{{url('blog/la-fundacion-wiese-lanza-fondo-de-credito-educativo-para-carreras-tecnicas-en-alianza-con-ipfe/')}}">
        <img class="carousel-item__imagen d-block" src="{{url('images/slider31.jpg')}}" alt="Fundacion Wiese">
      </a>
    </div>
  </div>
</div>
<img class="mision__imagen-w2" src="{{url('images/w-izqquierda.png')}}">
<div class="container mision">
  <div class="row">
    <div class="mt-4 col-12 mision__texto">
      <p class="mision__parrafo wow fadeIn">La Fundación Wiese trabaja para que</p>
      <h2 class="mision__h2 wow fadeIn" data-wow-delay="0.5s">Mejores ciudadanos forjen el Perú del futuro</h2>
      <p class="mision__parrafo wow fadeIn" data-wow-delay="0.8s">Nuestra Misión</p>
      <h2 class="mision__h2 wow fadeIn" data-wow-delay="1s">Movilizamos personas e instituciones para transformar el Perú</h2>
      <p class="mision__parrafo wow fadeIn" data-wow-delay="1.5s">Nuestros Valores</p>
    </div>
  </div>
  <div class="row mt-4 mb-4">
    <div class="col-12 col-md-4">
      <div class="bx-mision pb-4">
        <div class="bx-mision__imagen">
          <img src="images/icono-identidad-3.png" alt="">
        </div>
        <div class="valores">
          <h4 class="valores__titulo">IDENTIDAD</h4>
          <p class="valores__contenido">La conciencia histórica que abraza nuestra diversidad, revalora a los peruanos y es fuente de desarrollo.</p>
        </div>
      </div>
    </div>
    <div class="col-12 col-md-4">
      <div class="bx-mision pb-4">
        <div class="bx-mision__imagen">
          <img src="images/icono-solidaridad-3.png" alt="">
        </div>
        <div class="valores">
          <h4 class="valores__titulo valores__titulo--color-lila">SOLIDARIDAD</h4>
          <p class="valores__contenido">La intervención capaz de transformar el desarrollo de las personas y comunidades más vulnerables.</p>
        </div>
      </div>
    </div>
    <div class="col-12 col-md-4">
      <div class="bx-mision pb-2">
        <div class="bx-mision__imagen">
          <img src="images/icono-equidad-3.png" alt="">
        </div>
        <div class="valores">
          <h4 class="valores__titulo valores__titulo--color-naranja">EQUIDAD</h4>
          <p class="valores__contenido">La igualdad en la valoración de cada peruano y el compromiso para otorgarles igualdad en el acceso a las oportunidades.</p>
        </div>
      </div>
    </div>
    <div class="col-12 valores__boton-purple">
      <a href="{{url('/quienes-somos')}}" class="valores__boton-purple-link">Conócenos <span class="proyectos__separador-flecha"><i class="fas fa-angle-right"></i></span></a>
    </div>
  </div>
</div>
<div class="container-fluid">
  <div class="proyectos">
    <div class="proyectos__title wow bounceInUp">
      <h2 class="proyectos__hprincipal">¿Cómo ayudamos? </h2>
      <h2 class="proyectos__hsecundario">La Fundación Wiese dirige</h2>
    </div>
    <div class="row proyectos__row">
      <div class="col-lg-4">
				<div class="proyectos__container-boton">
					<div class="proyectos__boton">
						<a href="{{url('/complejo-arqueologico-el-brujo')}}" class="proyectos__link">ver más  <span class="proyectos__separador-flecha"><i class="fas fa-angle-right"></i></span> </a>
					</div>
				</div>
        <div class="proyectos__proyecto wow slideInUp">
          <a href="{{url('/complejo-arqueologico-el-brujo')}}">
            <img src="images/proyecto1.png" class="img-fluid proyectos__img" alt="">
          </a>
          <div class="proyectos__background proyectos__background--fondo1">
            <div class="proyectos__iso">
              <img src="images/W-white.png" alt="" class="img-fluid proyectos__iso-imagen">
            </div>
            <div class="proyectos__call2action">
              <span class="proyectos__categoria">Programa</span>
              <br>
              <span class="proyectos__nombre">Arqueológico El Brujo</span>
              <br>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-4">
        <div class="proyectos__container-boton" style="text-align: right" >
          {{-- <span style="font-size:1.2rem;color: white;right: 40px;">Muy Pronto</span> --}}
          <a href="{{url('/fondo-solidario')}}" class="proyectos__link">ver más  <span class="proyectos__separador-flecha"><i class="fas fa-angle-right"></i></span> </a>
					<div class="proyectos__boton">
						<!--a href="{{url('/fondo-wiese')}}" class="proyectos__link">ver más  <span class="proyectos__separador-flecha"><i class="fas fa-angle-right"></i></span> </a-->
					</div>
				</div>
        <div class="proyectos__proyecto wow slideInUp" data-wow-delay="0.1s">
          <!--a href="{{url('/fondo-wiese')}}">
          </a-->
          <img src="images/proyecto2-1.jpg" class="img-fluid proyectos__img" alt="">
          <div class="proyectos__background proyectos__background--fondo2">
            <div class="proyectos__iso">
              <img src="images/W-white.png" alt="" class="img-fluid proyectos__iso-imagen">
            </div>
            <div class="proyectos__call2action">
              <span class="proyectos__categoria">Créditos Educativos</span><br>
              <span class="proyectos__categoria">Ayuda Humanitaria</span><br>
              <span class="proyectos__categoria">Emprendimiento</span><br>
              <br>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-4">
				<div class="proyectos__container-boton">
					<div class="proyectos__boton">
						<a href="{{url('/proyecto-educativo')}}" class="proyectos__link">ver más  <span class="proyectos__separador-flecha"><i class="fas fa-angle-right"></i></span> </a>
					</div>
				</div>
        <div class="proyectos__proyecto wow slideInUp" data-wow-delay="0.2s">
          <a href="{{url('/proyecto-educativo')}}">
            <img src="images/proyecto3.jpg" class="img-fluid proyectos__img" alt="">
          </a>
          <div class="proyectos__background proyectos__background--fondo3">
            <div class="proyectos__iso">
              <img src="images/W-white.png" alt="" class="img-fluid proyectos__iso-imagen">
            </div>
            <div class="proyectos__call2action">
              <span class="proyectos__categoria">Programa</span>
              <br>
              <span class="proyectos__nombre">Cálidad Educativa</span>
              <br>
            </div>
          </div>
        </div>
      </div>
      <!-- <div class="col-12 valores__boton-purple text-center">
        <a href="{{url('/fundacion-wiese-dirige')}}" class="valores__boton-purple-link">Ver todos los proyectos<span class="proyectos__separador-flecha"><i class="fas fa-angle-right"></i></span></a>
      </div> -->
      <div class="col-12 mt-5 info-extra">
        <div class="col-lg-6 info-extra__bloque-izquierda float-left pl-0  wow fadeIn" data-wow-delay="0.3s">
          <div class="info-extra__contenedor">
            <div class="info-extra__contenido d-flex flex-column justify-content-center align-items-center">
              <h2 class="info-extra__h2">Conoce nuestros Proyectos e Iniciativas</h2>
              <div class="info-extra__boton">
                <a href="{{url('/proyectos-iniciativas')}}" class="info-extra__link">ver más<span class="proyectos__separador-flecha"><i class="fas fa-angle-right"></i></span></a>
              </div>
            </div>
            <div class="info-extra__imagen">
              <img src="images/img1.png?v=1" class="info-extra__imagen-img img-fluid" alt="">
            </div>
          </div>
        </div>
        <div class="col-lg-6 info-extra__bloque-derecha float-left pr-0 wow fadeIn" data-wow-delay="0.6s">
          <div class="info-extra__contenedor">
            <div class="info-extra__contenido d-flex flex-column justify-content-center align-items-center">
              <h2 class="info-extra__h2">Conoce nuestras historias</h2>
              <div class="info-extra__boton">
                <a href="{{url('/historias')}}" class="info-extra__link">ver más<span class="proyectos__separador-flecha"><i class="fas fa-angle-right"></i></span></a>
              </div>
            </div>
            <div class="info-extra__imagen">
              <img src="images/img2.png?v=1" class="info-extra__imagen-img img-fluid" alt="">
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid mt-5">
  <div class="row">
    <div class="col-12 mx-auto mb-3">
      <h2 class="novedades__titulo"> Novedades </h2>
    </div>
    <div class="col-md-5ths col-xs-12 mx-auto mb-3 novedades__titulo-img">
      <a href="blog/"> <img src="images/novedades.jpg" class="img-fluid"></a>    
    </div>
    <!-- <div class="col-md-5ths col-xs-6 mx-auto">
      <a href="{{url('novedades/entrega-de-donativos')}}"><img src="{{url('images/novedades/noticia12.JPG')}}" class="img-fluid"></a>
    </div>
    <div class="col-md-5ths col-xs-6 col-noticia">
      <p class="fechas__parrafo"><img src="images/calendar.png" class="fechas__calendario"><span class="fechas__titulo">31 de Julio</span></p>
      <p class="fechas__contenido"><a href="{{url('novedades/entrega-de-donativos')}}">La Fundación Wiese hace entrega de donativos a niños y adultos mayores del sur del Perú</a></p>
      <a class="noticia__link noticia__link--bottom" href="{{ url('/novedades/entrega-de-donativos') }}">Leer Noticia <span class="participa__separador-flecha"><i class="fas fa-angle-right"></i></span></a>
    </div>
    <div class="col-md-5ths col-xs-6 mx-auto mb-3">
      <a href="{{url('novedades/nueva-identidad-visual')}}"><img src="{{url('images/novedades/novedad1.jpg')}}" class="img-fluid"></a>
    </div>
    <div class="col-md-5ths col-xs-6 col-noticia">
      <p class="fechas__parrafo"><img src="images/calendar.png" class="fechas__calendario"><span class="fechas__titulo">28 de Junio</span></p>
      <p class="fechas__contenido"><a href="{{url('novedades/nueva-identidad-visual')}}">Una nueva etapa, una nueva identidad visual</a></p>
      <a class="noticia__link noticia__link--bottom" href="{{ url('/novedades/nueva-identidad-visual') }}">Leer Noticia <span class="participa__separador-flecha"><i class="fas fa-angle-right"></i></span></a>
    </div> -->
    <?php $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"); ?>
    @foreach($articulos as $articulo)
    <div class="col-md-5ths col-xs-6 mx-auto mb-3">
      <a href="{{ url('blog/?p='.$articulo->ID) }}"><img src="{{ $articulo->imagen }}" class="img-fluid"></a>
    </div>
    <div class="col-md-5ths col-xs-6 col-noticia">
      <p class="fechas__parrafo"><img src="images/calendar.png" class="fechas__calendario"><span class="fechas__titulo">{{ date('d',strtotime($articulo->post_date)) ." de " . $meses[date('n',strtotime($articulo->post_date))-1] }}</span></p>
      <p class="fechas__contenido"><a href="{{ url('blog/?p='.$articulo->ID) }}">{{ $articulo->post_title }}</a></p>
      <a class="noticia__link noticia__link--bottom" href="{{ url('blog/?p='.$articulo->ID) }}">Leer Noticia <span class="participa__separador-flecha"><i class="fas fa-angle-right"></i></span></a>
    </div>
    @endforeach
  </div>
</div>
<div class="container-fluid">
  <div class="row">
    <div class="col-12 link-purple">
      <strong><a href="{{url('/blog')}}"> Ver todas <img src="images/flecha-purple.png" alt=""></a></strong>
    </div>
  </div>
</div>
<!-- <div class="container-fluid mt-5">
  <div class="row">
    <div class="col-md-12 text-right pr-0 pl-0">
      <img class="img-fluid" src="images/don augusto.png">
    </div>
  </div>
</div> -->
<div class="objetivos">
  <h2 class="objetivos__titulo wow fadeIn" data-wow-delay="0.3s">Objetivos de desarrollo sostenible</h2>
  <div class="container-fluid objetivos__container-texto">
    <p class="objetivos__descripcion wow fadeIn" data-wow-delay="0.5s">El 25 de septiembre de 2015, los líderes mundiales adoptaron un conjunto de objetivos globales para erradicar la pobreza, proteger el planeta y asegurar la prosperidad para todos como parte de una nueva agenda de desarrollo sostenible. El trabajo
      de la Fundación Wiese está alineado principalmente con los siguientes Objetivos de Desarrollo Sostenible</p>
  </div>
  <div class="container-fluid">
    <div class="row justify-content-center">
      <div class="objetivos__item">
        <a href="https://www.un.org/sustainabledevelopment/es/health/" target="_blank">
          <img class="objetivos__img img-fluid" src="{{url('images/quienes-somos/objetivo1.png')}}">
        </a>
      </div>
      <div class="objetivos__item">
        <a href="https://www.un.org/sustainabledevelopment/es/education/" target="_blank">
          <img class="objetivos__img img-fluid" src="{{url('images/quienes-somos/objetivo2.png')}}">
        </a>
      </div>
      <div class="objetivos__item">
        <a href="https://www.un.org/sustainabledevelopment/es/gender-equality/" target="_blank">
          <img class="objetivos__img img-fluid" src="{{url('images/quienes-somos/objetivo3.png')}}">
        </a>
      </div>
      <div class="objetivos__item">
        <a href="https://www.un.org/sustainabledevelopment/es/economic-growth/" target="_blank">
          <img class="objetivos__img img-fluid" src="{{url('images/quienes-somos/objetivo4.png')}}">
        </a>
      </div>
      <div class="objetivos__item">
        <a href="https://www.un.org/sustainabledevelopment/es/inequality/" target="_blank">
          <img class="objetivos__img img-fluid" src="{{url('images/quienes-somos/objetivo5.png')}}">
        </a>
      </div>
      <div class="objetivos__item">
        <a href="https://www.un.org/sustainabledevelopment/es/cities/" target="_blank">
          <img class="objetivos__img img-fluid" src="{{url('images/quienes-somos/objetivo6.png')}}">
        </a>
      </div>
      <div class="objetivos__item">
        <a href="https://www.un.org/sustainabledevelopment/es/globalpartnerships/" target="_blank">
          <img class="objetivos__img img-fluid" src="{{url('images/quienes-somos/objetivo7.png')}}">
        </a>
      </div>
      <div class="objetivos__item">
        <a href="http://www.undp.org/content/undp/es/home/sustainable-development-goals.html" target="_blank">
          <img class="objetivos__img img-fluid" src="{{url('images/quienes-somos/objetivos.png')}}">
        </a>
      </div>
    </div>
  </div>
</div>
@include('frontend.partials.newsletter')
@endsection
@section('scripts')
<style>
  @media (min-width: 992px) {
    .cabecera_principal {
      height: auto!important;
    }
  }
</style>
@endsection
