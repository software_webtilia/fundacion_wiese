@extends('frontend.layouts.layout_fondo')
@section('title', 'Fundación Wiese')
@section('content')
<div class="participa__banner d-flex align-items-center">
	<img src="{{  url('images/fondo-emprendedor/banner-conoce-fondo.jpg') }}" class="img-fluid" alt="">
</div>
<section class="conoce">
  <div class="container-fluid">
  	<div class="row">
  		<div class="col-md-2 pl-0">
  				<img class="mision__imagen-w img-fluid" src="{{url('images/W-morado.png')}}">
  		</div>
  		<div class="col-md-8">
  			<div class="participa__titulo-box text-center">
  				<h1 class="participa__title conoce__title__color">Fondo Emprendedor</h1>
  				<p class="participa__subtitle">Invierte e impulsa en emprendimientos con claro potencial de crecimiento, autosostenibilidad e impacto social, cuya misión sea la de atender un problema social relevante del país. </p>
  			</div>
  		</div>
  	</div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-md-12 pl-0 text-center">
        <h1 class="conoce__title__color conoce__title__color--tamano">Conoce el Fondo</h1>
      </div>
      <div class="col-xs-12 col-md-5 mx-auto">
        <div class="conoce__contenido">
          <h2 class="conoce__contenido__h2">¿Qué es el <span>FONDO EMPRENDEDOR?</span></h2>
          <p class="conoce__contenido__p">El Fondo Solidario para el Emprendimiento Social (FONDO EMPRENDEDOR) de la Fundación Wiese es una plataforma de Inversión de Impacto que busca identificar e impulsar empresas debidamente constituidas en el Perú que  </p>
          <p class="conoce__contenido__p">(1) hayan sido creadas con la misión de atender un problema social relevante para las comunidades más vulnerables, a través de sus operaciones comerciales, </p>
          <p class="conoce__contenido__p">(2) se encuentren en las etapas tempranas de su operación, es decir previas a su escalamiento comercial,</p>
          <p class="conoce__contenido__p">(3) puedan acreditar alguna evidencia de éxito de su modelo de negocio, en términos de potencial de crecimiento económico, autosostenibilidad e impacto social.</p>
          <p class="conoce__contenido__p">Los emprendimientos sociales seleccionados para ser parte del Portafolio del FONDO EMPRENDEDOR, recibirán asesoría especializada y financiamiento a medida (préstamos blandos, donaciones revolventes y grants), durante un período máximo de 30 meses, contra compromisos formales de escalabilidad comercial, autosostenibilidad e impacto social.</p>
        </div>
      </div>
      <div class="col-xs-12 col-md-5 mx-auto historias__imagen d-flex flex-column justify-content-center">
  			<img class="historias__imagen-img img-fluid" src="{{url('images/historias/foto_video.jpg')}}">
  	  </div>
			<div class="col-xs-12 col-md-6">
				<div class="img-nesst" style="margin: auto; text-align: center;">
					<a href="https://www.nesst.org" target="_blank">
						<img class="historias__imagen-img img-fluid" src="{{url('images/nesst.png')}}" style="width: 170px;">
					</a>
				</div>
			</div>
			<div class="col-md-6 textnesst">
				<p class="conoce__contenido__p">
					<a href="https://www.nesst.org" class="ahrefclass_nesst" target="_blank"><b><u>NESsT:</u></b></a>  es el aliado seleccionado por la Fundación Wiese para operar el Fondo Emprendedor, encargándose de gestionar la convocatoria, el Due diligence y la asesoría /acompañamiento durante la etapa de portafolio. NESsT es una organización internacional que apoya e invierte en empresas sociales que generan empleo y mejoran ingresos de personas en situación de vulnerabilidad.  NESsT ha apoyado 14,000 organizaciones en 55 países para evaluar, identificar y desarrollar modelos de negocio sostenibles.<br>
					Si quieres conocer más sobre Nesst, ingresa <a href="https://www.nesst.org" target="_blank"><b><u>aquí.</u></b></a>
				</p>
			</div>
    </div>
  </div>
	{{-- nuevo--}}
	{{-- FIn de nuevo --}}
  <div class="container mb-5">
    <div class="proyectos proceso__emprendedor">
      <div class="proyectos__title wow bounceInUp">
        <h2 class="proyectos__hprincipal conoce__title__color">El Proceso del Fondo Emprendedor</h2>
      </div>
      <div class="row">
        <div class="col-md-4 col-xs-12 col-sm-12">
          <div class="proceso__emprendedor__item">
						<span class="proceso__emprendedor__item--span"></span>
             <div class="proceso__emprendedor__item--img">
               <span class="numero">1</span>
               <p>3 meses</p>
               <img src="{{asset('images/fondo-emprendedor/iconos/proceso-fondo-1.png')}}"  alt="" class="img-fluid">
             </div>
             <div class="proceso__emprendedor__item--contenido">
               <h3 style="font-size: 1.375rem;">Convocatoria general y admisión de postulaciones</h3>
               <p class="parrafo">del 15/11/2018 al 11/02/2019</p>
							 <br>
							 <p>
							 <a href="" class="postular_emprendedor">Postula aquí</a></p>
							 {{-- <p class="parrafo">Conoce las postulaciones aptas, que serán evaluadas para pasar a la etapa de Due Dilligence</p> --}}
               {{-- <a href="#" class="btn-transparente">Postula aqui <i class="fas fa-angle-right"></i></a> <br> --}}
               {{-- <a href="#" class="pdf"><i class="far fa-file-pdf"></i>PDF descargable</a> --}}
							 {{-- <a href="#" class="pdf pdf--disabled"><i class="far fa-file-pdf"></i> PDF descargable</a> --}}
             </div>
          </div>
        </div>
        <div class="col-md-4 col-xs-12 col-sm-12">
          <div class="proceso__emprendedor__item proceso__emprendedor__item--disabled">
						<span class="proceso__emprendedor__item--span"></span>
             <div class="proceso__emprendedor__item--img">
               <span class="numero numero--disabled">2</span>
               <p>6 meses</p>
               <img src="{{asset('images/fondo-emprendedor/iconos/proceso-fondo-2.png')}}"  alt="" class="img-fluid">
             </div>
             <div class="proceso__emprendedor__item--contenido">
               <h3 style="font-size: 1.375rem;">Postulaciones en etapa de Due Dilligence</h3>
               <p class="parrafo">Marzo 2019 – Noviembre 2019</p>
               <p class="parrafo"><b>Próximamente</b></p>
               {{-- <a href="#" class="pdf pdf--disabled"><i class="far fa-file-pdf"></i> PDF descargable</a> --}}
             </div>
          </div>
        </div>
        <div class="col-md-4 col-xs-12 col-sm-12">
          <div class="proceso__emprendedor__item proceso__emprendedor__item--disabled">
             <div class="proceso__emprendedor__item--img">
               <span class="numero numero--disabled">3</span>
               <p>24 meses</p>
               <img src="{{asset('images/fondo-emprendedor/iconos/proceso-fondo-3.png')}}"  alt="" class="img-fluid">
             </div>
             <div class="proceso__emprendedor__item--contenido">
               <h3 style="font-size: 1.375rem;">Postulaciones en etapa de Portafolio</h3>
               <p class="parrafo">-</p>
              <p class="parrafo"><b>Próximamente</b></p>
               {{-- <a href="#" class="pdf pdf--disabled"><i class="far fa-file-pdf"></i> PDF descargable</a> --}}
             </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="criterios">
    <div class="container">
      <div class="row">
        <div class="col-md-4">
          <div class="criterios__contenido">
            <img src="{{asset('images/fondo-emprendedor/iconos/criterios.svg')}}" alt="criterios">
            <h2>Criterios de Evaluación</h2>
            <p class="conoce__contenido__p">Para la evaluación de las Postulaciones admitidas, el Comité de Selección tendrá en cuenta los siguientes criterios:</p>
            <ul>
              <li style="font-size: 1.1rem;"><i class="fas fa-circle"></i> Impacto Social Actual y Potencial</li>
              <li style="font-size: 1.1rem;"><i class="fas fa-circle"></i> Viabilidad del modelo de negocio </li>
              <li style="font-size: 1.1rem;"><i class="fas fa-circle"></i> Equipo emprendedor</li>
              <li style="font-size: 1.1rem;"><i class="fas fa-circle"></i> Valor agregado del Fondo Emprendedor</li>
							<li style="font-size: 1.1rem;"><i class="fas fa-circle"></i> Innovación</li>
            </ul>
          </div>
        </div>
        <div class="col-md-4">
          <div class="criterios__contenido">
            <img src="{{asset('images/fondo-emprendedor/iconos/postular.svg')}}" alt="Postular">
            <h2>¿Cómo postular?</h2>
            <p class="conoce__contenido__p">Para que tu postulación pueda ser admitida y debidamente considerada, asegúrate de presentarla conforme a Bases para la Aplicación al FONDO EMPRENDEDOR. </p>
            <p><a href="{{url('archivos/fondo-emprendedor.pdf')}}" download="Fondo-Emprendedor"><i class="far fa-file-pdf"></i> Descarga las Bases del Fondo Emprendedor</a></p>
            <p>Descarga el cronograma oficial del proceso de Convocatoria</p>
            <p><a href="{{url('archivos/cronograma-fondo-emprendedor.pdf')}}" download="Cronograma-Fondo-Emprendedor"><i class="far fa-file-pdf"></i> Descarga el Cronograma del Fondo Emprendedor</a></p>
          </div>
        </div>
        <div class="col-md-4">
          <div class="criterios__contenido">
            <img src="{{asset('images/fondo-emprendedor/iconos/fondo-emprendedor.svg')}}" alt="fondo-emprendedor">
            <h2>Postula al fondo emprendedor</h2>
            <p class="conoce__contenido__p">Antes de postular al Fondo Emprendedor, asegúrate de descargarte y leer las <b><a href="{{url('archivos/fondo-emprendedor.pdf')}}" download="Fondo-Emprendedor">Bases del Fondo Emprendedor.</a></b></p><br>
            <a href="{{route('solidario.postula')}}" class="btn-anaranjado">Postula aquí</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection
@push('scripts')
<script src="{{asset('assets/js/contact-emprendedor.js')}}"></script>
<script type="text/javascript">
$(document).ready(function() {
	var images = "{{asset('images/logo-emprendedor.png')}}";
	$("#mi_imagen_person").attr("src",images);
});
</script>
@endpush
