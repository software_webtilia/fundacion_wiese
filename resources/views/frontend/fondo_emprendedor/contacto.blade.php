@extends('frontend.layouts.layout_fondo')
@section('title', 'Fundación Wiese')

@section('content')
<div class="participa__banner participa__banner--position d-flex align-items-center">
	<img src="{{ url('images/fondo-emprendedor/banner-contacto.jpg') }}" class="img-fluid" alt="">
</div>
<img class="mision__imagen-w mision__imagen-w--margin" src="{{url('images/quienes-somos/w-naranja.png')}}">

<div class="container contacto">
	<div class="row">

		<div class="col-12 col-lg-6 contacto__box-texto">
			<h1 class="participa__title">Contáctanos</h1>
			<p class="contacto__texto">Escríbenos y nos pondremos en contacto contigo lo más pronto posible</p>
			<ul class="body__listado infocontacto">
				<li><i class="fas fa-user"></i> <strong>Nombre de contacto:</strong> <label id="name_contact"></label> </li>
				<li><i class="fas fa-phone telefono"></i> Lima: <label id="name_telef"></label> </li>
				<li><i class="fas fa-envelope"></i> <label id="name_email"></label> </li>
			</ul>
		</div>




		<div class="col-12 col-lg-5 contacto__box">
			<h2 class="contacto__formulario-titulo">Déjanos tus datos</h2>
			<form id="fondoContacto" name="fondoContacto" class="contacto__formulario" action="{{ route('savecontact') }}">
				<div class="form-group form-group--margin col-12 ">
			        <select class="form-control contacto__input" id="infocontacto_id" name="infocontacto_id" onchange="getval(this);">
								<option value="">Escoger Programa</option>
									<option value="1">Créditos Educativos</option>
									<option value="2">Ayuda Humanitaria</option>
									<option value="3">Fondo Emprendedor</option>
							</select>
			  </div>
				<div class="form-group form-group--margin col-12 ">
			        <input type="text" class="form-control contacto__input" id="nombres" name="nombres" placeholder="*Ingrese nombres">
			  </div>
				<div class="form-group form-group--margin col-12 ">
			     <input type="text" class="form-control contacto__input" id="apellidos" name="apellidos" placeholder="*Ingrese apellidos">
			  </div>
			  <div class="form-group form-group--margin col-12 ">
			      <input type="email" class="form-control contacto__input" id="email" name="email" placeholder="*Ingrese email">
			  </div>
				<div class="form-group form-group--margin col-12 ">
			        <input type="number" class="form-control contacto__input" id="telefono" name="telefono" maxlength="9" placeholder="*Teléfono">
			   </div>
				<div class="form-group form-group--margin col-12 ">
			        <textarea class="form-control contacto__input" id="mensaje" name="mensaje" rows="4" placeholder="Escribe un mensaje"></textarea>
			  </div>
				<div class="form-group form-group--margin col-12 " style="color: white;">
			        <input id='acepto_chk' name='acepto_chk' type='checkbox' value='1'><a href="{{route('politica.privacidad')}}" style="color: white;"  target=”_blank”>Acepto Política de protección de datos</a>
			   </div>
			  <div class="col-12 col-md-4">
		        <button type="submit" class="form-group  btn contacto__submit">Enviar</button>
		    </div>
			</form>
		</div>

	</div>
</div>

@endsection

{{-- @section('scripts')
<script>
var URL_SITE='{{ url('')}}';
var map;
var infowindow;
var setZoom = 15;

var coordenada = {
  lat: parseFloat(19.4360237),
  lng: parseFloat(-99.1546097)
};

function initMap() {

  map = new google.maps.Map(document.getElementById('map'), {
    center: coordenada,
    zoom: setZoom
  });

}
$('#id_of_field').change(function() {
    // $(this).val() will work here
});

</script>

@endsection --}}
@push('scripts')

<script type="text/javascript">
$(document).ready(function() {
$(".infocontacto").css("display", "none");
$(".btn_contacoclase").css("display", "none");
});
function getval() {
		event.preventDefault(); jQuery.noConflict();
	    var dato = $('#infocontacto_id').val();
			if(dato){
				$(".infocontacto").css("display", "block");
				$.ajax({
								async: false,
								type: 'GET',
								url: 'infocontacto/'+dato,
								data: {},
								dataType: 'json',
								success: function(response) {
									$("#name_contact").text(response.infocontact.nombre);
									$("#name_telef").text(response.infocontact.telefono);
									$("#name_email").text(response.infocontact.email);
								},
								error: function(jqXHR, status, err) {
										alert("Local error callback.");
										console.log(err);
								}
				});
			}else{
				$(".infocontacto").css("display", "none");
			}
}

</script>
@endpush
