@extends('frontend.layouts.layout_fondo')
@section('title', 'Fundación Wiese')

@section('content')
<div class="participa__banner d-flex align-items-center">
	<img src="{{  url('images/fondo-emprendedor/banner-conoce-fondo.jpg') }}" class="img-fluid" alt="">
</div>
<section class="postula">
  <div class="container-fluid">
  	<div class="row">
  		<div class="col-md-2 pl-0">
  				<img class="mision__imagen-w img-fluid" src="{{url('images/W-morado.png')}}">
  		</div>
  		<div class="col-md-8">
  			<div class="participa__titulo-box text-center postula__titulo">
          <span class="participa__titulo--miga"><a href="{{route('solidario.conocefondo')}}"><i class="fas fa-angle-left"></i> Volver</a></span>
  				<h1 class="participa__title conoce__title__color">Postula al Fondo Emprendedor </h1>
  				<p class="participa__subtitle participa__subtitle--postula">Para que tu postulación pueda ser admitida y debidamente considerada, asegúrate de presentarla conforme a Bases para la Aplicación al FONDO EMPRENDEDOR. Por ello, antes de postular, asegúrate de descargarte y leer las <b><a href="{{url('archivos/fondo-emprendedor.pdf')}}" download="Fondo-Emprendedor">Bases del Fondo Emprendedor.</a></b></p>
  			</div>
  		</div>
  	</div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-xs-12 col-sm-12 mx-auto">
        <div class="postula__form">
          {{-- <script type="text/javascript" src="https://nesstforms.formstack.com/forms/js.php/portfolio_application_form_esp"></script><noscript><a href="https://nesstforms.formstack.com/forms/portfolio_application_form_esp" title="Online Form">Online Form - Portfolio Application Form Spanish</a></noscript> --}}
					<script type="text/javascript" src="https://nesstforms.formstack.com/forms/js.php/wiese_application_form"></script><noscript><a href="https://nesstforms.formstack.com/forms/wiese_application_form" title="Online Form">Online Form - Wiese Application Form ESP</a></noscript>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection

@push('scripts')
	<script type="text/javascript">
	$(document).ready(function() {
	  $(".btn_contacoclase").css("display", "none");
	});
	</script>
@endpush
