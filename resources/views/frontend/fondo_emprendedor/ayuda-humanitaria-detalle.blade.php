@extends('frontend.layouts.layout_fondo')
@section('title', 'Fundación Wiese')

@section('content')
<div class="participa__banner d-flex align-items-center">
	<img src="{{ url('images/fondo-emprendedor/banner-ayuda-humanitaria.jpg') }}" class="img-fluid" alt="">
</div>

<div class="container-fluid">
	<div class="row">
		<div class="col-md-2 pl-0">
				<img class="mision__imagen-w img-fluid" src="{{url('images/proyectos-dirige/w-magenta.png')}}">
		</div>
		<div class="col-md-8">
			<div class="participa__titulo-box text-center">
        <span class="participa__titulo--miga"><a href="{{route('solidario.humanitaria')}}"><i class="fas fa-angle-left"></i>  Ayuda Humanitaria</a></span>
				<h1 class="participa__title participa__title--color">Campaña reciente</h1>
				<p class="participa__subtitle">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Phasellus hendrerit. Pellentesque aliquet nibh nec urna. In nisi neque, aliquet vel, dapibus id, mattis vel, nisi. </p>
			</div>
		</div>
		<div class="col-md-2">
		</div>
	</div>
</div>
<section class="ayuda__detalle">
  <div class="container">
    <div class="row">
        <div class="col-md-6">
            <div class="ayuda__detalle__contenido">
              <p>Sed egestas, ante et vulputate volutpat, eros pede semper est, vitae luctus metus libero eu augue. Morbi purus libero, faucibus adipiscing, commodo quis, gravida id, est. Sed lectus. Praesent elementum hendrerit tortor. Sed semper lorem at felis. Vestibulum volutpat, lacus a ultrices sagittis, mi neque euismod dui, eu pulvinar nunc sapien ornare nisl. Phasellus pede arcu, dapibus eu, fermentum et, dapibus sed, urna.</p>
              <p>Morbi interdum mollis sapien. Sed ac risus. Phasellus lacinia, magna a ullamcorper laoreet, lectus arcu pulvinar risus, vitae facilisis libero dolor a purus. Sed vel lacus. Mauris nibh felis, adipiscing varius, adipiscing in, lacinia vel, tellus. Suspendisse ac urna. Etiam pellentesque mauris ut lectus. Nunc tellus ante, mattis eget, gravida vitae, ultricies ac, leo. Integer leo pede, ornare a, lacinia eu, vulputate vel, nisl.</p>
              <p>Praesent dapibus, neque id cursus faucibus, tortor neque egestas auguae, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus.</p>
              <p class="participa__item-descripcion participa__item-descripcion--pdf"><a href="#"><i class="far fa-file-pdf"></i> Informe de cierre de campaña</a></p>
            </div>
        </div>
        <div class="col-md-5 mx-auto">
          <img src="{{ url('images/fondo-emprendedor/ayuda-detalle-1.jpg') }}"  class="img-fluid" alt="">
        </div>
    </div>
  </div>
</section>

@endsection

@section('scripts')

@endsection
