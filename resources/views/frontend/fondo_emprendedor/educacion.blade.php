@extends('frontend.layouts.layout_fondo')
@section('title', 'Fundación Wiese')
@section('content')
<div class="d-flex align-items-center">
	<img src="{{ url('images/fondo-emprendedor/banner-educacion.jpg') }}" class="img-fluid" alt="">
</div>

<div class="container-fluid">
	<div class="row">
		<div class="col-md-2 pl-0">
				<img class="mision__imagen-w img-fluid" src="{{url('images/quienes-somos/w-naranja.png')}}">
		</div>
		<div class="col-md-8">
			<div class="participa__titulo-box text-center">
				<h1 class="participa__title">Programa de Créditos Educativos</h1>operado por IPFE
				<p class="participa__subtitle">Financia programas de crédito educativo para peruanos talentosos de escasos recursos económicos, aspirando a permitirles transformar su vida y la de sus familias, a través de la educación.</p>
			</div>
		</div>
		<div class="col-md-2">
		</div>
	</div>
</div>


<div class="container">
  <div class="row credito-educativo">
    <div class="col-md-6">
      <img class="img-fluid"  alt="" src="{{ url('images/fondo-emprendedor/programa-credito-educativo.jpg')}}">
    </div>

    <div class="col-md-6">
      <h2 class="credito-educativo__titulo">Sobre el Programa de Créditos Educativos de la Fundación Wiese</h2>
      <p class="credito-educativo__parrafo">
				El Programa de Créditos Educativos de la Fundación Wiese, operado por IPFE, está dirigido por el momento a estudiantes de carreras técnicas de SENATI, a quiénes les ofrece el financiamiento de la segunda mitad de su carrera. El Programa maneja condiciones sociales de crédito, es decir busca adaptar los desembolsos mensuales que sus beneficiarios deben realizar para costear sus estudios, a sus posibilidades reales de pago, mediante tasas de interés subvencionadas.</p>
      <p class="credito-educativo__parrafo">
				Adicionalmente, el Programa brinda a sus beneficiarios una serie de talleres orientados al desarrollo de sus habilidades socio-emocionales, así como un sistema de acompañamiento y soporte emocional permanente, hasta la culminación de sus estudios.
      </p>
			<p class="credito-educativo__parrafo">
				De esta manera, la Fundación Wiese contribuye a disminuir la deserción educativa de jóvenes talentosos que tienen clara su vocación y facilitarles su adecuada inserción en el mercado laboral.
      </p>
    </div>
  </div>

  <div class="row personajes-educativo">

    <div class="col-md-3 text-center">
			<a href="#" data-toggle="modal" data-target="#modaleducacion_pago"><img src="{{ url('images/fondo-emprendedor/educacion-monto.jpg')}}" alt="" class="img-fluid"></a>
    </div>

    <div class="col-md-6 text-center">
      <img src="{{ url('images/fondo-emprendedor/educacion-personajes.jpg')}}" alt="" class="img-fluid">
    </div>

    <div class="col-md-3 text-center">
			<a href="#" data-toggle="modal" data-target="#modaleducacion_benefi"><img src="{{ url('images/fondo-emprendedor/educacion-beneficiario.jpg')}}" alt="" class="img-fluid"></a>
    </div>

  </div>

</div>

<div class="container-fluid mb-5">
  <div class="proyectos">
    <div class="proyectos__title wow bounceInUp">
      <h2 class="proyectos__hprincipal proyectos__hprincipal--naranja">Preguntas frecuentes</h2>
    </div>
  </div>

<div class="preguntas-frecuentes">

<div id="accordion">
  <div class="card">
    <div class="card-header" id="heading1">
      <h5 class="mb-0">
        <a class="btn btn-link pregunta" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
          I. ¿Quiénes intervienen en el crédito educativo?
        </a>
      </h5>
    </div>

    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
      <div class="card-body">
        <ul class="respuesta-lista">
          <li class="respuesta-lista__item">El Beneficiario (estudiante).</li>
          <li class="respuesta-lista__item">El Titular del Crédito (responsable del pago, que podría ser el mismo estudiante, en caso pueda acreditar ingresos).</li>
          <li class="respuesta-lista__item">El Fiador Solidario (aval).</li>
        </ul>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="heading2">
      <h5 class="mb-0">
        <a class="btn btn-link collapsed pregunta" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
          II. ¿Cuáles son los requisitos para postular al programa?
        </a>
      </h5>
    </div>
    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
      <div class="card-body">
        <ul class="respuesta-lista">
          <li class="respuesta-lista__item">Haber concluido, al menos, el III semestre de estudios.</li>
					<li class="respuesta-lista__item">Tener un promedio académico de I, II y III ciclo aprobado (promedio ponderado), al momento de la postulación.</li>
					<li class="respuesta-lista__item">Haber concluido el tercer semestre académico y haber sido promovido al ciclo siguiente.</li>
					<li class="respuesta-lista__item">Completar y enviar el formato de Solicitud de Crédito Educativo a creditos.fw@ipfe.org.pe.</li>
					<li class="respuesta-lista__item">Asistir a la cita personal programada por IPFE para evaluar tu caso particular y definir los pasos a seguir.</li>
        </ul>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="heading3">
      <h5 class="mb-0">
        <a class="btn btn-link collapsed pregunta" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
          III. ¿Qué financia el programa?
        </a>
      </h5>
    </div>
    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
      <div class="card-body">
        <ul class="respuesta-lista">
          <li class="respuesta-lista__item">El programa financia el pago de matrícula y/o pensión del IV, V y/o VI ciclo académico.</li>
        </ul>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="heading4">
      <h5 class="mb-0">
        <a class="btn btn-link collapsed pregunta" data-toggle="collapse" data-target="#collapse4" aria-expanded="false" aria-controls="collapseThree">
          IV. ¿Cuáles son las condiciones y beneficios?
        </a>
      </h5>
    </div>
    <div id="collapse4" class="collapse" aria-labelledby="heading4" data-parent="#accordion">
      <div class="card-body">
        <ul class="respuesta-lista">
          <li class="respuesta-lista__item">Los créditos son en soles.</li>
          <li class="respuesta-lista__item">Tasa de interés social del 8% (las tasas referenciales en el mercado bordean el 20%).</li>
          <li class="respuesta-lista__item">No se cobran portes, ni comisiones de ningún tipo.</li>
					<li class="respuesta-lista__item">Cuotas equivalentes al 50% de la pensión académica.</li>
					<li class="respuesta-lista__item">Periodo de repago inicia a los 30 días después del desembolso del crédito educativo.</li>
					<li class="respuesta-lista__item">Plazos de pago de 12 hasta 36 meses.</li>
        </ul>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="heading5">
      <h5 class="mb-0">
        <a class="btn btn-link collapsed pregunta" data-toggle="collapse" data-target="#collapse5" aria-expanded="false" aria-controls="collapseThree">
          V. ¿Qué documentos requiere IPFE para evaluar y aprobar tu crédito?
        </a>
      </h5>
    </div>
    <div id="collapse5" class="collapse" aria-labelledby="heading5" data-parent="#accordion">
      <div class="card-body">
				A continuación presentamos una relación referencial de la documentación requerida. Sin embargo, para la Fundación Wiese cada caso es particular.</br>
				Por lo tanto, es importante que completes la Solicitud de Crédito Educativo, con al menos todos tus datos de contacto, de manera que IPFE pueda programar una cita contigo para asesorarte adecuadamente.</p>
        <p class="pl-3"><strong>Documentos del estudiante:</strong></p>
        <ul class="respuesta-lista">
          <li class="respuesta-lista__item">Copia de DNI vigente.</li>
          <li class="respuesta-lista__item">Solicitud de crédito debidamente llenada y firmada el Titular y Fiador Solidario.</li>
          <li class="respuesta-lista__item">Presupuesto de ingresos y egresos.</li>
          <li class="respuesta-lista__item">Último consolidado de notas (SINFO).</li>
          <li class="respuesta-lista__item">Cotización del ciclo a estudiar o cronograma de pagos (SINFO).</li>
          <li class="respuesta-lista__item">Copia del último recibo cancelado de un servicio público (agua, luz o teléfono).</li>
          <li class="respuesta-lista__item">Croquis del domicilio.</li>
        </ul>
        <p class="pl-3 pt-2"><strong>Documentos del Titular (responsable del pago) y Fiador solidario (aval) y cónyuges de ser el caso.
        <br>Para trabajadores dependientes:</strong>
        </p>
        <ul class="respuesta-lista">
          <li class="respuesta-lista__item">Copia de DNI vigente.</li>
          <li class="respuesta-lista__item">Solicitud de crédito debidamente llenada y firmada el Titular y Fiador Solidario.</li>
          <li class="respuesta-lista__item">Presupuesto de ingresos y egresos.</li>
          <li class="respuesta-lista__item">Último consolidado de notas (SINFO).</li>
          <li class="respuesta-lista__item">Cotización del ciclo a estudiar o cronograma de pagos (SINFO).</li>
          <li class="respuesta-lista__item">Copia del último recibo cancelado de un servicio público (agua, luz o teléfono).</li>
          <li class="respuesta-lista__item">Croquis del domicilio.</li>
        </ul>
				<p class="pl-3 pt-2"><strong>Para trabajadores independientes:</strong>
        </p>
        <ul class="respuesta-lista">
          <li class="respuesta-lista__item">Copia de DNI vigente.</li>
          <li class="respuesta-lista__item">Fotocopia de los recibos por honorarios de los 6 últimos meses.</li>
          <li class="respuesta-lista__item">Fotocopia del Contrato de locación de servicios.</li>
          <li class="respuesta-lista__item">Copia del último recibo cancelado de un servicio público (agua, luz o teléfono).</li>
          <li class="respuesta-lista__item">Croquis de domicilio</li>
        </ul>
      </div>
     </div>
  </div>
	<div class="card">
    <div class="card-header" id="heading6">
      <h5 class="mb-0">
        <a class="btn btn-link collapsed pregunta" data-toggle="collapse" data-target="#collapse6" aria-expanded="false" aria-controls="collapseThree">
          VI. ¿Qué debes tener en cuenta?
        </a>
      </h5>
    </div>
    <div id="collapse6" class="collapse" aria-labelledby="heading6" data-parent="#accordion">
      <div class="card-body">
        <ul class="respuesta-lista">
          <li class="respuesta-lista__item">En caso de aprobarse el crédito, IPFE se comunicará con el estudiante beneficiario para entregarle ciertos documentos que deberá leer, comprender y suscribir en señal de conformidad, como por ejemplo el Reglamento del Programa de Crédito de Estudios.</li>
          <li class="respuesta-lista__item">Asimismo se le informará al beneficiario respecto al cronograma de actividades incluidas en el programa en las que deberá participar.</li>
        </ul>
      </div>
    </div>
  </div>
	<div class="card">
    <div class="card-header" id="heading7">
      <h5 class="mb-0">
        <a class="btn btn-link collapsed pregunta" data-toggle="collapse" data-target="#collapse7" aria-expanded="false" aria-controls="collapseThree">
					¿Cómo puedes obtener mayor información del programa?
        </a>
      </h5>
    </div>
    <div id="collapse7" class="collapse" aria-labelledby="heading7" data-parent="#accordion">
      <div class="card-body">
				<p class="pl-3 pt-2"><strong>Contáctanos</strong> </p>
        <ul class="respuesta-lista">
          <li class="respuesta-lista__item">Nombre de contacto: Carolina Boza</li>
          <li class="respuesta-lista__item">Telefono : 965 407 559</li>
					<li class="respuesta-lista__item">Correo: creditos.fw@ipfe.org.pe</li>
				  <li class="respuesta-lista__item">Para más información <a href="#" class="btn_contacoclase"><u>contáctanos aquí</u></a></li>

        </ul>
      </div>
    </div>
  </div>
</div>
</div>
</div>
<!-- Modal educacion -->
<div class="modal fade bd-example-modal-lg modal__educacion" id="modaleducacion_pago" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-body modal__educacion__body">
        <button type="button" class="close btn-cerrar" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <div class="container-fluid">
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
              <div class="">
                <img class="img-fluid" src="{{url('images/fondo-emprendedor/chico.jpg')}}">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade bd-example-modal-lg modal__educacion" id="modaleducacion_benefi" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-body modal__educacion__body">
        <button type="button" class="close btn-cerrar" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <div class="container-fluid">
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
              <div class="">
                <img class="img-fluid" src="{{url('images/fondo-emprendedor/chica.jpg')}}">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


<div class="modal fade bd-example-modal-lg modal__fondo__emprendedor" id="modalcontacto2" tabindex="-1" role="dialog" aria-labelledby="pruebamodalTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-body modal__fondo__emprendedor__body">
        <button type="button" class="close btn-cerrar" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <div class="container-fluid">
          <div class="row">
            	<img class="franja img-fluid" src="{{url('images/W-morado.png')}}">
            <div class="col-xs-12 col-sm-12 col-md-6">
              <div class="modal__fondo__emprendedor__body__contenido">
                <h2>Fondo emprendedor, operado por NESST</h2>
                <ul class="body__listado">
                  <li><i class="fas fa-user"></i> <strong>Nombre de contacto:</strong> <label id="namecontact2"></label> </li>
                  <li><i class="fas fa-phone telefono"></i> Lima: <label id="nametelef2"></label> </li>
                  <li><i class="fas fa-envelope"></i> <label id="namecorreo2"></label> </li>
                </ul>
              </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6">
              <div class="modal__fondo__emprendedor__body__formulario">
                <h4>Escríbenos</h4>
                <p>Escríbenos y nos pondremos en contacto contigo lo más pronto posible</p>
                <form class="row"  id="save_contacto2">
                  <div class="form-group col-md-6">
                    <input type="text" class="form-control modal__input modal__input--tamano" id="fondoNombres" name="nombres" placeholder="Ingrese nombres" />
                  </div>
                  <div class="form-group col-md-6">
                    <input type="text" class="form-control modal__input modal__input--tamano" id="fondoApellidos" name="apellidos"  placeholder="Ingrese apellidos" />
                  </div>
                  <div class="form-group col-md-6">
                    <input type="email" class="form-control modal__input modal__input--tamano" id="fondoEmail" name="email"  placeholder="Ingrese email" required/>
                  </div>
                  <div class="form-group col-md-6">
                    <input type="text" class="form-control modal__input modal__input--tamano" id="fondoTelefono" name="telefono"  placeholder="Télefono" required/>
                  </div>
                  <div class="form-group col-12">
          			        <textarea class="form-control modal__input" id="fondoMensaje" name="mensaje" rows="4" placeholder="Mensaje" required/></textarea>
          			  </div>
                  <div class="col-xs-12 col-md-4">
          		        <button type="submit" class="form-group  btn contacto__submit">Enviar</button>
          		    </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>



@endsection

@push('scripts')
<script src="{{asset('assets/js/contact-education.js')}}"></script>
<script type="text/javascript">
$(document).ready(function() {
	var images = "{{asset('images/logo-educacion.png')}}";
	$("#mi_imagen_person").attr("src",images);
});

$("#btn_contact2").click( function(){
  		event.preventDefault(); jQuery.noConflict();
      $.ajax({
      		    async: false,
      		    type: 'GET',
      		    url: '{{route('infocontacto',3)}}',
      		    data: {},
      		    dataType: 'json',
      		    success: function(response) {
      					$("#namecontact2").text(response.infocontact.nombre);
      					$("#nametelef2").text(response.infocontact.telefono);
      					$("#namecorreo2").text(response.infocontact.email);
      					$('#modalcontacto2').modal('show');

      		    },
      		    error: function(jqXHR, status, err) {
      		        alert("Local error callback.");
      		        console.log(err);
      		    }
      });
});
$('#save_contacto2').on('submit', function(e) {
	e.preventDefault();
	var formData = new FormData(this);
	formData.append('_token', $('input[name=_token]').val());
	formData.append('infocontacto_id', 3);
	$.ajax({
	            type:'POST',
	            url: '{{route('savecontact')}}',
	            data:formData,
	            cache:false,
	            contentType: false,
	            processData: false,
	            success:function(data){
								$('#save_contacto2')[0].reset();
								$('#modalcontacto2').modal('hide');
	              $("#fondoNombres").val("");
								$("#fondoApellidos").val("");
								$("#fondoEmail").val("");
								$("#fondoTelefono").val("");
								$("#mensaje").val("");
								swal(
							 'Excelente!!!',
							 data.mensaje,
							 'success'
							 )


	            },
	            error: function(jqXHR, text, error){
	                console.log(error);
	            }
	        });


});
</script>
@endpush
