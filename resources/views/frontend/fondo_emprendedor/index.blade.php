@extends('frontend.layouts.layout_fondo')
@section('title', 'Fundación Wiese')
@section('content')
<div class="container-fluid">
  <div class="banner-fondo row">
    <div class="col-md-12 pl-0 pr-0">
      <img src="{{url('images/fondo-emprendedor/fondo-solidario.jpg')}}" class="img-fluid" alt="">
      <div class="banner-fondo__parrafo">
          <h1 class="banner-fondo__titulo">Fondo Solidario</h1>
          <p>Plataforma creada por la Fundación Wiese con el objetivo de ampliar su impacto social, a través de la dirección y patrocinio de programas operados por terceros.</p>
      </div>
    </div>
  </div>
  <div class="botones-fondo row">
    <div class="col-md-4 pl-1 pr-1 col-xs-12 col-sm-12">
      <div class="info-extra__contenedor">
        <div class="info-extra__contenido d-flex flex-column justify-content-center align-items-center">
          <h2 class="info-extra__h2">Créditos Educativos</h2>
          <p class="info-extra__parrafo">Financia programas de crédito educativo para peruanos talentosos de escasos recursos económicos, aspirando a permitirles transformar su vida y la de sus familias, a través de la educación.</p>
          <div class="info-extra__boton">
            <a href="{{route('solidario.education')}}" class="info-extra__link">ver más<span class="proyectos__separador-flecha"><i class="fas fa-angle-right"></i></span></a>
          </div>
        </div>
        <div class="info-extra__imagen">
          <img src="images/fondo-emprendedor/proyecto-fondo1.jpg" class="info-extra__imagen-img img-fluid" alt="">
        </div>
      </div>
    </div>
    <div class="col-md-4 pl-1 pr-1 col-xs-12 col-sm-12">
      <div class="info-extra__contenedor">
        <div class="info-extra__contenido d-flex flex-column justify-content-center align-items-center">
          <h2 class="info-extra__h2">Ayuda Humanitaria</h2>
          <p class="info-extra__parrafo">Contribuye directamente y canaliza las donaciones de personas e instituciones para atender a las comunidades más vulnerables ante situaciones de emergencia y desastres.</p>
          <div class="info-extra__boton">
            <a href="{{route('solidario.humanitaria')}}" class="info-extra__link">ver más<span class="proyectos__separador-flecha"><i class="fas fa-angle-right"></i></span></a>
          </div>
        </div>
        <div class="info-extra__imagen">
          <img src="images/fondo-emprendedor/proyecto-fondo2.jpg" class="info-extra__imagen-img img-fluid" alt="">
        </div>
      </div>
    </div>
    <div class="col-md-4 pl-1 pr-1 col-xs-12 col-sm-12">
      <div class="info-extra__contenedor">
        <div class="info-extra__contenido d-flex flex-column justify-content-center align-items-center">
          <h2 class="info-extra__h2">Fondo Emprendedor</h2>
          <p class="info-extra__parrafo">Impulsa emprendimientos con claro potencial de crecimiento, autosostenibilidad e impacto social, cuya misión sea la de atender un problema social relevante del país.</p>
          <div class="info-extra__boton">
            <a href="{{route('solidario.conocefondo')}}" class="info-extra__link">ver más<span class="proyectos__separador-flecha"><i class="fas fa-angle-right"></i></span></a>
          </div>
        </div>
        <div class="info-extra__imagen">
          <img src="images/fondo-emprendedor/proyecto-fondo3.jpg" class="info-extra__imagen-img img-fluid" alt="">
        </div>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid mt-5">
  <div class="row">
    <div class="col-12 mx-auto mb-3">
      <h2 class="novedades__titulo"> Novedades </h2>
    </div>
    <div class="col-md-5ths col-xs-12 mx-auto mb-3 novedades__titulo-img">
      <a href="{{url('/blog/categoria/fondo-solidario/')}}"><img src="images/novedades.jpg" class="img-fluid"></a>    
    </div>
    <?php $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"); ?>
    @foreach($articulos as $articulo)

      <!--?php if($articulo->ID == 2255 || $articulo->ID == 2016) {?-->
        <div class="col-md-5ths col-xs-6 mx-auto mb-3">
          <a href="{{ url('blog/?p='.$articulo->ID) }}"><img src="{{ $articulo->imagen }}" class="img-fluid"></a>
        </div>
        <div class="col-md-5ths col-xs-6 col-noticia">
          <p class="fechas__parrafo"><img src="images/calendar.png" class="fechas__calendario"><span class="fechas__titulo">{{ date('d',strtotime($articulo->post_date)) ." de " . $meses[date('n',strtotime($articulo->post_date))-1] }}</span></p>
          <p class="fechas__contenido"><a href="{{ url('blog/?p='.$articulo->ID) }}">{{ $articulo->post_title }}</a></p>
          <a class="noticia__link noticia__link--bottom" href="{{ url('blog/?p='.$articulo->ID) }}">Leer Noticia <span class="participa__separador-flecha"><i class="fas fa-angle-right"></i></span></a>
        </div>
      <!--?php } ?-->
    @endforeach
  </div>
</div>

<div class="container-fluid">
  <div class="row">
    <div class="col-12 link-purple">
      <a href="{{url('/blog/categoria/fondo-solidario/')}}"> Ver todas <img src="images/flecha-purple.png" alt=""></a>
    </div>
  </div>
</div>


@endsection

@push('scripts')
<script type="text/javascript">
$(document).ready(function() {
  $(".btn_contacoclase").css("display", "none");
});
$("#btn_contact2").click( function(){
  		event.preventDefault(); jQuery.noConflict();
      $.ajax({
      		    async: false,
      		    type: 'GET',
      		    url: '{{route('infocontacto',3)}}',
      		    data: {},
      		    dataType: 'json',
      		    success: function(response) {
      					$("#namecontact2").text(response.infocontact.nombre);
      					$("#nametelef2").text(response.infocontact.telefono);
      					$("#namecorreo2").text(response.infocontact.email);
      					$('#modalcontacto2').modal('show');
      		    },
      		    error: function(jqXHR, status, err) {
      		        alert("Local error callback.");
      		        console.log(err);
      		    }
      });
});
$('#save_contacto2').on('submit', function(e) {
	e.preventDefault();
	var formData = new FormData(this);
	formData.append('_token', $('input[name=_token]').val());
	formData.append('infocontacto_id', 3);
	$.ajax({
	            type:'POST',
	            url: '{{route('savecontact')}}',
	            data:formData,
	            cache:false,
	            contentType: false,
	            processData: false,
	            success:function(data){
								$('#save_contacto2')[0].reset();
								$('#modalcontacto2').modal('hide');
	              $("#fondoNombres").val("");
								$("#fondoApellidos").val("");
								$("#fondoEmail").val("");
								$("#fondoTelefono").val("");
								$("#mensaje").val("");
								swal(
							 'Excelente!!!',
							 data.mensaje,
							 'success'
							 )
	            },
	            error: function(jqXHR, text, error){
	                console.log(error);
	            }
	        });
});
</script>
@endpush
