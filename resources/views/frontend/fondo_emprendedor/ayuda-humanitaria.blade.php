@extends('frontend.layouts.layout_fondo')
@section('title', 'Fundación Wiese')

@section('content')
<div class="participa__banner d-flex align-items-center">
	<img src="{{  url('images/fondo-emprendedor/banner-ayuda-humanitaria.jpg') }}" class="img-fluid" alt="">
</div>

<div class="container-fluid">
	<div class="row">
		<div class="col-md-2 pl-0">
				<img class="mision__imagen-w img-fluid" src="{{url('images/proyectos-dirige/w-magenta.png')}}">
		</div>
		<div class="col-md-8">
			<div class="participa__titulo-box text-center">
				<h1 class="participa__title participa__title--color">Programa de Ayuda Humanitaria</h1>
				<p class="participa__subtitle">Contribuye directamente y canaliza las donaciones de personas e instituciones para atender a las comunidades más vulnerables ante situaciones de emergencia y desastres. </p>
        <h2 class="participa__item-title">Sobre el Programa de Atención de Emergencias y Desastres</h2>
				<p class="participa__subtitle participa__subtitle--margin">La Fundación Wiese destina anualmente un monto fijo de dinero a activar e impulsar una campaña de ayuda humanitaria. Esta campaña anual busca contribuir a aliviar el sufrimiento de comunidades vulnerables que deben enfrentar situaciones de emergencias y desastres.</p>
			</div>
		</div>
		<div class="col-md-2">
		</div>
	</div>
</div>

<div id="filters" class="container">
  <ul>
		  <li class="participa__li">
				<div class="row participa__item">
					<div class="participa__imagen col-12 col-lg-6 pb-3 pl-0">
						<img class="participa__imagen-img img-fluid" src="../images/fondo-emprendedor/ayuda/ayuda-2.jpg">
					</div>
					<div class="participa__box col-12 col-lg-6 pb-3">
					  	<h2 class="participa__item-title">Tu Solidaridad Abriga (2018) </h2>
              <p class="participa__programa mt-3">Campañas de auxilio ante emergencias</p>
              <p  class="participa__item-descripcion">La particularmente severa y prolongada ola de frío que en el 2018 enfrentaron las zonas alto andinas de nuestro país ocasionó daños a la vida, la salud y la economía de los habitantes más vulnerables de 11 regiones del país, declaradas en situación de emergencia por el Estado Peruano.
               Ante ello, la FUNDACIÓN WIESE, conjuntamente con ADRA Perú y Panamericana TV, impulsó y promovió una campaña de ayuda humanitaria que proporcionó ropa de abrigo a 3002 niños y adultos mayores, así como kits x 2 frazadas a 1400 familias que viven en 54 distritos ubicados por encima de los 3,500 m.s.n.m. de cuatro regiones del país: Tacna, Puno, Huancavelica y Cuzco. Este año, estos distritos soportaron temperaturas de hasta -18Cº. </p>
               <span class="participa__item-descripcion participa__item-descripcion--label">Estado:</span>
               <span class="participa__item-descripcion participa__item-descripcion--estado"><strong>cerrado</strong></span>
               <p class="participa__item-descripcion participa__item-descripcion--pdf"><a href="{{url('archivos/informe_solidaridad_abriga.pdf')}}" download="InformeSolidaridad"><i class="far fa-file-pdf"></i> Informe de cierre de campaña</a></p>
					</div>
				</div>
			</li>
      <li class="participa__li">
				<div class="row participa__item">
					<div class="participa__imagen col-12 col-lg-6 pb-3 pl-0">
						<img class="participa__imagen-img img-fluid" src="../images/fondo-emprendedor/ayuda/ayuda-3.jpg">
					</div>
					<div class="participa__box col-12 col-lg-6 pb-3">
					  	<h2 class="participa__item-title">Juntos por la Costa Norte (2017) </h2>
              <p class="participa__programa mt-3">Campañas de auxilio ante desastres</p>
              <p  class="participa__item-descripcion">Ante los devastadores efectos de los huaycos e inundaciones ocurridos en el primer trimestre de 2017, como consecuencia del Fenómeno del Niño, la Fundación Wiese tomó acción inmediata, articulando y gestionando una red de ayuda humanitaria en alianza con diversas instituciones públicas y privadas, con el objetivo de aliviar el sufrimiento de los damnificados más vulnerables en las regiones de Piura, Lambayeque y La Libertad. Esta campaña recibió el nombre de “Juntos por la Costa Norte”, y a través de ella se logró atender oportuna y eficazmente a 15,620 familias.</p>
              <span class="participa__item-descripcion participa__item-descripcion--label">Estado:</span>
              <span class="participa__item-descripcion participa__item-descripcion--estado"><strong>cerrado</strong></span>
              <p class="participa__item-descripcion participa__item-descripcion--pdf"><a href="#"><i class="far fa-file-pdf"></i> Informe de cierre de campaña</a></p>
					</div>
				</div>
			</li>
	</ul>
	<!--<div class="participa__msj">
			<h4>Lo sentimos, no hay resultados de búsqueda para las opciones que seleccionaste.</h4>
	</div>-->
</div>

@endsection


@push('scripts')
<script src="{{asset('assets/js/contact-ayuda.js')}}"></script>
<script type="text/javascript">
$(document).ready(function() {
	var images = "{{asset('images/logo-ayudahumanitaria.png')}}";
	$("#mi_imagen_person").attr("src",images);
});


$("#btn_contact2").click( function(){
  		event.preventDefault(); jQuery.noConflict();
      $.ajax({
      		    async: false,
      		    type: 'GET',
      		    url: '{{route('infocontacto',3)}}',
      		    data: {},
      		    dataType: 'json',
      		    success: function(response) {
      					$("#namecontact2").text(response.infocontact.nombre);
      					$("#nametelef2").text(response.infocontact.telefono);
      					$("#namecorreo2").text(response.infocontact.email);
      					$('#modalcontacto2').modal('show');

      		    },
      		    error: function(jqXHR, status, err) {
      		        alert("Local error callback.");
      		        console.log(err);
      		    }
      });
});
$('#save_contacto2').on('submit', function(e) {
	e.preventDefault();
	var formData = new FormData(this);
	formData.append('_token', $('input[name=_token]').val());
	formData.append('infocontacto_id', 3);
	$.ajax({
	            type:'POST',
	            url: '{{route('savecontact')}}',
	            data:formData,
	            cache:false,
	            contentType: false,
	            processData: false,
	            success:function(data){
								$('#save_contacto2')[0].reset();
								$('#modalcontacto2').modal('hide');
	              $("#fondoNombres").val("");
								$("#fondoApellidos").val("");
								$("#fondoEmail").val("");
								$("#fondoTelefono").val("");
								$("#mensaje").val("");
								swal(
							 'Excelente!!!',
							 data.mensaje,
							 'success'
							 )


	            },
	            error: function(jqXHR, text, error){
	                console.log(error);
	            }
	        });


});
</script>
@endpush
