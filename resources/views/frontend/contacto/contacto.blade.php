@extends('frontend.layouts.layout')
@section('title', 'Fundación Wiese')

@section('content')
<div class="participa__banner participa__banner--position d-flex align-items-center">
	<img src="{{ url('images/contacto/banner_contacto.jpg') }}" class="img-fluid" alt="">
</div>
<img class="mision__imagen-w mision__imagen-w--margin" src="{{url('images/contacto/w-roja.png')}}">

<div class="container contacto">
	<div class="row">

		<div class="col-12 col-lg-6 contacto__box-texto">
			<h1 class="contacto__titulo">Contáctanos</h1>
			<p class="contacto__texto">Escríbenos y nos pondremos en contacto contigo lo más pronto posible</p>
			<hr class="contacto__separador">
			<h3 class="contacto__subtitulo">Fundación Wiese</h3>
			<ul>
				<li class="contacto__item"><a href="" class="contacto__enlace"><img class="contacto__item-img" src="{{url('images/contacto/phone.png')}}">Lima: 511 611 4343 anexo 122</a></li>
				<li class="contacto__item"><a href="mailto:info@fundacionwiese.org" class="contacto__enlace"><img class="contacto__item-img" src="{{url('images/contacto/email.png')}}">info@fundacionwiese.org</a></li>
				<li class="contacto__item"><img class="contacto__item-img" src="{{url('images/contacto/pin.png')}}">Av. Canaval y Moreyra 522, Piso 16. CP: 15047. Lima - Perú</li>
			</ul>
		</div>

		<div class="col-12 col-lg-5 contacto__box">
			<h2 class="contacto__formulario-titulo">Déjanos tus datos</h2>
			<form id="formContacto" name="formContacto" class="contacto__formulario" action="{{ url('contacto') }}">
				<div class="form-group form-group--margin col-12 ">
			        <input type="text" class="form-control contacto__input" id="nombres" name="nombres" placeholder="*Ingrese nombres">
		    </div>
				<div class="form-group form-group--margin col-12 ">
			     <input type="text" class="form-control contacto__input" id="apellidos" name="apellidos" placeholder="*Ingrese apellidos">
			  </div>
			  <div class="form-group form-group--margin col-12 ">
			      <input type="email" class="form-control contacto__input" id="email" name="email" placeholder="*Ingrese email">
			  </div>
				<div class="form-group form-group--margin col-12 ">
			        <input type="text" class="form-control contacto__input" id="telefono" name="telefono" placeholder="*Teléfono">
			   </div>
				<div class="form-group form-group--margin col-12 ">
			        <textarea class="form-control contacto__input" id="mensaje" name="mensaje" rows="4" placeholder="Escribe un mensaje"></textarea>
			  </div>
				<div class="form-group form-group--margin col-12 " style="color: white;">
			        <input type="checkbox" required id="checkpolitica" name="checkpolitica" checked>   <a href="{{route('politica.privacidad')}}" style="color: white;" target=”_blank”>Acepto Política de protección de datos</a>
			   </div>
			  <div class="col-12 col-md-4">
		        <button type="submit" class="form-group  btn contacto__submit">Enviar</button>
		    </div>
			</form>
		</div>

	</div>
</div>

<!--div id="map"></div-->

@include('frontend.partials.newsletter')
@endsection

@section('scripts')
<!--script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCntLm5sM5Yfl6ISA7PeusH1P5UQEeQdNU&callback=initMap">
</script-->

<script>
var URL_SITE='{{ url('')}}';
var map;
var infowindow;
var setZoom = 15;

var coordenada = {
  lat: parseFloat(19.4360237),
  lng: parseFloat(-99.1546097)
};

function initMap() {

  map = new google.maps.Map(document.getElementById('map'), {
    center: coordenada,
    zoom: setZoom
  });

  /*infowindow = new google.maps.InfoWindow({
    minWidth: 250,
    zIndex: -1
  });

  var image = {
    url: URL_SITE + '/images/pin.png'
  };

  var coordinates = {
    lat: parseFloat(-12.2127980),
    lng: parseFloat(-76.8632870)
  }

  marker = new google.maps.Marker({
    map: map,
    draggable: true,
    animation: google.maps.Animation.DROP,
    position: coordinates,
    icon: image,
  });

  marker.addListener('click', toggleBounce);

  function toggleBounce() {
    if($('#myInfo').length > 0){
      $('#myInfo').delay(100).fadeIn(400);
      $('#hijito').children().delay(10).fadeIn(400);
      $('#hijito').children(':nth-child(2)').delay(10).fadeIn(400);
    }else{
      infowindow.open(map, marker);
    }
  }

  var contentString = '<div id="content-map"><a href="javascript:void(0)" class="closebtn closeMap">&times;</a>'+
                      '<h2 id="firstHeading" class="firstHeading"> Hacienda Queirolo </h2>'+
                      '<div id="bodyContent">'+
                      '<p> Calle 6 Mz. J.Lote 129.Urbanización Casa Blanca, Pachacamac. </p>'+
                      '<p><span style="font-weight:bold;">Teléfono:</span > (511) 261 - 3772. Anx. 105</p></div></div>';

  infowindow.setContent(contentString);
  infowindow.open(map, marker);

  map.setCenter(marker.getPosition());
  map.setZoom(setZoom);

  google.maps.event.addListener(infowindow, 'domready', function() {

    $( ".closeMap" ).click(function() {
        $('#myInfo').delay(80).fadeOut(200);
        $('#hijito').children().delay(50).fadeOut(200);
        $('#hijito').children(':nth-child(2)').delay(50).fadeOut(200);
    });

    var iwOuter1 = $('.gm-style-iw');
    iwOuter1.attr('id', 'myInfo');
    var iwOuter = $('#myInfo');
    iwOuter.parent().parent().css({left: '115px'});

    var iwBackground = iwOuter.prev();
    iwBackground.children(':nth-child(2)').css({'display' : 'none'});
    iwBackground.children(':nth-child(4)').css({'display' : 'none'});

    iwBackground.children(':nth-child(1)').css({'display' : 'none'});
    iwBackground.children(':nth-child(3)').attr('id', 'hijito');
    $('#hijito').children().css({left : '-127px'});
    $('#hijito').children(':nth-child(2)').css({left : '-112px'});
    iwBackground.children(':nth-child(3)').find('div').children().css({'box-shadow': 'rgba(178, 178, 178, 0.6) 0px 1px 6px', 'z-index' : '1'});

    var iwCloseBtn = iwOuter.next();
    iwCloseBtn.css({'display' : 'none'});
  });*/
}

</script>

@endsection
