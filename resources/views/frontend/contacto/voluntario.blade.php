@extends('frontend.layouts.layout')
@section('title', 'Fundación Wiese')

@section('content')
<div class="participa__banner participa__banner--position-right d-flex align-items-center">
	<img src="{{ url('images/historias/banner_historias.jpg') }}" class="img-fluid" alt="">
</div>
<img class="mision__imagen-w mision__imagen-w--width" src="{{url('images/contacto/banner_voluntario.jpg')}}">
<div class="container">
	<div class="participa__titulo-box text-center">
		<h1 class="participa__title">Sé un voluntario</h1>
		<p class="participa__subtitle">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque molestie hendrerit velit, sit amet mollis risus sodales eget. Sed lectus est, lacinia bibendum elementum ac, posuere vel nunc. Vestibulum eget mi lectus. Proin vulputate ante in nisl posuere maximus in ut tellus. Morbi placerat suscipit metus eu egestas.</p>
	</div>
</div>

<div class="container voluntario">
	<div class="row">
		<div class="col-12 col-lg-10 voluntario__box">
			<h2 class="voluntario__formulario-titulo">Déjanos tus datos</h2>
			<form class="voluntario__formulario">
				<div class="row">
					<div class="form-group form-group--margin col-12 col-lg-6 ">
				        <input type="text" class="form-control voluntario__input" id="nombres" name="nombres" placeholder="*Ingrese nombres">
				    </div>
					<div class="form-group form-group--margin col-12 col-lg-6">
				        <input type="text" class="form-control voluntario__input" id="apellidos" name="apellidos" placeholder="*Ingrese apellidos">
				    </div>
				    <div class="form-group form-group--margin col-12 col-lg-6">
				        <input type="email" class="form-control voluntario__input" id="email" name="email" placeholder="*Ingrese email">
				    </div>
					<div class="form-group form-group--margin col-12 col-lg-6">
				        <input type="text" class="form-control voluntario__input" id="dni" name="dni" placeholder="*DNI">
				    </div>
					<div class="form-group form-group--margin col-12 ">
						<span class="voluntario__select__arr"></span>
				        <select class="form-control voluntario__select">
						  <option>Voluntariado</option>
						</select>
				    	<p class="voluntario__nota">(Ingrese una breve descripción del voluntariado que deseas realizar)</p>
				    </div>
				</div>
			    <div class="col-12 col-md-3 pl-0">
		          <button type="submit" class="form-group  btn voluntario__submit">Enviar</button>
		        </div>
			</form>
		</div>
		<div class="col-12 col-lg-2">
			<img class="img-fluid" src="{{ url('images/contacto/img1.jpg') }}">
		</div>
	</div>
</div>
@include('frontend.partials.newsletter')
@endsection

@section('scripts')
@endsection
