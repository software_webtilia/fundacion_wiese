<header class="cabecera_principal">

  <div class="logo-mob d-lg-none">
    <a href="{{url('/')}}">
      <img src="{{url('/images/logo.png')}}" class="logo-mob__imagen" alt="">
    </a>
  </div>

    <div class="{{ (Request::is('/') ? 'container contenedor_menu d-none d-md-none d-lg-block  position-absolute' : 'container contenedor_menu d-none d-md-none d-lg-block') }}">
	    <div class="barra_top">
	        <div class="contenedor">
	          <div class="menu_top_right">
	            <a href="#" class="mail"><img src="{{url('/images/phone.png')}}">+511 611 4343 Anexo 103 </a>
	            <a href="mailto:info@fundacionwiese.org" class="mail"><img src="{{url('/images/email.png')}}">fondosolidario@fundacionwiese.org</a>
	            <a href="https://www.facebook.com/fundacionwiesePE/" target="_blank" class="icon"><i class="fab fa-facebook-f"></i></a>
	            <a href="https://www.youtube.com/channel/UCXhwI8NNyUNFoeC_oYUpZsg" target="_blank" class="icon"><i class="fab fa-youtube"></i></a>
              <a href="https://www.linkedin.com/company/fundacion-augusto-nwiese/" target="_blank" class="icon"><i class="fab fa-linkedin-in"></i></a>
	           </div>
	        </div>
	    </div>
	    <div class="contenedor_header">
	        <nav class="cbp-hsmenu-wrapper" id="cbp-hsmenu-wrapper">
	          <div class="cbp-hsinner">
	            <div class="logo">
	              <a href="{{url('fondo-solidario')}}">
	                <img  id="mi_imagen_person" src="{{url('/images/fw_logo.png')}}" style="margin-top:1.5rem" alt="">
	              </a>
	            </div>
	            <ul class="{{ (Request::is('/') ? 'cbp-hsmenu menu_colap position-absolute' : 'cbp-hsmenu menu_colap') }}">
	              <li class="visible_tablet"><a href="#" id="cerrar_menucolapse"><i class="fa fa-close" aria-hidden="true"></i></a></li>
                <li><a href="{{route('wiese.index')}}" class="inicio">Inicio</a></li>
	              <li><a href="{{route('solidario.education')}}" class="inicio">Créditos Educativos</a></li>
                <li><a href="{{route('solidario.humanitaria')}}" class="quienes_somos">Ayuda Humanitaria</a></li>
	              <li class="menu_max"><a href="{{route('solidario.conocefondo')}}" name="como-ayudamos" class="como_ayudamos2">Fondo Emprendedor</a>
                  <ul class="col cbp-hssubmenu menu_niveles d-flex">
                    <div class="d-flex flex-column box_menu box_menu--left355">
                      <span class="flecha flecha--left400"></span>
                      <li class="menu_niveles__menu2"><a href="{{route('solidario.conocefondo')}}" >Conoce el Fondo</a></li>
                      <li class="menu_niveles__menu2"><a href="{{url('/fondo-solidario/fondo-emprendedor#proceso')}}">El Proceso</a></li>
                      <li class="menu_niveles__menu2"><a href="" id="btn_contact2">Contáctanos</a></li>
                    </div>
                  </ul>
                </li>

                <li><a href="{{route('solidario.contacto')}}" class="quienes_somos">Contáctanos</a></li>

	            </ul>
	          </div>
	        </nav>
	    </div>
    </div>
</header>

 <div id="mySidenav" class="sidenav">
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>

  <aside class="sidebar">
    <div id="leftside-navigation" class="nano">
      <ul class="nano-content">
        <li>
          <a href="{{route('solidario.education')}}"><i class="fa fa-home"></i><span>Educación</span></a>
        </li>
        <li>
          <a href="{{route('solidario.humanitaria')}}"><i class="fa fa-hands-helping"></i><span>Ayuda Humanitaria</span></a>
        </li>
        <li class="sub-menu">
          <a href="javascript:void(0);"><i class="fas fa-newspaper"></i><span>Fondo Emprendedor</span></a>
          <ul>
            <li><a href="{{route('solidario.conocefondo')}}">Conoce el Fondo</a></li>
            <li><a href="{{url('/fondo-solidario/fondo-emprendedor#proceso')}}"  >El Proceso</a></li>
            <li><a href="{{route('solidario.contacto')}}">Contáctanos</a></li>
          </ul>
        </li>
        <li><a href="{{route('solidario.contacto')}}" class="quienes_somos"><i class="fas fa-inbox"></i><span>Contáctanos</span></a></li>
      </ul>
    </div>
  </aside>
</div>

<div id="myCanvasNav" class="overlay3" onclick="closeNav()" style="width: 100%; opacity: 0.8;"></div>

<div class="container-menu d-lg-none">
    <div class="container-menu-icon">
        <div id="menuHamburguer" onclick="openNav()"><i class="fas fa-bars"></i></div>
    </div>
</div>


<div class="modal fade bd-example-modal-lg modal__fondo__emprendedor" id="modalcontacto2" tabindex="-1" role="dialog" aria-labelledby="pruebamodalTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-body modal__fondo__emprendedor__body">
        <button type="button" class="close btn-cerrar" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <div class="container-fluid">
          <div class="row">
            	<img class="franja img-fluid" src="{{url('images/W-morado.png')}}">
            <div class="col-xs-12 col-sm-12 col-md-6">
              <div class="modal__fondo__emprendedor__body__contenido">
                <h2>Fondo emprendedor, operado por NESST</h2>
                <ul class="body__listado">
                  <li><i class="fas fa-user"></i> <strong>Nombre de contacto:</strong> <label id="namecontact2"></label> </li>
                  <li><i class="fas fa-phone telefono"></i> Lima: <label id="nametelef2"></label> </li>
                  <li><i class="fas fa-envelope"></i> <label id="namecorreo2"></label> </li>
                </ul>
              </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6">
              <div class="modal__fondo__emprendedor__body__formulario">
                <h4>Escríbenos</h4>
                <p>Escríbenos y nos pondremos en contacto contigo lo más pronto posible</p>
                <form class="row"  id="save_contacto2">
                  <div class="form-group col-md-6">
                    <input type="text" class="form-control modal__input modal__input--tamano" id="fondoNombres" name="nombres" placeholder="Ingrese nombres" required/>
                  </div>
                  <div class="form-group col-md-6">
                    <input type="text" class="form-control modal__input modal__input--tamano" id="fondoApellidos" name="apellidos"  placeholder="Ingrese apellidos" />
                  </div>
                  <div class="form-group col-md-6">
                    <input type="email" class="form-control modal__input modal__input--tamano" id="fondoEmail" name="email"  placeholder="Ingrese email" required/>
                  </div>
                  <div class="form-group col-md-6">
                    <input type="number" class="form-control modal__input modal__input--tamano" id="fondoTelefono" name="telefono"  placeholder="Télefono" required/>
                  </div>
                  <div class="form-group col-12">
          			        <textarea class="form-control modal__input" id="fondoMensaje" name="mensaje" rows="4" placeholder="Mensaje" required/></textarea>
          			  </div>
                  <div class="col-xs-12 col-md-4">
          		        <button type="submit" class="form-group  btn contacto__submit">Enviar</button>
          		    </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
