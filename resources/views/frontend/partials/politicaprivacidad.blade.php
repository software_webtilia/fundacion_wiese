@extends('frontend.layouts.layout_fondo')

@section('title', 'Fundación Wiese')

@section('stylesheet')

<style>

.participa__listado{

  margin-top: 5rem;

}

</style>

@stop

@section('content')



<div class="container-fluid">

	<div class="row">

    <div class="col-md-8 offset-md-2">

      <div class="participa__titulo text-center">

        <h1 class="participa__title conoce__title__color">Política de protección de datos personales de la Fundación Wiese</h1>

            <p class="participa__subtitle participa__subtitle--mBottom">FUNDACIÓN AUGUSTO N. WIESE, en adelante la FUNDACIÓN, garantiza la seguridad y
            confidencialidad en el tratamiento de los datos de carácter personal y sensible facilitados
            por sus trabajadores, proveedores, clientes y beneficiarios de conformidad con lo
            dispuesto en la Ley N° 29733, Ley de Protección de Datos Personales y/o sus normas
            reglamentarias, complementarias, modificatorias, sustitutorias y demás disposiciones
            aplicables (en adelante, la Ley).</p>
            <p class="participa__subtitle participa__subtitle--mBottom">Toda información proporcionada a la FUNDACIÓN a través de sus titulares y/o terceros
            será objeto de tratamiento automatizado e incorporada en una o más bases de datos de
            las que la FUNDACIÓN será titular y responsable, conforme a los términos previstos por
            la Ley. La FUNDACIÓN respeta la privacidad de cada una de las personas naturales que
            le suministren sus datos personales a través de los diferentes puntos de recolección y
            captura de dicha información.</p>
            <p class="participa__subtitle participa__subtitle--mBottom">La FUNDACIÓN podrá transferir la información a su casa matriz o miembros del grupo
            económico al cual pertenece y/o terceros con los que éstas mantengan una relación
            contractual, dentro y fuera del territorio nacional, supuesto en el cual sus datos serán
            almacenados en los sistemas informáticos de cualquiera de ellos. En dichos supuestos,
            la FUNDACIÓN garantiza el mantenimiento de la confidencialidad y el tratamiento seguro
            de la información. El uso de la información por las empresas antes indicadas se
            circunscribirá a los fines contenidos en este documento.</p>
            <p class="participa__subtitle participa__subtitle--mBottom">La política de privacidad de la FUNDACIÓN le asegura al titular de los datos personales
            y/o sensibles el ejercicio en cualquier momento de los derechos de información, acceso,
            actualización, inclusión, rectificación, supresión o cancelación, oposición, tratamiento
            objetivo, derecho a la tutela y a ser indemnizado en caso sea afectado a consecuencia
            del incumplimiento de la Ley por parte de la FUNDACIÓN. Asimismo, el titular de los
            datos personales y/o sensibles puede revocar su consentimiento, en los términos
            establecidos en la Ley.</p>
            <p class="participa__subtitle participa__subtitle--mBottom">La FUNDACIÓN declara haber adoptado los niveles de seguridad de protección de los
            datos personales, instalando todos los medios y adoptando todas las medidas técnicas,
            organizativas y legales a su alcance que garanticen la seguridad y eviten la alteración,
            pérdida, tratamiento o acceso no autorizado a los datos personales, de conformidad con
            la Directiva de Seguridad de la Información Administrada por los Bancos de Datos
            Personales, aprobada por Res. Directoral N° 019-2013-JUS/DGPDP.
            En caso los titulares deseen ejercer los derechos contemplados en la Ley sobre sus datos
            personales, se deberán comunicar con Milagros Arquíñigo al teléfono (511) 611 4343
            anexo 122 y/o correo electrónico info@fundacionwiese.org.</p>
            <br>
            <p class="participa__subtitle participa__subtitle--mBottom" style="text-align: right;">FUNDACIÓN AUGUSTO N. WIESE
            <br>Lima, 15 de Noviembre de 2018</p>
            <br>
      </div>

    </div>

		<div class="col-md-2">

		</div>

	</div>

</div>







@endsection





@push('scripts')

<script src="{{asset('assets/js/contact-ayuda.js')}}"></script>

<script type="text/javascript">

$(document).ready(function() {

 $(".btn_contacoclase").css("display", "none");

});



</script>

@endpush

