<footer>

  <div class="container footer_content">
    <div class="row">
      <div class="col-xs-12 col-sm-6 col-md-3 footer_content__container">
        <article>
          <img src="{{url('images/logo-white.png')}}" alt="fundacion wiese" class="footer_content__logo" >
          <div class="">
            <div class="footer_content__direccion">Av. Canaval y Moreyra 522, Piso 16 <br> San Isidro 15036, Lima - Perú</div>
            <div class="footer_content__telefono"><img src="{{url('images/phone-white.png')}}" class="mr-3">+511 611 4343 anx. 122 </div>
            <div class="footer_content__email">
              <a href="mailto:info@fundacionwiese.org" class="footer_content__enlace">
                <img src="{{url('images/email-white.png')}}" class="mr-3">info@fundacionwiese.org
              </a>
            </div>
          </div>
        </article>
      </div>
      <div class="col-xs-6 col-sm-6 col-md-3 footer_content__menu d-none d-sm-block">
        <p>MENÚ</p>
        <hr class="footer_content__menu-hr">
        <ul class="footer_content__lista">
          <li><a href="{{url('')}}" class="footer_content__item-lista-enlace">Inicio</a></li>
          <li><a href="{{url('/fundacion-wiese-dirige')}}" class="footer_content__item-lista-enlace">¿Cómo ayudamos?</a></li>
          <li><a href="{{url('/quienes-somos')}}" class="footer_content__item-lista-enlace">¿Quiénes somos?</a></li>
          <li><a href="{{url('/blog')}}" class="footer_content__item-lista-enlace">Novedades</a></li>
          <li><a href="{{url('/contacto')}}" class="footer_content__item-lista-enlace">Contáctanos</a></li>
          <li><a href="{{url('/bolsa-de-trabajo')}}" class="footer_content__item-lista-enlace">Bolsa de trabajo</a></li>
        </ul>
      </div>
      <div class="col-xs-6 col-sm-6 col-md-3 footer_content__links-interes d-none d-sm-block">
        <p>LINKS DE INTERÉS</p>
        <hr class="footer_content__links-interes-hr">
        <ul class="footer_content__lista">
          <li><a href="{{url('/complejo-arqueologico-el-brujo')}}" class="footer_content__item-lista-enlace">El Brujo</a></li>
          <li><a href="{{url('/proyecto-educativo')}}" class="footer_content__item-lista-enlace">Calidad Educativa</a></li>
          <li><a href="{{route('solidario.conocefondo')}}" class="footer_content__item-lista-enlace">Fondo Wiese Emprendedor</a></li>
          <li><a href="{{route('politica.privacidad')}}" class="footer_content__item-lista-enlace" target=”_blank”>Políticas de Privacidad y Protección de Datos</a></li>
          <li><p class="mt-2">Canal de Denuncias:<br /><a href="mailto:canaldedenuncias@fundacionwiese.org" class="footer_content__item-lista-enlace">canaldedenuncias@fundacionwiese.org</a></p></li>

        </ul>
      </div>
      <div class="col-xs-12 col-sm-6 col-md-3 footer_content__redes">
        <p class="footer_content__siguenos">Síguenos en:</p>
        <a href="https://www.facebook.com/fundacionwiesePE/" target="_blank" class="footer_content__icon mr-2"><i class="fab fa-facebook-f"></i></a>
        <a href="https://www.youtube.com/user/fundacionwieseperu" target="_blank" class="footer_content__icon mr-2"><i class="fab fa-youtube"></i></a>
        <a href="https://www.linkedin.com/company/fundacion-augusto-nwiese/" target="_blank" class="footer_content__icon mr-2"><i class="fab fa-linkedin-in"></i></a>

      </div>

    </div>
  </div>
  <div class="footer_bottom">
    <div class="container">
      <div class="footer_bottom__copyright">
        Copyright © 2018 Todos los derechos reservados  -
        <a href="https://webtilia.com" target="_blank" class="footer_bottom__enlace">By Webtilia.</a>
      </div>
      <div class="footer_bottom__botton">
        <a href=""><img src="{{url('images/flecha-arriba.png')}}"></a>
        <a href="" class="footer_bottom__enlace">Inicio</a>
      </div>
    </div>
  </div>
</footer>
