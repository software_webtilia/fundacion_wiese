<header class="cabecera_principal">

  <div class="logo-mob d-lg-none">
    <a href="{{url('/')}}">
      <img src="{{url('/images/logo.png')}}" class="logo-mob__imagen" alt="">
    </a>
  </div>

    <div class="{{ (Request::is('/') ? 'container contenedor_menu d-none d-md-none d-lg-block  position-absolute' : 'container contenedor_menu d-none d-md-none d-lg-block') }}">
	    <div class="barra_top">
	        <div class="contenedor">
	          <div class="menu_top_right">
	            <a href="#" class="mail"><img src="{{url('/images/phone.png')}}">+511 611 4343 anx. 122 </a>
	            <a href="mailto:info@fundacionwiese.org" class="mail"><img src="{{url('/images/email.png')}}">info@fundacionwiese.org</a>
	            <a href="https://www.facebook.com/fundacionwiesePE/" target="_blank" class="icon"><i class="fab fa-facebook-f"></i></a>
	            <a href="https://www.youtube.com/channel/UCXhwI8NNyUNFoeC_oYUpZsg" target="_blank" class="icon"><i class="fab fa-youtube"></i></a>
              <a href="https://www.linkedin.com/company/fundacion-augusto-nwiese/" target="_blank" class="icon"><i class="fab fa-linkedin-in"></i></a>
	           </div>
	        </div>
	    </div>
	    <div class="contenedor_header">
	        <nav class="cbp-hsmenu-wrapper" id="cbp-hsmenu-wrapper">
	          <div class="cbp-hsinner">
	            <div class="logo">
	              <a href="{{url('fondo-solidario')}}">
	                <img src="{{url('/images/logo.png')}}" class="logo__imagen" alt="">
	              </a>
	            </div>
	            <ul class="{{ (Request::is('/') ? 'cbp-hsmenu menu_colap position-absolute' : 'cbp-hsmenu menu_colap') }}">

	              <li class="visible_tablet"><a href="#" id="cerrar_menucolapse"><i class="fa fa-close" aria-hidden="true"></i></a></li>
	              <li><a href="{{url('/')}}" class="inicio">Inicio</a></li>
	              <li class="menu_max"><a href="{{ url('/proyectos-iniciativas') }}" name="como-ayudamos" class="como_ayudamos2">¿Cómo ayudamos?</a>
                  <ul class="col cbp-hssubmenu menu_niveles d-flex">
                    <div class="d-flex flex-column box_menu">
                      <span class="flecha"></span>
                      <!-- <li class="menu_niveles__menu1"><a href="{{ url('/fundacion-wiese-dirige') }}" >Fundación Wiese Dirige</a></li> -->
                      <li class="menu_niveles__menu2"><a href="{{ url('/proyectos-iniciativas') }}" >Proyectos e Iniciativas</a></li>
                      <li class="menu_niveles__menu2"><a href="{{ url('/historias') }}" >Historias</a></li>
                      <!-- <li class="menu_niveles__menu2"><a href="{{ url('/causas') }}" >Causas</a></li> -->
                    </div>

                    <!--img class="img_menu img-fluid" src="{{ url('images/menu.png') }}" alt=""/-->

                  </ul>
                </li>
	              <li><a href="{{url('/quienes-somos')}}" class="quienes_somos">¿Quiénes somos?</a></li>
                <li><a href="{{url('/blog')}}" class="quienes_somos">Novedades</a></li>
	              <!-- <li><a href="#" class="nosotros">Nosotros</a></li> -->
	              <li><a href="{{url('/contacto')}}" class="contacto">Contáctanos</a></li>

	              <!--li><a href="#" class="dona">Dona</a></li-->
	            </ul>
	          </div>
	        </nav>
	    </div>
    </div>
</header>


<div id="mySidenav" class="sidenav">
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>

  <aside class="sidebar">
    <div id="leftside-navigation" class="nano">
      <ul class="nano-content">
        <li>
          <a href="{{url('/')}}"><i class="fa fa-home"></i><span>Inicio</span></a>
        </li>
        <li class="sub-menu">
          <a href="javascript:void(0);"><i class="fas fa-hands-helping"></i><span>¿Cómo ayudamos?</span></a>
          <ul>
            <!-- <li><a href="{{ url('/fundacion-wiese-dirige') }}">Fundación Wiese Dirige</a></li> -->
            <li><a href="{{ url('/proyectos-iniciativas') }}">Proyectos e Iniciativas</a></li>
            <li><a href="{{ url('/historias') }}" >Historias</a></li>
          </ul>
        </li>
        <li>
          <a href="{{url('/quienes-somos')}}"><i class="fas fa-info-circle"></i><span>¿Quiénes somos?</span><!-- <i class="arrow fa fa-angle-right pull-right"></i> --></a>
        </li>
        <li>
          <a href="{{url('/blog')}}"><i class="fas fa-newspaper"></i><span>Novedades</span><!-- <i class="arrow fa fa-angle-right pull-right"></i> --></a>
        </li>
        <li>
          <a href="{{url('/contacto')}}"><i class="fas fa-inbox"></i><span>Contáctanos</span></a>
        </li>
        <li>
          <a href="javascript:void(0);"><i class="fas fa-phone"></i><span>+511 611 4343 anx. 122 </span></a>
        </li>
        <li>
          <a href="mailto:info@fundacionwiese.org" class="mail"><i class="fa fa-envelope"></i><span>info@fundacionwiese.org</span></a>
        </li>

        <li><a href="{{url('/bolsa-de-trabajo')}}" class="contacto"><i class="fas fa-briefcase"></i></i><span>Bolsa de trabajo</span></a></li>

      </ul>
    </div>
  </aside>
</div>

<div id="myCanvasNav" class="overlay3" onclick="closeNav()" style="width: 100%; opacity: 0.8;"></div>
<div class="container-menu d-lg-none">
    <div class="container-menu-icon">
        <div id="menuHamburguer" onclick="openNav()"><i class="fas fa-bars"></i></div>
    </div>
</div>
