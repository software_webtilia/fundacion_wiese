<!doctype html>
<html lang="es">
<head>
<meta charset="utf-8">
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<title>@yield('title')</title>
<link rel="apple-touch-icon" href="{{ asset('images/favicon.ico') }}"/>
<link rel="stylesheet" href="{{ url('css/app.css?v=2.6') }}">
<link rel="stylesheet" href="{{ url('assets/css/sweetalert2.min.css') }}">
@yield('stylesheet')
</head>
<body>

@include('frontend.partials.header_fondo')

@yield('content')
<div class="boton_contacto">
  <a hred="#"  class="btn_contacoclase">Contáctanos</a>
</div>

@include('frontend.fondo_emprendedor.modal_contacto')
@include('frontend.fondo_emprendedor.modal_contacto2')

@include('frontend.partials.footer_fondo')
<script src="{{ url('js/app.js?v=2.6') }}"></script>
<script src="{{ url('js/vendor.js?v=2.6') }}"></script>

   @stack('scripts')
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-78698981-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'UA-78698981-1');
  // $("#btn_prueba").click( function(){
  //   		console.log("entro");
  // });
</script>
</body>
</html>
