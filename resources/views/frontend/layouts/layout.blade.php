<!doctype html>
<html lang="es">
<head>
<meta charset="utf-8">
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<title>@yield('title')</title>
<link rel="apple-touch-icon" href="{{ asset('images/favicon.ico') }}"/>
<link rel="stylesheet" href="{{ url('css/app.css?v=2.6') }}">
@yield('stylesheet')
</head>
<body>

@include('frontend.partials.header')

@yield('content')

@include('frontend.partials.footer')
<script src="{{ url('js/app.js?v=2.6') }}"></script>
<script src="{{ url('js/app2.js') }}"></script>
@yield('scripts')
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-78698981-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'UA-78698981-1');
</script>
</body>
</html>
