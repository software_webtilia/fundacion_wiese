@extends('frontend.layouts.layout')
@section('title', 'Fundación Wiese')

@section('content')
<div class="participa__banner d-flex align-items-center">
	<img src="{{ url('images/como-ayudamos/banner_dirige.jpg') }}" class="img-fluid" alt="">
</div>

<div class="container-fluid">
	<div class="row">
		<div class="col-md-2 pl-0">
				<img class="mision__imagen-w img-fluid" src="{{url('images/proyectos-dirige/w-magenta.png')}}">
		</div>
		<div class="col-md-8">
			<div class="participa__titulo-box text-center">
				<h1 class="participa__title participa__title--color">La Fundación Wiese Dirige</h1>
				<p class="participa__subtitle">La Fundación Wiese, conforme al espíritu de su fundador, dedica las renta de sus bienes a personas y entidades cuyos objetivos no sean de lucro, sino esencialmente de solidaridad humana y presta su apoyo al desarrollo de la educación, el fomento de la cultura e investigación científica, sin por ello dejar de conceder ayuda a las entidades religiosas, de asistencia social hospitalaria y de beneficencia general. A lo largo de los últimos años, la Fundación Wiese viene dirigiendo los siguientes proyectos:</p>
			</div>
		</div>
		<div class="col-md-2">
		</div>
	</div>
</div>

<div class="container">
	<ul>
		<li class="participa__li">
			<div class="row participa__item participa__item--margen">
				<div class="participa__imagen col-12 col-lg-6 pb-3 pl-0">
					<img class="participa__imagen-img img-fluid" src="{{ url('images/como-ayudamos/dirige1.png') }}">
				</div>
				<div class="participa__box col-12 col-lg-6 pb-1">
					<h2 class="participa__item-title"> Programa Arqueológico El Brujo </h2>
					<p class="participa__item-descripcion">En el marco de sucesivos Convenios de Cooperación suscritos con el Ministerio de Cultura, la Fundación Wiese trabaja por la investigación, conservación, puesta en valor y difusión del Complejo Arqueológico El Brujo. Actuamos allí, inspirados por el amor al Perú y la pasión de Don Guillermo “Pancho” Wiese, quien inició este proyecto en agosto de 1990.</p>
					<div class="participa__boton">
            	<a href="{{ url('/complejo-arqueologico-el-brujo') }}" class="participa__link">Ver más  <span class="participa__separador-flecha"><i class="fas fa-angle-right"></i></span> </a>
          </div>
				</div>
			</div>
		</li>
		<li class="participa__li">
			<div class="row participa__item participa__item--margen">
				<div class="participa__imagen col-12 col-lg-6 pb-3 pl-0">
					<img class="participa__imagen-img img-fluid" src="{{ url('images/como-ayudamos/dirige3.png') }}">
				</div>
				<div class="participa__box col-12 col-lg-6 pb-1">
					<h2 class="participa__item-title">El Fondo Wiese </h2>
					<p class="participa__item-descripcion">El Fondo Wiese es la plataforma creada por la Fundación Wiese con el objetivo de ampliar su impacto social, a través de la dirección y patrocinio de programas operados por terceros.</p>
					<div class="participa__boton">
	                	<a href="{{ url('/fondo-wiese') }}" class="participa__link">Ver más  <span class="participa__separador-flecha"><i class="fas fa-angle-right"></i></span> </a>
	              </div>
				</div>
			</div>
		</li>
		<li class="participa__li">
			<div class="row participa__item participa__item--margen">
				<div class="participa__imagen col-12 col-lg-6 pb-3 pl-0">
					<img class="participa__imagen-img img-fluid" src="{{ url('images/como-ayudamos/dirige2.jpg') }}">
				</div>
				<div class="participa__box col-12 col-lg-6 pb-1">
					<h2 class="participa__item-title">Calidad Educativa: “Mejor clima escolar, mejores aprendizajes” </h2>
					<p class="participa__item-descripcion">Desde abril del 2016 La Fundación Wiese inició la segunda fase del Proyecto Calidad Educativa que lleva por nombre “Mejor clima escolar, mejores aprendizajes” y que trabaja con directores, subdirectores y docentes de 14 escuelas públicas de Lurín y Pachacamac. Esta segunda fase tiene como objetivo principal mejorar significativamente el clima institucional y de aula para favorecer el desarrollo de mejores enseñanzas y aprendizajes.</p>
					<div class="participa__boton">
	                	<a href="{{ url('/proyecto-educativo') }}" class="participa__link">Ver más  <span class="participa__separador-flecha"><i class="fas fa-angle-right"></i></span> </a>
	              </div>
				</div>
			</div>
		</li>
	</ul>
</div>

<div class="proyecto">
	<div class="container">
		<h1 class="proyecto__titulo">Proyectos Cerrados</h1>
		<div class="row">
			<div class="col-12 col-lg-6">
				<div class="proyecto__imagen">
					<img class="proyecto__imagen-img img-fluid" src="{{ url('images/como-ayudamos/proyecto1.png') }}">
				</div>
				<div class="proyecto__box">
					<h5 class="proyecto__item-title">Juntos por la Costa Norte del Perú (2017)</h5>
					<p class="proyecto__item-descripcion">Ante los devastadores efectos de los huaycos e inundaciones ocurridos en el primer trimestre, como consecuencia del Fenómeno del Niño, la Fundación Wiese tomó acción inmediata, articulando y gestionando una red de ayuda humanitaria en alianza con diversas instituciones públicas y privadas, con el objetivo de aliviar el sufrimiento de los peruanos damnificados más vulnerables en las regiones de Piura, Lambayeque y La Libertad. Esta campaña recibió el nombre de Juntos por la Costa Norte, y a través de ella, se logró atender oportuna y eficazmente a 15,620 familias.</p>

				</div>
			</div>
			<div class="col-12 col-lg-6">
				<div class="proyecto__imagen">
					<img class="proyecto__imagen-img img-fluid" src="{{ url('images/como-ayudamos/proyecto2.png') }}">
				</div>
				<div class="proyecto__box">
					<h5 class="proyecto__item-title">Calidad Educativa – Primera Etapa (2009-2016)</h5>
					<p class="proyecto__item-descripcion">Proyecto ejecutado en el marco de dos convenios de colaboración suscritos con la Dirección Regional de Educación de Ica (DREI) y la Dirección Regional de Educación de Lima Metropolitana (DRELM), con el objeto de contribuir con la mejora de la calidad educativa pública en los distritos de Alto Larán, Chincha Baja, Pachacamác y Lurín, teniendo como referente el proceso iniciado a nivel nacional de la municipalización de la educación.</p>

				</div>
			    <div class="col-12 link-purple pr-0 mb-4">
			      <a href="{{url('/proyectos-cerrados')}}"> Ver todas <img src="images/flecha-purple.png" alt=""></a>
			    </div>
			</div>
		</div>
	</div>
</div>
@include('frontend.partials.newsletter')
@endsection

@section('scripts')

@endsection
