@extends('frontend.layouts.layout')
@section('title', 'Fundación Wiese')

@section('content')
<div class="participa__banner d-flex align-items-center">
	<img src="{{ url('images/como-ayudamos/banner_participa.jpg') }}" class="img-fluid" alt="">
</div>

<div class="container-fluid">
	<div class="row">
		<div class="col-md-2 pl-0">
				<img class="mision__imagen-w img-fluid" src="{{url('images/proyectos-dirige/w-magenta.png')}}">
		</div>
		<div class="col-md-8">
			<div class="participa__titulo-box text-center">
				<h1 class="participa__title participa__title--color">Conoce nuestros proyectos e iniciativas</h1>
				<p class="participa__subtitle">La Fundación Wiese, conforme al espíritu de su fundador, dedica las rentas de sus bienes a personas y entidades cuyo objetivo no sean de lucro, sino esencialmente de solidaridad humana, y presta su apoyo al desarrollo de la educación, el fomento de la cultura e investigación científica, sin por ello dejar de conceder ayuda a las entidades religiosas, de asistencia social hospitalaria y de beneficencia general. A lo largo de los últimos años, ha dirigido, patrocinado, articulado y/o auspiciado los siguientes proyectos y campañas: </p>
			</div>
		</div>
		<div class="col-md-2">
		</div>
	</div>
</div>

<div id="filters" class="container">
	<div class="participa__filters">
		<p class="participa__filters-p"> Filtrar proyectos por:</p>
		<form id="formFilter" name="formFilter" class="participa__form row justify-content-center align-items-center" action="{{ url('proyectos-iniciativas#filters') }}" method="get">
      <div class="form-group col-5 col-md-4 pl-0">
				<select class="form-control" id="beneficiario" name="beneficiario">
					<option value="" disabled="" selected="">Beneficiario</option>
					@foreach($beneficiaries as $beneficiary)
						<option value="{{$beneficiary->id}}" @if($beneficiario && $beneficiario == $beneficiary->id) selected @endif	>{{$beneficiary->nombre}}</option>
					@endforeach
				</select>
      </div>
      <div class="form-group col-5 col-md-4 pl-0">
				<select class="form-control" id="ubicacion" name="ubicacion" >
					<option value="" disabled="" selected="">Ubicación</option>
					@foreach($locations as $location)
						<option value="{{$location->id}}" @if($ubicacion && $ubicacion == $location->id) selected @endif>{{$location->nombre}}</option>
					@endforeach
				</select>
      </div>
      <div class="col-2 col-md-4 newsletter__submit-container pl-0 pr-0">
        <button type="submit" class="form-group  btn participa__submit">Filtrar</button>
      </div>
    </form>
	</div>

	@if(count($projects) > 0)
	<ul>
		@foreach($projects as $project)
			<li class="participa__li">
				<div class="row participa__item">
					<div class="participa__imagen col-12 col-lg-6 pb-3 pl-0">
						<img class="participa__imagen-img img-fluid" src="{{ url('images/como-ayudamos/'.$project->imagen) }}">
					</div>
					<div class="participa__box col-12 col-lg-6 pb-3">
						@if($project->estado == 1)
							<span class="participa__nuevo">Nuevo</span>
						@endif
						<h2 class="participa__item-title"> {{ $project->titulo }} </h2>
						<p class="participa__programa mt-3"> {{ $project->programa }} </p>
						<p class="participa__item-descripcion"> {{ $project->descripcion }} </p>
						@if($project->estado != 3 && $project->link)
						<div class="participa__boton">
								<a href="{{ $project->link }}" target="_blank" class="participa__link">Ver más  <span class="participa__separador-flecha"><i class="fas fa-angle-right"></i></span> </a>
						</div>
						@endif
						@if($project->estado == 3)
							<span class="participa__item-descripcion participa__item-descripcion--label">Estado:</span>
							<span class="participa__item-descripcion participa__item-descripcion--estado"><strong>cerrado</strong></span>
						@endif
					</div>
				</div>
			</li>
		@endforeach
	</ul>
	@else
		<div class="participa__msj">
			<h4>Lo sentimos, no hay resultados de búsqueda para las opciones que seleccionaste.</h4>
		</div>
	@endif
</div>

@include('frontend.partials.newsletter')
@endsection

@section('scripts')
<!-- <script>
$("#formFilter").submit(function(e) {
	e.preventDefault(); // avoid to execute the actual submit of the form.
	var token = document.head.querySelector('meta[name="csrf-token"]');
	var data = $(this).serialize();
	var url = $(this).attr('action');

	$.ajax({
			method: 'POST',
			url: url,
			data: data,
			headers: {
					'X-CSRF-TOKEN': token.content
			},
			success: function (response) {
					console.log(response);
					$('document').html(response);
			}
	});
});
</script> -->
@endsection
