@extends('frontend.layouts.layout')
@section('title', 'Fundación Wiese')

@section('content')
<div class="d-flex align-items-center">
	<img src="{{ url('images/proyectos-dirige/banner_fondo_wiese.jpg') }}" class="img-fluid" alt="">
</div>

<div class="container-fluid">
	<div class="row">
		<div class="col-md-2 pl-0">
				<img class="mision__imagen-w img-fluid" src="{{url('images/proyectos-dirige/w-lila.jpg')}}">
		</div>
		<div class="col-md-8">
			<div class="participa__titulo-box text-center">
				<h1 class="tabs-box__titulo">Fondo Solidario</h1>
				<p class="participa__subtitle participa__subtitle--center">"Una plataforma que nos permite dirigir programas operados por terceros."</p>
				<ul class="nav nav-tabs d-flex justify-content-center" role="tablist">
				  <li class="nav-item complejo__item complejo__item--lila" data-color="lila">
				    <a class="nav-link active complejo__link complejo__link_active complejo__link_active--lila" href="#proyectos-dirige" role="tab" data-toggle="tab">¿Qué Hacemos?</a>
				  </li>
				  <li class="nav-item complejo__item complejo__item--naranja" data-color="naranja">
				    <a class="nav-link complejo__link" href="#objetivos-estrategicos" role="tab" data-toggle="tab">Objetivos Estratégicos</a>
				  </li>
				  <li class="nav-item complejo__item complejo__item--rojo" data-color="rojo">
				    <a class="nav-link complejo__link" href="#equipo-trabajo" role="tab" data-toggle="tab">Equipo de trabajo</a>
				  </li>
				</ul>
			</div>
		</div>
		<div class="col-md-2">
		</div>
	</div>
</div>

<div class="complejo">
	<div class="container">
		<!-- Tab panes -->
		<div class="tab-content complejo__content">
			<div role="tabpanel" class="tab-pane fade in active show" id="proyectos-dirige">
			 	<div class="row">
			  		<div class="col-12 col-lg-6 complejo__descripcion">
			  			<p>El Fondo Solidario es la plataforma creada por la Fundación Wiese con el objetivo de ampliar su impacto social, a través de la dirección y patrocinio de programas operados por terceros.</p>
              <p>El Fondo Solidario se divide en tres categorías:</p>
              <ul>
                <li >Educación (operado por IPFE)</li>
                <li style="color: #b2b2b2;">Emprendedor (próximamente)</li>
                <li style="color: #b2b2b2;">Salud (próximamente)</li>
              </ul>
			  		</div>
			  		<div class="col-12 col-lg-6 complejo__img">
			  			<img class="img-fluid" src="{{ url('images/proyectos-dirige/fondo_wiese1.jpg') }}">
			  		</div>
			  	</div>
			</div>
		  	<div role="tabpanel" class="tab-pane fade" id="objetivos-estrategicos">
			  	<div class="row">
			  		<div class="col-12 col-lg-6 complejo__descripcion">
			  			<ul>
			  				<li class="complejo__descripcion-item"><span class="complejo__descripcion--bold-naranja">Educación</span><br> Financiar programas de crédito educativo para peruanos de comunidades vulnerables.</li>
			  				<li class="complejo__descripcion-item"><span class="complejo__descripcion--bold-naranja">Emprendedor</span><br>Impulsar emprendimientos con claro potencial de autosostenibilidad, cuya misión sea la de atender un problema social relevante del país.</li>
			  			</ul>
			  		</div>
			  		<div class="col-12 col-lg-6 complejo__img">
			  			<img class="img-fluid" src="{{ url('images/proyectos-dirige/fondo_wiese2.jpg') }}">
			  		</div>
			  	</div>
		  	</div>
		  	<div role="tabpanel" class="tab-pane fade" id="equipo-trabajo">
		  		<div class="col-12 tab-content__img pl-0 pr-0">
		  			<img class="img-fluid" src="{{ url('images/proyectos-dirige/fondo_wiese3.jpg') }}">
			  	</div>
		  	</div>
		</div>


	</div>
</div>

<div class="container-fluit beneficiados d-lg-flex justify-content-lg-center" style="background: url({{ url('images/proyectos-dirige/fondo3.jpg') }}) no-repeat center/cover;">
	<!-- <img class="img-fluid" src="{{ url('images/proyectos-dirige/fondo.png') }}"> -->
	<div class="col-12 col-lg-5 col-xl-4 beneficiados__box d-none">

		<!-- <div class="d-flex justify-content-center align-items-end mb-3">
			<span class="beneficiados__numero">
				<span class="beneficiados__numero-letra beneficiados__numero-letra--top-educacion">MÁS DE</span><br>160</span>
			<span class="beneficiados__numero-letra">
				<br>NIÑOS Y EDUCADORES <br> IMPACTADOS
			</span>
		</div> -->
		<!-- <h1 class="beneficiados__subtitulo">BENEFICIADAS</h1>
		<h2 class="beneficiados__subtitulo-h3">What you purchase supports</h2> -->

		<ul>
			<li>
				<div class="beneficiados__item d-flex justify-content-center align-items-end mt-5 mb-3">
					<div class="beneficiados__img beneficiados__img--margin">
						<img class="img-fluid" src="{{ url('images/proyectos-dirige/icon9.png') }}">
					</div>
					<div class="text-left ml-3">
						<span class="beneficiados__item-numero">160</span>
						<p class="beneficiados__item-texto mb-1">Estudiantes <br>  Beneficiados</p>
					</div>
				</div>
			</li>
			<li>
				<div class="beneficiados__item d-flex justify-content-center align-items-end mt-5 mb-3">
					<div class="beneficiados__img">
						<img class="img-fluid" src="{{ url('images/proyectos-dirige/icon8.png') }}">
					</div>
					<div class="text-left ml-3">
						<span class="beneficiados__item-numero">7</span>
						<p class="beneficiados__item-texto mb-1">Emprendimientos <br>  Capacitados</p>
					</div>
				</div>
			</li>
			<li>
				<div class="beneficiados__item d-flex justify-content-center align-items-end mt-5 mb-3">
					<div class="beneficiados__img">
						<img class="img-fluid" src="{{ url('images/proyectos-dirige/icon7.png') }}">
					</div>
					<div class="text-left ml-3">
						<span class="beneficiados__item-numero">4</span>
						<p class="beneficiados__item-texto mb-1">Emprendimeintos <br> Financiados <br> y Asesorados </p>
					</div>
				</div>
			</li>

		</ul>
	</div>
</div>

@include('frontend.partials.newsletter')

@endsection

@section('scripts')
 <script type="text/javascript">
 		var URL_SITE="{{ url('') }}";
 </script>
@endsection
