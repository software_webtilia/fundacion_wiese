@extends('frontend.layouts.layout')
@section('title', 'Fundación Wiese')

@section('content')
<div class="participa__banner d-flex align-items-center">
	<img src="{{ url('images/proyectos-dirige/banner_proyectos_cerrados.jpg') }}" class="img-fluid" alt="">
</div>

<div class="container-fluid">
	<div class="row">
		<div class="col-md-2 pl-0">
				<img class="mision__imagen-w img-fluid" src="{{url('images/proyectos-dirige/w-magenta.png')}}">
		</div>
		<div class="col-md-8">
			<div class="participa__titulo-box text-center">
				<h1 class="participa__title participa__title--color">Proyectos Cerrados</h1>
				<p class="participa__subtitle">La Fundación Wiese, conforme al espíritu de su fundador, dedica las renta de sus bienes a personas y entidades cuyos objetivos no sean de lucro, sino esencialmente de solidaridad humana y presta su apoyo al desarrollo de la educación, el fomento de la cultura e investigación científica, sin por ello dejar de conceder ayuda a las entidades religiosas, de asistencia social hospitalaria y de beneficencia general. A lo largo de los últimos años, ha dirigido, patrocinado, articulado y auspiciado los siguientes proyectos:</p>
			</div>
		</div>
		<div class="col-md-2">
		</div>
	</div>
</div>

<div class="container">

	<ul>
		@foreach($projects as $project)
		<li class="participa__li">
			<div class="row participa__item">
				<div class="participa__imagen col-12 col-lg-6 pb-5 pl-0">
					<img class="participa__imagen-img img-fluid" src="{{ url('images/como-ayudamos/'.$project->imagen) }}">
				</div>
				<div class="participa__box col-12 col-lg-6 pb-5">
					<h2 class="participa__item-title">{{ $project->titulo }}</h2>
					<p class="participa__item-descripcion"> {{ $project->descripcion }}</p>
				</div>
			</div>
		</li>
		@endforeach
		<!-- <li class="participa__li">
			<div class="row participa__item">
				<div class="participa__imagen col-12 col-lg-6 pb-5 pl-0">
					<img class="participa__imagen-img img-fluid" src="{{ url('images/proyectos-dirige/proyecto_cerrado2.png') }}">
				</div>
				<div class="participa__box col-12 col-lg-6 pb-5">
					<h2 class="participa__item-title">Calidad Educativa – Primera Etapa (2009-2016)</h2>
					<p class="participa__item-descripcion">Proyecto ejecutado en el marco de dos convenios de colaboración suscritos con la Dirección Regional de Educación de Ica (DREI) y la Dirección Regional de Educación de Lima Metropolitana (DRELM), con el objeto de contribuir con la mejora de la calidad educativa pública en los distritos de Alto Larán, Chincha Baja, Pachacamác y Lurín, teniendo como referente el proceso iniciado a nivel nacional de la municipalización de la educación.</p>
				</div>
			</div>
		</li>
		<li class="participa__li">
			<div class="row participa__item">
				<div class="participa__imagen col-12 col-lg-6 pb-5 pl-0">
					<img class="participa__imagen-img img-fluid" src="{{ url('images/proyectos-dirige/proyecto_cerrado3.png') }}">
				</div>
				<div class="participa__box col-12 col-lg-6 pb-5">
					<h2 class="participa__item-title">Clínica de Puente Piedra (2008-2017)</h2>
					<p class="participa__item-descripcion">Con el objeto promover la implementación de un                        establecimiento de salud de primer nivel de atención para la prevención y promoción de la salud, la atención integral y el desarrollo social de los habitantes más vulnerables de la zona de Puente Piedra, Carabayllo, Ventanilla, Mi Perú, Ancón y Santa Rosa, la Fundación Wiese edificó una clínica, ubicada en el distrito de Puente Piedra, en el cono norte de Lima. Hoy sigue contribuyendo, a través de nuevas iniciativas con el el mencionado establecimiento.</p>
				</div>
			</div>
		</li>
		<li class="participa__li">
			<div class="row participa__item">
				<div class=" participa__imagen col-12 col-lg-6 pb-5 pl-0">
					<img class="participa__imagen-img img-fluid" src="{{ url('images/proyectos-dirige/proyecto_cerrado4.png') }}">
				</div>
				<div class="participa__box col-12 col-lg-6 pb-5">
					<h2 class="participa__item-title">Desarrollo Sostenible del Turismo en el Corredor de Conchucos (2015-2017)</h2>
					<p class="participa__item-descripcion">El propósito de este proyecto fue el de generar ingresos y empleo, así como incentivar el desarrollo sostenido y responsable del turismo en los distritos de Chavín de Huántar, Acopalca y San Marcos, ubicados en el Corredor de Conchucos, en Ancash. Este convenio se ejecutó en colaboración con la Compañía Minera Antamina y el Municipio de Chavín de Huántar.</p>
				</div>
			</div>
		</li>
		<li class="participa__li">
			<div class="row participa__item">
				<div class=" participa__imagen col-12 col-lg-6 pb-5 pl-0">
					<img class="participa__imagen-img img-fluid" src="{{ url('images/proyectos-dirige/proyecto_cerrado5.png') }}">
				</div>
				<div class="participa__box col-12 col-lg-6 pb-5">
					<h2 class="participa__item-title">Investigación Arqueológica y Conservación del Complejo Arqueológico de Chavín de Huántar (2013-2016)</h2>
					<p class="participa__item-descripcion">Durante cuatro años, la Fundación Wiese participó en este proyecto, brindando su apoyo en la administración de los recursos destinados por la Compañía Minera Antamina a la ejecución de los trabajos de investigación arqueológica y conservación del Complejo Arqueológico de Chavín de     Huántar, en la región de Ancash.  </p>
				</div>
			</div>
		</li>
		<li class="participa__li">
			<div class="row participa__item">
				<div class=" participa__imagen col-12 col-lg-6 pb-5 pl-0">
					<img class="participa__imagen-img img-fluid" src="{{ url('images/proyectos-dirige/proyecto_cerrado6.png') }}">
				</div>
				<div class="participa__box col-12 col-lg-6 pb-5">
					<h2 class="participa__item-title">Película Chavín de Huántar, el teatro del más allá (2015-2016)</h2>
					<p class="participa__item-descripcion">El objetivo de este documental dramatizado fue dar a conocer la cultura Chavín al mundo. Cómo se originó hace 3200 años, y la importancia que tuvo su influencia en las culturas posteriores. La Fundación Wiese y Wanda Films co-produjeron este largometraje que fue difundido a nivel nacional en varias salas de cine y a nivel global, a través de la señal de National Geographic. </p>
				</div>
			</div>
		</li> -->
	</ul>
	<div class="text-right">
		<a class="participa__link" href="{{url('/fundacion-wiese-dirige')}}"> < Regresar</a>
	</div>
</div>

@include('frontend.partials.newsletter')
@endsection

@section('scripts')

@endsection
