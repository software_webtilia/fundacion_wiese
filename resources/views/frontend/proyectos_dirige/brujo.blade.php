@extends('frontend.layouts.layout')
@section('title', 'Fundación Wiese')
@section('content')
<div class="d-flex align-items-center">
	<img src="{{ url('images/proyectos-dirige/banner_brujo.jpg') }}" class="img-fluid" alt="">
	<div class="banner_brujo">
		<h1 class="banner_brujo__titulo">Conoce más sobre El Brujo</h1>
		<div class="proyectos__boton mt-4">
			<a href="http://www.elbrujo.pe" target="_blank" class="proyectos__link">ver más  <span class="proyectos__separador-flecha"><i class="fas fa-angle-right"></i></span> </a>
		</div>
	</div>
</div>
<div class="container-fluid">
	<div class="row">
		<div class="col-md-2 pl-0">
				<img class="mision__imagen-w img-fluid" src="{{url('images/proyectos-dirige/w-lila.jpg')}}">
		</div>
		<div class="col-md-8">
			<div class="participa__titulo-box text-center">
				<h1 class="tabs-box__titulo">Complejo Arqueológico El Brujo</h1>
				<p class="participa__subtitle">El Brujo, un destino fascinante para todos sus visitantes y fuente de nuevos conocimientos para la humanidad.</p>
				<ul class="nav nav-tabs d-flex justify-content-center" role="tablist" da>
				  <li class="nav-item complejo__item complejo__item--lila" data-color="lila">
				    <a class="nav-link active complejo__link complejo__link_active complejo__link_active--lila" href="#proyectos-dirige" role="tab" data-toggle="tab">¿Qué hacemos?</a>
				  </li>
				  <li class="nav-item complejo__item complejo__item--naranja" data-color="naranja">
				    <a class="nav-link complejo__link" href="#objetivos-estrategicos" role="tab" data-toggle="tab">Objetivos estratégicos</a>
				  </li>
				  <li class="nav-item complejo__item complejo__item--rojo" data-color="rojo">
				    <a class="nav-link complejo__link" href="#equipo-trabajo" role="tab" data-toggle="tab">Equipo de trabajo</a>
				  </li>
				  <li class="nav-item complejo__item complejo__item--morado" data-color="morado">
				    <a class="nav-link complejo__link" href="#eventos-academicos" role="tab" data-toggle="tab">Eventos Académicos</a>
				  </li>
				</ul>
			</div>
		</div>
		<div class="col-md-2">
		</div>
	</div>
</div>
<div class="complejo">
	<div class="container">
		<!-- Tab panes -->
		<div class="tab-content complejo__content">
			<div role="tabpanel" class="tab-pane fade in active show" id="proyectos-dirige">
			 	<div class="row">
			  		<div class="col-12 col-lg-6 complejo__descripcion">
							<p>En el marco de sucesivos Convenios de Cooperación suscritos con el Ministerio de Cultura, la Fundación Wiese  trabaja por la investigación, conservación, puesta en valor y difusión del Complejo Arqueológico El Brujo.  <span class="complejo__descripcion--bold-lila">Actuamos allí, inspirados por el amor al Perú</span> y la pasión de Don Guillermo “Pancho” Wiese, quien inició este proyecto en agosto de 1990. </p>
			  		</div>
			  		<div class="col-12 col-lg-6 complejo__img">
			  			<img class="img-fluid" src="{{ url('images/proyectos-dirige/img1.png') }}">
			  		</div>
			  	</div>
			</div>
		  	<div role="tabpanel" class="tab-pane fade" id="objetivos-estrategicos">
			  	<div class="row">
			  		<div class="col-12 col-lg-6 complejo__descripcion">
			  			<ul>
			  				<li class="complejo__descripcion-item"><span class="complejo__descripcion--bold-naranja">Impulsar nuevos proyectos de investigación científica</span> en colaboración con organizaciones, instituciones y aliados estratégicos.</li>
			  				<li class="complejo__descripcion-item"><span class="complejo__descripcion--bold-naranja">Garantizar el acceso</span> de públicos diversos al conocimiento generado.</li>
			  				<li class="complejo__descripcion-item"><span class="complejo__descripcion--bold-naranja">Integrar</span> a las comunidades aledañas al destino turístico.</li>
			  			</ul>
			  		</div>
			  		<div class="col-12 col-lg-6 complejo__img">
			  			<img class="img-fluid" src="{{ url('images/proyectos-dirige/img2.png') }}">
			  		</div>
			  	</div>
		  	</div>
		  	<div role="tabpanel" class="tab-pane fade" id="equipo-trabajo">
		  		<div class="col-12 tab-content__img pl-0 pr-0">
		  			<img class="img-fluid" src="{{ url('images/proyectos-dirige/img3.png') }}">
			  	</div>
		  	</div>
		  	<div role="tabpanel" class="tab-pane fade" id="eventos-academicos">
		  		<div class="col-12 tab-content__img pl-0 pr-0">
		  			<a target="_blank" href="https://www.elbrujo.pe/investigacion-y-publicaciones/eventos-academicos/coloquio-academico-2018/"><img class="img-fluid" src="{{ url('images/proyectos-dirige/eventos_el_brujo_01.jpg') }}"></a>		  			
			  	</div>
		  	</div
		</div>
	</div>
</div>
<div class="container-fluit beneficiados d-lg-flex justify-content-lg-center" style="background: url({{ url('images/proyectos-dirige/fondo.jpg') }}) no-repeat center/cover;">
	<!-- <img class="img-fluid" src="{{ url('images/proyectos-dirige/fondo.png') }}"> -->
	<div class="col-12 col-lg-5 col-xl-4 beneficiados__box">
		<div class="d-flex justify-content-center align-items-end mb-3">
			<span class="beneficiados__numero">
				<span class="beneficiados__numero-letra beneficiados__numero-letra--top">MÁS DE</span><br>415</span>
			<span class="beneficiados__numero-letra">
				<span class="beneficiados__numero-letra beneficiados__numero-letra--ceros">000</span>
				<br>PERSONAS<br>IMPACTADAS
			</span>
		</div>
		<!-- <h1 class="beneficiados__subtitulo">BENEFICIADAS</h1>
		<h2 class="beneficiados__subtitulo-h3">What you purchase supports</h2> -->
		<ul>
			<li>
				<div class="beneficiados__item d-flex justify-content-center align-items-end mt-5 mb-3">
					<div class="beneficiados__img beneficiados__img--width">
						<img class="img-fluid beneficiados__item-img" src="{{ url('images/proyectos-dirige/icon4.png') }}">
					</div>
					<div class="text-left ml-3">
						<span class="beneficiados__item-numero">147</span>
						<p class="beneficiados__item-texto mb-2">Puestos de <br> trabajo <br> generados</p>
					</div>
				</div>
			</li>
			<li>
				<div class="beneficiados__item d-flex justify-content-center align-items-end mt-5 mb-3">
					<div class="beneficiados__img">
						<img class="img-fluid beneficiados__item-img" src="{{ url('images/proyectos-dirige/icon5.png') }}">
					</div>
					<div class="text-left ml-3">
						<span class="beneficiados__item-numero">415,326</span>
						<p class="beneficiados__item-texto mb-2">Turistas <br> atendidos </p>
					</div>
				</div>
			</li>
			<li>
				<div class="beneficiados__item d-flex justify-content-center align-items-end mt-5 mb-3">
					<div class="beneficiados__img">
						<img class="img-fluid beneficiados__item-img" src="{{ url('images/proyectos-dirige/icon6.png') }}">
					</div>
					<div class="text-left ml-3">
						<span class="beneficiados__item-numero">26</span>
						<p class="beneficiados__item-texto mb-2">Negocios <br> impulsados</p>
					</div>
				</div>
			</li>
		</ul>
	</div>
</div>
@include('frontend.partials.newsletter')
@endsection
@section('scripts')
 <script type="text/javascript">
 		var URL_SITE="{{ url('') }}";
 </script>
@endsection
