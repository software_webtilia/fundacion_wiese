@extends('frontend.layouts.layout')
@section('title', 'Fundación Wiese')

@section('content')
<div class="d-flex align-items-center">
	<img src="{{ url('images/proyectos-dirige/banner_proyecto_educativo.jpg') }}" class="img-fluid" alt="">
</div>

<div class="container-fluid">
	<div class="row">
		<div class="col-md-2 pl-0">
				<img class="mision__imagen-w img-fluid" src="{{url('images/proyectos-dirige/w-lila.jpg')}}">
		</div>
		<div class="col-md-8">
			<div class="participa__titulo-box text-center">
				<h1 class="tabs-box__titulo">{!! $project_direct->titulo !!}</h1>
				<p class="participa__subtitle">{!! $project_direct->descripcion_general !!}</p>
				<ul class="nav nav-tabs d-flex justify-content-center" role="tablist">
				  <li class="nav-item complejo__item complejo__item--lila" data-color="lila">
				    <a class="nav-link active complejo__link complejo__link_active complejo__link_active--lila" href="#proyectos-dirige" role="tab" data-toggle="tab">¿Qué Hacemos?</a>
				  </li>
				  <li class="nav-item complejo__item complejo__item--naranja" data-color="naranja">
				    <a class="nav-link complejo__link" href="#objetivos-estrategicos" role="tab" data-toggle="tab">Objetivos Estratégicos</a>
				  </li>
				  <li class="nav-item complejo__item complejo__item--rojo" data-color="rojo">
				    <a class="nav-link complejo__link" href="#equipo-trabajo" role="tab" data-toggle="tab">Equipo de trabajo</a>
				  </li>
				</ul>
			</div>
		</div>
		<div class="col-md-2">
		</div>
	</div>
</div>

<div class="complejo">
	<div class="container">
		<!-- Tab panes -->
		<div class="tab-content complejo__content">
			<div role="tabpanel" class="tab-pane fade in active show" id="proyectos-dirige">
			 	<div class="row">
			  		<div class="col-12 col-lg-6 complejo__descripcion">
			  			{!! $project_direct->descripcion_1 !!}
			  		</div>
			  		<div class="col-12 col-lg-6 complejo__img">
			  			<img class="img-fluid" src="{{ url('images/proyectos-dirige/'.$project_direct->imagen1) }}">
			  		</div>
			  	</div>
			</div>
		  	<div role="tabpanel" class="tab-pane fade" id="objetivos-estrategicos">
			  	<div class="row">
			  		<div class="col-12 col-lg-6 complejo__descripcion">
			  			{!! $project_direct->descripcion_2 !!}
			  		</div>
			  		<div class="col-12 col-lg-6 complejo__img">
			  			<img class="img-fluid" src="{{ url('images/proyectos-dirige/'.$project_direct->imagen2) }}">
			  		</div>
			  	</div>
		  	</div>
		  	<div role="tabpanel" class="tab-pane fade" id="equipo-trabajo">
		  		<div class="col-12 tab-content__img pl-0 pr-0">
		  			<img class="img-fluid" src="{{ url('images/proyectos-dirige/'.$project_direct->equipo) }}">
			  	</div>
		  	</div>
		</div>


	</div>
</div>

<div class="container-fluit beneficiados d-lg-flex justify-content-lg-center" style="background: url({{ url('images/proyectos-dirige/'.$project_direct->fondo) }}) no-repeat center/cover;">
	<div class="col-12 col-lg-5 col-xl-4 beneficiados__box">

    @if($project_direct->impacto_numero)
		<div class="d-flex justify-content-center align-items-end mb-3">
			<span class="beneficiados__numero">
				<span class="beneficiados__numero-letra beneficiados__numero-letra--top-educacion">MÁS DE</span><br>{{$project_direct->impacto_numero}}</span>
			{!!$project_direct->impacto_label!!}
		</div>
    @endif

		<ul>
			@foreach($project_direct->impacts as $impact)
				<li>
					<div class="beneficiados__item d-flex justify-content-center align-items-end mt-5 mb-3">
						<div class="beneficiados__img beneficiados__img--margin">
							<img class="img-fluid beneficiados__item-img @if($project_direct->id == 3) p-0 @endif" src="{{ url('images/proyectos-dirige/'.$impact->icono) }}">
						</div>
						<div class="text-left ml-3">
							<span class="beneficiados__item-numero">{{ $impact->cantidad }}</span>
							<p class="beneficiados__item-texto mb-1">{!! $impact->label !!}</p>
						</div>
					</div>
				</li>
			@endforeach
		</ul>
	</div>
</div>

@include('frontend.partials.newsletter')

@endsection

@section('scripts')
 <script type="text/javascript">
 		var URL_SITE="{{ url('') }}";
 </script>
@endsection
