@extends('frontend.layouts.layout')
@section('title', 'Fundación Wiese')

@section('content')
<div class="participa__banner d-flex align-items-center">
	<img src="{{ url('images/novedades/obras-por-impuestos-el-brujo.jpg') }}" class="img-fluid" alt="">
</div>


<div class="container-fluid">
	<div class="row">
		<div class="col-md-2 pl-0">
				<img class="mision__imagen-w img-fluid" src="{{url('images/w-izqquierda.png')}}">
		</div>
		<div class="col-md-8">
			<div class="participa__titulo-box text-center">
				<h1 class="participa__title participa__title--color-gris ">El Ministerio de Cultura, el Gobierno Regional de La Libertad e Inmuebles Panamericana firman el convenio de obras por impuestos a realizarse en El Brujo</h1>
				<p class="fechas__parrafo fechas__parrafo--detalle"><img src="{{url('images/calendar.png')}}" class="fechas__calendario"><span class="fechas__titulo">12 de Febrero</span></p>
				<p class="participa__subtitle participa__subtitle--detalle"></p>
			</div>
		</div>
		<div class="col-md-2">
		</div>
	</div>
</div>

<div class="container">
	<div class="detalle">
		<!-- <img class="detalle__imagen-img img-fluid" src="{{ url('images/novedades/detalle1.jpg') }}"> -->
		<div class="row">
			<div class="col-12 ">
				<p class="detalle__parrafo">El Brujo es un yacimiento arqueológico fascinante, un lugar que registra evidencias de ocupaciones humanas de 14,000 años de antigüedad, entre otras características únicas asociadas con sus hallazgos, con la belleza de su paisaje y con una cultura inmaterial milenaria que se mantiene viva gracias a los peruanos que habitan en su zona de influencia.</p>
				<p class="detalle__parrafo">La Fundación Wiese, en alianza con el Ministerio de Cultura y en cumplimiento del mandato de su fundador, viene trabajando hace 27 años para que El Brujo continúe siendo una fuente permanente de nuevos conocimientos para la humanidad, así como un motivo de orgullo y desarrollo para todos los peruanos.</p>
				<p class="detalle__parrafo">En el marco de esta alianza se han llevado a cabo numerosos proyectos de investigación científica y promoción turística de diversa naturaleza.</p>
				<p class="detalle__parrafo">El proyecto de Investigación Científica financiado mediante el mecanismo de Obras por Impuestos, firmado el pasado viernes 9 de febrero, tiene como objetivo general: “incrementar la oferta turística en El Complejo Arqueológico El Brujo”, para lo cual se destinarán recursos (S/3.5 millones) a los trabajos de excavación, documentación y difusión de los hallazgos en tres nuevas zonas dentro del complejo; zonas donde estudios recientes han aportado evidencias de potencial arqueológico y turístico.</p>
				<p class="detalle__parrafo">Gracias al concurso del Ministerio de Cultura, el Gobierno Regional de La Libertad, Inmuebles Panamericana y la Fundación Wiese se hará posible este proyecto que es el primero en su género que se implementa a través del mecanismo de Obras por Impuestos.</p>
				<p class="detalle__parrafo">Esta alianza podría marcar el inicio de un futuro círculo virtuoso que permita a través de la cultura generar mayor desarrollo para el Perú.</p>
			</div>

			<!-- <div class="col-12 col-md-6">
				<img class="detalle__imagen-img img-fluid" src="{{ url('images/novedades/detalle2.jpg') }}">
			</div>
			<div class="col-12 col-md-6">
				<img class="detalle__imagen-img img-fluid" src="{{ url('images/novedades/detalle3.jpg') }}">
			</div> -->

		</div>
	</div>
	<!--hr class="detalle__hr"-->
</div>

<!--div class="container">
	<h2 class="detalle__titulo-novedades ">Más Novedades</h2>
	<div class="row">
		<div class="row col-12 col-xl-6 noticia">
			<div class="col-12 col-lg-6 noticia__imgen">
				<img class="participa__imagen-img img-fluid" src="{{ url('images/novedades/noticia9.jpg') }}">
			</div>
			<div class="col-12 col-lg-6">
				<div class="">
					<p class="fechas__parrafo fechas__parrafo--top"><img src="{{url('images/calendar.png')}}" class="fechas__calendario"><span class="fechas__titulo">23 de Mayo</span></p>
      				<p class="noticia__subtitulo">La Fundación Wiese lanza fondo de crédito educativo para carreras técnicas en alianza con IPFE</p>
      				<p class="noticia__contenido">Esta iniciativa aporta a reducir la tasa de deserción educativa y cubrir la demanda insatisfecha de profesionales técnicos calificados que requiere el país ...</p>
      				<a class="noticia__link" href="{{ url('/novedades/fondo-credito-educativo') }}">Leer Noticia <span class="participa__separador-flecha"><i class="fas fa-angle-right"></i></span></a>
				</div>
			</div>
		</div>
		<div class="row col-12 col-xl-6 noticia">
			<div class="col-12 col-lg-6 noticia__imgen">
				<img class="participa__imagen-img img-fluid" src="{{ url('images/novedades/noticia9.jpg') }}">
			</div>
			<div class="col-12 col-lg-6">
				<div class="">
					<p class="fechas__parrafo fechas__parrafo--top"><img src="{{url('images/calendar.png')}}" class="fechas__calendario"><span class="fechas__titulo">28 de Junio</span></p>
      				<p class="noticia__subtitulo">Una nueva etapa, una nueva identidad visual</p>
      				<p class="noticia__contenido">El propósito de este cambio es alinear la imagen visual de la Fundación con la manera en que su comportamiento es percibido por sus distintos públicos de interés...</p>
      				<a class="noticia__link" href="{{ url('/novedades/nueva-identidad-visual') }}">Leer Noticia <span class="participa__separador-flecha"><i class="fas fa-angle-right"></i></span></a>
				</div>
			</div>
		</div>
	</div>
</div-->
@include('frontend.partials.newsletter')
@endsection

@section('scripts')

@endsection
