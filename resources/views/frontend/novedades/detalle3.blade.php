@extends('frontend.layouts.layout')
@section('title', 'Fundación Wiese')

@section('content')
<div class="participa__banner d-flex align-items-center">
	<img src="{{ url('images/novedades/te-acompano-en-tu-desarrollo.jpg') }}" class="img-fluid" alt="">
</div>


<div class="container-fluid">
	<div class="row">
		<div class="col-md-2 pl-0">
				<img class="mision__imagen-w img-fluid" src="{{url('images/w-izqquierda.png')}}">
		</div>
		<div class="col-md-8">
			<div class="participa__titulo-box text-center">

				<h1 class="participa__title participa__title--color-gris ">Fundación Wiese comienza con los Talleres de Padres de Familia: “Te acompaño en tu desarrollo”</h1>
				<p class="fechas__parrafo fechas__parrafo--detalle"><img src="{{url('images/calendar.png')}}" class="fechas__calendario"><span class="fechas__titulo">15 de Mayo</span></p>
				<p class="participa__subtitle participa__subtitle--detalle">Desde abril del 2016 la <strong>Fundación Wiese</strong> inició la segunda fase del Proyecto Calidad Educativa que lleva por nombre “Mejor clima escolar, mejores aprendizajes” y que trabaja con directores, subdirectores y docentes de 14 escuelas públicas de Lurín y Pachacamac. Esta segunda fase tiene como objetivo principal mejorar significativamente el clima institucional y de aula para favorecer el desarrollo de mejores enseñanzas y aprendizajes.</p>
			</div>
		</div>
		<div class="col-md-2">
		</div>
	</div>
</div>

<div class="container">
	<div class="detalle">
		<!-- <img class="detalle__imagen-img img-fluid" src="{{ url('images/novedades/detalle1.jpg') }}"> -->
		<div class="row">
			<div class="col-12">
				<p class="detalle__parrafo">En el marco de ese objetivo, la Fundación Wiese realizó  un proyecto piloto a forma de talleres dirigidos a padres y madres de familia de dos Instituciones Educativas que son beneficiarias de nuestra propuesta. En estos talleres se pudieron observar los siguientes aspectos más críticos:</p>

				<p class="detalle__parrafo" style="margin-left: 5%;">
				1.	Distancia entre la familia y la escuela: poco interés y participación en actividades de la escuela y en lo que concierne a sus hijos como por ejemplo el aprendizaje o el comportamiento.<br><br>
				2.	Distancia entre la familia y el niño o adolescente, conforme van creciendo la familia de alguna manera se desentiende. Se percibe negligencia en muchos casos.<br><br>
				3.	Uso del castigo físico como una forma de educar y criar a sus hijos, repitiendo patrones de cómo fueron criados ellos de niños. La violencia está justificada.<br><br>
				4.	Inequidad de género: las niñas tienen más responsabilidades que los niños en las tareas del hogar, muchas de ellas asumidas obligatoriamente aun cuando estas no correspondan con la edad. También se repiten los patrones de las responsabilidades que asumieron las madres desde niñas.<br><br>
				5.	Escasa educación de las madres e historias de embarazo temprano y adolescente que se observó se repite en las hijas mayores de las que participaron de los talleres. Si bien las hijas mayores concluyeron la secundaria, ellas se embarazaron tempranamente como sus madres.</p>

				<p class="detalle__parrafo">Luego de trabajar con los padres y madres de familia, se consiguió que estos tomen conciencia sobre la importancia de contar con un entorno positivo para sus menores hijos, pudiéndose lograr los siguientes objetivos:</p>

				<p class="detalle__parrafo" style="margin-left: 5%;">1.	Descubrir y tomar conciencia que como padres y madres la tarea no ha terminado y que es necesario realizar un acercamiento a sus hijos en esta etapa de desarrollo que es la adolescencia y hacer efectivo su acompañamiento.<br><br>
				2.	Tomar conciencia de un mayor acercamiento a la escuela, un mayor compromiso con la educación de sus hijos, sumar esfuerzos.<br><br>
				3.	Un cierto cuestionamiento al uso de la violencia y el uso del castigo físico.<br><br>
				4.	Comprender que el cuidado de los hijos e hijas es integral no solo abarca un aspecto.<br><br>
				5.	Comprender los patrones que se repiten y son necesarios cambiar: modos de crianza, roles de género, embarazo temprano, educación sobre todo de las mujeres (Esto es aún incipiente).</p>

				<p class="detalle__parrafo">Por ello, desde este año, el Proyecto Calidad Educativa: “Mejor clima escolar, mejores aprendizajes, ha incluido dentro de sus actividades regulares el trabajo con los padres y madres de familia de las 14 escuelas que dirigimos, buscando contribuir a mejorar y fortalecer las relaciones entre padres e hijos y las relaciones entre los padres y la escuela; con el fin de tomar conciencia de la responsabilidad como padre-madres desde el Ser y la promesa, y no solo desde el hacer-hacer.</p>

				<p class="detalle__parrafo"><strong>El cuidado en la crianza de los hijos</strong><br><br>
				En estos talleres también buscaremos identificar y prevenir conductas de riesgo social: adicciones (alcohol, drogas), embarazo adolescente, violencia en el hogar y la escuela. Uso de internet y redes sociales (cabinas en el barrio). Otras áreas del cuidado, cuerpo, comportamiento, salud, espíritu y aprendizaje.<br>
				En el caso de los niños pequeños se incidirá en el cuidado acorde con su edad.
				</p>

				<p class="detalle__parrafo"><strong>Consecuencias versus castigo</strong><br><br>
				Un tema importante dentro de los talleres es que los padres y madres comprendan la diferencia entre castigo (hacer daño) y consecuencias de nuestros actos. La propuesta de presentar las consecuencias como la posibilidad que el niño/joven puede elegir acciones positivas para su vida. Por otro lado, le enseñamos a nuestros hijos a reflexionar críticamente, sobretodo asumir responsabilidad sobre sus actos, incluido el poder aprender de sus errores. Por otro lado, el castigo presenta resultados negativos para la salud física y mental de las personas sobre-todo al hacer uso de la violencia física y psicológica. (Estadísticas y leyes de protección a nivel nacional e internacional del niño-niña-adolescente).<br><br>
				<i>“El clima en el aula que se logra permite que los estudiantes puedan generar opciones creativas para resolver problemas, presentar sus posiciones de maneras asertivas, escuchar activamente a los demás, tomar diversas perspectivas, generar empatía con sus compañeros y poner en práctica su pensamiento crítico frente a lo que pasa en su contexto cercano.” </i> (Enrique Chaux)

				</p>
			</div>
			<!-- <div class="col-12 col-md-6">
				<img class="detalle__imagen-img img-fluid" src="{{ url('images/novedades/detalle2.jpg') }}">
			</div>
			<div class="col-12 col-md-6">
				<img class="detalle__imagen-img img-fluid" src="{{ url('images/novedades/detalle3.jpg') }}">
			</div> -->
		</div>
	</div>
	<!--hr class="detalle__hr"-->
</div>

<!--div class="container">
	<h2 class="detalle__titulo-novedades ">Más Novedades</h2>
	<div class="row">
		<div class="row col-12 col-xl-6 noticia">
			<div class="col-12 col-lg-6 noticia__imgen">
				<img class="participa__imagen-img img-fluid" src="{{ url('images/novedades/noticia10.jpg') }}">
			</div>
			<div class="col-12 col-lg-6">
				<div class="">
					<p class="fechas__parrafo fechas__parrafo--top"><img src="{{url('images/calendar.png')}}" class="fechas__calendario"><span class="fechas__titulo">2 de Febrero</span></p>
      				<p class="noticia__subtitulo">El Ministerio de Cultura, el Gobierno Regional de La Libertad e Inmuebles Panamericana firman el convenio de obras por impuestos a realizarse en El Brujo</p>
      				<p class="noticia__contenido">El Brujo es un yacimiento arqueológico fascinante, un lugar que registra evidencias de ocupaciones humanas de 14,000 años de antigüedad...</p>
      				<a class="noticia__link" href="{{ url('/novedades/obras-por-impuestos-el-brujo') }}">Leer Noticia <span class="participa__separador-flecha"><i class="fas fa-angle-right"></i></span></a>
				</div>
			</div>
		</div>
		<div class="row col-12 col-xl-6 noticia">
			<div class="col-12 col-lg-6 noticia__imgen">
				<img class="participa__imagen-img img-fluid" src="{{ url('images/novedades/noticia9.jpg') }}">
			</div>
			<div class="col-12 col-lg-6">
				<div class="">
					<p class="fechas__parrafo fechas__parrafo--top"><img src="{{url('images/calendar.png')}}" class="fechas__calendario"><span class="fechas__titulo">28 de Junio</span></p>
      				<p class="noticia__subtitulo">Una nueva etapa, una nueva identidad visual</p>
      				<p class="noticia__contenido">El propósito de este cambio es alinear la imagen visual de la Fundación con la manera en que su comportamiento es percibido por sus distintos públicos de interés...</p>
      				<a class="noticia__link" href="{{ url('/novedades/nueva-identidad-visual') }}">Leer Noticia <span class="participa__separador-flecha"><i class="fas fa-angle-right"></i></span></a>
				</div>
			</div>
		</div>
	</div>
</div-->
@include('frontend.partials.newsletter')
@endsection

@section('scripts')

@endsection
