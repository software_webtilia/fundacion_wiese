@extends('frontend.layouts.layout')
@section('title', 'Fundación Wiese')

@section('content')
<div class="participa__banner d-flex align-items-center">
	<img src="{{ url('images/novedades/banners-noticias-identidad.jpg') }}" class="img-fluid" alt="">
</div>


<div class="container-fluid">
	<div class="row">
		<div class="col-md-2 pl-0">
				<img class="mision__imagen-w img-fluid" src="{{url('images/w-izqquierda.png')}}">
		</div>
		<div class="col-md-8">
			<div class="participa__titulo-box text-center">

				<h1 class="participa__title participa__title--color-gris ">Una nueva etapa, una nueva identidad visual</h1>
				<p class="fechas__parrafo fechas__parrafo--detalle"><img src="{{url('images/calendar.png')}}" class="fechas__calendario"><span class="fechas__titulo">28 de Junio</span></p>
				<p class="participa__subtitle participa__subtitle--detalle">Después de 58 años de trabajo continuo por el Perú, la Fundación Wiese renueva su imagen.</p>
			</div>
		</div>
		<div class="col-md-2">
		</div>
	</div>
</div>

<div class="container">
	<div class="detalle">
		<!-- <img class="detalle__imagen-img img-fluid" src="{{ url('images/novedades/novedad1.jpg') }}"> -->
		<div class="row">
			<div class="col-12 col-lg-6">
				<p class="detalle__parrafo">El propósito de este cambio es alinear la imagen visual de la Fundación con la manera en que su comportamiento es percibido por sus distintos públicos de interés.</p>
			
				<p class="detalle__parrafo">El nuevo logotipo sintetiza en su construcción un estilo ágil y dinámico, de  innovación constante y orientación a la acción. </p>
			
				<p class="detalle__parrafo">Su gama de colores vibrantes, relacionados a la peruanidad, simboliza nuestra identidad: multicultural y diversa. Los colores llevados a un gradiente transmiten movimiento, progreso y crecimiento.</p>

				<p class="detalle__parrafo">Finalmente la "W", su elemento identitario, representa nuestro compromiso con el país y la aspiración de que cada cosa que hagamos construya algo más grande  y de mayor impacto.</p>
			</div>
			<div class="col-12 col-lg-6">
				
				<p class="detalle__parrafo">A través de su nuevo sistema visual, la Fundación Wiese busca transmitir la visión de la Familia peruana que la respalda y la misión encomendada al equipo que la gestiona: <i> identificar, inspirar e impulsar a mejores ciudadanos para transformar el Perú, promoviendo la identidad, la solidaridad y la  equidad.</i></p>
				<div class="text-center mt-5">
					<img class="mx-auto img-fluid" src="{{ url('images/logo.png') }}">
				</div>
			</div>

		</div>
	</div>
	<!--hr class="detalle__hr"-->
</div>

<!--div class="container">
	<h2 class="detalle__titulo-novedades ">Más Novedades</h2>
	<div class="row">
		<div class="row col-12 col-xl-6 noticia">
			<div class="col-12 col-lg-6 noticia__imgen">
				<img class="participa__imagen-img img-fluid" src="{{ url('images/novedades/noticia9.jpg') }}">
			</div>
			<div class="col-12 col-lg-6">
				<div class="">
					<p class="fechas__parrafo fechas__parrafo--top"><img src="{{url('images/calendar.png')}}" class="fechas__calendario"><span class="fechas__titulo">28 de Junio</span></p>
      				<p class="noticia__subtitulo">Una nueva etapa, una nueva identidad visual</p>
      				<p class="noticia__contenido">El propósito de este cambio es alinear la imagen visual de la Fundación con la manera en que su comportamiento es percibido por sus distintos públicos de interés...</p>
      				<a class="noticia__link" href="{{ url('/novedades/nueva-identidad-visual') }}">Leer Noticia <span class="participa__separador-flecha"><i class="fas fa-angle-right"></i></span></a>
				</div>
			</div>
		</div>
		<div class="row col-12 col-xl-6 noticia">
			<div class="col-12 col-lg-6 noticia__imgen">
				<img class="participa__imagen-img img-fluid" src="{{ url('images/novedades/noticia10.jpg') }}">
			</div>
			<div class="col-12 col-lg-6">
				<div class="">
					<p class="fechas__parrafo fechas__parrafo--top"><img src="{{url('images/calendar.png')}}" class="fechas__calendario"><span class="fechas__titulo">15 de Mayo</span></p>
      				<p class="noticia__subtitulo">Fundación Wiese comienza con los Talleres de Padres de Familia: “Te acompaño en tu desarrollo”</p>
      				<p class="noticia__contenido">Desde abril del 2016 la <strong>Fundación Wiese</strong> inició la segunda fase del Proyecto Calidad Educativa que lleva por nombre “Mejor clima escolar, mejores aprendizajes” y que trabaja con directores, subdirectores y docentes de 14 escuelas públicas de Lurín y Pachacamac...</p>
      				<a class="noticia__link" href="{{ url('/novedades/te-acompano-en-tu-desarrollo') }}">Leer Noticia <span class="participa__separador-flecha"><i class="fas fa-angle-right"></i></span></a>
				</div>
			</div>
		</div>
	</div>
</div-->
@include('frontend.partials.newsletter')
@endsection

@section('scripts')

@endsection
