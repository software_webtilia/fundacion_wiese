@extends('frontend.layouts.layout')
@section('title', 'Fundación Wiese')

@section('content')
<div class="participa__banner d-flex align-items-center">
	<img src="{{ url('images/novedades/banners_noticia_abriga.jpg') }}" class="img-fluid" alt="">
</div>


<div class="container-fluid">
	<div class="row">
		<div class="col-md-2 pl-0">
				<img class="mision__imagen-w img-fluid" src="{{url('images/w-izqquierda.png')}}">
		</div>
		<div class="col-md-8">
			<div class="participa__titulo-box text-center">
				<h1 class="participa__title participa__title--color-gris ">“TU SOLIDARIDAD ABRIGA” <br> Campaña contra la ola de frío en la Sierra</h1>
				<p class="fechas__parrafo fechas__parrafo--detalle"><img src="{{url('images/calendar.png')}}" class="fechas__calendario"><span class="fechas__titulo">12 de Junio</span></p>
				<p class="participa__subtitle participa__subtitle--detalle">•	La FUNDACIÓN WIESE en alianza con ADRA PERÚ dan inicio a campaña de ayuda humanitaria a nivel nacional. <br>
				•	La campaña pretende proteger del frío a las poblaciones vulnerables Alto Andinas del Perú, ante los efectos ocasionados por las heladas y nevadas. </p>
			</div>
		</div>
		<div class="col-md-2">
		</div>
	</div>
</div>

<div class="container">
	<div class="detalle">
		<!-- <img class="detalle__imagen-img img-fluid" src="{{ url('images/novedades/detalle1.jpg') }}"> -->
		<div class="row">
			<div class="col-12 ">
				<p class="detalle__parrafo">La severa ola de frío que enfrentan las zonas Alto Andinas de nuestro país viene ocasionado daños a la vida, salud, educación, actividad agrícola, ganadera e infraestructura de sus habitantes. </p>
				<p class="detalle__parrafo">Especialmente afectadas se encuentran <u>las poblaciones más vulnerables: los niños y ancianos que por su pobreza no pueden comprar ropa de abrigo.</u> A la fecha, los medios periodísticos han dado cuenta de un alarmante incremento en el número de casos de niños afectados con neumonía y de cuatro fallecidos.</p>
				<p class="detalle__parrafo">La situación ha sobrepasado la capacidad de respuesta de los Gobiernos Regionales de Apurímac, Ayacucho, Arequipa, Cusco, Huancavelica, Huánuco, Junín, Moquegua, Pasco, Puno y Tacna por lo que, con Decretos Supremos N° 060-2018 PCM y 062-2018 PCM, de fechas 12 y 21 de junio de 2018, el Gobierno Peruano ha declarado el Estado de Emergencia, por peligro inminente a consecuencia de la ocurrencia de heladas y nevadas, en 64 distritos de 28 provincias ubicadas en las mencionadas regiones. </p>
				<p class="detalle__parrafo">La presente iniciativa, impulsada y promovida por la FUNDACIÓN WIESE, pretende servir como punto de partida para la activación de una campaña de ayuda humanitaria a nivel nacional que proporcione ropa de abrigo a niños de entre 0 y 12 años y a adultos mayores de 60 años de las regiones afectadas, a través de la entrega de <u>kits de abrigo valorizados en S/ 50 que contienen una chompa, un gorro, una chalina y un par de guantes de lana.</u></p>
				<p class="detalle__parrafo">La primera entrega, co-financiada por la FUNDACIÓN WIESE y ADRA PERÚ, será de 1000 kits para ayudar a un igual número de personas (800 niños y 200 adultos mayores) en las zonas alto andinas del límite entre las regiones de Tacna y Puno, por ser una de las zonas más alejadas, vulnerables y menos atendidas a nivel nacional.</p>
				<p class="detalle__parrafo">Con todo el dinero recaudado por la Fundación Wiese para esta campaña serán entregados, en coordinación con los Centros de Operaciones de Emergencia  Regionales (COER), kits de abrigo a las personas afectadas, debidamente empadronadas. En todos los casos, las entregas serán hechas directamente por ADRA a través de sus voluntarios y supervisadas por la FUNDACIÓN WIESE. Al concluir la campaña la FUNDACIÓN WIESE emitirá y enviará a sus donantes un informe de cierre de campaña, dando cuenta de los distritos y personas atendidos, así como de los ingresos y gastos de la campaña.</p>
				<p class="detalle__parrafo">Estos kits de abrigo incluyen: chompa, chalina, chullo y guantes tejidos en lana. Se procurará realizar la compra de kits de abrigo en las mismas regiones afectadas para además contribuir con la economía y generación de puestos de trabajo en las zonas a intervenir.</p>
				<p class="detalle__parrafo"><strong>Puedes contribuir con la campaña, realizando tus donaciones a las Cuentas Corrientes BCP FUNDACIÓN AUGUSTO N. WIESE DONACIONES en S/ Nº 193-2409290-0-62, CCI: 002 193 002409290062 10 y en US$ 193-2405869-1-15, CCI 002 193 002405869115 15; y enviarnos tu voucher de depósito a  <a href="mailto:info@fundacionwiese.org"><span style="color: blue">info@fundacionwiese.org</span></a> .</strong></p>
				<p class="detalle__parrafo"><strong>¡Súmate ahora, y esta vez, unamos al Perú para ayudar a nuestros hermanos más vulnerables!</strong></p>
			</div>
		</div>
	</div>
</div>

@include('frontend.partials.newsletter')
@endsection

@section('scripts')

@endsection
