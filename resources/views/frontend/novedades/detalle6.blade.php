@extends('frontend.layouts.layout')
@section('title', 'Fundación Wiese')

@section('content')
<div class="participa__banner d-flex align-items-center">
	<img src="{{ url('images/novedades/banners_noticias_ipae.jpg') }}" class="img-fluid" alt="">
</div>


<div class="container-fluid">
	<div class="row">
		<div class="col-md-2 pl-0">
				<img class="mision__imagen-w img-fluid" src="{{url('images/w-izqquierda.png')}}">
		</div>
		<div class="col-md-8">
			<div class="participa__titulo-box text-center">
				<h1 class="participa__title participa__title--color-gris ">La Fundación Wiese recibe Premio IPAE a la Empresa en Cultura</h1>
				<p class="fechas__parrafo fechas__parrafo--detalle"><img src="{{url('images/calendar.png')}}" class="fechas__calendario"><span class="fechas__titulo">20 de Junio</span></p>
				<p class="participa__subtitle participa__subtitle--detalle">Ramón Barúa, Scotiabank y la Fundación Wiese son los ganadores de los premios IPAE 2017</p>
			</div>
		</div>
		<div class="col-md-2">
		</div>
	</div>
</div>

<div class="container">
	<div class="detalle">
		<!-- <img class="detalle__imagen-img img-fluid" src="{{ url('images/novedades/detalle1.jpg') }}"> -->
		<div class="row">
			<div class="col-12 ">
				<p class="detalle__parrafo">El 20 de junio, en la Explanada de la Huaca Pucllana, IPAE realizó la ceremonia de entrega de los Premios IPAE al Empresario y a las Empresas que contribuyen de manera destacada al desarrollo de una educación de excelencia y al progreso y difusión de la cultura peruana.</p>
				<p class="detalle__parrafo">La Fundación Wiese fue honrada por con el premio en la categoría Cultura por su labor en el Complejo Arqueológico El Brujo. Este premio reconoce tres aspectos importantes: Primero, la trascendencia que tiene el proyecto ganador en el desarrollo o preservación del patrimonio artístico o cultural del Perú para el mundo. Segundo: el grado de repercusión en la sociedad que, gracias al esfuerzo institucional, puede acceder al arte y la cultura que nos une como nación. Y tercero, la proyección a futuro en función a la generación de vínculos permanentes y de largo plazo.</p>
				<p class="detalle__parrafo">La labor de la Fundación en el Brujo, se enmarca además dentro de los valores de Solidaridad, Equidad y, principalmente, dentro del valor de Identidad que la institución promueve. Una Identidad, entendida como la consciencia histórica que abraza nuestra diversidad cultural, revalora a los peruanos y es fuente de desarrollo para la sociedad.</p>
				<p class="detalle__parrafo">Sin embargo, esta labor y este reconocimiento no habrían sido posibles sin la visión y compromiso de Don Guillermo “Pancho” Wiese de Osma. Él, junto al Arql. Régulo Franco, emprendieron en 1990, la aventura de convertir una pampa estéril profusamente huaqueada, interrumpida por unos cuantos “cerros”, en lo que hoy es el Complejo Arqueológico El Brujo.</p>
				<p class="detalle__parrafo">Por lo tanto la Fundación Wiese recibe este premio como un tributo a su memoria y en su nombre renueva su compromiso de seguir trabajando para que mejores ciudadanos sean quienes forjen el Perú del futuro. </p>
			</div>
		</div>
	</div>
	<!--hr class="detalle__hr"-->
</div>

<!--div class="container">
	<h2 class="detalle__titulo-novedades ">Más Novedades</h2>
	<div class="row">
		<div class="row col-12 col-xl-6 noticia">
			<div class="col-12 col-lg-6 noticia__imgen">
				<img class="participa__imagen-img img-fluid" src="{{ url('images/novedades/noticia9.jpg') }}">
			</div>
			<div class="col-12 col-lg-6">
				<div class="">
					<p class="fechas__parrafo fechas__parrafo--top"><img src="{{url('images/calendar.png')}}" class="fechas__calendario"><span class="fechas__titulo">23 de Mayo</span></p>
      				<p class="noticia__subtitulo">La Fundación Wiese lanza fondo de crédito educativo para carreras técnicas en alianza con IPFE</p>
      				<p class="noticia__contenido">Esta iniciativa aporta a reducir la tasa de deserción educativa y cubrir la demanda insatisfecha de profesionales técnicos calificados que requiere el país ...</p>
      				<a class="noticia__link" href="{{ url('/novedades/fondo-credito-educativo') }}">Leer Noticia <span class="participa__separador-flecha"><i class="fas fa-angle-right"></i></span></a>
				</div>
			</div>
		</div>
		<div class="row col-12 col-xl-6 noticia">
			<div class="col-12 col-lg-6 noticia__imgen">
				<img class="participa__imagen-img img-fluid" src="{{ url('images/novedades/noticia9.jpg') }}">
			</div>
			<div class="col-12 col-lg-6">
				<div class="">
					<p class="fechas__parrafo fechas__parrafo--top"><img src="{{url('images/calendar.png')}}" class="fechas__calendario"><span class="fechas__titulo">28 de Junio</span></p>
      				<p class="noticia__subtitulo">Una nueva etapa, una nueva identidad visual</p>
      				<p class="noticia__contenido">El propósito de este cambio es alinear la imagen visual de la Fundación con la manera en que su comportamiento es percibido por sus distintos públicos de interés...</p>
      				<a class="noticia__link" href="{{ url('/novedades/nueva-identidad-visual') }}">Leer Noticia <span class="participa__separador-flecha"><i class="fas fa-angle-right"></i></span></a>
				</div>
			</div>
		</div>
	</div>
</div-->
@include('frontend.partials.newsletter')
@endsection

@section('scripts')

@endsection
