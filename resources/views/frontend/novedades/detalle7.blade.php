@extends('frontend.layouts.layout')
@section('title', 'Fundación Wiese')

@section('content')
<div class="participa__banner d-flex align-items-center">
	<img src="{{ url('images/novedades/banners_donativos.jpg') }}" class="img-fluid" alt="">
</div>


<div class="container-fluid">
	<div class="row">
		<div class="col-md-2 pl-0">
				<img class="mision__imagen-w img-fluid" src="{{url('images/w-izqquierda.png')}}">
		</div>
		<div class="col-md-8">
			<div class="participa__titulo-box text-center">
				<h1 class="participa__title participa__title--color-gris ">La Fundación Wiese hace entrega de donativos a niños y <br> adultos mayores del sur del Perú</h1>
				<p class="fechas__parrafo fechas__parrafo--detalle"><img src="{{url('images/calendar.png')}}" class="fechas__calendario"><span class="fechas__titulo">31 de Julio</span></p>
				<p class="participa__subtitle participa__subtitle--detalle">Tu Solidaridad Abriga, la campaña promovida por la Fundación Wiese, ADRA y Panamericana Televisión, entrega ropa de abrigo a niños de entre 0 y 12 años y a adultos mayores de 60 años, así como frazadas a todas las familias de las comunidades atendidas.</p>
			</div>
		</div>
		<div class="col-md-2">
		</div>
	</div>
</div>

<div class="container">
	<div class="detalle">
		<!-- <img class="detalle__imagen-img img-fluid" src="{{ url('images/novedades/detalle1.jpg') }}"> -->
		<div class="row">
			<div class="col-12 ">
				<p class="detalle__parrafo">La Fundación Wiese, a través de un primer donativo, activó esta campaña, entregando 1200 kits de abrigo y 1000 frazadas en región Tacna, la más alejada del territorio nacional y una de las menos atendidas.</p>
				<p class="detalle__parrafo">Hoy estamos en Puno, la región que concentra el mayor número de personas afectadas, y ya nos encontramos alistando una tercera entrega para Huancavelica, la región más pobre, donde también estaremos presentes para supervisar directamente y apoyar durante la entrega de los kits que se compren con dinero recaudado a través de la Fundación Wiese.</p>
				<!--  GALERÍA DE FOTOS -->
				<img class="detalle__imagen-img img-fluid" src="{{ url('images/novedades/detalle4.jpg') }}">
				<p class="detalle__parrafo">Todo el dinero recaudado por la Fundación Wiese para esta campaña será entregado a través de los voluntarios y bajo los protocolos internacionales de entrega de ADRA, quién en coordinación con los Centros de Operaciones de Emergencia Regionales (COER) y las autoridades locales, verifican que no se duplique la ayuda y la entregan directamente en la mano de las personas afectadas, debidamente empadronadas. </p>
				<p class="detalle__parrafo">La compra de kits de abrigo se realiza en las mismas regiones afectadas para además contribuir con la economía y generación de puestos de trabajo en las zonas a intervenir.</p>
				<p class="detalle__parrafo">De esta forma, los aportes de la FW pretenden, además de llevar ayuda directa a los afectados por esta emergencia, inspirar la solidaridad de los ciudadanos peruanos de buen corazón, para que contribuyan con nuestros hermanos peruanos más vulnerables de los 64 distritos y 28 provincias declaradas en emergencia en el territorio nacional. </p>
			</div>

			<!--  VIDEO -->
			<div class="col-12 col-md-6 mt-3">
				<img class="detalle__imagen-img img-fluid" src="{{ url('images/novedades/detalle5.jpg') }}">
			</div>
			<div class="col-12 col-md-6 mt-3">
				<a href="https://www.youtube.com/watch?v=mufdhOP7X_4&rel=0&autoplay=1&showinfo=0&iv_load_policy=3&playlist=mufdhOP7X_4" data-lity>
				<img class="detalle__imagen-img img-fluid" src="{{ url('images/novedades/detalle6.jpg') }}"></a>
			</div>

			<div class="col-12 ">
				<p class="detalle__parrafo">Hacemos una invocación a las personas e instituciones de buen corazón para que desde donde se encuentren, como puedan y prefieran, se sumen a la ayuda.</p>
				<p class="detalle__parrafo">Para más información sobre esta campaña pueden visitar <a href="{{url('/novedades/tu-solidaridad-abriga')}}"><strong>http://www.fundacionwiese.org/novedades/tu-solidaridad-abriga</strong></a>  </p>
				<p class="detalle__parrafo">Cada kit de abrigo, valorizados en S/ 50, contienen una chompa, un gorro, una chalina y un par de guantes de lana. Y por cada familia se entregan dos frazadas.</p>
			</div>
		</div>
	</div>
	<!--hr class="detalle__hr"-->
</div>

<!--div class="container">
	<h2 class="detalle__titulo-novedades ">Más Novedades</h2>
	<div class="row">
		<div class="row col-12 col-xl-6 noticia">
			<div class="col-12 col-lg-6 noticia__imgen">
				<img class="participa__imagen-img img-fluid" src="{{ url('images/novedades/noticia9.jpg') }}">
			</div>
			<div class="col-12 col-lg-6">
				<div class="">
					<p class="fechas__parrafo fechas__parrafo--top"><img src="{{url('images/calendar.png')}}" class="fechas__calendario"><span class="fechas__titulo">23 de Mayo</span></p>
      				<p class="noticia__subtitulo">La Fundación Wiese lanza fondo de crédito educativo para carreras técnicas en alianza con IPFE</p>
      				<p class="noticia__contenido">Esta iniciativa aporta a reducir la tasa de deserción educativa y cubrir la demanda insatisfecha de profesionales técnicos calificados que requiere el país ...</p>
      				<a class="noticia__link" href="{{ url('/novedades/fondo-credito-educativo') }}">Leer Noticia <span class="participa__separador-flecha"><i class="fas fa-angle-right"></i></span></a>
				</div>
			</div>
		</div>
		<div class="row col-12 col-xl-6 noticia">
			<div class="col-12 col-lg-6 noticia__imgen">
				<img class="participa__imagen-img img-fluid" src="{{ url('images/novedades/noticia9.jpg') }}">
			</div>
			<div class="col-12 col-lg-6">
				<div class="">
					<p class="fechas__parrafo fechas__parrafo--top"><img src="{{url('images/calendar.png')}}" class="fechas__calendario"><span class="fechas__titulo">28 de Junio</span></p>
      				<p class="noticia__subtitulo">Una nueva etapa, una nueva identidad visual</p>
      				<p class="noticia__contenido">El propósito de este cambio es alinear la imagen visual de la Fundación con la manera en que su comportamiento es percibido por sus distintos públicos de interés...</p>
      				<a class="noticia__link" href="{{ url('/novedades/nueva-identidad-visual') }}">Leer Noticia <span class="participa__separador-flecha"><i class="fas fa-angle-right"></i></span></a>
				</div>
			</div>
		</div>
	</div>
</div-->
@include('frontend.partials.newsletter')
@endsection

@section('scripts')

@endsection
