@extends('frontend.layouts.layout')
@section('title', 'Fundación Wiese')

@section('content')
<div class="participa__banner d-flex align-items-center">
	<img src="{{ url('images/novedades/banner_novedades.jpg') }}" class="img-fluid" alt="">
</div>

<div class="container-fluid">
	<div class="row">
		<div class="col-md-2 pl-0">
				<img class="mision__imagen-w img-fluid" src="{{url('images/novedades/w-naranja.png')}}">
		</div>
		<div class="col-md-8">
			<div class="participa__titulo-box text-center">
				<h1 class="participa__title ">Novedades</h1>
				<p class="participa__subtitle" style="text-align:center">Descubre las últimas noticias de Fundación Wiese</p>
			</div>
		</div>
		<div class="col-md-2">
		</div>
	</div>
</div>


<div class="container">
	<div class="row">
		<div class="row col-12 col-xl-6 noticia">
			<div class="col-12 col-lg-6 noticia__imgen">
				<img class="participa__imagen-img img-fluid" src="{{ url('images/novedades/noticia12.JPG') }}">
			</div>
			<div class="col-12 col-lg-6">
				<div class="">
					<p class="fechas__parrafo fechas__parrafo--top"><img src="{{url('images/calendar.png')}}" class="fechas__calendario"><span class="fechas__titulo">31 de Julio</span></p>
      				<p class="noticia__subtitulo">La Fundación Wiese hace entrega de donativos a niños y adultos mayores del sur del Perú</p>
      				<p class="noticia__contenido">Tu Solidaridad Abriga, la campaña promovida por la Fundación Wiese, ADRA y Panamericana Televisión, entrega ropa de abrigo a niños...</p>
      				<a class="noticia__link" href="{{ url('/novedades/entrega-de-donativos') }}">Leer Noticia <span class="participa__separador-flecha"><i class="fas fa-angle-right"></i></span></a>
				</div>
			</div>
		</div>
		<div class="row col-12 col-xl-6 noticia">
			<div class="col-12 col-lg-6 noticia__imgen">
				<img class="participa__imagen-img img-fluid" src="{{ url('images/novedades/novedad1.jpg') }}">
			</div>
			<div class="col-12 col-lg-6">
				<div class="">
					<p class="fechas__parrafo fechas__parrafo--top"><img src="{{url('images/calendar.png')}}" class="fechas__calendario"><span class="fechas__titulo">28 de Junio</span></p>
      				<p class="noticia__subtitulo">Una nueva etapa, una nueva identidad visual</p>
      				<p class="noticia__contenido">El propósito de este cambio es alinear la imagen visual de la Fundación con la manera en que su comportamiento es percibido por sus distintos públicos de interés...</p>
      				<a class="noticia__link" href="{{ url('/novedades/nueva-identidad-visual') }}">Leer Noticia <span class="participa__separador-flecha"><i class="fas fa-angle-right"></i></span></a>
				</div>
			</div>
		</div>
		<div class="row col-12 col-xl-6 noticia">
			<div class="col-12 col-lg-6 noticia__imgen">
				<img class="participa__imagen-img img-fluid" src="{{ url('images/novedades/noticias_0.jpg') }}">
			</div>
			<div class="col-12 col-lg-6">
				<div class="">
					<p class="fechas__parrafo fechas__parrafo--top"><img src="{{url('images/calendar.png')}}" class="fechas__calendario"><span class="fechas__titulo">20 de Junio</span></p>
      				<p class="noticia__subtitulo">La Fundación Wiese recibe Premio IPAE a la Empresa en Cultura</p>
      				<p class="noticia__contenido">Ramón Barúa, Scotiabank y la Fundación Wiese son los ganadores de los premios IPAE 2017...</p>
      				<a class="noticia__link" href="{{ url('/novedades/premio-ipae') }}">Leer Noticia <span class="participa__separador-flecha"><i class="fas fa-angle-right"></i></span></a>
				</div>
			</div>
		</div>
		<div class="row col-12 col-xl-6 noticia">
			<div class="col-12 col-lg-6 noticia__imgen">
				<img class="participa__imagen-img img-fluid" src="{{ url('images/novedades/noticias_1.jpg') }}">
			</div>
			<div class="col-12 col-lg-6">
				<div class="">
					<p class="fechas__parrafo fechas__parrafo--top"><img src="{{url('images/calendar.png')}}" class="fechas__calendario"><span class="fechas__titulo">12 de Junio</span></p>
      				<p class="noticia__subtitulo">“TU SOLIDARIDAD ABRIGA” <br> Campaña contra la ola de frío en la Sierra</p>
      				<p class="noticia__contenido">La FUNDACIÓN WIESE en alianza con ADRA PERÚ dan inicio a campaña de ayuda humanitaria a nivel nacional...</p>
      				<a class="noticia__link" href="{{ url('/novedades/tu-solidaridad-abriga') }}">Leer Noticia <span class="participa__separador-flecha"><i class="fas fa-angle-right"></i></span></a>
				</div>
			</div>
		</div>
		<div class="row col-12 col-xl-6 noticia">
			<div class="col-12 col-lg-6 noticia__imgen">
				<img class="participa__imagen-img img-fluid" src="{{ url('images/novedades/novedad2.jpg') }}">
			</div>
			<div class="col-12 col-lg-6">
				<div class="">
					<p class="fechas__parrafo fechas__parrafo--top"><img src="{{url('images/calendar.png')}}" class="fechas__calendario"><span class="fechas__titulo">23 de Mayo</span></p>
      				<p class="noticia__subtitulo">La Fundación Wiese lanza fondo de crédito educativo para carreras técnicas en alianza con IPFE</p>
      				<p class="noticia__contenido">Esta iniciativa aporta a reducir la tasa de deserción educativa y cubrir la demanda insatisfecha de profesionales ...</p>
      				<a class="noticia__link" href="{{ url('/novedades/fondo-credito-educativo') }}">Leer Noticia <span class="participa__separador-flecha"><i class="fas fa-angle-right"></i></span></a>
				</div>
			</div>
		</div>
		<div class="row col-12 col-xl-6 noticia">
			<div class="col-12 col-lg-6 noticia__imgen">
				<img class="participa__imagen-img img-fluid" src="{{ url('images/novedades/noticia-fundacion3.jpg') }}">
			</div>
			<div class="col-12 col-lg-6">
				<div class="">
					<p class="fechas__parrafo fechas__parrafo--top"><img src="{{url('images/calendar.png')}}" class="fechas__calendario"><span class="fechas__titulo">15 de Mayo</span></p>
      				<p class="noticia__subtitulo">Fundación Wiese comienza con los Talleres de Padres de Familia: “Te acompaño en tu desarrollo”</p>
      				<p class="noticia__contenido">Desde abril del 2016 la <strong>Fundación Wiese</strong> inició la segunda fase del Proyecto Calidad Educativa ...</p>
      				<a class="noticia__link" href="{{ url('/novedades/te-acompano-en-tu-desarrollo') }}">Leer Noticia <span class="participa__separador-flecha"><i class="fas fa-angle-right"></i></span></a>
				</div>
			</div>
		</div>
		<div class="row col-12 col-xl-6 noticia">
			<div class="col-12 col-lg-6 noticia__imgen">
				<img class="participa__imagen-img img-fluid" src="{{ url('images/novedades/noticia-fundacion2.jpg') }}">
			</div>
			<div class="col-12 col-lg-6">
				<div class="">
					<p class="fechas__parrafo fechas__parrafo--top"><img src="{{url('images/calendar.png')}}" class="fechas__calendario"><span class="fechas__titulo">12 de Febrero</span></p>
      				<p class="noticia__subtitulo">El Ministerio de Cultura, el GORE e Inmuebles Panamericana firman el convenio OXI en El Brujo</p>
      				<p class="noticia__contenido">El Brujo es un yacimiento arqueológico fascinante, un lugar que registra evidencias de ocupaciones humanas ...</p>
      				<a class="noticia__link" href="{{ url('/novedades/obras-por-impuestos-el-brujo') }}">Leer Noticia <span class="participa__separador-flecha"><i class="fas fa-angle-right"></i></span></a>
				</div>
			</div>
		</div>
	</div>
</div>

@include('frontend.partials.newsletter')
@endsection

@section('scripts')

@endsection
