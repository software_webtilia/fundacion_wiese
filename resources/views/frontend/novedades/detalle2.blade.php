@extends('frontend.layouts.layout')
@section('title', 'Fundación Wiese')

@section('content')
<div class="participa__banner d-flex align-items-center">
	<img src="{{ url('images/novedades/fondo-credito-educativo.jpg') }}" class="img-fluid" alt="">
</div>


<div class="container-fluid">
	<div class="row">
		<div class="col-md-2 pl-0">
				<img class="mision__imagen-w img-fluid" src="{{url('images/w-izqquierda.png')}}">
		</div>
		<div class="col-md-8">
			<div class="participa__titulo-box text-center">

				<h1 class="participa__title participa__title--color-gris ">La Fundación Wiese lanza fondo de crédito educativo para carreras técnicas en alianza con IPFE</h1>
				<p class="fechas__parrafo fechas__parrafo--detalle"><img src="{{url('images/calendar.png')}}" class="fechas__calendario"><span class="fechas__titulo">23 de Mayo</span></p>
				<p class="participa__subtitle participa__subtitle--detalle">•	Esta iniciativa aporta a reducir la tasa de deserción educativa y cubrir la demanda insatisfecha de profesionales técnicos calificados que requiere el país.<br><br>
				•	La Fundación Wiese, en línea con su labor en favor de la educación, ha creado un fondo revolvente de US$ 250,000 para financiar estudios en carreras técnicas, mediante un programa social de crédito educativo.<br><br>
				•	IPFE es la institución seleccionada para administrar el mencionado fondo, debido a su experiencia en gestión de oportunidades educativas que le permite mantener tasas de morosidad menores al 3%</p>
			</div>
		</div>
		<div class="col-md-2">
		</div>
	</div>
</div>

<div class="container">
	<div class="detalle">
		<!-- <img class="detalle__imagen-img img-fluid" src="{{ url('images/novedades/detalle1.jpg') }}"> -->
		<div class="row">
			<div class="col-12">
				<p class="detalle__parrafo">“Creemos en la solidaridad que es capaz de transformar el destino de las personas y trabajamos en favor de la igualdad en el acceso a las oportunidades para todos los peruanos; consideramos que este Programa de Crédito de Estudios es una manera concreta de demostrarlo.” Augusto Wiese Moreyra, miembro de la Junta de Administración de la Fundación Wiese, señaló que esta institución ha destinado un fondo revolvente de US$250,000 para financiar, la continuidad y culminación satisfactoria de los estudios de jóvenes de escasos recursos económicos que cursan estudios profesionales técnicos. </p>

				<p class="detalle__parrafo">Además de ofrecer financiamiento a una tasa de interés social, el Programa de Crédito de Estudios Fundación Wiese – IPFE, potencia las habilidades socioemocionales de sus beneficiarios, a través de un componente de acompañamiento. De esta manera, se reducen las tasas de deserción y morosidad.</p>

				<p class="detalle__parrafo">Como parte de las acciones del programa, se ha lanzado la primera convocatoria, que en primera instancia se encuentra orientada a estudiantes del SENATI, en todas sus sedes a nivel nacional, pero se espera ampliar el beneficio a otras especialidades e institutos superiores tecnológicos. </p>

				<p class="detalle__parrafo">El Instituto Peruano de Fomento Educativo (IPFE), que actualmente administra un fondo de créditos educativos en el que aportan más de 90 empresas peruanas -con una tasa de morosidad menor al 3%-, es quien se encargará de la gestión del programa. Luis Augusto Ducassi Wiese, presidente del IPFE, anunció que la meta de esta alianza es reducir la deserción estudiantil, en especial a nivel técnico, que es donde existe un mayor desequilibrio debido a que existen altas tasas de deserción educativa y una gran demanda insatisfecha en el mercado laboral de profesionales técnicos calificados.</p>

				<p class="detalle__parrafo"><strong>Sobre la Fundación Wiese</strong><br>La Fundación Wiese, creada en 1960, tiene dentro de sus fines la realización del bien individual y colectivo, mediante el apoyo a la educación, la salud, la cultura y la investigación científica. En este marco, sus acciones y decisiones se orientan a movilizar personas e instituciones para lograr que El Perú del Futuro sea forjado por mejores ciudadanos.</p>

				<p class="detalle__parrafo"><strong>Sobre el IPFE</strong><br>El Instituto Peruano de Fomento Educativo (IPFE) es una asociación civil sin fines de lucro, fundada en 1962, con el propósito de colaborar en el acceso a la educación de miles de personas que quieran superarse a nivel personal y profesional, generando oportunidades a partir de la administración de fondos para becas y créditos.</p>

				<p class="detalle__parrafo"><strong>PREGUNTAS FRECUENTES</strong><br><br>
				<strong>1.	¿Qué conceptos cubre este programa?</strong><br>
				El programa financia el pago de la matrícula y las pensiones académicas.</p>

				<p class="detalle__parrafo"><strong>2.	¿Quiénes pueden postular?</strong><br>
				Pueden postular alumnos a partir del IV ciclo de las carreras técnicas de SENATI, siempre que hayan demostrado buen rendimiento académico, y que los responsables de su crédito educativo pueda presentar sustento de ingresos y mantengan un buen reporte ante las centrales de riesgo.</p>

				<p class="detalle__parrafo"><strong>3.	¿En qué consiste el acompañamiento?</strong><br>
				El acompañamiento busca brindar soporte a los beneficiarios del Programa y favorecer el desarrollo de sus competencias y habilidades socioemocionales, con el objetivo de contribuir a que puedan culminar sus estudios e se inserten exitosamente en el mercado laboral. Para ello, el Programa les ofrece talleres y les hace seguimiento, mediante una plataforma virtual desarrollada exclusivamente con este propósito.</p>

			</div>
		</div>
	</div>
	<!--hr class="detalle__hr"-->
</div>

<!--div class="container">
	<h2 class="detalle__titulo-novedades ">Más Novedades</h2>
	<div class="row">
		<div class="row col-12 col-xl-6 noticia">
			<div class="col-12 col-lg-6 noticia__imgen">
				<img class="participa__imagen-img img-fluid" src="{{ url('images/novedades/noticia9.jpg') }}">
			</div>
			<div class="col-12 col-lg-6">
				<div class="">
					<p class="fechas__parrafo fechas__parrafo--top"><img src="{{url('images/calendar.png')}}" class="fechas__calendario"><span class="fechas__titulo">28 de Junio</span></p>
      				<p class="noticia__subtitulo">Una nueva etapa, una nueva identidad visual</p>
      				<p class="noticia__contenido">El propósito de este cambio es alinear la imagen visual de la Fundación con la manera en que su comportamiento es percibido por sus distintos públicos de interés...</p>
      				<a class="noticia__link" href="{{ url('/novedades/nueva-identidad-visual') }}">Leer Noticia <span class="participa__separador-flecha"><i class="fas fa-angle-right"></i></span></a>
				</div>
			</div>
		</div>
		<div class="row col-12 col-xl-6 noticia">
			<div class="col-12 col-lg-6 noticia__imgen">
				<img class="participa__imagen-img img-fluid" src="{{ url('images/novedades/noticia10.jpg') }}">
			</div>
			<div class="col-12 col-lg-6">
				<div class="">
					<p class="fechas__parrafo fechas__parrafo--top"><img src="{{url('images/calendar.png')}}" class="fechas__calendario"><span class="fechas__titulo">2 de Febrero</span></p>
      				<p class="noticia__subtitulo">El Ministerio de Cultura, el Gobierno Regional de La Libertad e Inmuebles Panamericana firman el convenio de obras por impuestos a realizarse en El Brujo</p>
      				<p class="noticia__contenido">El Brujo es un yacimiento arqueológico fascinante, un lugar que registra evidencias de ocupaciones humanas de 14,000 años de antigüedad...</p>
      				<a class="noticia__link" href="{{ url('/novedades/obras-por-impuestos-el-brujo') }}">Leer Noticia <span class="participa__separador-flecha"><i class="fas fa-angle-right"></i></span></a>
				</div>
			</div>
		</div>
	</div>
</div-->
@include('frontend.partials.newsletter')
@endsection

@section('scripts')

@endsection
