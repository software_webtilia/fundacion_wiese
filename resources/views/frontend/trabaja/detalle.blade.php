@extends('frontend.layouts.layout')
@section('title', 'Fundación Wiese')
@section('content')
<div class="d-flex align-items-center">
	<img src="{{ url('images/trabaja/banner_bolsa_de_trabajo.jpg') }}" class="img-fluid" alt="">
</div>
<div class="container-fluid">
	<div class="row">
		<div class="col-md-2 pl-0">
				<img class="mision__imagen-w img-fluid" src="{{url('images/proyectos-dirige/w-lila.jpg')}}">
		</div>
		<div class="col-md-8">
			<div class="participa__titulo-box text-center">
				<h1 class="participa__title participa__title--color-lila">Bolsa de trabajo</h1>
				<p class="participa__subtitle">Estamos buscando colaboradores que compartan nuestro compromiso promoviendo y trabajando los valores de Identidad, Equidad y Solidaridad; para forjar el Perú del futuro. ¡Únete a nuestro equipo!</p>
			</div>
		</div>
		<div class="col-md-2">
		</div>
	</div>
</div>
@if(count($aviso) > 0)
@foreach($aviso as $aviso)
<div class="container trabaja-detalle-container">
  <div class="row">
    <div class="col-md-6">
      <h1 class="trabaja-detalle__posicion">POSICIÓN: {{ $aviso->titulo }}</h1>
      <p class="trabaja-detalle__programa">{{ $aviso->subtitulo }}</p>
			  <i class="fas fa-map-marker-alt trabaja-detalle__icon pt-2"> <span class="trabaja-detalle__local">{{ $aviso->lugar }}</span> </i>
	  
	  <div class="trabaja-detalle__fecha">
	  <p><strong>Publicado:</strong> {{ substr($aviso->fecha_inicio, 8, 2)}}-{{substr($aviso->fecha_inicio, 5, 2)}}-{{substr($aviso->fecha_inicio, 0, 4)}}</p>
	  <p><strong>Oferta vigente hasta:</strong> {{ substr($aviso->fecha_fin, 8, 2)}}-{{substr($aviso->fecha_fin, 5, 2)}}-{{substr($aviso->fecha_fin, 0, 4)}}</p>
	  </div>
    </div>
    <div class="col-md-6 text-right">
      <a href="{{url('bolsa-de-trabajo')}}" class="trabaja-detalle__link"> < Bolsa de trabajo</a> <br>
    </div>
  </div>
  <hr></hr>
  <div class="row">
    <div class="col-md-12">
      {!! $aviso->descripcion !!}
    </div>
    <div class="col-md-12 text-center mt-5 pb-5">
        <button type="submit" class="form-group  btn trabaja-detalle__submit" data-toggle="modal" data-target="#registroModal">Postula Ahora</button>
    </div>
  </div>
</div>
@endforeach
@endif
<!-- Modal -->
<div class="modal fade" id="registroModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-content__cinta">
        <img src="{{ url('images/trabaja/cinta-form.jpg') }}" alt="">
      </div>
      <div class="modal-content__cerrar">
        <button type="button" class="close modal-content__boton-cerrar" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Déjanos tus datos para poder contactarte</h5>
      </div>
      <div class="modal-header2">
        <h1 class="modal-title2">Gracias</h1> <br>
        <h3 class="modal-title3">Tu información ha sido enviada correctamente.</h3>
      </div>
      <div class="modal-body">
				<form id="formTrabaja" name="formTrabaja" class="" action="{{url('bolsa-de-trabajo')}}">
        <span class="modal-body__mensaje">Por favor, ingresa tus datos en este formulario y nos pondremos en contacto contigo.</span>
				<span class="errorLabel"></span>
        <div class="container-fluid form-container">
          <div class="row">
							<div class="col-md-6 pt-3">
								<input type="text" class="form-control" name="inputNombres" id="inputNombres" placeholder="Nombres" value="">
								<label for="inputNombres" generated="true" class="error"></label>
							</div>
							<div class="col-md-6 pt-3">
								<input type="text" class="form-control" name="inputApellidos" id="inputApellidos" placeholder="Apellidos" value="">
								<label for="inputApellidos" generated="true" class="error"></label>
							</div>
							<div class="col-md-6 pt-3">
								<input type="text" class="form-control" name="inputTelefono" id="inputTelefono" placeholder="Teléfono" value="">
								<label for="inputTelefono" generated="true" class="error"></label>
							</div>
							<div class="col-md-6 pt-3">
								<input type="text" class="form-control" name="inputCorreo" id="inputCorreo" placeholder="Correo electrónico" name="" value="">
								<label for="inputCorreo" generated="true" class="error"></label>
							</div>
							<div class="col-md-6 pt-3">
								<select class="form-control" id="departamento" name="departamento" >
									<option value="" disabled="" selected="">Región</option>
									  <option value="" selected>Región</option>
										@foreach($zone as $valor)
			                <option value="{{$valor->departamento}}">{{$valor->departamento}}</option>
			              @endforeach
								</select>
								<label for="departamento" generated="true" class="error"></label>
							</div>
							<div class="col-md-6 pt-3">
								<select class="form-control" id="provincia" name="provincia" disabled>
									<option value="" selected>Provincia</option>
								</select>
								<label for="provincia" generated="true" class="error"></label>
							</div>
							<div class="col-md-6 pt-3">
								<input type="text" class="form-control" name="inputProfesion" id="inputProfesion" placeholder="Profesión" value="">
								<label for="inputProfesion" generated="true" class="error"></label>
							</div>
							<div class="col-md-6 pt-3">
								<div class="file__label">
										Adjunta tu CV <br> (pdf o word. Max. 1MB)
								</div>
								<div class="file__container">
										<input name="inputCv" id="inputCv" type="file" class="form-control file">
										<span class="errorLabel"></span>
								</div>
								<label for="inputCv" generated="true" class="error"></label>
							</div>
						</div>
        </div>
      </div>
      <div class="modal-footer pt-4 pb-5">
        <button type="submit" class="form-group trabaja__envia  btn trabaja-detalle__submit trabaja-detalle__enviar">Envía tus datos</button>
				<button type="button" class="form-group trabaja__envia  btn trabaja-detalle__aceptar">Aceptar</button>
      </div>
			</form>
    </div>
  </div>
</div>
@include('frontend.partials.newsletter')
@endsection
@section('scripts')
<script type="text/javascript">
var URL_SITE='{{url('')}}';
</script>
@endsection
