@extends('frontend.layouts.layout')

@section('title', 'Fundación Wiese')


@section('content')

<div class="d-flex align-items-center">

	<img src="{{ url('images/trabaja/banner_bolsa_de_trabajo.jpg') }}" class="img-fluid" alt="">

</div>



<div class="container-fluid">

	<div class="row">

		<div class="col-md-2 pl-0">

				<img class="mision__imagen-w img-fluid" src="{{url('images/proyectos-dirige/w-lila.jpg')}}">

		</div>

		<div class="col-md-8">

			<div class="participa__titulo-box text-center">

				<h1 class="participa__title participa__title--color-lila">Bolsa de trabajo</h1>

				<p class="participa__subtitle">Trabajamos para promover los valores de Identidad, Equidad y Solidaridad; y así contribuimos a que mejores ciudadanos forjen el Perú del futuro. ¡Únete a nuestro equipo!</p>

			</div>

		</div>

		<div class="col-md-2">

		</div>

	</div>

</div>



<div class="container trabaja-container">



  <!--div class="participa__filters">

		<p class="participa__filters-p"> Filtrar proyectos por:</p>

		<form id="formFilter" name="formFilter" class="participa__form row justify-content-center align-items-center" action="{{ url('proyectos-iniciativas#filters') }}" method="get">

      <div class="form-group col-5 col-md-4 pl-0">

				<select class="form-control" id="programa" name="programa">

					<option value="" disabled="" selected="">Programa</option>

					@foreach($programs as $program)

						<option value="{{$program->id}}" @if($programa && $programa == $program->id) selected @endif	>{{$program->nombre}}</option>

					@endforeach

				</select>

      </div>



      <div class="col-2 col-md-4 newsletter__submit-container pl-0 pr-0">

        <button type="button" class="form-group  btn participa__submit">Filtrar</button>

      </div>

    </form>

	</div-->



  <div class="row">


    <div class="col-md-12">

      <!--hr></hr-->

    </div>



		@if(count($advertisements) > 0)

		@foreach($advertisements as $advertisement)

    <div class="col-md-6">

      <div class="card trabaja-card">

        <h2 class="trabaja-card__h2">{{ $advertisement->titulo }}</h2>

        <h3 class="participa__programa participa__programa--lila">{{ $advertisement->subtitulo }}</h3>

        <p class="participa__item-descripcion mt-1">{{ $advertisement->sumilla }}</p>

        <i class="fas fa-map-marker-alt trabaja-card__icon pt-2"> <span class="trabaja-card__local">{{ $advertisement->lugar }}</span> </i>

		<div class="trabaja-detalle__fecha">
	  	<p><strong>Publicado:</strong> {{ substr($advertisement->fecha_inicio, 8, 2)}}-{{substr($advertisement->fecha_inicio, 5, 2)}}-{{substr($advertisement->fecha_inicio, 0, 4)}}</p>
	  	<p><strong>Oferta vigente hasta:</strong> {{ substr($advertisement->fecha_fin, 8, 2)}}-{{substr($advertisement->fecha_fin, 5, 2)}}-{{substr($advertisement->fecha_fin, 0, 4)}}</p>
		</div>
					<div class="proyectos__boton trabaja__boton pt-5">

						<a href="{{url('/bolsa-de-trabajo/' . $advertisement->id . '/'. str_slug($advertisement->titulo))}}" class="trabaja__link">ver más

              <span class="proyectos__separador-flecha">

              <i class="fas fa-angle-right"></i></span>

            </a>

					</div>



      </div>

		</div>

		@endforeach

  	@else

			<div class="col-md-12">

				<h4>Lo sentimos, no hay avisos disponibles por el momento.</h4>

			</div>

		@endif





  </div>


	<div class="row">
 	 <div class="col-md-12">
		 <br><br>
 		 <h1>Únete a nuestro equipo</h1>
		 <h5>Tu futuro esta aquí, dejanos tus datos y forma parte de una gran causa.</h5>
 	 </div>
 	 <div class="col-md-12 mt-5 pb-5">
 			 <button type="submit" class="form-group  btn trabaja-detalle__submit" data-toggle="modal" data-target="#postulaModal">Súmate a FW</button>
 	 </div>
  </div>

</div>


<div class="modal fade" id="postulaModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-content__cinta">
        <img src="{{ url('images/trabaja/cinta-form.jpg') }}" alt="">
      </div>
      <div class="modal-content__cerrar">
        <button type="button" class="close modal-content__boton-cerrar" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-header">
        <h2 class="modal-title" id="exampleModalLabel">Conéctese con nosotros</h2>
      </div>

			<span class="modal-body__mensaje text-center">Déjanos tus datos para poder contactarte.</span>
      <br><br>
      <div class="modal-header2">
        <h1 class="modal-title2">Gracias</h1> <br>
        <h3 class="modal-title3">Tu información ha sido enviada correctamente.</h3>
      </div>
      <div class="modal-body">
				<form id="formPostula" name="formPostula" class="" action="{{url('postulacion')}}">
        <span class="modal-body__mensaje"></span>
				<span class="errorLabel"></span>
        <div class="container-fluid form-container">
          <div class="row">
							<div class="col-md-6 pt-3">
								<input type="text" class="form-control" name="inputNombres" id="inputNombres" placeholder="Nombres" value="">
								<label for="inputNombres" generated="true" class="error"></label>
							</div>
							<div class="col-md-6 pt-3">
								<input type="text" class="form-control" name="inputApellidos" id="inputApellidos" placeholder="Apellidos" value="">
								<label for="inputApellidos" generated="true" class="error"></label>
							</div>
							<div class="col-md-6 pt-3">
								<input type="text" class="form-control" name="inputProfesion" id="inputProfesion" placeholder="Profesión" value="">
								<label for="inputProfesion" generated="true" class="error"></label>
							</div>
							<div class="col-md-6 pt-3">
								<input type="text" class="form-control" name="inputDireccion" id="inputDireccion" placeholder="Dirección" value="">
								<label for="inputDireccion" generated="true" class="error"></label>
							</div>
							<div class="col-md-6 pt-3">
								<input type="text" class="form-control" name="inputTelefono" id="inputTelefono" placeholder="Teléfono" value="">
								<label for="inputTelefono" generated="true" class="error"></label>
							</div>
							<div class="col-md-6 pt-3">
								<input type="text" class="form-control" name="inputCorreo" id="inputCorreo" placeholder="Correo electrónico" name="" value="">
								<label for="inputCorreo" generated="true" class="error"></label>
							</div>
							<div class="col-md-6 pt-3">
								<select class="form-control" id="area" name="area_id" >
									<option value="" disabled="" selected="">Área de interés</option>
									  <option value="" selected>Área de interés</option>
										  @foreach($areas as $a)
										  <option value="{{ $a->id }}">{{ $a->nombre }}</option>
											@endforeach
								</select>
								<label for="area" generated="true" class="error"></label>
							</div>
							<div class="col-md-6 pt-3">
								<div class="file__label">
										Adjunta tu CV <br> (pdf o word. Max. 1MB)
								</div>
								<div class="file__container">
										<input name="inputCv" id="inputCv" type="file" class="form-control file">
										<span class="errorLabel"></span>
								</div>
								<label for="inputCv" generated="true" class="error"></label>
							</div>
						</div>
        </div>
      </div>
      <div class="modal-footer pt-4 pb-5">
        <button type="submit" class="form-group trabaja__envia  btn trabaja-detalle__submit trabaja-detalle__enviar">Envía tus datos</button>
				<button type="button" class="form-group trabaja__envia  btn trabaja-detalle__aceptar">Aceptar</button>
      </div>
			</form>
    </div>
  </div>
</div>
@include('frontend.partials.newsletter')



@endsection



@section('scripts')



<script type="text/javascript">
var URL_SITE='{{url('')}}';

	$('.slider').slick({

	  	dots: true,

	  	infinite: true,

	  	speed: 300,

	  	slidesToShow: 4,

	  	slidesToScroll: 1,

	  	autoplay: true,

  		autoplaySpeed: 2000,

  		centerMode: true,

			responsive: [

		    {

		      breakpoint: 1024,

		      settings: {

		        slidesToShow: 3,

		        slidesToScroll: 3,

						dots: true

		      }

		    },

		    {

		      breakpoint: 700,

		      settings: {

		        slidesToShow: 2,

		        slidesToScroll: 2,

						dots: false

		      }

		    },

		    {

		      breakpoint: 480,

		      settings: {

		        slidesToShow: 1,

		        slidesToScroll: 1,

						dots: false

		      }

		    }

	  ]

	});

</script>

@endsection
