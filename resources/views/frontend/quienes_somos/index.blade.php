@extends('frontend.layouts.layout')
@section('title', 'Fundación Wiese')
@section('content')
<div class="d-flex align-items-center">
	<img src="{{ url('images/quienes-somos/banner_quienes_somos.jpg') }}" class="img-fluid" alt="">
</div>
<div class="container-fluid">
	<div class="row">
		<div class="col-md-2 pl-0">
				<img class="mision__imagen-w img-fluid" src="{{url('images/quienes-somos/w-naranja.png')}}">
		</div>
		<div class="col-md-8">
			<div class="participa__titulo-box text-center">
				<h1 class="participa__title">¿Quiénes somos?</h1>
				<p class="participa__subtitle">Somos una institución sin fin de lucro que promueve (gestiona, lidera y articula) proyectos educativos, culturales e innovadores de alto impacto social, a través de alianzas estratégicas de largo plazo y un equipo de trabajo comprometido y competente.</p>
			</div>
		</div>
		<div class="col-md-2">
		</div>
	</div>
</div>
<div class="container nota">
  <div class="row">
    <div class="col-6 col-md-3 nota__imagen">
      <img class="img-fluid" src="{{url('images/quienes-somos/augusto-wiese.jpg')}}">
    </div>
		<div class="col-6 col-md-9 text-center nota__texto">
			<div class="nota__contenedor">
				<div class="nota__comillas">
						<img src="{{url('images/comillas.png')}}"  alt="" class="nota__comillas-img">
				</div>
				<div class="nota__palabras">
						Por el presente instrumento, yo, Augusto N. Wiese instituyo una Fundación que llevará mi nombre y que tendrá por fin realizar el bien individual y colectivo.
				</div>
			</div>
			<div class="nota__augusto d-none d-sm-block">
				<h2 class="nota__h2">Don Augusto N. Wiese Eslava</h2>
				<p class="nota__parrafo">10 de agosto de 1960</p>
				<p class="nota__parrafo2">Conoce su <a href="{{url('archivos/biografia_don_augusto_wiese_eslava.pdf')}}" target="_blank" class="nota__link">historia</a> y <a href="{{url('archivos/estatuto.pdf')}}" target="_blank" class="nota__link">estatuto</a></p>
			</div>
    </div>
		<div class="col-12 d-block d-sm-none nota__texto">
			<div class="nota__augusto">
				<h2 class="nota__h2">Don Augusto N. Wiese Eslava</h2>
				<p class="nota__parrafo">10 de agosto de 1960</p>
				<p class="nota__parrafo2">Conoce su <a href="{{url('archivos/biografia_don_augusto_wiese_eslava.pdf')}}" target="_blank" class="nota__link">historia</a> y <a href="{{url('archivos/estatuto.pdf')}}" target="_blank" class="nota__link">estatuto</a></p>
			</div>
		</div>
  </div>
</div>
<div class="container mision">
  <div class="row">
    <div class="mt-4 col-12 mision__texto">
      <p class="mision__parrafo wow fadeIn">La Fundación Wiese trabaja para que</p>
      <h2 class="mision__h2 wow fadeIn" data-wow-delay="0.5s">Mejores ciudadanos forjen el Perú del futuro</h2>
      <p class="mision__parrafo wow fadeIn" data-wow-delay="0.8s">Nuestra Misión</p>
      <h2 class="mision__h2 wow fadeIn" data-wow-delay="1s">Movilizamos personas e instituciones para transformar el Perú</h2>
      <p class="mision__parrafo wow fadeIn" data-wow-delay="1.5s">Nuestros Valores</p>
    </div>
  </div>
  <div class="row mt-4 mb-4">
    <div class="col-12 col-md-4">
      <div class="bx-mision pb-4">
        <div class="bx-mision__imagen">
          <img src="images/icono-identidad-3.png" alt="">
        </div>
        <div class="valores">
          <h4 class="valores__titulo">IDENTIDAD</h4>
          <p class="valores__contenido">La conciencia histórica que abraza nuestra diversidad, revalora a los peruanos y es fuente de desarrollo.</p>
        </div>
      </div>
    </div>
    <div class="col-12 col-md-4">
      <div class="bx-mision pb-4">
        <div class="bx-mision__imagen">
          <img src="images/icono-solidaridad-3.png" alt="">
        </div>
        <div class="valores">
          <h4 class="valores__titulo valores__titulo--color-lila">SOLIDARIDAD</h4>
          <p class="valores__contenido">La intervención capaz de transformar el destino de las personas y comunidades más vulnerables.</p>
        </div>
      </div>
    </div>
    <div class="col-12 col-md-4">
      <div class="bx-mision pb-2">
        <div class="bx-mision__imagen">
          <img src="images/icono-equidad-3.png" alt="">
        </div>
        <div class="valores">
          <h4 class="valores__titulo valores__titulo--color-naranja">EQUIDAD</h4>
          <p class="valores__contenido">La igualdad en la valoración de cada persona y el compromiso para otorgarles igualdad en el acceso a las oportunidades.</p>
        </div>
      </div>
    </div>
	</div>
</div>
<div class="junta">
	<div class="container-fluid objetivos__container-texto">
	  	<h2 class="junta__titulo">Junta de Administración</h2>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h2 class="junta__subtitulo">Presidente y vicepresidente</h2>
			</div>
			<div class="col-6 col-md-6 col-lg-3 mt-2">
				<img class="img-fluid junta__img" src="{{url('images/quienes-somos/ClotildeWieseOsma.jpg')}}">
				<div class="junta__box">
					<h3 class="junta__nombre">Clotilde Wiese de la Puente</h3>
					<p class="junta__cargo">Presidenta</p>
				</div>
			</div>
			<div class="col-6 col-md-6 col-lg-3 mt-2">
				<img class="img-fluid junta__img" src="{{url('images/quienes-somos/AugustoWiese.jpg')}}">
				<div class="junta__box">
					<h3 class="junta__nombre">Augusto Wiese Moreyra</h3>
					<p class="junta__cargo">Vicepresidente</p>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<h2 class="junta__subtitulo">Miembros de la Junta</h2>
			</div>
			<div class="col-6 col-md-6 col-lg-3 mt-2">
				<img class="img-fluid junta__img" src="{{url('images/quienes-somos/MariluWiese.jpg')}}">
				<div class="junta__box">
					<h3 class="junta__nombre">Marilú Wiese Moreyra</h3>
					<!--p class="junta__cargo">Directora</p-->
				</div>
			</div>
			<div class="col-6 col-md-6 col-lg-3 mt-2">
				<img class="img-fluid junta__img" src="{{url('images/quienes-somos/staff-mujer.jpg')}}">
				<div class="junta__box">
					<h3 class="junta__nombre">Silvia Wiese de Osma</h3>
					<!--*p class="junta__cargo">Directora</p-->
				</div>
			</div>
			<div class="col-6 col-md-6 col-lg-3 mt-2">
				<img class="img-fluid junta__img" src="{{url('images/quienes-somos/staff-mujer.jpg')}}">
				<div class="junta__box">
					<h3 class="junta__nombre">Caridad de la Puente Wiese</h3>
					<!--p class="junta__cargo">Directora</p-->
				</div>
			</div>
			<div class="col-6 col-md-6 col-lg-3 mt-2">
				<img class="img-fluid junta__img" src="{{url('images/quienes-somos/director3.jpg')}}">
				<div class="junta__box">
					<h3 class="junta__nombre">Alejandro Harmsen Andress</h3>
					<!--p class="junta__cargo">Director</p-->
				</div>
			</div>
			<div class="col-6 col-md-6 col-lg-3 mt-2">
				<img class="img-fluid junta__img" src="{{url('images/quienes-somos/juan-carlos-cuglievan.jpg')}}">
				<div class="junta__box">
					<h3 class="junta__nombre">Juan Carlos Cuglievan Balarezo</h3>
					<!--p class="junta__cargo">Director</p-->
				</div>
			</div>
			<div class="col-6 col-md-6 col-lg-3 mt-2">
				<img class="img-fluid junta__img" src="{{url('images/quienes-somos/staff-hombre.jpg')}}">
				<div class="junta__box">
					<h3 class="junta__nombre">Luis Augusto Ducassi Wiese</h3>
					<!--p class="junta__cargo">Director</p-->
				</div>
			</div>
			<div class="col-6 col-md-6 col-lg-3 mt-2">
				<img class="img-fluid junta__img" src="{{url('images/quienes-somos/staff-hombre.jpg')}}">
				<div class="junta__box">
					<h3 class="junta__nombre">Gonzalo de la Puente Wiese</h3>
					<!--p class="junta__cargo">Director</p-->
				</div>
			</div>
			<div class="col-6 col-md-6 col-lg-3 mt-2">
				<img class="img-fluid junta__img" src="{{url('images/quienes-somos/staff-mujer.jpg')}}">
				<div class="junta__box">
					<h3 class="junta__nombre">Lorena Wiese Moreyra</h3>
					<!--p class="junta__cargo">Directora</p-->
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<h2 class="junta__subtitulo">Gerente General</h2>
			</div>
			<div class="col-6 col-md-6 col-lg-3 mt-2">
				<img class="img-fluid junta__img" src="{{url('images/quienes-somos/gerente.jpg')}}">
				<div class="junta__box">
					<h3 class="junta__nombre">Ingrid Claudet Lascosque</h3>
					<p class="junta__cargo">Gerente General</p>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="aliados">
	<h2 class="aliados__titulo">Nuestros aliados</h2>
	<div class="container aliados__container">
		<div class="slider">
			<div class="item aliados__img">
			    <img class="img-fluid" src="{{url('images/quienes-somos/aliado1.png')}}">
			</div>
			<div class="item aliados__img">
			    <img class="img-fluid" src="{{url('images/quienes-somos/aliado2.png')}}">
			</div>
			<div class="item aliados__img">
			   <img class="img-fluid" src="{{url('images/quienes-somos/aliado3.jpg')}}">
			</div>
			<!-- <div class="item aliados__img">
			    <img class="img-fluid" src="{{url('images/quienes-somos/aliado4.png')}}">
			</div> -->
			<div class="item aliados__img">
			    <img class="img-fluid" src="{{url('images/quienes-somos/aliado5.jpg')}}">
			</div>
			<div class="item aliados__img">
			    <img class="img-fluid" src="{{url('images/quienes-somos/aliado12.jpg')}}">
			</div>
			<div class="item aliados__img">
			    <img class="img-fluid" src="{{url('images/quienes-somos/aliado6.jpg')}}">
			</div>
			<div class="item aliados__img">
			    <img class="img-fluid" src="{{url('images/quienes-somos/aliado7.jpg')}}">
			</div>
			<div class="item aliados__img">
			    <img class="img-fluid" src="{{url('images/quienes-somos/aliado8.jpg')}}">
			</div>
			<div class="item aliados__img">
			    <img class="img-fluid" src="{{url('images/quienes-somos/aliado9.jpg')}}">
			</div>
			<div class="item aliados__img">
			    <img class="img-fluid" src="{{url('images/quienes-somos/aliado10.jpg')}}">
			</div>
			<div class="item aliados__img">
			    <img class="img-fluid" src="{{url('images/quienes-somos/aliado11.jpg')}}">
			</div>
			<div class="item aliados__img">
			    <img class="img-fluid" src="{{url('images/quienes-somos/aliado12.png')}}">
			</div>
		</div>
	</div>
</div>
@include('frontend.partials.newsletter')
@endsection
@section('scripts')
<script type="text/javascript">
	$('.slider').slick({
	  	dots: true,
	  	infinite: true,
	  	speed: 300,
	  	slidesToShow: 4,
	  	slidesToScroll: 1,
	  	autoplay: true,
  		autoplaySpeed: 2000,
  		centerMode: true,
			responsive: [
		    {
		      breakpoint: 1024,
		      settings: {
		        slidesToShow: 3,
		        slidesToScroll: 3,
						dots: true
		      }
		    },
		    {
		      breakpoint: 700,
		      settings: {
		        slidesToShow: 2,
		        slidesToScroll: 2,
						dots: false
		      }
		    },
		    {
		      breakpoint: 480,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1,
						dots: false
		      }
		    }
	  ]
	});
</script>
@endsection
