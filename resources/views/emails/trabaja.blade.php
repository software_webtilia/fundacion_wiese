<h4>Datos de Postulante</h4>
<p><b>ID:</b> {{ $data->id }}</p>
<p><b>AVISO:</b> {{ $data->titulo }}</p>
<hr>

<p><b>Nombres:</b> {{ $data->nombres }}</p>
<p><b>Apellidos:</b> {{ $data->apellidos }}</p>
<p><b>Teléfono:</b> {{ $data->telefono }}</p>
<p><b>Email:</b> {{ $data->correo }}</p>
<p><b>Región:</b> {{ $data->departamento }}</p>
<p><b>Provincia:</b> {{ $data->provincia }}</p>
<p><b>Profesión:</b> {{ $data->profesion }}</p>
<p><b>CV:</b> <a href="{{ asset('storage/archivos/postulantes/') }}/{{ $data->cv }}" target="_blank">{{ $data->cv }}</a></p>
