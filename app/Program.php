<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Program extends Model
{
  protected $table = 'programs';

  protected $fillable = ['id', 'nombre', 'descripcion', 'orden', 'estado'];

}
