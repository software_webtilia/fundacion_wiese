<?php
namespace App\Helpers\Backend;
use Illuminate\Support\Facades\DB;
use Image;
class Backend {
  public static function wpArticle()
  {
    $articulos = DB::connection('wp')->table('wp_posts')
                                                  ->where('post_type', 'post')
                                                  ->where('post_status', 'publish')
                                                  ->orderBy('ID', 'DESC')
                                                  ->take(2)
                                                  ->get();
    foreach ($articulos as $key => $articulo) {
      $wpImagen = DB::connection('wp')->table('wp_postmeta')
      ->where('meta_key', '_thumbnail_id')
      ->where('post_id', $articulo->ID)
      ->first();
      $wpImagen = DB::connection('wp')->table('wp_posts')
      ->where('post_type', 'attachment')
      ->where('ID', $wpImagen->meta_value)
      ->first();
      // $img = Image::make($wpImagen->guid);
      // $img->fit(400, 400);
      // $articulos[$key]->imagen = $img->encode('data-url');
      $articulos[$key]->imagen = $wpImagen->guid;
    }
    return $articulos;
  }


  public static function wpArticle_fondo_solidario()
  {
    $wpCategorys = DB::connection('wp')->table('wp_term_relationships')
       ->where('term_taxonomy_id', 184)
       ->orderBy('object_id', 'DESC')
       ->take(2) 
       ->get();
      
       $articulos = DB::connection('wp')->table('wp_posts')
                                                  ->where('post_type', 'post')
                                                  ->where('post_status', 'publish')  
                                                  ->where('ID', $wpCategorys[0]->object_id) 
                                                  ->orWhere('ID', $wpCategorys[1]->object_id)                                               
                                                  ->orderBy('ID', 'DESC')
                                                  ->take(2)
                                                  ->get();
                                             
    foreach ($articulos as $key => $articulo) {
      $wpImagen = DB::connection('wp')->table('wp_postmeta')
      ->where('meta_key', '_thumbnail_id')
      ->where('post_id', $articulo->ID)
      ->first();
      $wpImagen = DB::connection('wp')->table('wp_posts')
      ->where('post_type', 'attachment')
      ->where('ID', $wpImagen->meta_value)
      ->first();
      $articulos[$key]->imagen = $wpImagen->guid;
    }
    return $articulos;
  }
}
