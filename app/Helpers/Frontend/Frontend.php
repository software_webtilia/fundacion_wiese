<?php
namespace App\Helpers\Frontend;

use App\SiteSetting;
use Mail;
use View;

class Frontend {

		public static function sendMail($template, $params_template, $from_email, $from_name, $to_emails, $subject){
      Mail::send($template, $params_template, function ($m) use ($from_email, $from_name, $to_emails, $subject) {
        $m->from($from_email, $from_name);
        $m->to($to_emails, 'Fundación Wiese')->subject($subject);
      });
    }

		public static function sendMailNative($template, $params_template, $from_email, $from_name, $to_emails, $subject){

				try {
					//dd($params_template);
	        $view = View::make($template, [
	              'data' => $params_template
	        ]);

	        $html = $view->render();
	        $to=$to_emails;

	        $headers = "From: info@fundacionwiese.org" . "\r\n";
	    		$headers .= "MIME-Version: 1.0\r\n";
	    		$headers .= "Content-Type: text/html; charset=UTF-8\r\n";
                if($to_emails != "info@fundacionwiese.org"){
				$headers .= "Cc: info@fundacionwiese.org\r\n"; 
				}
					mail($to, "=?UTF-8?B?".base64_encode($subject)."?=", $html, $headers);

	        } catch (Exception $e) {
	            dd($e);
	      }

		}

}
