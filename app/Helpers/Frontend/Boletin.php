<?php

namespace App\Helpers\Frontend;
use Mail;
use View;

class Boletin {

  public static function sendMailBoletin($template, $params_template,$to_emails, $subject){

      try {
        //dd($params_template);
        $view = View::make($template, [
              'data' => $params_template
        ]);

        $html = $view->render();
        $to=$to_emails;

        $headers = "From: info@fundacionwiese.org" . "\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=UTF-8\r\n";

        mail($to, "=?UTF-8?B?".base64_encode($subject)."?=", $html, $headers);

        } catch (Exception $e) {
            dd($e);
      }

  }
}
