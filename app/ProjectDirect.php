<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectDirect extends Model
{
  protected $table = 'project_directs';
  protected $fillable = [
    'id', 'titulo', 'descripcion_general', 'descripcion_1', 'iamgen1', 'descripcion_2', 'iamgen2', 'equipo', 'fondo', 'impacto_numero', 'impacto_label'
  ];

  public function impacts()
	{
	    return $this->hasMany('App\Impact');
	}

}
