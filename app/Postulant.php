<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Postulant extends Model
{
    protected $table = 'postulants';

    protected $fillable = [
      'id', 'nombres', 'apellidos', 'telefono', 'correo', 'departamento', 'provincia', 'profesion', 'cv', 'advertisement_id', 'estado'
    ];

    public function advertisement()
      {
          return $this->hasMany('App\Advertisement');
      }

}
