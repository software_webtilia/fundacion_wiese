<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Beneficiary extends Model
{
  protected $table = 'beneficiaries';
  protected $fillable = [
    'id', 'nombres', 'orden', 'estado'
  ];

  public function projects(){
    return $this->belongsToMany('App\Project','project_locations');
  }

}
