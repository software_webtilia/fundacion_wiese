<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
  protected $table = 'projects';
  protected $fillable = [
    'id', 'beneficiary_id', 'location_id', 'titulo', 'programa', 'link', 'descripcion', 'orden', 'estado'
  ];

  public function beneficiaries(){
    return $this->belongsToMany('App\Beneficiary','project_beneficiaries');
  }

  public function locations(){
    return $this->belongsToMany('App\Location','project_locations');
  }
}
