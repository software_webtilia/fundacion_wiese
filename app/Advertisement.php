<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Advertisement extends Model
{
  protected $table = 'advertisements';
  protected $fillable = [
    'id', 'titulo', 'subtitulo', 'sumilla', 'descripcion', 'lugar','orden','fecha_inicio','fecha_fin', 'estado', 'project_id'
  ];

  public function program()
    {
        return $this->hasOne('App\Program');
    }

}
