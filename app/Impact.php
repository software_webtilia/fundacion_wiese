<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Impact extends Model
{
  protected $table = 'impacts';
  protected $fillable = [
    'id', 'cantidad', 'label', 'icono', 'orden', 'estado'
  ];

  public function project_direct(){
    return $this->belongsTo('App\ProjectDirect');
  }
}
