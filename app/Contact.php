<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
  protected $table = 'contacts';
	protected $fillable = [
		'id','infocontacto_id','nombres','apellidos', 'telefono', 'email', 'mensaje','estado'
	];
}
