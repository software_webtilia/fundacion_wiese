<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Worker extends Model
{
  protected $table = 'workers';

  protected $fillable = [
    'id', 'nombres', 'apellidos', 'telefono', 'correo', 'area', 'direccion', 'profesion', 'cv', 'estado'
  ];
}
