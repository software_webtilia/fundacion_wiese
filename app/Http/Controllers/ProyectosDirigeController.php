<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class ProyectosDirigeController extends Controller
{
	public function brujo()
	{
			$project_direct = \App\ProjectDirect::where('id', '1')->first();
	    return view('frontend/proyectos_dirige/brujo', ['project_direct' => $project_direct]);
	}

	public function educacion()
	{
			$project_direct = \App\ProjectDirect::where('id', '2')->first();
	    return view('frontend/proyectos_dirige/educacion', ['project_direct' => $project_direct]);
	}

	public function cerrados()
	{
			$projects = \App\Project::where('estado', '3')->orderBy('orden', 'asc')->get();
	    return view('frontend/proyectos_dirige/cerrados', ['projects' => $projects]);
	}

	public function fondo()
	{
			$project_direct = \App\ProjectDirect::where('id', '3')->first();
			return view('frontend/proyectos_dirige/fondo', ['project_direct' => $project_direct]);
	}
}
