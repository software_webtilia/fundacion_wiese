<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Zone;
use Maatwebsite\Excel\Facades\Excel;
use Validator;

use DB;

use App\Helpers\Frontend\Frontend as HelperFrontend;

class TrabajaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $programa          = ($request && $request->get('programa')) ? $request->get('programa') : null;
        $programs = \App\Program ::where('estado', 'A')->orderBy('orden', 'asc')->get();
        $avisos = \App\Advertisement ::where('estado', 'PUB')->orderBy('orden', 'asc')->get();

        $areas = \App\Area::where('estado', 'A')->get();

        return view('frontend/trabaja/index', ['programs' => $programs, 'programa' => $programa, 'advertisement' => $avisos, 'areas'=>$areas]);
    }


    public function store(Request $request)
    {
        //
        $validator = Validator::make($request->all(), [
  	      'inputNombres'    => 'required',
  	      'inputApellidos'    => 'required',
  	      'inputCorreo'  => 'required|numeric|digits_between:7,9',
  	      'inputCorreo'     => 'required|email',
  	      'departamento'   => 'required',
          'provincia'   => 'required',
          'inputProfesion'   => 'required'
  	    ]);

        if ($validator->fails()) {
  	      return response()->json($validator->errors(), 422);
  	    }

        $postulant = new \App\Postulant;

        $postulant->advertisement_id  = 1;
        $postulant->nombres  = $request->input('inputNombres');
  	    $postulant->apellidos  = $request->input('inputApellidos');
  	    $postulant->correo    = $request->input('inputCorreo');
  	    $postulant->telefono = $request->input('inputTelefono');
  	    $postulant->departamento  = $request->input('departamento');
        $postulant->provincia  = $request->input('provincia');
        $postulant->profesion  = $request->input('inputProfesion');

        if($request->hasFile('inputCv')) {
          $file = $request->file('inputCv');
          $name = time() . '-' . $file->getClientOriginalName();
          $postulant->cv = $name;
          $file->move(storage_path('/app/public/archivos/postulantes/'), $name);
        }

  	    $postulant->save();

        $postulants = DB::table('postulants')
            ->join('advertisements', 'postulants.advertisement_id', '=', 'advertisements.id')
            ->select('postulants.*', 'advertisements.titulo')
            ->where('postulants.id', $postulant->id)
            ->get();

      //$results = [];
        $result = new \stdClass();
        foreach ($postulants as $postulant) {

                $result->id = $postulant->id;
                $result->titulo = $postulant->titulo;
                $result->nombres = $postulant->nombres;
                $result->apellidos = $postulant->apellidos;
                $result->telefono = $postulant->telefono;
                $result->correo = $postulant->correo;
                $result->profesion = $postulant->profesion;
                $result->departamento = $postulant->departamento;
                $result->provincia = $postulant->provincia;
                $result->cv = $postulant->cv;
                //$results[] = $result;
        }


        $emails = 'info@fundacionwiese.org';
  	    $subject = 'Postulante desde la Bolsa de Trabajo de Fundación Wiese';
        HelperFrontend::sendMailNative('emails.trabaja', $result , $postulant->correo, $postulant->nombres .' '.$postulant->apellidos, $emails, $subject);
        return response()->json(array('msg' => 'Save'), 201);

    }


    public function postulacionStore(Request $request)
    {
        //
        $validator = Validator::make($request->all(), [
  	      'inputNombres'    => 'required',
  	      'inputApellidos'    => 'required',
  	      'inputCorreo'  => 'required|numeric|digits_between:7,9',
  	      'inputCorreo'     => 'required|email',
  	      'area_id'   => 'required',
          'inputDireccion'   => 'required',
          'inputProfesion'   => 'required'
  	    ]);

        if ($validator->fails()) {
  	      return response()->json($validator->errors(), 422);
  	    }

        $postulant = new \App\Worker;

        $postulant->nombres  = $request->input('inputNombres');
  	    $postulant->apellidos  = $request->input('inputApellidos');
  	    $postulant->correo    = $request->input('inputCorreo');
  	    $postulant->telefono = $request->input('inputTelefono');
  	    $postulant->area_id  = $request->input('area_id');
        $postulant->direccion  = $request->input('inputDireccion');
        $postulant->profesion  = $request->input('inputProfesion');

        if($request->hasFile('inputCv')) {
          $file = $request->file('inputCv');
          $name = time() . '-' . $file->getClientOriginalName();
          $postulant->cv = $name;
          $file->move(storage_path('/app/public/archivos/postulantes/'), $name);
        }

  	    $postulant->save();

      //   $postulants = DB::table('workers')
      //       ->select('postulants.*')
      //       ->where('postulants.id', $postulant->id)
      //       ->get();
      //
      // //$results = [];
      //   $result = new \stdClass();
      //   foreach ($postulants as $postulant) {
      //
      //           $result->id = $postulant->id;
      //           $result->titulo = $postulant->titulo;
      //           $result->nombres = $postulant->nombres;
      //           $result->apellidos = $postulant->apellidos;
      //           $result->telefono = $postulant->telefono;
      //           $result->correo = $postulant->correo;
      //           $result->profesion = $postulant->profesion;
      //           $result->departamento = $postulant->departamento;
      //           $result->provincia = $postulant->provincia;
      //           $result->cv = $postulant->cv;
      //           //$results[] = $result;
      //   }


        // $emails = 'info@fundacionwiese.org';
  	    // $subject = 'Postulante desde la Bolsa de Trabajo de Fundación Wiese';
        // HelperFrontend::sendMailNative('emails.trabaja', $result , $postulant->correo, $postulant->nombres .' '.$postulant->apellidos, $emails, $subject);

        return response()->json(array('msg' => 'Save'), 201);

    }


    public function show($id)
    {
        //
    }


    public function trabajaDetail($id,$nombre){
      $zone = Zone::groupBy('departamento')->select('departamento')->get();
      $aviso = \App\Advertisement ::where('id',$id)->get();
      $program = \App\Program::find($id)->program;

      return view('frontend/trabaja/detalle', compact('zone','aviso','program'));
    }

    /*public function detalle(Request $request){
      $zone = Zone::groupBy('departamento')->select('departamento')->get();
      return view('frontend/trabaja/detalle', compact('zone'));
    }*/

    public function provincia(Request $request)
    {
      $provincia = \App\Zone::where('departamento', $request->input('departamento'))->groupBy('provincia')->select('provincia')->get();
      return response()->json(array('provincia' => $provincia), 200);
    }

    public function export(Request $request)
    {
    //  dd('holaa');
      $inicio= $request->fecha_inicio;
      $fin= $request->fecha_fin;

      Excel::create('Postulantes', function($excel) use($inicio, $fin)  {
       // Excel::create('Postulant', function($excel) {
         $excel->sheet('Postulantes', function($sheet) use($inicio, $fin) {
             // $excel->sheet('Postulantes', function($sheet) {

             // $postulantes = \App\Postulant::All();

             if($inicio == null && $fin == null){
             $postulantes = DB::table('postulants')
                         ->join('advertisements', 'advertisements.id', '=', 'postulants.advertisement_id')
                         ->select('advertisements.id','advertisements.titulo as anuncio','postulants.*', DB::raw('DATE_SUB(postulants.created_at, INTERVAL 5 HOUR) as created_at'))
                         ->orderBy('postulants.created_at','desc')
                         ->get();
            }elseif($fin == null){
              $postulantes = DB::table('postulants')
                          ->join('advertisements', 'advertisements.id', '=', 'postulants.advertisement_id')
                          ->select('advertisements.id','advertisements.titulo as anuncio','postulants.*', DB::raw('DATE_SUB(postulants.created_at, INTERVAL 5 HOUR) as created_at'))
                          ->orderBy('postulants.created_at','desc')
                          ->whereDate('postulants.created_at', $inicio  )
                          ->get();
            }else{
                $postulantes = DB::table('postulants')
                            ->join('advertisements', 'advertisements.id', '=', 'postulants.advertisement_id')
                            ->select('advertisements.id','advertisements.titulo as anuncio','postulants.*', DB::raw('DATE_SUB(postulants.created_at, INTERVAL 5 HOUR) as created_at'))
                            ->orderBy('postulants.created_at','desc')
                            ->whereBetween('postulants.created_at',[$inicio , $fin] )
                            ->get();
            }

            $exist= count($postulantes);

            if($exist == 0){
              $data[] = array(
                'Nombres'=> '',
                'Apellidos'=> '',
                'Teléfono'=>'',
                'Correo'=> '',
                'Departamento'=>'',
                'Provincia'=>'',
                'Profesión'=>'',
                'Cv'=>'',
                'Anuncio'=>'',
                'Fecha de registro'=>''
               );
            }else{

             foreach ($postulantes as $c)
              {
                $resultado = str_replace(" ", "%20", $c->cv);

                 $data[] = array(
                     "Nombres"=> $c->nombres,
                     "Apellidos"=> $c->apellidos,
                     "Teléfono" => $c->telefono,
                     "Correo"=> $c->correo,
                     "Departamento"=> $c->departamento,
                     "Provincia"=> $c->provincia ,
                     "Profesión"=> $c->profesion,
                     "Cv"=> storage_path('/app/public/archivos/postulantes/').$c->cv,
                     "Anuncio" => $c->anuncio,
                     "Fecha de registro" => $c->created_at,
                  );
              }
            }

            $sheet->fromArray($data);

             });
         })->export('xlsx');
    }

    public function exportGeneral(Request $request)
    {
    //  dd('holaa');
      $inicio= $request->fecha_inicio;
      $fin= $request->fecha_fin;

      Excel::create('PostulantesGenerales', function($excel) use($inicio, $fin)  {
       // Excel::create('Postulant', function($excel) {
         $excel->sheet('postulacionGeneral', function($sheet) use($inicio, $fin) {
             // $excel->sheet('Postulantes', function($sheet) {

             // $postulantes = \App\Postulant::All();

             if($inicio == null && $fin == null){
             $postulantes = DB::table('workers')
                         ->join('areas', 'areas.id', '=', 'workers.area_id')
                         ->select('areas.id','areas.nombre as area','workers.*', DB::raw('DATE_SUB(workers.created_at, INTERVAL 5 HOUR) as created_at'))
                         ->orderBy('workers.created_at','desc')
                         ->get();
            }elseif($fin == null){
              $postulantes = DB::table('workers')
                          ->join('areas', 'areas.id', '=', 'workers.area_id')
                          ->select('areas.id','areas.nombre as area','workers.*', DB::raw('DATE_SUB(workers.created_at, INTERVAL 5 HOUR) as created_at'))
                          ->orderBy('workers.created_at','desc')
                          ->whereDate('workers.created_at', $inicio  )
                          ->get();
            }else{
                $postulantes = DB::table('workers')
                            ->join('areas', 'areas.id', '=', 'workers.area_id')
                            ->select('areas.id','areas.nombre as area','workers.*', DB::raw('DATE_SUB(workers.created_at, INTERVAL 5 HOUR) as created_at'))
                            ->orderBy('workers.created_at','desc')
                            ->whereBetween('workers.created_at',[$inicio , $fin] )
                            ->get();
            }

            $exist= count($postulantes);

            if($exist == 0){
              $data[] = array(
                'Nombres'=> '',
                'Apellidos'=> '',
                'Teléfono'=>'',
                'Correo'=> '',
                'Área de interés'=>'',
                'Dirección'=>'',
                'Profesión'=>'',
                'Cv'=>'',
                'Fecha de registro'=>''
               );
            }else{

             foreach ($postulantes as $c)
              {
                $resultado = str_replace(" ", "%20", $c->cv);
                 $data[] = array(
                     "Nombres"=> $c->nombres,
                     "Apellidos"=> $c->apellidos,
                     "Télefono" => $c->telefono,
                     "Correo"=> $c->correo,
                     "Área de interés"=> $c->area,
                     "Dirección"=> $c->direccion,
                     "Profesión"=> $c->profesion,
                     "Cv"=> storage_path('/app/public/archivos/postulantes/').$c->cv,
                     "Fecha de registro" => $c->created_at,
                  );
              }
            }

            $sheet->fromArray($data);

             });
         })->export('xlsx');
    }

}
