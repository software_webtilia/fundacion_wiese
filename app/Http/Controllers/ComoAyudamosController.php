<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class ComoAyudamosController extends Controller
{
  public function participa(Request $request)
  {
    $beneficiario          = ($request && $request->get('beneficiario')) ? $request->get('beneficiario') : null;
    $ubicacion             = ($request && $request->get('ubicacion')) ? $request->get('ubicacion') : null;

    $projects = \App\Project::when($beneficiario, function ($query) use ($beneficiario) {
                      return $query->whereHas('beneficiaries', function($q) use ($beneficiario) {
                        return $q->where('beneficiary_id', $beneficiario);
                      });
                  })
                  ->when($ubicacion, function ($query) use ($ubicacion) {
                    return $query->whereHas('locations', function($q) use ($ubicacion) {
                      return $q->where('location_id', $ubicacion);
                    });
                  })
                  ->where('estado', '>=' , '1')
                  ->orderBy('orden', 'asc')->get();

    $locations = \App\Location::where('estado', '1')->orderBy('orden', 'asc')->get();
    $beneficiaries = \App\Beneficiary ::where('estado', '1')->orderBy('orden', 'asc')->get();
    return view('frontend/como_ayudamos/participa', ['projects' => $projects, 'locations' => $locations, 'beneficiaries' => $beneficiaries, 'beneficiario' => $beneficiario, 'ubicacion' => $ubicacion]);
  }

  public function dirige()
  {
    return view('frontend/como_ayudamos/dirige', []);
  }

  public function auspicia()
  {
    return view('frontend/como_ayudamos/auspicia', []);
  }

}
