<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Backend;
use DB;
use Image;

class HomeController extends Controller
{
  public function index()
  {

    $articulos = Backend::wpArticle();
    return view('frontend/home', ['articulos' => $articulos]);
  }
  public function politicaprivacidad(){
    return view('frontend.partials.politicaprivacidad');
  }

}
