<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class NovedadesController extends Controller
{
  public function index()
  {
    return view('frontend/novedades/index', []);
  }

  public function detalle1()
  {
    return view('frontend/novedades/detalle1', []);
  }

  public function detalle2()
  {
    return view('frontend/novedades/detalle2', []);
  }

  public function detalle3()
  {
    return view('frontend/novedades/detalle3', []);
  }

  public function detalle4()
  {
    return view('frontend/novedades/detalle4', []);
  }

  public function detalle5()
  {
    return view('frontend/novedades/detalle5', []);
  }

  public function detalle6()
  {
    return view('frontend/novedades/detalle6', []);
  }

  public function detalle7()
  {
    return view('frontend/novedades/detalle7', []);
  }

}
