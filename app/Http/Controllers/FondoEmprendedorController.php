<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Backend;
class FondoEmprendedorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articulos = Backend::wpArticle_fondo_solidario();
        return view('frontend/fondo_emprendedor/index', ['articulos' => $articulos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function educacion()
    {
        return view('frontend/fondo_emprendedor/educacion', []);
    }
    public function humanitaria()
    {
        return view('frontend/fondo_emprendedor/ayuda-humanitaria', []);
    }
    public function contacto()
    {
        return view('frontend/fondo_emprendedor/contacto', []);
    }
    public function fondo()
    {
        return view('frontend/fondo_emprendedor/conoce-fondo', []);
    }
    public function postula()
    {
        return view('frontend/fondo_emprendedor/postula', []);
    }
     public function humanitariaDetalle()
    {
        return view('frontend/fondo_emprendedor/ayuda-humanitaria-detalle', []);
    }
}
