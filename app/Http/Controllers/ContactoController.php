<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Validator;
use App\InfoContact;
use App\Contact;
use Mail;
use App\Helpers\Frontend\Frontend as HelperFrontend;
use App\Helpers\Frontend\Boletin as HelperBoletin;

class ContactoController extends Controller
{
	public function index()
	{
	    return view('frontend/contacto/contacto', []);
	}

	public function voluntario()
	{
	    return view('frontend/contacto/voluntario', []);
	}

	public function enviar(Request $request)
  	{
	    $validator = Validator::make($request->all(), [
	      'nombres'    => 'required',
	      'apellidos'    => 'required',
	      'telefono'  => 'required|numeric|digits_between:7,9',
	      'email'     => 'required|email',
	      'mensaje'   => 'required'
	    ]);

	    if ($validator->fails()) {
	      return response()->json($validator->errors(), 422);
	    }

	    $contacto = new \App\Contact;
			$contacto->infocontacto_id  = $request->input('nombres');
	    $contacto->nombres  = $request->input('nombres');
	    $contacto->apellidos  = $request->input('apellidos');
	    $contacto->email    = $request->input('email');
	    $contacto->telefono = $request->input('telefono');
	    $contacto->mensaje  = $request->input('mensaje');

	    $contacto->save();

	    // Enviar Email
	    $emails = 'info@fundacionwiese.org';
	    $subject = 'Contácto #' . $contacto->id . ' desde la web Fundación Wiese';
	    HelperFrontend::sendMail('emails.contactanos', ['contacto' => $contacto], $contacto->email, $contacto->nombres .' '.$contacto->apellidos, $emails, $subject);

	    return response()->json(array('msg' => 'Enviado'), 201);
	}

	public function enviarNewsletter(Request $request)
  	{
	    $validator = Validator::make($request->all(), [
	      'telefono'  => 'required|numeric|digits_between:7,9',
	      'email'     => 'required|email',
	    ]);

	    if ($validator->fails()) {
	      return response()->json($validator->errors(), 422);
	    }

	    $newsletter = new \App\Newsletter;

	    $newsletter->telefono = $request->input('telefono');
	    $newsletter->email    = $request->input('email');

	    $newsletter->save();

			// Enviar Email
	    $emails = 'info@fundacionwiese.org';
	    $subject = 'Newsletter #' . $newsletter->id . ' desde la web Fundación Wiese';
			$result = $newsletter;
			HelperBoletin::sendMailBoletin('emails.newsletter', $result ,$emails, $subject);

	    return response()->json(array('msg' => 'Enviado'), 201);
	}

	public function infocontact($id){
		$infocontact = InfoContact::find($id);
		return response()->json([ 'infocontact'  => $infocontact]);

	}
	public function savecontact(Request $request){
			$validator = Validator::make($request->all(),
                ['nombres'         => 'required',
                 'apellidos'     => 'required',
								 'email'				=> 'required|email',
								 'telefono'				=> 'required',
								 'mensaje'				=> 'required'
                ]
            );
						if ($validator->fails())
            {
                return response()->json([
                    'status'    => false,
                    'mensaje'   => 'Verifica haber  Llena todos los campos',
                    'errores'   => $validator->errors()
                ]);
            }


			$info_contact = InfoContact::where('id',$request->get('infocontacto_id'))->first();

			$newContacto = Contact::create($request->all());
			$result = Contact::find($newContacto->id);
			// $this->_sendEmail_Contacto($info_contact,$newContacto);

        $emails = $info_contact->email;
	    $subject = 'Fondo Solidario / Contacto para ' . $info_contact->title . '';
        HelperFrontend::sendMailNative('emails.contactanos', $result , $result->correo, $result->nombres .' '.$result->apellidos, $emails, $subject);

			return response()->json([
                    'status'    => true,
                    'mensaje'   => 'Gracias por comunicarte con nosotros. Pronto nos pondremos en contacto contigo'
                ]);

	}

	public function _sendEmail_Contacto($info_contact,$newContacto)
    {

			  $info_conta = InfoContact::where('id',$info_contact)->first();
        $contacto = Contact::find($newContacto->id);

        Mail::send('emails.contactanos', ['contacto' => $contacto], function ($m) use ($contacto,$info_conta) {

						$m->from($contacto->email, 'nuevo cliente contactar');
            $m->to($info_conta->email, 'Fundación WIESE')->subject('Contácto #' . $contacto->id . ' desde la web Fundación Wiese');
						if($info_conta->id == 1){
							$m->cc("info@fundacionwiese.org");
						}
        });
    }
}
