<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InfoContact extends Model
{

  protected $table = 'infocontacto';
  protected $fillable = [
    'id', 'nombre', 'telefono', 'email','estado'
  ];

}
