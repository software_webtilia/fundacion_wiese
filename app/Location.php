<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
  protected $table = 'locations';
  protected $fillable = [
    'id', 'nombres', 'orden', 'estado'
  ];

  public function projects(){
    return $this->belongsToMany('App\Project','project_locations');
  }
}
